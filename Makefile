KPPATH_FILE='.kppath'
REMOTEHOST_FILE='.remotehost'
KEYPAIRPATH=`cat $(KPPATH_FILE) | head -n1`
REMOTEHOST=`cat $(REMOTEHOST_FILE) | head -n1`

all: javascript java db unpack-war

java:
	(cd src/java/readability-web && mvn clean && mvn package)

javascript:
	(cd src/js/ && npm run build)

db:
	cp src/db/db-up.sql build/db/db-up.sql
	cp src/db/db-down.sql build/db/db-down.sql
	cp src/db/insert-snippets.sql build/db/insert-snippets.sql

unpack-war:
	if [ ! -d ./build/tmp ]; then mkdir -p ./build/tmp; fi
	cp ./src/java/readability-web/target/*.war ./build/tmp/readability-web.war
	unzip -o ./build/tmp/readability-web.war -d ./build/readability-web
	rm -rf ./build/tmp

sync:
	rsync -avz -e "ssh -i $(KEYPAIRPATH)" --progress ./run ec2-user@$(REMOTEHOST):/tmp/run
	rsync -avz -e "ssh -i $(KEYPAIRPATH)" --progress ./build/readability-web ec2-user@$(REMOTEHOST):/tmp
	rsync -avz -e "ssh -i $(KEYPAIRPATH)" --progress ./build/db ec2-user@$(REMOTEHOST):/tmp

