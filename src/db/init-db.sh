#!/bin/bash

set -eu
set -o pipefail

die() {
	echo "$?{1}" 1>&2
	exit 1
}

force=false
if [ "$?{1}" == 'force' ]; then
	force=true
fi

# verifica se o banco de dados readability existe
psql -d readability -w -c '' || die "Não foi possível acessar o banco de dados. Certifique-se de que ele existe"

# certifica-se de que a experiment_sessions e a profiles estão vazias antes de continuar

must_check_ex_sessions=false
[ $(psql -d readability -t -w -c "select count(*) from pg_tables where tablename='experiment_sessions'" 2> /dev/null | sed -e 's/^[ \t]\+//' ) != '0' ] && must_check_ex_sessions=true
es_count=0 
if [[ ! "$force" ]]  && [[ "$must_check_ex_sessions" ]]; then
	es_count=$(psql -d readability -t -w -c 'select count(*) from experiment_sessions' | sed -e 's/^[ \t]*//' 2> /dev/null)
	[ "$es_count" -eq 0 ] || die "A base está carregada com '$es_count' experiment_sessions. Recomenda-se usar uma base nova"
fi

must_check_profiles=false
[ $(psql -d readability -t -w -c "select count(*) from pg_tables where tablename='profiles'" 2> /dev/null | sed -e 's/^[ \t]\+//' ) != '0' ] && must_check_profiles=true
pf_count=0
if [[ ! "$force" ]] && [[ "$must_check_profiles" ]]; then
	pf_count=$(psql -d readability -t -w -c 'select count(*) from profiles' | sed -e 's/^[ \t]*//' 2> /dev/null)
	[ "$pf_count" -eq 0 ] || die "A base está carregada com '$pf_count' profiles. Recomenda-se usar uma base nova"
fi 

psql -d readability -w -f db-down.sql || die "Falha na execução do script db-down.sql" 
psql -d readability -w -f db-up.sql || "Falha na inicialização do banco de dados" 
rm -f ./insert-snippets.sql
./generate-import-snippets-sql.py || die "Falha durante a geração do script de carga de snippets" 
psql -d readability -w -f insert-snippets.sql
rm -f ./insert-snippets.sql

