generated with:
./generate-sql-practice-stamps.py  | sort

update snippet_pairs set practice="CALLBACK" where hash="7c3dda8f15f26ddf0a78a328a4ecfa6e"
update snippet_pairs set practice="CALLBACK" where hash="8d93b7e6deec544988915b53a4500b3d"
update snippet_pairs set practice="CALLBACK" where hash="b3a5498c02342b8832c35ba9bd5be569"
update snippet_pairs set practice="DI" where hash="104dac0818e33a87640e887b97c8b480"
update snippet_pairs set practice="DI" where hash="a6631e8b1fb80151af7da9b96dfb8692"
update snippet_pairs set practice="DI" where hash="eecf792c7384c790c3408295f0a03f02"
update snippet_pairs set practice="MAPPING" where hash="28a0fc236f2038705950458bce99e986"
update snippet_pairs set practice="MAPPING" where hash="dd1bdcdd8fd08fad27fe5fa334d98ca9"
update snippet_pairs set practice="MAPPING" where hash="eb8cae15c33e02ad0af8d5f8f73258f0"
update snippet_pairs set practice="PROXY" where hash="365ec6833afcf8a01f92d8705c4a860c"
update snippet_pairs set practice="PROXY" where hash="ca9c2afca324cb63a7652de0c750e45d"
update snippet_pairs set practice="PROXY" where hash="d46a7470cc0af40b744f14ee994a38e9"
update snippet_pairs set practice="RULES" where hash="6488766c2f90cf468f14df23493cde66"
update snippet_pairs set practice="RULES" where hash="ad5313c9a61e91191bd6b0b1f65cce33"
update snippet_pairs set practice="RULES" where hash="e6ef5b3f9a62809e821fd06486fc098f"
