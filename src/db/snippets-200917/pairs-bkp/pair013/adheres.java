public class Product{

  @NotNull
  private Integer id;

  @NotEmpty
  @LengthRange(min=5, max=20)
  private String name;

  @RegularExpression("^[8]{1}[0-9]{11}$")
  private String barcode;

  public boolean isValid(){
    Validator v = new Validator();
    return v.validate(this);
  }
}
