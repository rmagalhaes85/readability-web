public class Product{

  private Integer id;

  private String name;

  private String barcode;

  public boolean isValid(){
    Validator v = new Validator();
    v.fieldNotNull("id");
    v.fieldNotEmpty("name");
    v.fieldLengthRange("name",5,20);
    v.fieldRegularExpression("barcode","^[8]{1}[0-9]{11}$");
    return v.validate(this);
  }
}
