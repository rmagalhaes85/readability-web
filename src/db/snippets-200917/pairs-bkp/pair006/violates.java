public class BuyController{

  private ShoppingCart cart = Session.get("cart");

  public addItem(Product product){
    cart.add(product);
  } 

  public removeItem(Product product){
    cart.remove(product);
  } 

}
