public class User{

  private String id;

  private String login;

  public String toXML(){
    XMLConverter xc = new XMLConverter();
    xc.mapAttributeToElement("id","identifier");
    xc.mapAttributeToElement("login","username");
    return xc.toXML(this) 
  } 
}
