public class ExternalServices{

  public init(){
     ServicePublisher sp = new ServicePublisher();
     sp.addService(this,"getProductData","product", "get");
     sp.addService(this,"setProductData","product", "set");
     sp.publish();
  }

  public String getProductData(){
     //methodbody
  } 

  public void setProductData(String data){
     //methodbody
  }
}
