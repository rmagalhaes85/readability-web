public class Airplane{

  private String state = "ground";

  public void takeOff(){
     if(!state.equals("ground"))
        throw new IllegalObjectStateException();
     // take off logic
     state = "air";
  }

  public void land(){
     if(!state.equals("air"))
        throw new IllegalObjectStateException();
     // landing logic
     state = "ground";
  }

  public void fly(){
     if(!state.equals("air"))
        throw new IllegalObjectStateException();
     // fly logic
  }
}
