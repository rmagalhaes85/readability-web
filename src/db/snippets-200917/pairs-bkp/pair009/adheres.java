public class Parameters{

  @NumericValue
  private long timeout;

  @TextValue(name=“file”)
  private String filename;

  @IsParameterPresent
  private boolean verbose;

  public static void main(String[] args){
     ParamMapper mapper = new ParamMapper();
     Parameters p = mapper.map(Parameters.class);
     //software logic
  }
}
