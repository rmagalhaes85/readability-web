public class SocialServiceIntegration{

  public UserData retrieveUserData(String idNumber){
    try{
       SocialServiceInvoker ssi = new SocialServiceInvoker();
       return ssi.getUserInfo(idNumber);
    } catch(ExternalInvocationException ex){
       handleError(ex);
    }
  } 

  public void handleErros(Exception ex){
    //error handling
  }
}
