#!/bin/bash

PAIRS_FOLDER="./pairs"

for folder in $(ls "$PAIRS_FOLDER")
do
	pair_folder="$PAIRS_FOLDER/$folder"
	if [ -d "$pair_folder" ]; then
		adheres_file="$pair_folder/adheres.java"
		violates_file="$pair_folder/violates.java"
		diff_adheres_violates="$pair_folder/adheres2violates.diff.txt"
		diff_violates_adheres="$pair_folder/violates2adheres.diff.txt"
		diff -Z --unchanged-line-format="" --old-line-format="" --new-line-format="%dn " $adheres_file $violates_file > $diff_adheres_violates
		diff -Z --unchanged-line-format="" --old-line-format="" --new-line-format="%dn " $violates_file $adheres_file > $diff_violates_adheres
	fi
done

