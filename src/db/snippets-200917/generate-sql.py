#!/usr/bin/env python2

import os
import re
import hashlib
import random

from pygments import highlight
from pygments.lexers import JavaLexer
from pygments.formatters import HtmlFormatter

PAIRS_DIR = "pairs"
PAIRS_DESCRIPTOR = "pair.txt"
ADHERES_FILE = "adheres.java"
VIOLATES_FILE = "violates.java"
ADHERES_VIOLATES_DIFF_FILE = "adheres2violates.diff.txt"
VIOLATES_ADHERES_DIFF_FILE = "violates2adheres.diff.txt"
SNIPPETS_SQL_FILE = "./insert-snippets.sql"

java_lexer = JavaLexer()
html_formatter = HtmlFormatter(linenos='inline');

sql_file = open(SNIPPETS_SQL_FILE, "w")

def check_file (path):
    if not os.path.exists(path):
        print("File \"" + path + "\" not found")
        return False
    return True

def matches_attr (line, attr):
    return not re.search(attr, line) is None

def read_attr (line):
    attr_descr, attr_val = line.split(':')
    return attr_val

def die_attr (attr_name):
    print ("Attribute \"" + attr_name + "\" not found in file \"" + PAIRS_DESCRIPTOR + "\"")
    quit(1)

def generate_md5hash(adheres_file_path, violates_file_path):
    with open(adheres_file_path, "rb") as adheres_file, open(violates_file_path, "rb") as violates_file:
        concatenation = adheres_file.read() + violates_file.read()
        m = hashlib.md5(concatenation)
        return m.hexdigest()

def pyg_code(file_path):
    with open(file_path, "r") as code_file:
        return highlight(code_file.read(), java_lexer, html_formatter)

def generate_snippet_pair_insert(hash_code, snippet_a_content, snippet_b_content, most_readable):
    return "SELECT insert_pair('" + hash_code + "', '" + snippet_a_content + "', '" + snippet_b_content + "', '" + most_readable + "');"

def sort_snippets(adheres_content, violates_content):
    if random.random() > .5:
        return adheres_content, violates_content, 'A'
    else:
        return violates_content, adheres_content, 'B'

def process_line_numbers(snippet_content, diff_lines):
    def line_highlight_replacer(m):
        return '<span class="lineno linehl">' + (' ' + m.group(1))[-2:] + ' </span>'

    for line in diff_lines:
        if line <> '':
            snippet_content = re.sub('<span class="lineno">\s*(' + line + ')\s*</span>', line_highlight_replacer, snippet_content)

    return snippet_content

def generate_sql_instructions(hash_code, adheres_content, violates_content, adheres_violates_diff_lines, violates_adheres_diff_lines):
    def sort_diffs(most_readable):
        if most_readable == 'A':
            return violates_adheres_diff_lines, adheres_violates_diff_lines
        else:
            return adheres_violates_diff_lines, violates_adheres_diff_lines

    snippet_a_content, snippet_b_content, most_readable = sort_snippets(adheres_content, violates_content)
    #diff_lines = adheres_violates_diff_lines if most_readable == 'A' else violates_adheres_diff_lines
    diff_lines_a, diff_lines_b = sort_diffs(most_readable)
    snippet_a_content_processed = process_line_numbers(snippet_a_content, diff_lines_a)
    snippet_b_content_processed = process_line_numbers(snippet_b_content, diff_lines_b)
    sql_file.write(generate_snippet_pair_insert(hash_code, snippet_a_content_processed, snippet_b_content_processed, most_readable) + '\n')
    sql_file.write('-------\n\n')

def close_sql_file():
    close(sql_file)

def get_line_numbers(diff_file_path):
    with open(diff_file_path, 'r') as diff_file:
        return diff_file.read().split(' ')

if not os.path.exists(PAIRS_DIR):
    print("\""+ PAIRS_DIR + "\" directory expected")
    quit(1)
# Para cada diretorio
for root, dirs, files in os.walk(PAIRS_DIR):
    if root == PAIRS_DIR:
        continue

    pairs_file_path = os.path.join(root, PAIRS_DESCRIPTOR)
    adheres_file_path = os.path.join(root, ADHERES_FILE)
    violates_file_path = os.path.join(root, VIOLATES_FILE)
    adheres_violates_diff_path = os.path.join(root, ADHERES_VIOLATES_DIFF_FILE)
    violates_adheres_diff_path = os.path.join(root, VIOLATES_ADHERES_DIFF_FILE)
# Valida a presenca do arquivo pair.txt
    if not check_file(pairs_file_path):
        continue

# Valida a presenca dos arquivos adheres.java e violates.java
    if not check_file(adheres_file_path):
        continue

    if not check_file(violates_file_path):
        continue
# Valida a presenca de uma instrucao Tested-Practice no arquivo pair.txt
# Valida a presenca de uma instrucao Comments no arquivo pair.txt
    tested_practice = None
    comments = None

    with open(pairs_file_path) as descriptor_file:
        for description_line in descriptor_file:
            if matches_attr(description_line, "Tested-Practice"):
                tested_practice = read_attr(description_line)
            elif matches_attr(description_line, "Comments"):
                comments = read_attr(description_line)
    
    if tested_practice is None: die_attr ("Tested-Practice")
    if comments is None: die_attr ("Comments")


# Calcula o hash do conteudo da concatenacao entre adheres.java e violates.java. Este sera o hash do snippet pair
    hash_code = generate_md5hash(adheres_file_path, violates_file_path)

# Executa o pygmentize nos arquivos adheres.java e violates.java
    adheres_pyg = pyg_code(adheres_file_path)
    violates_pyg = pyg_code(violates_file_path)
    adheres_violates_diff_lines = get_line_numbers(adheres_violates_diff_path)
    violates_adheres_diff_lines = get_line_numbers(violates_adheres_diff_path)
    generate_sql_instructions(hash_code, adheres_pyg, violates_pyg, adheres_violates_diff_lines, violates_adheres_diff_lines)
