public class Parameters{

  private long timeout;

  private String filename;

  private boolean verbose;

  public static void main(String[] args){
     ParamMapper mapper = new ParamMapper();
     mapper.addTextParameter("file", "filename");
     mapper.addNumericParameter("timeout", "timeout");
     mapper.verifyParameterPresence("verbose", "verbose");
     Parameters p = mapper.map(Parameters.class);
     //software logic
  }

}
