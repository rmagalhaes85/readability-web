public class Person{

  private double height;

  private String name;

  private int age;

  public List<Difference> compare(Person p){
    Comparator c = new Comparator(Person.class);
    c.addNumericTolerance("height",0.01);
    c.ignoreInComparison("name");
    return c.compare(this, p);
  }
}
