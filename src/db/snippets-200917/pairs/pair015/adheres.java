public class Person{

  @NumericTolerance(0.01)
  private double height;

  private String name;

  @IgnoreInComparison
  private int age;

  public List<Difference> compare(Person p){
    Comparator c = new Comparator(Person.class);
    return c.compare(this, p);
  }
}

