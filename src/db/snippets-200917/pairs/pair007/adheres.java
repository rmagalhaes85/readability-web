public class User{

  @XMLElement("identifier")
  private String id;

  @XMLElement("username") 
  private String login;

  public String toXML(){
    XMLConverter xc = new XMLConverter();
    return xc.toXML(this) 
  } 
}
