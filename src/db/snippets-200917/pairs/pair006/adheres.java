public class BuyController{

  @Session
  private ShoppingCart cart;

  public addItem(Product product){
    cart.add(product);
  } 

  public removeItem(Product product){
    cart.remove(product);
  } 

}
