public class SocialServiceIntegration{

  public UserData retrieveUserData(String idNumber){
    SocialServiceInvoker ssi = new SocialServiceInvoker();
    return ssi.getUserInfo(idNumber);
  } 

  @HandleException(ExternalInvocationException.class)
  public void handleErros(Exception ex){
    //error handling
  }
}
