@InitialState("ground")
public class Airplane{

  @ValidState("ground")
  @ChangeState("air")
  public void takeOff(){
     // take off logic
  }

  @ValidState("air")
  @ChangeState("ground")
  public void land(){
     // landing logic
  }

  @ValidState("air")
  public void fly(){
     // fly logic
  }
}
