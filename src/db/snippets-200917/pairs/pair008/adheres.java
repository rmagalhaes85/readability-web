public class ExternalServices{

  public init(){
     ServicePublisher sp = new ServicePublisher();
     sp.addServices(this);
     sp.publish();
  }

  @ServiceURL(base="product",name="get")
  public String getProductData(){
     //methodbody
  } 

  @ServiceURL(base="product",name="set")
  public void setProductData(String data){
     //methodbody
  }
}
