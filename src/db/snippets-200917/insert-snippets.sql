SELECT insert_pair('eb8cae15c33e02ad0af8d5f8f73258f0', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">ExternalServices</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">public</span> <span class="nf">init</span><span class="o">(){</span>
<span class="lineno"> 4 </span>     <span class="n">ServicePublisher</span> <span class="n">sp</span> <span class="o">=</span> <span class="k">new</span> <span class="n">ServicePublisher</span><span class="o">();</span>
<span class="lineno linehl"> 5 </span>     <span class="n">sp</span><span class="o">.</span><span class="na">addServices</span><span class="o">(</span><span class="k">this</span><span class="o">);</span>
<span class="lineno"> 6 </span>     <span class="n">sp</span><span class="o">.</span><span class="na">publish</span><span class="o">();</span>
<span class="lineno"> 7 </span>  <span class="o">}</span>
<span class="lineno"> 8 </span>
<span class="lineno linehl"> 9 </span>  <span class="nd">@ServiceURL</span><span class="o">(</span><span class="n">base</span><span class="o">=</span><span class="s">&quot;product&quot;</span><span class="o">,</span><span class="n">name</span><span class="o">=</span><span class="s">&quot;get&quot;</span><span class="o">)</span>
<span class="lineno">10 </span>  <span class="kd">public</span> <span class="n">String</span> <span class="nf">getProductData</span><span class="o">(){</span>
<span class="lineno">11 </span>     <span class="c1">//methodbody</span>
<span class="lineno">12 </span>  <span class="o">}</span> 
<span class="lineno">13 </span>
<span class="lineno linehl">14 </span>  <span class="nd">@ServiceURL</span><span class="o">(</span><span class="n">base</span><span class="o">=</span><span class="s">&quot;product&quot;</span><span class="o">,</span><span class="n">name</span><span class="o">=</span><span class="s">&quot;set&quot;</span><span class="o">)</span>
<span class="lineno">15 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">setProductData</span><span class="o">(</span><span class="n">String</span> <span class="n">data</span><span class="o">){</span>
<span class="lineno">16 </span>     <span class="c1">//methodbody</span>
<span class="lineno">17 </span>  <span class="o">}</span>
<span class="lineno">18 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">ExternalServices</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">public</span> <span class="nf">init</span><span class="o">(){</span>
<span class="lineno"> 4 </span>     <span class="n">ServicePublisher</span> <span class="n">sp</span> <span class="o">=</span> <span class="k">new</span> <span class="n">ServicePublisher</span><span class="o">();</span>
<span class="lineno linehl"> 5 </span>     <span class="n">sp</span><span class="o">.</span><span class="na">addService</span><span class="o">(</span><span class="k">this</span><span class="o">,</span><span class="s">&quot;getProductData&quot;</span><span class="o">,</span><span class="s">&quot;product&quot;</span><span class="o">,</span> <span class="s">&quot;get&quot;</span><span class="o">);</span>
<span class="lineno linehl"> 6 </span>     <span class="n">sp</span><span class="o">.</span><span class="na">addService</span><span class="o">(</span><span class="k">this</span><span class="o">,</span><span class="s">&quot;setProductData&quot;</span><span class="o">,</span><span class="s">&quot;product&quot;</span><span class="o">,</span> <span class="s">&quot;set&quot;</span><span class="o">);</span>
<span class="lineno"> 7 </span>     <span class="n">sp</span><span class="o">.</span><span class="na">publish</span><span class="o">();</span>
<span class="lineno"> 8 </span>  <span class="o">}</span>
<span class="lineno"> 9 </span>
<span class="lineno">10 </span>  <span class="kd">public</span> <span class="n">String</span> <span class="nf">getProductData</span><span class="o">(){</span>
<span class="lineno">11 </span>     <span class="c1">//methodbody</span>
<span class="lineno">12 </span>  <span class="o">}</span> 
<span class="lineno">13 </span>
<span class="lineno">14 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">setProductData</span><span class="o">(</span><span class="n">String</span> <span class="n">data</span><span class="o">){</span>
<span class="lineno">15 </span>     <span class="c1">//methodbody</span>
<span class="lineno">16 </span>  <span class="o">}</span>
<span class="lineno">17 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('a6631e8b1fb80151af7da9b96dfb8692', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">BusinesComponent</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@Connection</span><span class="o">(</span><span class="n">url</span><span class="o">=</span><span class="s">&quot;jdbc:oracle:dbhost.dbname&quot;</span><span class="o">,</span><span class="n">user</span><span class="o">=</span><span class="s">&quot;user&quot;</span><span class="o">,</span><span class="n">password</span><span class="o">=</span><span class="s">&quot;password&quot;</span><span class="o">);</span>
<span class="lineno">4 </span>  <span class="kd">private</span> <span class="n">Conection</span> <span class="n">c</span><span class="o">;</span>
<span class="lineno">5 </span>
<span class="lineno">6 </span>  <span class="kd">public</span> <span class="nf">BusinesComponent</span><span class="o">(){</span>
<span class="lineno">7 </span>  <span class="o">}</span> 
<span class="lineno">8 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">BusinesComponent</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno">3 </span>  <span class="kd">private</span> <span class="n">Conection</span> <span class="n">c</span><span class="o">;</span>
<span class="lineno">4 </span>
<span class="lineno">5 </span>  <span class="kd">public</span> <span class="nf">BusinesComponent</span><span class="o">(){</span>
<span class="lineno linehl"> 6 </span>    <span class="n">c</span> <span class="o">=</span> <span class="n">DriverManager</span><span class="o">.</span><span class="na">getConnection</span><span class="o">(</span><span class="s">&quot;jdbc:oracle:dbhost.dbname&quot;</span><span class="o">,</span><span class="s">&quot;user&quot;</span><span class="o">,</span><span class="s">&quot;password&quot;</span><span class="o">);</span>  
<span class="lineno">7 </span>  <span class="o">}</span> 
<span class="lineno">8 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('104dac0818e33a87640e887b97c8b480', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">BuyController</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@Session</span>
<span class="lineno linehl"> 4 </span>  <span class="kd">private</span> <span class="n">ShoppingCart</span> <span class="n">cart</span><span class="o">;</span>
<span class="lineno"> 5 </span>
<span class="lineno"> 6 </span>  <span class="kd">public</span> <span class="nf">addItem</span><span class="o">(</span><span class="n">Product</span> <span class="n">product</span><span class="o">){</span>
<span class="lineno"> 7 </span>    <span class="n">cart</span><span class="o">.</span><span class="na">add</span><span class="o">(</span><span class="n">product</span><span class="o">);</span>
<span class="lineno"> 8 </span>  <span class="o">}</span> 
<span class="lineno"> 9 </span>
<span class="lineno">10 </span>  <span class="kd">public</span> <span class="nf">removeItem</span><span class="o">(</span><span class="n">Product</span> <span class="n">product</span><span class="o">){</span>
<span class="lineno">11 </span>    <span class="n">cart</span><span class="o">.</span><span class="na">remove</span><span class="o">(</span><span class="n">product</span><span class="o">);</span>
<span class="lineno">12 </span>  <span class="o">}</span> 
<span class="lineno">13 </span>
<span class="lineno">14 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">BuyController</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="kd">private</span> <span class="n">ShoppingCart</span> <span class="n">cart</span> <span class="o">=</span> <span class="n">Session</span><span class="o">.</span><span class="na">get</span><span class="o">(</span><span class="s">&quot;cart&quot;</span><span class="o">);</span>
<span class="lineno"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">public</span> <span class="nf">addItem</span><span class="o">(</span><span class="n">Product</span> <span class="n">product</span><span class="o">){</span>
<span class="lineno"> 6 </span>    <span class="n">cart</span><span class="o">.</span><span class="na">add</span><span class="o">(</span><span class="n">product</span><span class="o">);</span>
<span class="lineno"> 7 </span>  <span class="o">}</span> 
<span class="lineno"> 8 </span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="nf">removeItem</span><span class="o">(</span><span class="n">Product</span> <span class="n">product</span><span class="o">){</span>
<span class="lineno">10 </span>    <span class="n">cart</span><span class="o">.</span><span class="na">remove</span><span class="o">(</span><span class="n">product</span><span class="o">);</span>
<span class="lineno">11 </span>  <span class="o">}</span> 
<span class="lineno">12 </span>
<span class="lineno">13 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('28a0fc236f2038705950458bce99e986', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">User</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">id</span><span class="o">;</span>
<span class="lineno"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">login</span><span class="o">;</span>
<span class="lineno"> 6 </span>
<span class="lineno"> 7 </span>  <span class="kd">public</span> <span class="n">String</span> <span class="nf">toXML</span><span class="o">(){</span>
<span class="lineno"> 8 </span>    <span class="n">XMLConverter</span> <span class="n">xc</span> <span class="o">=</span> <span class="k">new</span> <span class="n">XMLConverter</span><span class="o">();</span>
<span class="lineno linehl"> 9 </span>    <span class="n">xc</span><span class="o">.</span><span class="na">mapAttributeToElement</span><span class="o">(</span><span class="s">&quot;id&quot;</span><span class="o">,</span><span class="s">&quot;identifier&quot;</span><span class="o">);</span>
<span class="lineno linehl">10 </span>    <span class="n">xc</span><span class="o">.</span><span class="na">mapAttributeToElement</span><span class="o">(</span><span class="s">&quot;login&quot;</span><span class="o">,</span><span class="s">&quot;username&quot;</span><span class="o">);</span>
<span class="lineno">11 </span>    <span class="k">return</span> <span class="n">xc</span><span class="o">.</span><span class="na">toXML</span><span class="o">(</span><span class="k">this</span><span class="o">)</span> 
<span class="lineno">12 </span>  <span class="o">}</span> 
<span class="lineno">13 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">User</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@XMLElement</span><span class="o">(</span><span class="s">&quot;identifier&quot;</span><span class="o">)</span>
<span class="lineno"> 4 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">id</span><span class="o">;</span>
<span class="lineno"> 5 </span>
<span class="lineno linehl"> 6 </span>  <span class="nd">@XMLElement</span><span class="o">(</span><span class="s">&quot;username&quot;</span><span class="o">)</span> 
<span class="lineno"> 7 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">login</span><span class="o">;</span>
<span class="lineno"> 8 </span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="n">String</span> <span class="nf">toXML</span><span class="o">(){</span>
<span class="lineno">10 </span>    <span class="n">XMLConverter</span> <span class="n">xc</span> <span class="o">=</span> <span class="k">new</span> <span class="n">XMLConverter</span><span class="o">();</span>
<span class="lineno">11 </span>    <span class="k">return</span> <span class="n">xc</span><span class="o">.</span><span class="na">toXML</span><span class="o">(</span><span class="k">this</span><span class="o">)</span> 
<span class="lineno">12 </span>  <span class="o">}</span> 
<span class="lineno">13 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

SELECT insert_pair('eecf792c7384c790c3408295f0a03f02', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">ScheduleSystem</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno">3 </span>  <span class="kd">public</span> <span class="nf">scheduleTask</span><span class="o">(</span><span class="n">Task</span> <span class="n">t</span><span class="o">,</span> <span class="n">Date</span> <span class="n">date</span><span class="o">){</span>
<span class="lineno linehl"> 4 </span>    <span class="n">AlertManager</span> <span class="n">am</span> <span class="o">=</span> <span class="n">ComponentFactory</span><span class="o">.</span><span class="na">getImplementation</span><span class="o">(</span><span class="n">AlertManager</span><span class="o">.</span><span class="na">class</span><span class="o">);</span>
<span class="lineno">5 </span>    <span class="n">am</span><span class="o">.</span><span class="na">createAlert</span><span class="o">(</span><span class="n">date</span><span class="o">,</span> <span class="n">t</span><span class="o">.</span><span class="na">getDescription</span><span class="o">());</span>
<span class="lineno">6 </span>    <span class="c1">//additional logic</span>
<span class="lineno">7 </span>  <span class="o">}</span> 
<span class="lineno">8 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">ScheduleSystem</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@Inject</span>
<span class="lineno linehl"> 4 </span>  <span class="n">AlertManager</span> <span class="n">am</span><span class="o">;</span>
<span class="lineno linehl"> 5 </span>
<span class="lineno"> 6 </span>  <span class="kd">public</span> <span class="nf">scheduleTask</span><span class="o">(</span><span class="n">Task</span> <span class="n">t</span><span class="o">,</span> <span class="n">Date</span> <span class="n">date</span><span class="o">){</span>
<span class="lineno"> 7 </span>    <span class="n">am</span><span class="o">.</span><span class="na">createAlert</span><span class="o">(</span><span class="n">date</span><span class="o">,</span> <span class="n">t</span><span class="o">.</span><span class="na">getDescription</span><span class="o">());</span>
<span class="lineno"> 8 </span>    <span class="c1">//additional logic</span>
<span class="lineno"> 9 </span>  <span class="o">}</span> 
<span class="lineno">10 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

SELECT insert_pair('ad5313c9a61e91191bd6b0b1f65cce33', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Person</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@NumericTolerance</span><span class="o">(</span><span class="mf">0.01</span><span class="o">)</span>
<span class="lineno"> 4 </span>  <span class="kd">private</span> <span class="kt">double</span> <span class="n">height</span><span class="o">;</span>
<span class="lineno"> 5 </span>
<span class="lineno"> 6 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">name</span><span class="o">;</span>
<span class="lineno"> 7 </span>
<span class="lineno linehl"> 8 </span>  <span class="nd">@IgnoreInComparison</span>
<span class="lineno"> 9 </span>  <span class="kd">private</span> <span class="kt">int</span> <span class="n">age</span><span class="o">;</span>
<span class="lineno">10 </span>
<span class="lineno">11 </span>  <span class="kd">public</span> <span class="n">List</span><span class="o">&lt;</span><span class="n">Difference</span><span class="o">&gt;</span> <span class="nf">compare</span><span class="o">(</span><span class="n">Person</span> <span class="n">p</span><span class="o">){</span>
<span class="lineno">12 </span>    <span class="n">Comparator</span> <span class="n">c</span> <span class="o">=</span> <span class="k">new</span> <span class="n">Comparator</span><span class="o">(</span><span class="n">Person</span><span class="o">.</span><span class="na">class</span><span class="o">);</span>
<span class="lineno">13 </span>    <span class="k">return</span> <span class="n">c</span><span class="o">.</span><span class="na">compare</span><span class="o">(</span><span class="k">this</span><span class="o">,</span> <span class="n">p</span><span class="o">);</span>
<span class="lineno">14 </span>  <span class="o">}</span>
<span class="lineno">15 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Person</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">private</span> <span class="kt">double</span> <span class="n">height</span><span class="o">;</span>
<span class="lineno"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">name</span><span class="o">;</span>
<span class="lineno"> 6 </span>
<span class="lineno"> 7 </span>  <span class="kd">private</span> <span class="kt">int</span> <span class="n">age</span><span class="o">;</span>
<span class="lineno"> 8 </span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="n">List</span><span class="o">&lt;</span><span class="n">Difference</span><span class="o">&gt;</span> <span class="nf">compare</span><span class="o">(</span><span class="n">Person</span> <span class="n">p</span><span class="o">){</span>
<span class="lineno">10 </span>    <span class="n">Comparator</span> <span class="n">c</span> <span class="o">=</span> <span class="k">new</span> <span class="n">Comparator</span><span class="o">(</span><span class="n">Person</span><span class="o">.</span><span class="na">class</span><span class="o">);</span>
<span class="lineno linehl">11 </span>    <span class="n">c</span><span class="o">.</span><span class="na">addNumericTolerance</span><span class="o">(</span><span class="s">&quot;height&quot;</span><span class="o">,</span><span class="mf">0.01</span><span class="o">);</span>
<span class="lineno linehl">12 </span>    <span class="n">c</span><span class="o">.</span><span class="na">ignoreInComparison</span><span class="o">(</span><span class="s">&quot;name&quot;</span><span class="o">);</span>
<span class="lineno">13 </span>    <span class="k">return</span> <span class="n">c</span><span class="o">.</span><span class="na">compare</span><span class="o">(</span><span class="k">this</span><span class="o">,</span> <span class="n">p</span><span class="o">);</span>
<span class="lineno">14 </span>  <span class="o">}</span>
<span class="lineno">15 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('365ec6833afcf8a01f92d8705c4a860c', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">CommentManager</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@VerifyUserRole</span><span class="o">(</span><span class="s">&quot;admin&quot;</span><span class="o">);</span>
<span class="lineno">4 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">excludeComment</span><span class="o">(</span><span class="n">Comment</span> <span class="n">c</span><span class="o">){</span>
<span class="lineno">5 </span>    <span class="c1">//method body</span>
<span class="lineno">6 </span>  <span class="o">}</span>
<span class="lineno">7 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">CommentManager</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno">3 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">excludeComment</span><span class="o">(</span><span class="n">Comment</span> <span class="n">c</span><span class="o">){</span>
<span class="lineno linehl"> 4 </span>    <span class="n">SecurityComponent</span><span class="o">.</span><span class="na">verifyUserRole</span><span class="o">(</span><span class="s">&quot;admin&quot;</span><span class="o">);</span>
<span class="lineno">5 </span>    <span class="c1">//method body</span>
<span class="lineno">6 </span>  <span class="o">}</span>
<span class="lineno">7 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('e6ef5b3f9a62809e821fd06486fc098f', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Product</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">private</span> <span class="n">Integer</span> <span class="n">id</span><span class="o">;</span>
<span class="lineno"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">name</span><span class="o">;</span>
<span class="lineno"> 6 </span>
<span class="lineno"> 7 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">barcode</span><span class="o">;</span>
<span class="lineno"> 8 </span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="kt">boolean</span> <span class="nf">isValid</span><span class="o">(){</span>
<span class="lineno">10 </span>    <span class="n">Validator</span> <span class="n">v</span> <span class="o">=</span> <span class="k">new</span> <span class="n">Validator</span><span class="o">();</span>
<span class="lineno linehl">11 </span>    <span class="n">v</span><span class="o">.</span><span class="na">fieldNotNull</span><span class="o">(</span><span class="s">&quot;id&quot;</span><span class="o">);</span>
<span class="lineno linehl">12 </span>    <span class="n">v</span><span class="o">.</span><span class="na">fieldNotEmpty</span><span class="o">(</span><span class="s">&quot;name&quot;</span><span class="o">);</span>
<span class="lineno linehl">13 </span>    <span class="n">v</span><span class="o">.</span><span class="na">fieldLengthRange</span><span class="o">(</span><span class="s">&quot;name&quot;</span><span class="o">,</span><span class="mi">5</span><span class="o">,</span><span class="mi">20</span><span class="o">);</span>
<span class="lineno linehl">14 </span>    <span class="n">v</span><span class="o">.</span><span class="na">fieldRegularExpression</span><span class="o">(</span><span class="s">&quot;barcode&quot;</span><span class="o">,</span><span class="s">&quot;^[8]{1}[0-9]{11}$&quot;</span><span class="o">);</span>
<span class="lineno">15 </span>    <span class="k">return</span> <span class="n">v</span><span class="o">.</span><span class="na">validate</span><span class="o">(</span><span class="k">this</span><span class="o">);</span>
<span class="lineno">16 </span>  <span class="o">}</span>
<span class="lineno">17 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Product</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@NotNull</span>
<span class="lineno"> 4 </span>  <span class="kd">private</span> <span class="n">Integer</span> <span class="n">id</span><span class="o">;</span>
<span class="lineno"> 5 </span>
<span class="lineno linehl"> 6 </span>  <span class="nd">@NotEmpty</span>
<span class="lineno linehl"> 7 </span>  <span class="nd">@LengthRange</span><span class="o">(</span><span class="n">min</span><span class="o">=</span><span class="mi">5</span><span class="o">,</span> <span class="n">max</span><span class="o">=</span><span class="mi">20</span><span class="o">)</span>
<span class="lineno"> 8 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">name</span><span class="o">;</span>
<span class="lineno"> 9 </span>
<span class="lineno linehl">10 </span>  <span class="nd">@RegularExpression</span><span class="o">(</span><span class="s">&quot;^[8]{1}[0-9]{11}$&quot;</span><span class="o">)</span>
<span class="lineno">11 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">barcode</span><span class="o">;</span>
<span class="lineno">12 </span>
<span class="lineno">13 </span>  <span class="kd">public</span> <span class="kt">boolean</span> <span class="nf">isValid</span><span class="o">(){</span>
<span class="lineno">14 </span>    <span class="n">Validator</span> <span class="n">v</span> <span class="o">=</span> <span class="k">new</span> <span class="n">Validator</span><span class="o">();</span>
<span class="lineno">15 </span>    <span class="k">return</span> <span class="n">v</span><span class="o">.</span><span class="na">validate</span><span class="o">(</span><span class="k">this</span><span class="o">);</span>
<span class="lineno">16 </span>  <span class="o">}</span>
<span class="lineno">17 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

SELECT insert_pair('d46a7470cc0af40b744f14ee994a38e9', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">FinantialOperation</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="kd">private</span> <span class="n">TransactionManager</span> <span class="n">tm</span><span class="o">;</span>
<span class="lineno linehl"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">transferMoney</span><span class="o">(</span><span class="n">Account</span> <span class="n">source</span><span class="o">,</span> <span class="n">Account</span> <span class="n">destiny</span><span class="o">,</span> <span class="kt">double</span> <span class="n">amount</span><span class="o">){</span>
<span class="lineno linehl"> 6 </span>  	<span class="n">tm</span><span class="o">.</span><span class="na">initTransaction</span><span class="o">();</span>
<span class="lineno"> 7 </span>    <span class="c1">//method body</span>
<span class="lineno linehl"> 8 </span>    <span class="n">tm</span><span class="o">.</span><span class="na">finishTransaction</span><span class="o">();</span>
<span class="lineno"> 9 </span>  <span class="o">}</span>
<span class="lineno">10 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">FinantialOperation</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@TransactionRequired</span>
<span class="lineno">4 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">transferMoney</span><span class="o">(</span><span class="n">Account</span> <span class="n">source</span><span class="o">,</span> <span class="n">Account</span> <span class="n">destiny</span><span class="o">,</span> <span class="kt">double</span> <span class="n">amount</span><span class="o">){</span>
<span class="lineno">5 </span>    <span class="c1">//method body</span>
<span class="lineno">6 </span>  <span class="o">}</span>
<span class="lineno">7 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

SELECT insert_pair('ca9c2afca324cb63a7652de0c750e45d', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">EvaluationComponent</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@AddPointsToUserOnSuccess</span><span class="o">(</span><span class="mi">10</span><span class="o">)</span>
<span class="lineno">4 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">evaluateProduct</span><span class="o">(</span><span class="n">Product</span> <span class="n">p</span><span class="o">,</span> <span class="n">Comment</span> <span class="n">c</span><span class="o">){</span>
<span class="lineno">5 </span>    <span class="c1">//method body</span>
<span class="lineno">6 </span>  <span class="o">}</span>
<span class="lineno">7 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">EvaluationComponent</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno">3 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">evaluateProduct</span><span class="o">(</span><span class="n">Product</span> <span class="n">p</span><span class="o">,</span> <span class="n">Comment</span> <span class="n">c</span><span class="o">){</span>
<span class="lineno">4 </span>    <span class="c1">//method body</span>
<span class="lineno linehl"> 5 </span>    <span class="k">if</span><span class="o">(</span><span class="n">sucess</span><span class="o">){</span>
<span class="lineno linehl"> 6 </span>    	<span class="n">Gamification</span><span class="o">.</span><span class="na">addPointsToUser</span><span class="o">(</span><span class="n">CurrentUser</span><span class="o">.</span><span class="na">get</span><span class="o">(),</span> <span class="mi">10</span><span class="o">);</span>
<span class="lineno linehl"> 7 </span>    <span class="o">}</span>
<span class="lineno">8 </span>  <span class="o">}</span>
<span class="lineno">9 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('b3a5498c02342b8832c35ba9bd5be569', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">InterfaceHandler</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@OnMouseEvent</span><span class="o">(</span><span class="n">numberOfClicks</span><span class="o">=</span><span class="mi">2</span><span class="o">,</span> <span class="n">componentName</span><span class="o">=</span><span class="s">&quot;OK_button&quot;</span><span class="o">)</span>
<span class="lineno linehl"> 4 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">handleClick</span><span class="o">(){</span>
<span class="lineno linehl"> 5 </span>    <span class="c1">//execute logic</span>
<span class="lineno">6 </span>  <span class="o">}</span> 
<span class="lineno">7 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno">1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">InterfaceHandler</span><span class="o">{</span>
<span class="lineno">2 </span>
<span class="lineno linehl"> 3 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">handleClick</span><span class="o">(</span><span class="n">MouseEvent</span> <span class="n">e</span><span class="o">){</span>
<span class="lineno linehl"> 4 </span>    <span class="k">if</span><span class="o">(</span><span class="n">e</span><span class="o">.</span><span class="na">numberOfClicks</span><span class="o">()</span> <span class="o">==</span> <span class="mi">2</span> <span class="n">and</span> <span class="n">e</span><span class="o">.</span><span class="na">componentName</span><span class="o">().</span><span class="na">equals</span><span class="o">(</span><span class="s">&quot;OK_button&quot;</span><span class="o">)){</span>
<span class="lineno linehl"> 5 </span>      <span class="c1">//execute logic</span>
<span class="lineno linehl"> 6 </span>    <span class="o">}</span>
<span class="lineno">7 </span>  <span class="o">}</span> 
<span class="lineno">8 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('7c3dda8f15f26ddf0a78a328a4ecfa6e', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Product</span> <span class="kd">implements</span> <span class="n">ObjectLoader</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="nd">@Override</span>
<span class="lineno"> 4 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">loadObject</span><span class="o">(</span><span class="n">String</span> <span class="n">filename</span><span class="o">){</span>
<span class="lineno"> 5 </span>    <span class="c1">//loading logic</span>
<span class="lineno linehl"> 6 </span>    <span class="n">cleanCache</span><span class="o">();</span>
<span class="lineno"> 7 </span>  <span class="o">}</span> 
<span class="lineno"> 8 </span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">cleanCache</span><span class="o">(){</span>
<span class="lineno">10 </span>    <span class="c1">//clear the cache data</span>
<span class="lineno">11 </span>  <span class="o">}</span>
<span class="lineno">12 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Product</span> <span class="kd">implements</span> <span class="n">ObjectLoader</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="nd">@Override</span>
<span class="lineno"> 4 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">loadObject</span><span class="o">(</span><span class="n">String</span> <span class="n">filename</span><span class="o">){</span>
<span class="lineno"> 5 </span>    <span class="c1">//loading logic</span>
<span class="lineno"> 6 </span>  <span class="o">}</span> 
<span class="lineno"> 7 </span>
<span class="lineno linehl"> 8 </span>  <span class="nd">@AfterLoad</span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">cleanCache</span><span class="o">(){</span>
<span class="lineno">10 </span>    <span class="c1">//clear the cache data</span>
<span class="lineno">11 </span>  <span class="o">}</span>
<span class="lineno">12 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

SELECT insert_pair('6488766c2f90cf468f14df23493cde66', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Airplane</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">state</span> <span class="o">=</span> <span class="s">&quot;ground&quot;</span><span class="o">;</span>
<span class="lineno linehl"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">takeOff</span><span class="o">(){</span>
<span class="lineno linehl"> 6 </span>     <span class="k">if</span><span class="o">(!</span><span class="n">state</span><span class="o">.</span><span class="na">equals</span><span class="o">(</span><span class="s">&quot;ground&quot;</span><span class="o">))</span>
<span class="lineno linehl"> 7 </span>        <span class="k">throw</span> <span class="k">new</span> <span class="n">IllegalObjectStateException</span><span class="o">();</span>
<span class="lineno"> 8 </span>     <span class="c1">// take off logic</span>
<span class="lineno linehl"> 9 </span>     <span class="n">state</span> <span class="o">=</span> <span class="s">&quot;air&quot;</span><span class="o">;</span>
<span class="lineno">10 </span>  <span class="o">}</span>
<span class="lineno">11 </span>
<span class="lineno">12 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">land</span><span class="o">(){</span>
<span class="lineno linehl">13 </span>     <span class="k">if</span><span class="o">(!</span><span class="n">state</span><span class="o">.</span><span class="na">equals</span><span class="o">(</span><span class="s">&quot;air&quot;</span><span class="o">))</span>
<span class="lineno linehl">14 </span>        <span class="k">throw</span> <span class="k">new</span> <span class="n">IllegalObjectStateException</span><span class="o">();</span>
<span class="lineno">15 </span>     <span class="c1">// landing logic</span>
<span class="lineno linehl">16 </span>     <span class="n">state</span> <span class="o">=</span> <span class="s">&quot;ground&quot;</span><span class="o">;</span>
<span class="lineno">17 </span>  <span class="o">}</span>
<span class="lineno">18 </span>
<span class="lineno">19 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">fly</span><span class="o">(){</span>
<span class="lineno linehl">20 </span>     <span class="k">if</span><span class="o">(!</span><span class="n">state</span><span class="o">.</span><span class="na">equals</span><span class="o">(</span><span class="s">&quot;air&quot;</span><span class="o">))</span>
<span class="lineno linehl">21 </span>        <span class="k">throw</span> <span class="k">new</span> <span class="n">IllegalObjectStateException</span><span class="o">();</span>
<span class="lineno">22 </span>     <span class="c1">// fly logic</span>
<span class="lineno">23 </span>  <span class="o">}</span>
<span class="lineno">24 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno linehl"> 1 </span><span class="nd">@InitialState</span><span class="o">(</span><span class="s">&quot;ground&quot;</span><span class="o">)</span>
<span class="lineno"> 2 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Airplane</span><span class="o">{</span>
<span class="lineno"> 3 </span>
<span class="lineno linehl"> 4 </span>  <span class="nd">@ValidState</span><span class="o">(</span><span class="s">&quot;ground&quot;</span><span class="o">)</span>
<span class="lineno linehl"> 5 </span>  <span class="nd">@ChangeState</span><span class="o">(</span><span class="s">&quot;air&quot;</span><span class="o">)</span>
<span class="lineno"> 6 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">takeOff</span><span class="o">(){</span>
<span class="lineno"> 7 </span>     <span class="c1">// take off logic</span>
<span class="lineno"> 8 </span>  <span class="o">}</span>
<span class="lineno"> 9 </span>
<span class="lineno linehl">10 </span>  <span class="nd">@ValidState</span><span class="o">(</span><span class="s">&quot;air&quot;</span><span class="o">)</span>
<span class="lineno linehl">11 </span>  <span class="nd">@ChangeState</span><span class="o">(</span><span class="s">&quot;ground&quot;</span><span class="o">)</span>
<span class="lineno">12 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">land</span><span class="o">(){</span>
<span class="lineno">13 </span>     <span class="c1">// landing logic</span>
<span class="lineno">14 </span>  <span class="o">}</span>
<span class="lineno">15 </span>
<span class="lineno linehl">16 </span>  <span class="nd">@ValidState</span><span class="o">(</span><span class="s">&quot;air&quot;</span><span class="o">)</span>
<span class="lineno">17 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">fly</span><span class="o">(){</span>
<span class="lineno">18 </span>     <span class="c1">// fly logic</span>
<span class="lineno">19 </span>  <span class="o">}</span>
<span class="lineno">20 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

SELECT insert_pair('8d93b7e6deec544988915b53a4500b3d', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">SocialServiceIntegration</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">public</span> <span class="n">UserData</span> <span class="nf">retrieveUserData</span><span class="o">(</span><span class="n">String</span> <span class="n">idNumber</span><span class="o">){</span>
<span class="lineno linehl"> 4 </span>    <span class="n">SocialServiceInvoker</span> <span class="n">ssi</span> <span class="o">=</span> <span class="k">new</span> <span class="n">SocialServiceInvoker</span><span class="o">();</span>
<span class="lineno linehl"> 5 </span>    <span class="k">return</span> <span class="n">ssi</span><span class="o">.</span><span class="na">getUserInfo</span><span class="o">(</span><span class="n">idNumber</span><span class="o">);</span>
<span class="lineno"> 6 </span>  <span class="o">}</span> 
<span class="lineno"> 7 </span>
<span class="lineno linehl"> 8 </span>  <span class="nd">@HandleException</span><span class="o">(</span><span class="n">ExternalInvocationException</span><span class="o">.</span><span class="na">class</span><span class="o">)</span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">handleErros</span><span class="o">(</span><span class="n">Exception</span> <span class="n">ex</span><span class="o">){</span>
<span class="lineno">10 </span>    <span class="c1">//error handling</span>
<span class="lineno">11 </span>  <span class="o">}</span>
<span class="lineno">12 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">SocialServiceIntegration</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">public</span> <span class="n">UserData</span> <span class="nf">retrieveUserData</span><span class="o">(</span><span class="n">String</span> <span class="n">idNumber</span><span class="o">){</span>
<span class="lineno linehl"> 4 </span>    <span class="k">try</span><span class="o">{</span>
<span class="lineno linehl"> 5 </span>       <span class="n">SocialServiceInvoker</span> <span class="n">ssi</span> <span class="o">=</span> <span class="k">new</span> <span class="n">SocialServiceInvoker</span><span class="o">();</span>
<span class="lineno linehl"> 6 </span>       <span class="k">return</span> <span class="n">ssi</span><span class="o">.</span><span class="na">getUserInfo</span><span class="o">(</span><span class="n">idNumber</span><span class="o">);</span>
<span class="lineno linehl"> 7 </span>    <span class="o">}</span> <span class="k">catch</span><span class="o">(</span><span class="n">ExternalInvocationException</span> <span class="n">ex</span><span class="o">){</span>
<span class="lineno linehl"> 8 </span>       <span class="n">handleError</span><span class="o">(</span><span class="n">ex</span><span class="o">);</span>
<span class="lineno linehl"> 9 </span>    <span class="o">}</span>
<span class="lineno">10 </span>  <span class="o">}</span> 
<span class="lineno">11 </span>
<span class="lineno">12 </span>  <span class="kd">public</span> <span class="kt">void</span> <span class="nf">handleErros</span><span class="o">(</span><span class="n">Exception</span> <span class="n">ex</span><span class="o">){</span>
<span class="lineno">13 </span>    <span class="c1">//error handling</span>
<span class="lineno">14 </span>  <span class="o">}</span>
<span class="lineno">15 </span><span class="o">}</span>
</pre></div>
', 'A');
-------

SELECT insert_pair('dd1bdcdd8fd08fad27fe5fa334d98ca9', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Parameters</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno"> 3 </span>  <span class="kd">private</span> <span class="kt">long</span> <span class="n">timeout</span><span class="o">;</span>
<span class="lineno"> 4 </span>
<span class="lineno"> 5 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">filename</span><span class="o">;</span>
<span class="lineno"> 6 </span>
<span class="lineno"> 7 </span>  <span class="kd">private</span> <span class="kt">boolean</span> <span class="n">verbose</span><span class="o">;</span>
<span class="lineno"> 8 </span>
<span class="lineno"> 9 </span>  <span class="kd">public</span> <span class="kd">static</span> <span class="kt">void</span> <span class="nf">main</span><span class="o">(</span><span class="n">String</span><span class="o">[]</span> <span class="n">args</span><span class="o">){</span>
<span class="lineno">10 </span>     <span class="n">ParamMapper</span> <span class="n">mapper</span> <span class="o">=</span> <span class="k">new</span> <span class="n">ParamMapper</span><span class="o">();</span>
<span class="lineno linehl">11 </span>     <span class="n">mapper</span><span class="o">.</span><span class="na">addTextParameter</span><span class="o">(</span><span class="s">&quot;file&quot;</span><span class="o">,</span> <span class="s">&quot;filename&quot;</span><span class="o">);</span>
<span class="lineno linehl">12 </span>     <span class="n">mapper</span><span class="o">.</span><span class="na">addNumericParameter</span><span class="o">(</span><span class="s">&quot;timeout&quot;</span><span class="o">,</span> <span class="s">&quot;timeout&quot;</span><span class="o">);</span>
<span class="lineno linehl">13 </span>     <span class="n">mapper</span><span class="o">.</span><span class="na">verifyParameterPresence</span><span class="o">(</span><span class="s">&quot;verbose&quot;</span><span class="o">,</span> <span class="s">&quot;verbose&quot;</span><span class="o">);</span>
<span class="lineno">14 </span>     <span class="n">Parameters</span> <span class="n">p</span> <span class="o">=</span> <span class="n">mapper</span><span class="o">.</span><span class="na">map</span><span class="o">(</span><span class="n">Parameters</span><span class="o">.</span><span class="na">class</span><span class="o">);</span>
<span class="lineno">15 </span>     <span class="c1">//software logic</span>
<span class="lineno">16 </span>  <span class="o">}</span>
<span class="lineno linehl">17 </span>
<span class="lineno">18 </span><span class="o">}</span>
</pre></div>
', '<div class="highlight"><pre><span></span><span class="lineno"> 1 </span><span class="kd">public</span> <span class="kd">class</span> <span class="nc">Parameters</span><span class="o">{</span>
<span class="lineno"> 2 </span>
<span class="lineno linehl"> 3 </span>  <span class="nd">@NumericValue</span>
<span class="lineno"> 4 </span>  <span class="kd">private</span> <span class="kt">long</span> <span class="n">timeout</span><span class="o">;</span>
<span class="lineno"> 5 </span>
<span class="lineno linehl"> 6 </span>  <span class="nd">@TextValue</span><span class="o">(</span><span class="n">name</span><span class="o">=</span><span class="s">&quot;file&quot;</span><span class="o">)</span>
<span class="lineno"> 7 </span>  <span class="kd">private</span> <span class="n">String</span> <span class="n">filename</span><span class="o">;</span>
<span class="lineno"> 8 </span>
<span class="lineno linehl"> 9 </span>  <span class="nd">@IsParameterPresent</span>
<span class="lineno">10 </span>  <span class="kd">private</span> <span class="kt">boolean</span> <span class="n">verbose</span><span class="o">;</span>
<span class="lineno">11 </span>
<span class="lineno">12 </span>  <span class="kd">public</span> <span class="kd">static</span> <span class="kt">void</span> <span class="nf">main</span><span class="o">(</span><span class="n">String</span><span class="o">[]</span> <span class="n">args</span><span class="o">){</span>
<span class="lineno">13 </span>     <span class="n">ParamMapper</span> <span class="n">mapper</span> <span class="o">=</span> <span class="k">new</span> <span class="n">ParamMapper</span><span class="o">();</span>
<span class="lineno">14 </span>     <span class="n">Parameters</span> <span class="n">p</span> <span class="o">=</span> <span class="n">mapper</span><span class="o">.</span><span class="na">map</span><span class="o">(</span><span class="n">Parameters</span><span class="o">.</span><span class="na">class</span><span class="o">);</span>
<span class="lineno">15 </span>     <span class="c1">//software logic</span>
<span class="lineno">16 </span>  <span class="o">}</span>
<span class="lineno">17 </span><span class="o">}</span>
</pre></div>
', 'B');
-------

