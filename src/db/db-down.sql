begin transaction;

drop function if exists label_snippet_with_category(char(32), varchar);
drop function if exists loop_test2(int);
drop function if exists loop_test();
drop function if exists insert_pair(varchar, varchar, varchar, varchar);
drop function if exists find_vote_by_experiment_session_and_snippet_pair(e bigint, s bigint);
drop table if exists vote_comments;
drop table if exists votes;
drop table if exists session_events;
drop table if exists experiment_sessions;
drop table if exists client_headers;
drop table if exists profiles;
drop table if exists working_sets_snippet_pairs;
drop table if exists snippet_pairs_categories;
drop table if exists snippet_pairs;
drop table if exists snippets;
drop table if exists categories;
drop table if exists working_sets;

commit;
