begin transaction;

create table working_sets (
	working_set_id serial not null primary key
);

-----

create table categories (
	category_id serial not null primary key,
	label varchar not null
);

create unique index ux_categories_label on categories(label);

-----

create table snippets (
	snippet_id serial not null primary key,
	content varchar not null
);

-----

create table snippet_pairs(
	snippet_pair_id serial not null primary key,
	hash char(32) not null,
	snippet_a int not null references snippets(snippet_id),
	snippet_b int not null references snippets(snippet_id),
	most_readable char(1) not null check (most_readable in ('A', 'B')),
	active boolean not null default(false)
);

-----

create table snippet_pairs_categories (
	snippet_pair_id int not null references snippet_pairs(snippet_pair_id),
	category_id int not null references categories(category_id)
);

create unique index ux_snippet_pairs_categories
on snippet_pairs_categories(snippet_pair_id, category_id);

-----

create table working_sets_snippet_pairs (
	working_set_id int not null references working_sets(working_set_id),
	snippet_pair_id int not null references snippet_pairs(snippet_pair_id),
	presentation_order smallint not null
);

------

create table profiles (
	profile_id serial not null primary key,
	name varchar null,
	email varchar null,
	programming_experience_time real null,
	java_programming_experience_time real null,
	floss_experience_time real null,
	annotations_experience_time real null,
	age varchar null,
	years_in_school varchar null,
	gender char(1) null check ( gender in ('M', 'F') ),
	programming_languages varchar null,
	natural_languages varchar null,
	operating_systems varchar null,
	english_confort_level smallint null check ( english_confort_level between 0 and 4 ),
	perceived_readability_level smallint null check ( perceived_readability_level between 0 and 4 ),
	learning_profile varchar null,
	professional_profile varchar null
);

-----

create table client_headers (
	client_header_id serial not null primary key,
	json_data text null
);

-----

create table experiment_sessions (
	experiment_session_id serial not null primary key,
	profile_id int null references profiles(profile_id),
	working_set_id int null references working_sets(working_set_id),
	client_header_id int null references client_headers(client_header_id),
	jsession_id varchar null,
	ga_client_cookie varchar null,
	locale varchar null,
	comments varchar null,
	start_time timestamp with time zone null,
	end_time timestamp with time zone null,
	origin varchar null 
);

-----

create table session_events (
	session_event_id serial not null primary key,
	experiment_session_id int not null references experiment_sessions(experiment_session_id),
	arrival_time timestamp with time zone not null,
	content varchar not null
);

-----

create table votes (
	vote_id serial not null primary key,
	experiment_session_id int not null references experiment_sessions(experiment_session_id),
	snippet_pair_id int not null references snippet_pairs(snippet_pair_id),
	snippet_order char(1) null,
	arrival_time timestamp with time zone not null,
	comments varchar null,
	strength varchar null,
	other_categorical_variables varchar null,
	last_vote boolean not null default(false)
);

-----

create or replace function find_vote_by_experiment_session_and_snippet_pair(e bigint, s bigint)
returns integer as $$
declare vote_id integer;
begin
	select v.vote_id into vote_id
	from votes v
	where v.experiment_session_id = $1
		and v.snippet_pair_id = $2
	order by v.arrival_time desc, v.vote_id desc
	limit 1;

	return vote_id;
end;
$$ language plpgsql;

-----

CREATE OR REPLACE FUNCTION insert_pair(hash_code varchar, snippet_a_content varchar, 
	snippet_b_content varchar, most_readable varchar)
RETURNS void
AS $$
DECLARE
	a_id INTEGER;
	b_id INTEGER;
BEGIN
	IF NOT EXISTS(SELECT hash FROM snippet_pairs WHERE hash = $1) THEN
		INSERT INTO snippets(content) VALUES (snippet_a_content);
		SELECT currval('snippets_snippet_id_seq') INTO a_id;

		INSERT INTO snippets(content) VALUES (snippet_b_content);
		SELECT currval('snippets_snippet_id_seq') INTO b_id;

		INSERT INTO snippet_pairs(hash, snippet_a, snippet_b, most_readable, active)
		VALUES(hash_code, a_id, b_id, most_readable, true);
	END IF;
END;
$$ LANGUAGE plpgsql;

-----

create or replace function loop_test()
returns table(experiment_session_id int) as $$
begin
	for experiment_session_id in
		select e.experiment_session_id
		from experiment_sessions e
		where e.start_time > '20170801'
	loop
		return next;
	end loop;

end;
$$ language plpgsql;

-----

create or replace function loop_test2(_snippet_pair_id int)
returns table(experiment_session_id integer, total bigint, divergents bigint) as $$
	select e.experiment_session_id,
		(select 
			count(tot.*) 
			from votes tot 
			where tot.experiment_session_id = e.experiment_session_id
				and tot.snippet_pair_id = $1
				and tot.last_vote=true) as total,
		(select
			count(div.*)
			from votes div
				inner join snippet_pairs sp 
				on div.snippet_pair_id = sp.snippet_pair_id
					and div.snippet_order <> sp.most_readable
			where div.experiment_session_id = e.experiment_session_id
				and div.snippet_pair_id = $1
				and div.last_vote=true) as divergents
	from experiment_sessions e
	where e.start_time > '20170801'
		and exists(
			select v.vote_id 
			from votes v 
			where v.experiment_session_id = e.experiment_session_id);
$$ language sql;

-----

create or replace function label_snippet_with_category(_snippet_hash char(32), _category_label varchar)
returns void as $$
declare
	pre_existent_categories int;
	pre_existent_associations int;
	snippet_pair_id_var int;
	category_id_var int;
begin
	if (select count(*) from snippet_pairs where hash = _snippet_hash) = 0 then
		raise 'No snippet pair with the informed hash';
	end if;

	pre_existent_categories := (select count(*) from categories where label = _category_label);
	if pre_existent_categories = 0 then
		insert into categories(label) values(_category_label);
	end if;

	snippet_pair_id_var := (select sp.snippet_pair_id from snippet_pairs sp where hash = _snippet_hash); 
	category_id_var := (select c.category_id from categories c where label = _category_label);

	pre_existent_associations := (
		select count(spc.*) 
		from snippet_pairs_categories spc 
		where spc.snippet_pair_id = snippet_pair_id_var
			and spc.category_id = category_id_var
	); 

	if pre_existent_associations = 0 then
		insert into snippet_pairs_categories (snippet_pair_id, category_id) 
		values (snippet_pair_id_var, category_id_var);
	end if; 
end
$$ 
language 'plpgsql';

commit;
