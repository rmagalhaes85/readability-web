create table working_sets (
	working_set_id serial not null primary key
);

-----

create table snippets (
	snippet_id serial not null primary key,
	content text not null
);

-----

create table snippet_pairs(
	snippet_pair_id serial not null primary key,
	hash char(32) not null,
	snippet_a int not null references snippets(snippet_id),
	snippet_b int not null references snippets(snippet_id),
	most_readable char(1) not null check (most_readable in ('A', 'B')),
	active boolean not null default(false)
);

-----

create table working_sets_snippet_pairs (
	working_set_id int not null references working_sets(working_set_id),
	snippet_pair_id int not null references snippet_pairs(snippet_pair_id),
	presentation_order smallint not null
);

------

create table profiles (
	profile_id serial not null primary key,
	name varchar(100) null,
	email varchar(100) null,
	programming_experience_time real null,
	java_programming_experience_time real null,
	floss_experience_time real null,
	age varchar(20) null,
	years_in_school varchar(50) null,
	gender char(1) null check ( gender in ('M', 'F') ),
	programming_languages varchar(500) null,
	natural_languages varchar(500) null,
	operating_systems varchar(500) null,
	english_confort_level smallint null check ( english_confort_level between 0 and 4 ),
	perceived_readability_level smallint null check ( perceived_readability_level between 0 and 4 ),
	learning_profile varchar(10) null
);

-----

create table client_headers (
	client_header_id serial not null primary key,
	json_data text null
);

-----

create table experiment_sessions (
	experiment_session_id serial not null primary key,
	profile_id int null references profiles(profile_id),
	working_set_id int null references working_sets(working_set_id),
	client_header_id int null references client_headers(client_header_id),
	jsession_id varchar(100) null,
	ga_client_cookie varchar(50) null,
	locale varchar(10) null,
	comments varchar(5000) null,
	start_time timestamp null,
	end_time timestamp null
);

-----

create table session_events (
	session_event_id serial not null primary key,
	experiment_session_id int not null references experiment_sessions(experiment_session_id),
	arrival_time timestamp not null,
	content varchar(5000) not null
);

-----

create table votes (
	vote_id serial not null primary key,
	experiment_session_id int not null references experiment_sessions(experiment_session_id),
	snippet_pair_id int not null references snippet_pairs(snippet_pair_id),
	snippet_order char(1) not null check (snippet_order in ('A', 'B')),
	arrival_time timestamp not null,
	comments varchar(5000) null,
	last_vote boolean not null default(false)
);

-----

create table vote_comments (
	vote_comment_id serial not null primary key,
	vote_id integer not null references votes(vote_id),
	comments varchar(5000) null
);

-----

create or replace function find_vote_by_experiment_session_and_snippet_pair(e bigint, s bigint)
returns integer as $$
declare vote_id integer;
begin
	select v.vote_id into vote_id
	from votes v
	where v.experiment_session_id = $1
		and v.snippet_pair_id = $2
	order by v.arrival_time desc, v.vote_id desc
	limit 1;

	return vote_id;
end;
$$ language plpgsql;



--insert into snippets(content) values('
--<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">	<span style="color: #008800; font-weight: bold">protected</span> ErrorMessageDTO <span style="color: #0066BB; font-weight: bold">processErrors</span><span style="color: #333333">(</span>BindingResult bindingResult<span style="color: #333333">)</span> <span style="color: #333333">{</span>
--		Assert<span style="color: #333333">.</span><span style="color: #0000CC">notNull</span><span style="color: #333333">(</span>bindingResult<span style="color: #333333">);</span>
--		ErrorMessageDTO result <span style="color: #333333">=</span> <span style="color: #008800; font-weight: bold">new</span> ErrorMessageDTO<span style="color: #333333">();</span>
--		Locale locale <span style="color: #333333">=</span> LocaleContextHolder<span style="color: #333333">.</span><span style="color: #0000CC">getLocale</span><span style="color: #333333">();</span>
--		
--		<span style="color: #008800; font-weight: bold">if</span> <span style="color: #333333">(</span>bindingResult<span style="color: #333333">.</span><span style="color: #0000CC">hasGlobalErrors</span><span style="color: #333333">())</span> <span style="color: #333333">{</span>
--			String msg <span style="color: #333333">=</span> messageSource<span style="color: #333333">.</span><span style="color: #0000CC">getMessage</span><span style="color: #333333">(</span>bindingResult<span style="color: #333333">.</span><span style="color: #0000CC">getGlobalError</span><span style="color: #333333">().</span><span style="color: #0000CC">getCode</span><span style="color: #333333">(),</span>
--					bindingResult<span style="color: #333333">.</span><span style="color: #0000CC">getGlobalError</span><span style="color: #333333">().</span><span style="color: #0000CC">getArguments</span><span style="color: #333333">(),</span> locale<span style="color: #333333">);</span>
--			result<span style="color: #333333">.</span><span style="color: #0000CC">setGeneralMessage</span><span style="color: #333333">(</span>msg<span style="color: #333333">);</span>
--		<span style="color: #333333">}</span>
--
--		List<span style="color: #333333">&lt;</span>FieldError<span style="color: #333333">&gt;</span> errors <span style="color: #333333">=</span> bindingResult<span style="color: #333333">.</span><span style="color: #0000CC">getFieldErrors</span><span style="color: #333333">();</span>
--		
--		<span style="color: #008800; font-weight: bold">for</span> <span style="color: #333333">(</span>FieldError fieldError <span style="color: #333333">:</span> errors<span style="color: #333333">)</span> <span style="color: #333333">{</span>
--			String msg <span style="color: #333333">=</span> messageSource<span style="color: #333333">.</span><span style="color: #0000CC">getMessage</span><span style="color: #333333">(</span>fieldError<span style="color: #333333">.</span><span style="color: #0000CC">getDefaultMessage</span><span style="color: #333333">(),</span> fieldError<span style="color: #333333">.</span><span style="color: #0000CC">getArguments</span><span style="color: #333333">(),</span> locale<span style="color: #333333">);</span>
--			result<span style="color: #333333">.</span><span style="color: #0000CC">addFieldError</span><span style="color: #333333">(</span>fieldError<span style="color: #333333">.</span><span style="color: #0000CC">getField</span><span style="color: #333333">(),</span> msg<span style="color: #333333">);</span>
--		<span style="color: #333333">}</span>
--		
--		<span style="color: #008800; font-weight: bold">return</span> result<span style="color: #333333">;</span>
--	<span style="color: #333333">}</span>
--</pre></div>');
--
--
--
--insert into snippets(content) values('
--<!-- HTML generated using hilite.me --><div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span>
--	<span style="color: #008800; font-weight: bold">private</span> MessageSource messageSource<span style="color: #333333">;</span>
--	
--	<span style="color: #555555; font-weight: bold">@ExceptionHandler</span><span style="color: #333333">(</span>MethodArgumentNotValidException<span style="color: #333333">.</span><span style="color: #0000CC">class</span><span style="color: #333333">)</span>
--	<span style="color: #555555; font-weight: bold">@ResponseStatus</span><span style="color: #333333">(</span>HttpStatus<span style="color: #333333">.</span><span style="color: #0000CC">BAD_REQUEST</span><span style="color: #333333">)</span>
--	<span style="color: #555555; font-weight: bold">@ResponseBody</span>
--	<span style="color: #008800; font-weight: bold">public</span> ErrorMessageDTO <span style="color: #0066BB; font-weight: bold">processValidationError</span><span style="color: #333333">(</span>MethodArgumentNotValidException ex<span style="color: #333333">)</span> <span style="color: #333333">{</span>
--		<span style="color: #008800; font-weight: bold">return</span> <span style="color: #0066BB; font-weight: bold">processErrors</span><span style="color: #333333">(</span>ex<span style="color: #333333">.</span><span style="color: #0000CC">getBindingResult</span><span style="color: #333333">());</span>
--	<span style="color: #333333">}</span>
--</pre></div>');
--
--
--
--
--insert into snippets(content) values('snippet 3');
--
--
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('e5fa44f2b31c1fb553b6021e7360d07d', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('7448d8798a4380162d4b56f9b452e2f6', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('a3db5c13ff90a36963278c6a39e4ee3c', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('9c6b057a2b9d96a4067a749ee3b3b015', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('5d9474c0309b7ca09a182d888f73b37a', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('ccf271b7830882da1791852baeca1737', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('d3964f9dad9f60363c81b688324d95b4', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('136571b41aa14adc10c5f3c987d43c02', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('b6abd567fa79cbe0196d093a06727136', 1, 2);
--insert into snippet_pairs(hash, snippet_a, snippet_b) values ('4143d3a341877154d6e95211464e1df1', 1, 2);
--
