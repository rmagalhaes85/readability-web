#!/bin/bash

. ./generate-templates.sh

psql -U postgres -w -f db-down.sql -d readability 

#if [ "$1" = "reset" ] ; then
#  exit 0
#fi

psql -U postgres -w -f db-up.sql -d readability
psql -U postgres -w -f insert-snippets.sql -d readability

