#!/bin/bash

die_file() {
	if [ ! -f $1 ]; then echo "$1: file not found"; exit 1; fi
}

die_file '.remoteuser'
die_file '.remotehost'

keyparam=

if [ -f .kppath ]
then
	keyparam="-i "$(cat .kppath)
fi

ssh $keyparam $(cat .remoteuser)@$(cat .remotehost)
