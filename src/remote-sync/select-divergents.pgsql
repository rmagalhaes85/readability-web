\pset format wrapped

select sp.snippet_pair_id, sp.most_readable, v.snippet_order, (sp.most_readable <> v.snippet_order) as diverges
into temporary divergence_by_snippet_pair
from snippet_pairs sp inner join votes v on sp.snippet_pair_id = v.snippet_pair_id
                      inner join experiment_sessions e on v.experiment_session_id = e.experiment_session_id
where v.last_vote = true and e.end_time is not null
order by sp.snippet_pair_id;

select snippet_pair_id, count(*)
from divergence_by_snippet_pair
where diverges = false
group by snippet_pair_id;

--COPY (select distinct(dsp.snippet_pair_id),
--  coalesce((select count(dsp1.*) from divergence_by_snippet_pair dsp1 where dsp1.snippet_pair_id = dsp.snippet_pair_id), 0) as total_votes,
--  coalesce((select count(dsp2.*) from divergence_by_snippet_pair dsp2 where dsp2.snippet_pair_id = dsp.snippet_pair_id and dsp2.diverges = true), 0) as total_divergents
--from divergence_by_snippet_pair dsp
--order by dsp.snippet_pair_id) to '/tmp/results.tsv';

