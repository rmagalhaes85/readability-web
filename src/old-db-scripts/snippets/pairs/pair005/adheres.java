for (List<Expression> row : node.getRows()) {
    pageBuilder.declarePosition();
    IdentityLinkedHashMap<Expression, Type> expressionTypes = getExpressionTypes(
            context.getSession(),
            metadata,
            sqlParser,
            ImmutableMap.of(),
            ImmutableList.copyOf(row),
            emptyList(),
            false);
    for (int i = 0; i < row.size(); i++) {
        // evaluate the literal value
        Object result = ExpressionInterpreter.expressionInterpreter(row.get(i), metadata, context.getSession(), expressionTypes).evaluate(0);
        writeNativeValue(outputTypes.get(i), pageBuilder.getBlockBuilder(i), result);
    }
}
