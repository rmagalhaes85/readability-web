for (Map.Entry<Symbol, WindowNode.Function> entry : node.getWindowFunctions().entrySet()) {
    Optional<Integer> frameStartChannel = Optional.empty();
    Optional<Integer> frameEndChannel = Optional.empty();

    Frame frame = entry.getValue().getFrame();
    if (frame.getStartValue().isPresent()) {
        frameStartChannel = Optional.of(source.getLayout().get(frame.getStartValue().get()));
    }

    if (frame.getEndValue().isPresent()) {
        frameEndChannel = Optional.of(source.getLayout().get(frame.getEndValue().get()));
    }

    FrameInfo frameInfo = new FrameInfo(frame.getType(), frame.getStartType(), frameStartChannel, frame.getEndType(), frameEndChannel);
    FunctionCall functionCall = entry.getValue().getFunctionCall();
    Signature signature = entry.getValue().getSignature();

    ImmutableList.Builder<Integer> arguments = ImmutableList.builder();
    for (Expression argument : functionCall.getArguments()) {
        Symbol argumentSymbol = Symbol.from(argument);
        arguments.add(source.getLayout().get(argumentSymbol));
    }

    Symbol symbol = entry.getKey();
    WindowFunctionSupplier windowFunctionSupplier = metadata.getFunctionRegistry().getWindowFunctionImplementation(signature);

    Type type = metadata.getType(signature.getReturnType());
    windowFunctionsBuilder.add(window(windowFunctionSupplier, type, frameInfo, arguments.build()));
    windowFunctionOutputSymbolsBuilder.add(symbol);
}
