message.setDefaultInt32   (401);
message.setDefaultInt64   (402);
message.setDefaultUint32  (403);
message.setDefaultUint64  (404);
message.setDefaultSint32  (405);
message.setDefaultSint64  (406);
message.setDefaultFixed32 (407);
message.setDefaultFixed64 (408);
message.setDefaultSfixed32(409);
message.setDefaultSfixed64(410);
message.setDefaultFloat   (411);
message.setDefaultDouble  (412);
message.setDefaultBool    (false);
message.setDefaultString  ("415");
message.setDefaultBytes   (toBytes("416"));

message.setDefaultNestedEnum (TestAllTypes.NestedEnum.FOO);
message.setDefaultForeignEnum(ForeignEnum.FOREIGN_FOO);
message.setDefaultImportEnum (ImportEnum.IMPORT_FOO);
