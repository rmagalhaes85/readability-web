List<org.egov.commons.Installment> instList;
instList = new ArrayList<>(instTaxMap.keySet());
LOGGER.debug("createDemandForModify: instList: " + instList);
org.egov.ptis.domain.entity.demand.Ptdemand ptDemandOld;
org.egov.ptis.domain.entity.demand.Ptdemand ptDemandNew;
Map<String,org.egov.commons.Installment> yearwiseInstMap = InstallmentsForCurrYear(new Date());
org.egov.commons.Installment installmentFirstHalf = yearwiseInstMap.get(PropertyTaxConstants.CURRENTYEAR_FIRST_HALF);
org.egov.commons.Installment installmentSecondHalf = yearwiseInstMap.get(PropertyTaxConstants.CURRENTYEAR_SECOND_HALF);
final Map<String, org.egov.ptis.domain.entity.demand.Ptdemand> PtdemandMap = PtdemandsAsInstMap(oldProperty.getPtDemandSet());
ptDemandOld = PtdemandMap.get(installmentFirstHalf.getDescription());
final PropertyTypeMaster oldPropTypeMaster = oldProperty.getPropertyDetail().getPropertyTypeMaster();
final PropertyTypeMaster newPropTypeMaster = newProperty.getPropertyDetail().getPropertyTypeMaster();

