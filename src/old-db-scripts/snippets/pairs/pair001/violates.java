if (nodeToIndexMap == null) {
    nodeToIndexMap = new HashMap<IMXMLNode, Integer>();
    indexToNodeMap = new HashMap<Integer, IMXMLNode>();
}
nodeToIndexMap.put(node, instanceNodeCounter);
indexToNodeMap.put(instanceNodeCounter, node);
++instanceNodeCounter;  

InstructionList il;

if (getProject().getTargetSettings().getMxmlChildrenAsData()) {
    if (nodeToInstanceDescriptorMap == null) { nodeToInstanceDescriptorMap = new HashMap<IMXMLNode, InstructionList>(); }

    il = new InstructionList();

    nodeToInstanceDescriptorMap.put(node, il);
    // build the initializer function by processing the node
    
    Context stateContext = new Context((IMXMLInstanceNode)node, il);
    stateContext.isContentFactory = true;
    processNode(node, stateContext);
    stateContext.transfer(IL.MXML_CONTENT_FACTORY);
    stateContext.addInstruction(OP_newarray, stateContext.getCounter(IL.MXML_CONTENT_FACTORY));      
} else { context.addInstruction(OP_findpropstrict, deferredInstanceFromFunctionName);  }

