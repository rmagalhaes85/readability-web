IndexedDIP dip = bundle.getDip();
if (!dip.getFileIds().isEmpty()) {
    IndexedFile file = BrowserHelper.retrieve(IndexedFile.class, IdUtils.getFileId(dip.getFileIds().get(0)));
    bundle.setFile(file);
    bundle.setRepresentation(BrowserHelper.retrieve(IndexedRepresentation.class, file.getRepresentationUUID()));
    bundle.setAip(BrowserHelper.retrieve(IndexedAIP.class, file.getAipId()));
} else if (!dip.getRepresentationIds().isEmpty()) {
    IndexedRepresentation representation = BrowserHelper.retrieve(IndexedRepresentation.class,
    IdUtils.getRepresentationId(dip.getRepresentationIds().get(0)));
    bundle.setRepresentation(representation);
    bundle.setAip(BrowserHelper.retrieve(IndexedAIP.class, representation.getAipId()));
} else if (!dip.getAipIds().isEmpty()) {
    IndexedAIP aip = BrowserHelper.retrieve(IndexedAIP.class, dip.getAipIds().get(0).getAipId());
    bundle.setAip(aip);
}
