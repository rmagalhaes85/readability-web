IndexedDIP dip = b.getDip();
if (!dip.getFileIds().isEmpty()) {
    IndexedFile file = BrowserHelper.retrieve(IndexedFile.class, IdUtils.getFileId(dip.getFileIds().get(0)));
    b.setFile(file);
    b.setRepresentation(BrowserHelper.retrieve(IndexedRepresentation.class, file.getRepresentationUUID()));
    b.setAip(BrowserHelper.retrieve(IndexedAIP.class, file.getAipId()));
} else if (!dip.getRepresentationIds().isEmpty()) {
    IndexedRepresentation r = BrowserHelper.retrieve(IndexedRepresentation.class,
    IdUtils.getRepresentationId(dip.getRepresentationIds().get(0)));
    b.setRepresentation(r);
    b.setAip(BrowserHelper.retrieve(IndexedAIP.class, r.getAipId()));
} else if (!dip.getAipIds().isEmpty()) {
    IndexedAIP aip = BrowserHelper.retrieve(IndexedAIP.class, dip.getAipIds().get(0).getAipId());
    b.setAip(aip);
}

