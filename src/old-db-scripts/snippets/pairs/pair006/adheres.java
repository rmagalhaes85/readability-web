if (commChannel.getCommValue() != null 
        && isValidCommChannel
        && commChannel.getChannelUsage() != null 
        && !(commChannel.getChannelUsage().getID() == 0)) {
    String extCodeForChannelUsage = svc.getRemoteLookup(commChannel.getChannelUsage().getID(), 
            providerSystem.getCodeSystem().getText());

    if (extCodeForChannelUsage != null
            && PDSChannelUsage.WP.equals(commChannel.getChannelUsage())) {
        nk1.getBusinessPhoneNumber(xtnBusinessPhoneIter)
            .getTelecommunicationUseCode().setValue(extCodeForChannelUsage);
        isWorkAddressChannelUsage = Boolean.TRUE;
    } else if (extCodeForChannelUsage != null) {
        nk1.getPhoneNumber(xtnHomePhoneIter)
            .getTelecommunicationUseCode().setValue(extCodeForChannelUsage);
    } else if (PDSChannelUsage.WP.equals(commChannel.getChannelUsage())) {
        nk1.getBusinessPhoneNumber(xtnBusinessPhoneIter)
            .getTelecommunicationUseCode()
            .setValue(commChannel.getChannelUsage().getText());
        isWorkAddressChannelUsage = Boolean.TRUE;
    } else {
        nk1.getPhoneNumber(xtnHomePhoneIter)
            .getTelecommunicationUseCode()
            .setValue(commChannel.getChannelUsage().getText());
    }
}
