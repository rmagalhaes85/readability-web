if (commChannel.getCommValue() != null && isValidCommChannel) {
    if (commChannel.getChannelUsage() != null 
            && !(commChannel.getChannelUsage().getID() == 0)) {
        String extCodeForChannelUsage = svc.getRemoteLookup(commChannel.getChannelUsage().getID(), 
                providerSystem.getCodeSystem().getText());

        if (extCodeForChannelUsage != null) {
            if (PDSChannelUsage.WP.equals(commChannel.getChannelUsage())) {
                nk1.getBusinessPhoneNumber(xtnBusinessPhoneIter)
                    .getTelecommunicationUseCode().setValue(extCodeForChannelUsage);
                isWorkAddressChannelUsage = Boolean.TRUE;
            } else {
                nk1.getPhoneNumber(xtnHomePhoneIter)
                    .getTelecommunicationUseCode().setValue(extCodeForChannelUsage);
            }
        } else {
            if (PDSChannelUsage.WP.equals(commChannel.getChannelUsage())) {
                nk1.getBusinessPhoneNumber(xtnBusinessPhoneIter)
                    .getTelecommunicationUseCode()
                    .setValue(commChannel.getChannelUsage().getText());
                isWorkAddressChannelUsage = Boolean.TRUE;
            } else {
                nk1.getPhoneNumber(xtnHomePhoneIter)
                    .getTelecommunicationUseCode()
                    .setValue(commChannel.getChannelUsage().getText());
            }
        }
    }
}
