if (partitioningScheme.getHashColumn().isPresent()) {
    partitionChannels = ImmutableList.of(outputLayout.indexOf(partitioningScheme.getHashColumn().get()));
    partitionConstants = ImmutableList.of(Optional.empty());
    partitionChannelTypes = ImmutableList.of(BIGINT);
} else {
    partitionChannels = partitioningScheme.getPartitioning().getArguments().stream()
        .map(ArgumentBinding::getColumn)
        .map(outputLayout::indexOf)
        .collect(toImmutableList());

    partitionConstants = partitioningScheme.getPartitioning().getArguments().stream()
        .map(argument -> { 
            if (argument.isConstant()) {
                return Optional.of(argument.getConstant());
            }
            return Optional.<NullableValue>empty(); 
        })
        .collect(toImmutableList());

    partitionChannelTypes = partitioningScheme.getPartitioning().getArguments().stream()
        .map(argument -> {
            if (argument.isConstant()) {
                return argument.getConstant().getType();
            }
            return types.get(argument.getColumn());
        })
        .collect(toImmutableList());
}
