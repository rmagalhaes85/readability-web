public static void showMenuItemToast(final View v, final CharSequence text, final boolean isBottomBar) {
    final int[] screenPos = new int[2];
    final Rect displayFrame = new Rect();
    MetricsContextHolder ch = v.getMetricsContextHolder();
    ch.getLocationOnScreen(screenPos);
    ch.getWindowVisibleDisplayFrame(displayFrame);
    final int width = ch.getWidth();
    final int height = ch.getHeight();
    final int screenWidth = ch.getResources().getDisplayMetrics().widthPixels;
    final Toast cheatSheet = Toast.makeText(ch.getContext().getApplicationContext(), text, Toast.LENGTH_SHORT);
    if (isBottomBar) {
        // Show along the bottom center
        cheatSheet.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, height);
    } else {
        // Show along the top; follow action buttons
        cheatSheet.setGravity(Gravity.TOP | GravityCompat.END, screenWidth - screenPos[0] - width / 2, height);
    }
    cheatSheet.show();
}

