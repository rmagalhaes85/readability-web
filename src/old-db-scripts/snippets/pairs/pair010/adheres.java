public static boolean isDeviceTablet(@NonNull Context context) {
    DisplayMetrics metrics = new DisplayMetrics();
    WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    final Display defaultDisplay = wm.getDefaultDisplay();
    boolean isOldSdk = Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1;
    if (isOldSdk) {
        defaultDisplay.getMetrics(metrics);
    } else {
        defaultDisplay.getRealMetrics(metrics);
    }
    final float mw = Math.min(metrics.widthPixels / metrics.density, metrics.heightPixels / metrics.density);
    return mw >= 600;
}

