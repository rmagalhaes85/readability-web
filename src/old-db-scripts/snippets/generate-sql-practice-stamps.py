#!/usr/bin/env python2

import os
import re
import hashlib

PAIRS_DIR = "pairs"
PAIRS_DESCRIPTOR = "pair.txt"
ADHERES_FILE = "adheres.java"
VIOLATES_FILE = "violates.java"

def print_sql_update(hash_code, tested_practice):
    print("update snippet_pairs set practice=\"" + tested_practice + "\" where hash=\"" + hash_code + "\"")

def check_file (path):
    if not os.path.exists(path):
        print("File \"" + path + "\" not found")
        return False
    return True

def matches_attr (line, attr):
    return not re.search(attr, line) is None

def read_attr (line):
    attr_descr, attr_val = line.split(':')
    attr_val = attr_val.replace(' ', '')
    attr_val = attr_val.replace('\n', '')
    return attr_val

def die_attr (attr_name):
    print ("Attribute \"" + attr_name + "\" not found in file \"" + PAIRS_DESCRIPTOR + "\"")
    quit(1)

def generate_md5hash(adheres_file_path, violates_file_path):
    with open(adheres_file_path, "rb") as adheres_file, open(violates_file_path, "rb") as violates_file:
        concatenation = adheres_file.read() + violates_file.read()
        m = hashlib.md5(concatenation)
        return m.hexdigest()

if not os.path.exists(PAIRS_DIR):
    print("\""+ PAIRS_DIR + "\" directory expected")
    quit(1)
# Para cada diretorio
for root, dirs, files in os.walk(PAIRS_DIR):
    if root == PAIRS_DIR:
        continue

    pairs_file_path = os.path.join(root, PAIRS_DESCRIPTOR)
    adheres_file_path = os.path.join(root, ADHERES_FILE)
    violates_file_path = os.path.join(root, VIOLATES_FILE)
# Valida a presenca do arquivo pair.txt
    if not check_file(pairs_file_path):
        continue

# Valida a presenca dos arquivos adheres.java e violates.java
    if not check_file(adheres_file_path):
        continue

    if not check_file(violates_file_path):
        continue
# Valida a presenca de uma instrucao Tested-Practice no arquivo pair.txt
# Valida a presenca de uma instrucao Comments no arquivo pair.txt
    tested_practice = None
    comments = None

    with open(pairs_file_path) as descriptor_file:
        for description_line in descriptor_file:
            if matches_attr(description_line, "Tested-Practice"):
                tested_practice = read_attr(description_line)
            elif matches_attr(description_line, "Comments"):
                comments = read_attr(description_line)
    
    if tested_practice is None: die_attr ("Tested-Practice")
    if comments is None: die_attr ("Comments")

# Calcula o hash do conteudo da concatenacao entre adheres.java e violates.java. Este sera o hash do snippet pair
    hash_code = generate_md5hash(adheres_file_path, violates_file_path)

    try:
        print_sql_update(hash_code, tested_practice)
    except:
        print("Erro processando o snippet com descriptor = " + pairs_file_path)

