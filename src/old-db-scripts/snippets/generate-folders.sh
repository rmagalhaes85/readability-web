#!/bin/bash

for index in {1..11}
do
	folder_index="00"$index
	rev_folder_index=$(echo $folder_index | rev)
	folder_index=$(echo ${rev_folder_index:0:3} | rev)
	folder_name="pair"$folder_index
	if [ ! -d $folder_name ]
	then
		mkdir  $folder_name
		touch $folder_name/pair.txt
		echo "### Tested-Practice: REP."$index >> $folder_name/pair.txt
		echo "### Comments: " >> $folder_name/pair.txt
		touch $folder_name/violates.java
		touch $folder_name/adheres.java
	else
		echo "Folder $folder_name already exists"
	fi
done

