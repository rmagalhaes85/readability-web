CREATE OR REPLACE FUNCTION insert_pair(hash_code varchar, snippet_a_content varchar, 
	snippet_b_content varchar, most_readable varchar)
RETURNS void
AS $$
DECLARE
	a_id INTEGER;
	b_id INTEGER;
BEGIN
	IF NOT EXISTS(SELECT hash FROM snippet_pairs WHERE hash = $1) THEN
		INSERT INTO snippets(content) VALUES (snippet_a_content);
		SELECT currval('snippets_snippet_id_seq') INTO a_id;

		INSERT INTO snippets(content) VALUES (snippet_b_content);
		SELECT currval('snippets_snippet_id_seq') INTO b_id;

		INSERT INTO snippet_pairs(hash, snippet_a, snippet_b, most_readable, active)
		VALUES(hash_code, a_id, b_id, most_readable, true);
	END IF;
END;
$$ LANGUAGE plpgsql;
