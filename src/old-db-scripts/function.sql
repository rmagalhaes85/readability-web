create or replace function find_vote_by_experiment_session_and_snippet_pair(e bigint, s bigint)
returns integer as $$
declare vote_id integer;
begin
	select v.vote_id into vote_id
	from votes v
	where v.experiment_session_id = $1
		and v.snippet_pair_id = $2
	order by v.arrival_time asc, v.vote_id desc
	limit 1;

	return vote_id;
end;
$$ language plpgsql;

