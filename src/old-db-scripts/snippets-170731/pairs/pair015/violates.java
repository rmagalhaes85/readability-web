//alibaba/dubbo - RedisRegistry.java
public RedisRegistry(URL url) {
  super(url);
  if (url.isAnyHost()) {
    throw new IllegalStateException("registry address == null");
  }
  GenericObjectPool.Config config = new GenericObjectPool.Config();
  config.testOnBorrow = url.getParameter("test.on.borrow", true);
  config.testOnReturn = url.getParameter("test.on.return", false);
  config.testWhileIdle = url.getParameter("test.while.idle", false);
  if (url.getParameter("max.idle", 0) > 0)
    config.maxIdle = url.getParameter("max.idle", 0);
  if (url.getParameter("min.idle", 0) > 0)
    config.minIdle = url.getParameter("min.idle", 0);
  if (url.getParameter("max.active", 0) > 0)
    config.maxActive = url.getParameter("max.active", 0);
  if (url.getParameter("max.wait", url.getParameter("timeout", 0)) > 0)
    config.maxWait = url.getParameter("max.wait", 
        url.getParameter("timeout", 0));
  if (url.getParameter("num.tests.per.eviction.run", 0) > 0)
    config.numTestsPerEvictionRun = 
      url.getParameter("num.tests.per.eviction.run", 0);
  if (url.getParameter("time.between.eviction.runs.millis", 0) > 0)
    config.timeBetweenEvictionRunsMillis = 
      url.getParameter("time.between.eviction.runs.millis", 0);
  if (url.getParameter("min.evictable.idle.time.millis", 0) > 0)
    config.minEvictableIdleTimeMillis = 
      url.getParameter("min.evictable.idle.time.millis", 0);
  
  String cluster = url.getParameter("cluster", "failover");
  if (! "failover".equals(cluster) && ! "replicate".equals(cluster)) {
    throw new IllegalArgumentException("Unsupported redis cluster: " + 
        cluster + ". The redis cluster only supported failover or replicate.");
  }
  replicate = "replicate".equals(cluster);
  
  List<String> addresses = new ArrayList<String>();
  addresses.add(url.getAddress());
  String[] backups = url.getParameter(Constants.BACKUP_KEY, new String[0]);
  if (backups != null && backups.length > 0) {
    addresses.addAll(Arrays.asList(backups));
  }
  for (String address : addresses) {
    int i = address.indexOf(':');
    String host;
    int port;
    if (i > 0) {
      host = address.substring(0, i);
      port = Integer.parseInt(address.substring(i + 1));
    } else {
      host = address;
      port = DEFAULT_REDIS_PORT;
    }
    this.jedisPools.put(address, new JedisPool(config, host, port, 
        url.getParameter(
          Constants.TIMEOUT_KEY, 
          Constants.DEFAULT_TIMEOUT)));
  }
  
  this.reconnectPeriod = url.getParameter(
      Constants.REGISTRY_RECONNECT_PERIOD_KEY, 
      Constants.DEFAULT_REGISTRY_RECONNECT_PERIOD);
  String group = url.getParameter(Constants.GROUP_KEY, DEFAULT_ROOT);
  if (! group.startsWith(Constants.PATH_SEPARATOR)) {
    group = Constants.PATH_SEPARATOR + group;
  }
  if (! group.endsWith(Constants.PATH_SEPARATOR)) {
    group = group + Constants.PATH_SEPARATOR;
  }
  this.root = group;
  
  this.expirePeriod = url.getParameter(Constants.SESSION_TIMEOUT_KEY, 
      Constants.DEFAULT_SESSION_TIMEOUT);
  this.expireFuture = expireExecutor.scheduleWithFixedDelay(new Runnable() {
    public void run() {
      try {
        deferExpired(); 
      } catch (Throwable t) { 
        logger.error(
          "Unexpected exception occur at defer expire time, cause: " 
            + t.getMessage(), t);
      }
    }
  }, expirePeriod / 2, expirePeriod / 2, TimeUnit.MILLISECONDS);
}
