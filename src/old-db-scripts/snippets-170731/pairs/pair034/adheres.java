// elastic/elasticsearch - GeoQueryContext.java
private static ObjectParser<GeoQueryContext.Builder, Void> GEO_CONTEXT_PARSER = new ObjectParser<>(NAME, null);
static {
    GEO_CONTEXT_PARSER.declareField   ((parser, geoQueryContext, geoContextMapping) -> geoQueryContext.setGeoPoint(GeoUtils.parseGeoPoint(parser)), new ParseField(CONTEXT_VALUE), ObjectParser.ValueType.OBJECT);
    GEO_CONTEXT_PARSER.declareInt     (GeoQueryContext.Builder::setBoost,      new ParseField(CONTEXT_BOOST));
    // TODO : add string support for precision for GeoUtils.geoHashLevelsForPrecision()
    GEO_CONTEXT_PARSER.declareInt     (GeoQueryContext.Builder::setPrecision,  new ParseField(CONTEXT_PRECISION));
    // TODO : add string array support for precision for GeoUtils.geoHashLevelsForPrecision()
    GEO_CONTEXT_PARSER.declareIntArray(GeoQueryContext.Builder::setNeighbours, new ParseField(CONTEXT_NEIGHBOURS));
    GEO_CONTEXT_PARSER.declareDouble  (GeoQueryContext.Builder::setLat,        new ParseField("lat"));
    GEO_CONTEXT_PARSER.declareDouble  (GeoQueryContext.Builder::setLon,        new ParseField("lon"));
}
