// square/picasso - NetworkRequestHandlerTest.java
@Test public void doesNotForceLocalCacheOnlyWithAirplaneModeOffAndRetryCount() throws Exception {
  responses.add(responseOf(ResponseBody.create(null, new byte[10])));
  Action action = TestUtils.mockAction(URI_KEY_1, URI_1);
  networkHandler.load(action.getRequest(), 0);
  assertEquals("", requests.takeFirst().cacheControl().toString());
}

@Test public void withZeroRetryCountForcesLocalCacheOnly() throws Exception {
  responses.add(responseOf(ResponseBody.create(null, new byte[10])));
  Action action = TestUtils.mockAction(URI_KEY_1, URI_1);
  BitmapHunter hunter = new BitmapHunter(picasso, dispatcher, cache, stats, action, networkHandler);
  hunter.retryCount = 0;
  hunter.hunt();
  assertEquals(CacheControl.FORCE_CACHE.toString(), requests.takeFirst().cacheControl().toString());
}

@Test public void shouldRetryTwiceWithAirplaneModeOffAndNoNetworkInfo() throws Exception {
  Action action = TestUtils.mockAction(URI_KEY_1, URI_1);
  BitmapHunter hunter = new BitmapHunter(picasso, dispatcher, cache, stats, action, networkHandler);
  assertThat(hunter.shouldRetry(false, null)).isTrue();
  assertThat(hunter.shouldRetry(false, null)).isTrue();
  assertThat(hunter.shouldRetry(false, null)).isFalse();
    }
