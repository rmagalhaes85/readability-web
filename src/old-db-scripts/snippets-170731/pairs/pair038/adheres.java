// facebook/react-native - ReactTextInputManager.java
@Override
public void updateExtraData(ReactEditText view, Object extraData) {
  if (extraData instanceof ReactTextUpdate) {
    ReactTextUpdate updateInfo = (ReactTextUpdate) extraData;

    view.setPadding(
        (int) updateInfo.getPaddingLeft(),
        (int) updateInfo.getPaddingTop(),
        (int) updateInfo.getPaddingRight(),
        (int) updateInfo.getPaddingBottom());

    if (updateInfo.containsImages()) {
      Spannable spannable = updateInfo.getText();
      TextInlineImageSpan.possiblyUpdateInlineImageSpans(spannable, view);
    }
    view.maybeSetText(updateInfo);
  }
}
