// spring-projects/spring-boot - GrapeRootRepositorySystemSessionAutoConfigurationTests.java
@Test
public void noLocalRepositoryWhenNoGrapeRoot() {
    given(this.repositorySystem.newLocalRepositoryManager(eq(this.session),
            any(LocalRepository.class)))
                    .willAnswer(new Answer<LocalRepositoryManager>() {

                        @Override
                        public LocalRepositoryManager answer(
                                InvocationOnMock invocation) throws Throwable {
                            LocalRepository localRepository = invocation
                                    .getArgument(1);
                            return new SimpleLocalRepositoryManagerFactory()
                                    .newInstance(
                                            GrapeRootRepositorySystemSessionAutoConfigurationTests.this.session,
                                            localRepository);
                        }

                    });
    new GrapeRootRepositorySystemSessionAutoConfiguration().apply(this.session,
            this.repositorySystem);
    verify(this.repositorySystem, times(0))
            .newLocalRepositoryManager(eq(this.session), any(LocalRepository.class));
    assertThat(this.session.getLocalRepository()).isNull();
}

@Test
public void grapeRootConfiguresLocalRepositoryLocation() {
    given(this.repositorySystem.newLocalRepositoryManager(eq(this.session),
            any(LocalRepository.class)))
                    .willAnswer(new LocalRepositoryManagerAnswer());

    System.setProperty("grape.root", "foo");
    try {
        new GrapeRootRepositorySystemSessionAutoConfiguration().apply(this.session,
                this.repositorySystem);
    }
    finally {
        System.clearProperty("grape.root");
    }

    verify(this.repositorySystem, times(1))
            .newLocalRepositoryManager(eq(this.session), any(LocalRepository.class));

    assertThat(this.session.getLocalRepository()).isNotNull();
    assertThat(this.session.getLocalRepository().getBasedir().getAbsolutePath())
            .endsWith(File.separatorChar + "foo" + File.separatorChar + "repository");
}

private class LocalRepositoryManagerAnswer implements Answer<LocalRepositoryManager> {

    @Override
    public LocalRepositoryManager answer(InvocationOnMock invocation)
            throws Throwable {
        LocalRepository localRepository = invocation.getArgument(1);
        return new SimpleLocalRepositoryManagerFactory().newInstance(
                GrapeRootRepositorySystemSessionAutoConfigurationTests.this.session,
                localRepository);
    }

}
