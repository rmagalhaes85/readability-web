// elastic/elasticsearch - GeoDistanceSortBuilder.java
while ((token = parser.nextToken()) != XContentParser.Token.END_OBJECT) {
    if (token == XContentParser.Token.FIELD_NAME) {
        currentName = parser.currentName();
    } else if (token == XContentParser.Token.START_ARRAY) {
        parseGeoPoints(parser, geoPoints);

        fieldName = currentName;
    } else if (token == XContentParser.Token.START_OBJECT) {
        if (NESTED_FILTER_FIELD.match(currentName)) {
            nestedFilter = parseInnerQueryBuilder(parser);
        } else {
            // the json in the format of -> field : { lat : 30, lon : 12 }
            if (fieldName != null && fieldName.equals(currentName) == false) {
                throw new ParsingException(
                        parser.getTokenLocation(),
                        "Trying to reset fieldName to [{}], already set to [{}].",
                        currentName,
                        fieldName);
            }
            fieldName = currentName;
            GeoPoint point = new GeoPoint();
            GeoUtils.parseGeoPoint(parser, point);
            geoPoints.add(point);
        }
    } else if (token.isValue()) {
        if (ORDER_FIELD.match(currentName)) {
            order = SortOrder.fromString(parser.text());
        } else if (UNIT_FIELD.match(currentName)) {
            unit = DistanceUnit.fromString(parser.text());
        } else if (DISTANCE_TYPE_FIELD.match(currentName)) {
            geoDistance = GeoDistance.fromString(parser.text());
        } else if (VALIDATION_METHOD_FIELD.match(currentName)) {
            validation = GeoValidationMethod.fromString(parser.text());
        } else if (SORTMODE_FIELD.match(currentName)) {
            sortMode = SortMode.fromString(parser.text());
        } else if (NESTED_PATH_FIELD.match(currentName)) {
            nestedPath = parser.text();
        } else if (token == Token.VALUE_STRING){
            if (fieldName != null && fieldName.equals(currentName) == false) {
                throw new ParsingException(
                        parser.getTokenLocation(),
                        "Trying to reset fieldName to [{}], already set to [{}].",
                        currentName,
                        fieldName);
            }

            GeoPoint point = new GeoPoint();
            point.resetFromString(parser.text());
            geoPoints.add(point);
            fieldName = currentName;
        } else if (fieldName.equals(currentName)){
            throw new ParsingException(
                    parser.getTokenLocation(),
                    "Only geohashes of type string supported for field [{}]",
                    currentName);
        } else {
            throw new ParsingException(
                parser.getTokenLocation(),
                "[{}] does not support [{}]",
                NAME, currentName
            );
        }
    }
}
