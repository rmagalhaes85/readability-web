// spring-projects/spring-boot - ArchiveCommand.java
private GroovyCompiler createCompiler(OptionSet options) {
    List<RepositoryConfiguration> repositoryConfiguration = RepositoryConfigurationFactory
            .createDefaultRepositoryConfiguration();
    GroovyCompilerConfiguration configuration = new OptionSetGroovyCompilerConfiguration(
            options, this, repositoryConfiguration);
    GroovyCompiler groovyCompiler = new GroovyCompiler(configuration);
    groovyCompiler.getAstTransformations().add(0, new GrabAnnotationTransform());
    return groovyCompiler;
}

private List<URL> getClassPathUrls(GroovyCompiler compiler) {
    return new ArrayList<>(Arrays.asList(compiler.getLoader().getURLs()));
}

private List<MatchedResource> findMatchingClasspathEntries(List<URL> classpath,
        OptionSet options) throws IOException {
    ResourceMatcher matcher = new ResourceMatcher(
            options.valuesOf(this.includeOption),
            options.valuesOf(this.excludeOption));
    List<File> roots = new ArrayList<>();
    for (URL classpathEntry : classpath) {
        roots.add(new File(URI.create(classpathEntry.toString())));
    }
    return matcher.find(roots);
}

private void writeJar(File file, Class<?>[] compiledClasses,
        List<MatchedResource> classpathEntries, List<URL> dependencies)
                throws FileNotFoundException, IOException, URISyntaxException {
    final List<Library> libraries;
    try (JarWriter writer = new JarWriter(file)) {
        addManifest(writer, compiledClasses);
        addCliClasses(writer);
        for (Class<?> compiledClass : compiledClasses) {
            addClass(writer, compiledClass);
        }
        libraries = addClasspathEntries(writer, classpathEntries);
    }
    libraries.addAll(createLibraries(dependencies));
    Repackager repackager = new Repackager(file);
    repackager.setMainClass(PackagedSpringApplicationLauncher.class.getName());
    repackager.repackage(new Libraries() {

        @Override
        public void doWithLibraries(LibraryCallback callback) throws IOException {
            for (Library library : libraries) {
                callback.library(library);
            }
        }
    });
}
