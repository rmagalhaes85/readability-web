// facebook/fresco - BasePool.java
@VisibleForTesting
synchronized boolean canAllocate(int sizeInBytes) {
  int hardCap = mPoolParams.maxSizeHardCap;

  // even with our best effort we cannot ensure hard cap limit.
  // Return immediately - no point in trimming any space
  if (sizeInBytes > hardCap - mUsed.mNumBytes) {
    mPoolStatsTracker.onHardCapReached();
    return false;
  }

  // trim if we need to
  int softCap = mPoolParams.maxSizeSoftCap;
  if (sizeInBytes > softCap - (mUsed.mNumBytes + mFree.mNumBytes)) {
    trimToSize(softCap - sizeInBytes);
  }

  // check again to see if we're below the hard cap
  if (sizeInBytes > hardCap - (mUsed.mNumBytes + mFree.mNumBytes)) {
    mPoolStatsTracker.onHardCapReached();
    return false;
  }

  return true;
}
