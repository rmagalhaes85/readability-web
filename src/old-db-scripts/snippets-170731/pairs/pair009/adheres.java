// elastic/elasticsearch - CompletionSugestionBuilder.java
public SuggestionContext build(QueryShardContext context) throws IOException {
    CompletionSuggestionContext suggestionContext = new CompletionSuggestionContext(context);

    // copy over common settings to each suggestion builder
    final MapperService mapperService = context.getMapperService();
    populateCommonFields(mapperService, suggestionContext);

    suggestionContext.setFuzzyOptions(fuzzyOptions);
    suggestionContext.setRegexOptions(regexOptions);

    MappedFieldType mappedFieldType = mapperService.fullName(suggestionContext.getField());
    if (mappedFieldType == null ||
        mappedFieldType instanceof CompletionFieldMapper.CompletionFieldType == false) {
        throw new IllegalArgumentException("Field [" + suggestionContext.getField() + "] is not a completion suggest field");
    }
    if (mappedFieldType instanceof CompletionFieldMapper.CompletionFieldType) {
        CompletionFieldMapper.CompletionFieldType type = (CompletionFieldMapper.CompletionFieldType) mappedFieldType;
        suggestionContext.setFieldType(type);
        if (type.hasContextMappings() && contextBytes != null) {
            try (XContentParser contextParser = XContentFactory.xContent(contextBytes).createParser(context.getXContentRegistry(),
                    contextBytes)) {
                if (type.hasContextMappings() && contextParser != null) {
                    ContextMappings contextMappings = type.getContextMappings();
                    contextParser.nextToken();
                    Map<String, List<ContextMapping.InternalQueryContext>> queryContexts = new HashMap<>(contextMappings.size());
                    assert contextParser.currentToken() == XContentParser.Token.START_OBJECT;
                    XContentParser.Token currentToken;
                    String currentFieldName;
                    while ((currentToken = contextParser.nextToken()) != XContentParser.Token.END_OBJECT) {
                        if (currentToken == XContentParser.Token.FIELD_NAME) {
                            currentFieldName = contextParser.currentName();
                            final ContextMapping mapping = contextMappings.get(currentFieldName);
                            queryContexts.put(currentFieldName, mapping.parseQueryContext(contextParser));
                        }
                    }
                    suggestionContext.setQueryContexts(queryContexts);
                }
            }
        } else if (contextBytes != null) {
            throw new IllegalArgumentException("suggester [" + type.name() + "] doesn't expect any context");
        }
    }
    assert suggestionContext.getFieldType() != null : "no completion field type set";

    return suggestionContext;
}
