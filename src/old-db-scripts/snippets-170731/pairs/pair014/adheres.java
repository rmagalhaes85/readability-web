// elastic/elasticsearch - DirectCandidateGenerator.java
@Override
public CandidateSet drawCandidates(CandidateSet set) throws IOException {
    Candidate original = set.originalTerm;
    BytesRef term = preFilter(original.term, spare, byteSpare);
    final long frequency = original.frequency;
    spellchecker.setThresholdFrequency(this.suggestMode == SuggestMode.SUGGEST_ALWAYS ? 0 : thresholdFrequency(frequency, dictSize));
    SuggestWord[] suggestSimilar = spellchecker.suggestSimilar(new Term(field, term), numCandidates, reader, this.suggestMode);
    List<Candidate> candidates = new ArrayList<>(suggestSimilar.length);
    for (int i = 0; i < suggestSimilar.length; i++) {
        SuggestWord suggestWord = suggestSimilar[i];
        BytesRef candidate = new BytesRef(suggestWord.string);
        postFilter(new Candidate(candidate, internalFrequency(candidate), suggestWord.score,
                score(suggestWord.freq, suggestWord.score, dictSize), false), spare, byteSpare, candidates);
    }
    set.addCandidates(candidates);
    return set;
}

protected BytesRef preFilter(final BytesRef term, final CharsRefBuilder spare, final BytesRefBuilder byteSpare) throws IOException {
    if (preFilter == null) {
        return term;
    }
    final BytesRefBuilder result = byteSpare;
    analyze(preFilter, term, field, new TokenConsumer() {

        @Override
        public void nextToken() throws IOException {
            this.fillBytesRef(result);
        }
    }, spare);
    return result.get();
}

protected void postFilter(final Candidate candidate, final CharsRefBuilder spare, BytesRefBuilder byteSpare,
        final List<Candidate> candidates) throws IOException {
    if (postFilter == null) {
        candidates.add(candidate);
    } else {
        final BytesRefBuilder result = byteSpare;
        analyze(postFilter, candidate.term, field, new TokenConsumer() {
            @Override
            public void nextToken() throws IOException {
                this.fillBytesRef(result);

                if (posIncAttr.getPositionIncrement() > 0 && result.get().bytesEquals(candidate.term))  {
                    BytesRef term = result.toBytesRef();
                    // We should not use frequency(term) here because it will analyze the term again
                    // If preFilter and postFilter are the same analyzer it would fail.
                    long freq = internalFrequency(term);
                    candidates.add(new Candidate(result.toBytesRef(), freq, candidate.stringDistance,
                            score(candidate.frequency, candidate.stringDistance, dictSize), false));
                } else {
                    candidates.add(new Candidate(result.toBytesRef(), candidate.frequency, nonErrorLikelihood,
                            score(candidate.frequency, candidate.stringDistance, dictSize), false));
                }
            }
        }, spare);
    }
}
