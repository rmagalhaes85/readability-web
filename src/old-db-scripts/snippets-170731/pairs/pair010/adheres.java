// elastic/elasticsearch - QueryStringQueryBuilder.java
public static final boolean DEFAULT_AUTO_GENERATE_PHRASE_QUERIES = false;
public static final int DEFAULT_MAX_DETERMINED_STATES = 
    Operations.DEFAULT_MAX_DETERMINIZED_STATES;
public static final boolean DEFAULT_ENABLE_POSITION_INCREMENTS = true;
public static final boolean DEFAULT_ESCAPE = false;
public static final boolean DEFAULT_USE_DIS_MAX = true;
public static final int DEFAULT_FUZZY_PREFIX_LENGTH = 
    FuzzyQuery.defaultPrefixLength;
public static final int DEFAULT_FUZZY_MAX_EXPANSIONS = 
    FuzzyQuery.defaultMaxExpansions;
public static final int DEFAULT_PHRASE_SLOP = 0;
public static final float DEFAULT_TIE_BREAKER = 0.0f;
public static final Fuzziness DEFAULT_FUZZINESS = Fuzziness.AUTO;
public static final Operator DEFAULT_OPERATOR = Operator.OR;
public static final boolean DEFAULT_SPLIT_ON_WHITESPACE = true;

private static final ParseField QUERY_FIELD = 
    new ParseField("query");
private static final ParseField FIELDS_FIELD = 
    new ParseField("fields");
private static final ParseField DEFAULT_FIELD_FIELD = 
    new ParseField("default_field");
private static final ParseField DEFAULT_OPERATOR_FIELD = 
    new ParseField("default_operator");
private static final ParseField ANALYZER_FIELD = 
    new ParseField("analyzer");
private static final ParseField QUOTE_ANALYZER_FIELD = 
    new ParseField("quote_analyzer");
private static final ParseField ALLOW_LEADING_WILDCARD_FIELD = 
    new ParseField("allow_leading_wildcard");
private static final ParseField AUTO_GENERATE_PHRASE_QUERIES_FIELD = 
    new ParseField("auto_generate_phrase_queries");
private static final ParseField MAX_DETERMINIZED_STATES_FIELD = 
    new ParseField("max_determinized_states");
private static final ParseField LOWERCASE_EXPANDED_TERMS_FIELD = 
    new ParseField("lowercase_expanded_terms")
        .withAllDeprecated("Decision is now made by the analyzer");
private static final ParseField ENABLE_POSITION_INCREMENTS_FIELD = 
    new ParseField("enable_position_increments");
private static final ParseField ESCAPE_FIELD = 
    new ParseField("escape");
private static final ParseField USE_DIS_MAX_FIELD = 
    new ParseField("use_dis_max");
private static final ParseField FUZZY_PREFIX_LENGTH_FIELD = 
    new ParseField("fuzzy_prefix_length");
private static final ParseField FUZZY_MAX_EXPANSIONS_FIELD = 
    new ParseField("fuzzy_max_expansions");
private static final ParseField FUZZY_REWRITE_FIELD = 
    new ParseField("fuzzy_rewrite");
private static final ParseField PHRASE_SLOP_FIELD = 
    new ParseField("phrase_slop");
private static final ParseField TIE_BREAKER_FIELD = 
    new ParseField("tie_breaker");
private static final ParseField ANALYZE_WILDCARD_FIELD = 
    new ParseField("analyze_wildcard");
private static final ParseField REWRITE_FIELD = 
    new ParseField("rewrite");
private static final ParseField MINIMUM_SHOULD_MATCH_FIELD = 
    new ParseField("minimum_should_match");
private static final ParseField QUOTE_FIELD_SUFFIX_FIELD = 
    new ParseField("quote_field_suffix");
private static final ParseField LENIENT_FIELD = 
    new ParseField("lenient");
private static final ParseField LOCALE_FIELD = 
    new ParseField("locale")
        .withAllDeprecated("Decision is now made by the analyzer");
private static final ParseField TIME_ZONE_FIELD = 
    new ParseField("time_zone");
private static final ParseField SPLIT_ON_WHITESPACE = 
    new ParseField("split_on_whitespace");
private static final ParseField ALL_FIELDS_FIELD = 
    new ParseField("all_fields");
