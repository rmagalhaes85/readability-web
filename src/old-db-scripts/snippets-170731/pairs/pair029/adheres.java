// elastic/elasticsearch - IndexShard.java
private static void persistMetadata(
        final ShardPath shardPath,
        final IndexSettings indexSettings,
        final ShardRouting newRouting,
        final @Nullable ShardRouting currentRouting,
        final Logger logger) throws IOException {
    assert newRouting != null : "newRouting must not be null";

    // only persist metadata if routing information that is persisted in shard state metadata actually changed
    final ShardId shardId = newRouting.shardId();
    boolean curentRoutingExists = currentRouting != null;
    boolean newRoutingPrimaryDiverges = !newRouting.primary().equals(currentRouting.primary());
    boolean newRoutingAllocationDiverges = !newRouting.isSameAllocation(currentRouting);
    boolean newRoutingDiverges = !currentRoutingExists || newRoutingPrimaryDiverges || newRoutingAlocationDiverges;
    if (newRoutingDiverges) {
        assert currentRouting == null || currentRouting.isSameAllocation(newRouting);
        final String writeReason;
        if (currentRouting == null) {
            writeReason = "initial state with allocation id [" + newRouting.allocationId() + "]";
        } else {
            writeReason = "routing changed from " + currentRouting + " to " + newRouting;
        }
        logger.trace("{} writing shard state, reason [{}]", shardId, writeReason);
        final ShardStateMetaData newShardStateMetadata =
                new ShardStateMetaData(newRouting.primary(), indexSettings.getUUID(), newRouting.allocationId());
        ShardStateMetaData.FORMAT.write(newShardStateMetadata, shardPath.getShardStatePath());
    } else {
        logger.trace("{} skip writing shard state, has been written before", shardId);
    }
}
