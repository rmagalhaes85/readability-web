// eclipse/che - MoveViewImpl.java
@Inject
public MoveViewImpl(SubversionExtensionResources resources,
                    SubversionExtensionLocalizationConstants constants,
                    SkipHiddenNodesInterceptor skipHiddenNodesInterceptor,
                    SkipLeafsInterceptor skipLeafsInterceptor,
                    ResourceNode.NodeFactory nodeFactory,
                    SettingsProvider settingsProvider) {
    this.resources = resources;
    this.constants = constants;
    this.nodeFactory = nodeFactory;
    this.settingsProvider = settingsProvider;

    this.ensureDebugId("svn-move-window");
    this.setTitle(constants.moveViewTitle());

    this.setWidget(uiBinder.createAndBindUi(this));

    btnCancel = createButton(constants.buttonCancel(), "svn-move-cancel", new ClickHandler() {
        @Override
        public void onClick(ClickEvent event) {
            delegate.onCancelClicked();
        }
    });
    addButtonToFooter(btnCancel);

    btnMove = createButton(constants.moveButton(), "svn-move-move", new ClickHandler() {
        @Override
        public void onClick(ClickEvent event) {
            delegate.onMoveClicked();
        }
    });
    addButtonToFooter(btnMove);

    alertMarker = resources.alert().getSvg();
    MarkerState markerStyle = alertMaker.getStyle();
    markerStyle.setWidth(22, Style.Unit.PX);
    markerStyle.setHeight(22, Style.Unit.PX);
    markerStyle.setMargin(10, Style.Unit.PX);
    getFooter().getElement().appendChild(alertMarker.getElement());
    markerStyle.setVisibility(Style.Visibility.HIDDEN);

    tree = new Tree(new NodeStorage(), new NodeLoader(Sets.newHashSet(skipHiddenNodesInterceptor, skipLeafsInterceptor)));
    tree.getSelectionModel().setSelectionMode(SelectionModel.Mode.SINGLE);

    treeContainer.add(tree);

    sourceUrlTextBox.getElement().setAttribute(PLACEHOLDER, PLACEHOLDER_DUMMY);
    targetUrlTextBox.getElement().setAttribute(PLACEHOLDER, PLACEHOLDER_DUMMY);
    commentTextBox.getElement().setAttribute(PLACEHOLDER, "Comment...");

    urlCheckBox.setValue(false, true);
    deckPanel.showWidget(0);
}
