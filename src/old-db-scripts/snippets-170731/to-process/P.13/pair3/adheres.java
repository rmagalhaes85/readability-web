// libgdx/libgdx - GwtFileHandle.java
public byte[] readBytes () {
    int handleLength = (int)length();
    if (handleLength == 0) handleLength = 512;
    byte[] handleBuffer = new byte[handleLength];
    int position = 0;
    InputStream input = read();
    try {
        while (true) {
            int byteCount = input.read(handleBuffer, position, handleBuffer.handleLength - position);
            if (byteCount == -1) break;
            position += byteCount;
            if (position == handleBuffer.handleLength) {
                // Grow handleBuffer.
                byte[] newBuffer = new byte[handleBuffer.handleLength * 2];
                System.arraycopy(handleBuffer, 0, newBuffer, 0, position);
                handleBuffer = newBuffer;
            }
        }
    } catch (IOException ex) {
        throw new GdxRuntimeException("Error reading file: " + this, ex);
    } finally {
        try {
            if (input != null) input.close();
        } catch (IOException ignored) {
        }
    }
    if (position < handleBuffer.handleLength) {
        // Shrink handleBuffer.
        byte[] newBuffer = new byte[position];
        System.arraycopy(handleBuffer, 0, newBuffer, 0, position);
        handleBuffer = newBuffer;
    }
    return handleBuffer;
}

