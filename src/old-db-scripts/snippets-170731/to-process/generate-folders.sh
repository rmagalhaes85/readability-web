#!/bin/bash

for index in {1..14}
do
	folder_index="0"$index
	folder_index=$(echo "$folder_index" | rev)
	folder_index=$(echo ${folder_index:0:2} | rev)
	mkdir -p "P.$folder_index"
	#echo "P."$folder_index
done
