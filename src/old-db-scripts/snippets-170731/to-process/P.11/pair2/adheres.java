// eclipse/che MatchNode.java
public void updatePresentation(@NotNull NodePresentation presentation) {
    SpanElement spanElement = Elements.createSpanElement(styles.styles().presentableTextContainer());

    SpanElement lineNumberElement = Elements.createSpanElement();
    lineNumberElement.setInnerHTML(String.valueOf(match.getMatchLineNumber() + 1) + ":&nbsp;&nbsp;&nbsp;");
    spanElement.appendChild(lineNumberElement);

    SpanElement textElement = Elements.createSpanElement();
    Region matchInLine = match.getMatchInLine();
    String matchedLine = match.getMatchedLine();
    if (matchedLine != null && matchInLine != null) {
        String startLine = matchedLine.substring(0, matchInLine.getOffset());
        textElement.appendChild(Elements.createTextNode(startLine));
        SpanElement highlightElement = Elements.createSpanElement(resources.css().searchMatch());
        highlightElement
                .setInnerText(matchedLine.substring(matchInLine.getOffset(), matchInLine.getOffset() + matchInLine.getLength()));
        textElement.appendChild(highlightElement);

        textElement.appendChild(Elements.createTextNode(matchedLine.substring(matchInLine.getOffset() + matchInLine.getLength())));
    } else {
        textElement.appendChild(Elements.createTextNode("Can't find sources"));
    }
    spanElement.appendChild(textElement);

    presentation.setPresentableIcon(resources.searchMatch());
    presentation.setUserElement((Element)spanElement);
}

