// libgdx/ligdx - TexturePacker.java
private void writeImages (File outputDir, String scaledPackFileName, Array<Page> pages) {
    //...
    for (int r = 0, rn = page.outputRects.size; r < rn; r++) {
        Rect rect = page.outputRects.get(r);
        BufferedImage image = rect.getImage(imageProcessor);
        int iw = image.getWidth();
        int ih = image.getHeight();
        int rectX = page.x + rect.x, rectY = page.y + page.height - rect.y - rect.height;
        if (settings.duplicatePadding) {
            int amountX = settings.paddingX / 2;
            int amountY = settings.paddingY / 2;
            if (rect.rotated) {
                plotRotated(amountX, amountY, canvas, rectX, image, iw, ih);
            } else {
                plotNormal(amountX, amountY, canvas, rectX, image, iw, ih, rect);
            }
        }
        copy(image, 0, 0, iw, ih, canvas, rectX, rectY, rect.rotated);
        if (settings.debug) {
            g.setColor(Color.magenta);
            g.drawRect(rectX, rectY, rect.width - settings.paddingX - 1, rect.height - settings.paddingY - 1);
        }

        if (progress.update(r + 1, rn)) return;
    }
    // ...
}

protected void plotRotated(int amountX, int amountY, Canvas canvas, int rectX, BufferedImage image, int iw, int ih) {
    // Copy corner pixels to fill corners of the padding.
    for (int i = 1; i <= amountX; i++) {
        for (int j = 1; j <= amountY; j++) {
            plot(canvas, rectX - j, rectY + iw - 1 + i, image.getRGB(0, 0));
            plot(canvas, rectX + ih - 1 + j, rectY + iw - 1 + i, image.getRGB(0, ih - 1));
            plot(canvas, rectX - j, rectY - i, image.getRGB(iw - 1, 0));
            plot(canvas, rectX + ih - 1 + j, rectY - i, image.getRGB(iw - 1, ih - 1));
        }
    }
    // Copy edge pixels into padding.
    for (int i = 1; i <= amountY; i++) {
        for (int j = 0; j < iw; j++) {
            plot(canvas, rectX - i, rectY + iw - 1 - j, image.getRGB(j, 0));
            plot(canvas, rectX + ih - 1 + i, rectY + iw - 1 - j, image.getRGB(j, ih - 1));
        }
    }
    for (int i = 1; i <= amountX; i++) {
        for (int j = 0; j < ih; j++) {
            plot(canvas, rectX + j, rectY - i, image.getRGB(iw - 1, j));
            plot(canvas, rectX + j, rectY + iw - 1 + i, image.getRGB(0, j));
        }
    }

}

protected void plotNormal(int amountX, int amountY, Canvas canvas, int rectX, BufferedImage image, int iw, int ih, Rect rect) {
    // Copy corner pixels to fill corners of the padding.
    for (int i = 1; i <= amountX; i++) {
        for (int j = 1; j <= amountY; j++) {
            plot(canvas, rectX - i, rectY - j, image.getRGB(0, 0));
            plot(canvas, rectX - i, rectY + ih - 1 + j, image.getRGB(0, ih - 1));
            plot(canvas, rectX + iw - 1 + i, rectY - j, image.getRGB(iw - 1, 0));
            plot(canvas, rectX + iw - 1 + i, rectY + ih - 1 + j, image.getRGB(iw - 1, ih - 1));
        }
    }
    // Copy edge pixels into padding.
    for (int i = 1; i <= amountY; i++) {
        copy(image, 0, 0, iw, 1, canvas, rectX, rectY - i, rect.rotated);
        copy(image, 0, ih - 1, iw, 1, canvas, rectX, rectY + ih - 1 + i, rect.rotated);
    }
    for (int i = 1; i <= amountX; i++) {
        copy(image, 0, 0, 1, ih, canvas, rectX - i, rectY, rect.rotated);
        copy(image, iw - 1, 0, 1, ih, canvas, rectX + iw - 1 + i, rectY, rect.rotated);
    }

}
