// eclipse/che MatchNode.java
public void updatePresentation(@NotNull NodePresentation p) {
    SpanElement se = Elements.createSpanElement(st.styles().presentableTextContainer());

    SpanElement lne = Elements.createSpanElement();
    lne.setInnerHTML(String.valueOf(m.getMatchLineNumber() + 1) + ":&nbsp;&nbsp;&nbsp;");
    se.appendChild(lne);

    SpanElement te = Elements.createSpanElement();
    Region mil = m.getMatchInLine();
    String mtl = m.getMatchedLine();
    if (mtl != null && mil != null) {
        String sl = mtl.substring(0, mil.getOffset());
        te.appendChild(Elements.createTextNode(sl));
        SpanElement hle = Elements.createSpanElement(r.css().searchMatch());
        hle.setInnerText(mtl.substring(mil.getOffset(), mil.getOffset() + mil.getLength()));
        te.appendChild(hle);

        te.appendChild(Elements.createTextNode(mtl.substring(mil.getOffset() + mil.getLength())));
    } else {
        te.appendChild(Elements.createTextNode("Can't find sources"));
    }
    se.appendChild(te);

    p.setPresentableIcon(r.searchMatch());
    p.setUserElement((Element)se);
}

