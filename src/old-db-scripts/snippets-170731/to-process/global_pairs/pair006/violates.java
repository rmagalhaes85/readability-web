// spring-project/spring-boot - ResourceBanner.java
	@Override
	public void printBanner(Environment environment, Class<?> sourceClass,
			PrintStream out) 
        {
		try 
                {
			String banner = StreamUtils.copyToString(this.resource.getInputStream(),
					environment.getProperty("banner.charset", Charset.class,
							Charset.forName("UTF-8")));

			for (PropertyResolver resolver : getPropertyResolvers(environment,
					sourceClass)) 
                        {
				banner = resolver.resolvePlaceholders(banner);
			}
			out.println(banner);
		}
		catch (Exception ex) 
                {
			logger.warn("Banner not printable: " + this.resource + " (" + ex.getClass()
					+ ": '" + ex.getMessage() + "')", ex);
		}
	}

	protected List<PropertyResolver> getPropertyResolvers(Environment environment,
			Class<?> sourceClass) 
        {
		List<PropertyResolver> resolvers = new ArrayList<>();
		resolvers.add(environment);
		resolvers.add(getVersionResolver(sourceClass));
		resolvers.add(getAnsiResolver());
		resolvers.add(getTitleResolver(sourceClass));
		return resolvers;
	}
