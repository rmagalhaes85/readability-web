// eclipse/che - JavaFormatter.java
private static final class VariableTracker {
    private static final String CATEGORY = "__template_variables"; //$NON-NLS-1$
    private       Document            fDocument;
    private final TemplateBuffer      fBuffer;
    private       List<TypedPosition> fPositions;
