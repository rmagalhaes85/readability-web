#!/bin/bash

generate_global_pair_dir_name() {
	local index=$1
	index=$(echo "00"$index)
	index=$(echo $index | rev)
	index=${index:0:3}
	index=$(echo $index | rev)
	return $index
}

rm -rf ./global_pairs
mkdir ./global_pairs
global_pair_index=1
for practice_dir in $(ls -d P.??);
do
	practice_name=$practice_dir
	for intra_practice_pair_index in {1..3};
	do
		#echo "analyzing pair $intra_practice_pair_index of practice $practice_dir"	
		intra_practice_pair_descriptor_dir="./$practice_dir/pair$intra_practice_pair_index"
		#echo "$intra_practice_pair_descriptor_dir"
		#echo "generating global pair $global_pair_index"
		global_pair_dir=$(echo "00"$global_pair_index)
		global_pair_dir=$(echo $global_pair_dir | rev)
		global_pair_dir=${global_pair_dir:0:3}
		global_pair_dir=$(echo $global_pair_dir | rev)
		global_pair_dir="pair"$global_pair_dir
		global_pair_path="./global_pairs/"$global_pair_dir
		mkdir -p $global_pair_path
		adheres_snippet_file=$intra_practice_pair_descriptor_dir"/adheres.java"
		violates_snippet_file=$intra_practice_pair_descriptor_dir"/violates.java"
		cp $adheres_snippet_file $global_pair_path"/adheres.java"
		cp $violates_snippet_file $global_pair_path"/violates.java"
		echo "### Tested-Practice: "$practice_dir > $global_pair_path"/pair.txt"
		((global_pair_index++))
	done;
done;
