// elastic/elasticsearch - TransportSingleShardAction.java
class AsyncSingleAction {

    private final ActionListener<org.elasticsearch.action.support.single.shard.Response> listener;
    private final org.elasticsearch.cluster.routing.ShardsIterator shardIt;
    private final org.elasticsearch.action.support.single.shard.InternalRequest internalRequest;
    private final org.elasticsearch.cluster.node.DiscoveryNodes nodes;
    private volatile Exception lastFailure;
