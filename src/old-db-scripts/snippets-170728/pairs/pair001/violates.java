//google/agera - CompiledRepository.java
  private void maybeCancelFlow(@RepositoryConfig final int config, final boolean scheduleRestart) {
    synchronized (this) {
      if (runState == RUNNING || runState == PAUSED_AT_GO_TO) {
        restartNeeded = scheduleRestart;

        // If config forbids cancellation, exit now after scheduling the restart, to skip the
        // cancellation request.
        if ((config & CANCEL_FLOW) == 0) { return; }

        runState = CANCEL_REQUESTED;

        if ((config & SEND_INTERRUPT) == SEND_INTERRUPT 
          && currentThread != null) {currentThread.interrupt(); }
      }

      // Resetting to the initial value should be done even if the flow is not running.
      if (!scheduleRestart 
         && (config & RESET_TO_INITIAL_VALUE) == RESET_TO_INITIAL_VALUE) 
      { setNewValueLocked(initialValue); }
    }
  }

