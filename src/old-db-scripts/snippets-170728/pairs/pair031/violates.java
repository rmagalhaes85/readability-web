// elastic/elasticsearch - JvmStats.java
MemoryUsage mu = memoryMXBean.getHeapMemoryUsage();
long hu = mu.getUsed() < 0 ? 0 : mu.getUsed();
long hc = mu.getCommitted() < 0 ? 0 : mu.getCommitted();
long heapMax = mu.getMax() < 0 ? 0 : mu.getMax();
mu = memoryMXBean.getNonHeapMemoryUsage();
long nhu = mu.getUsed() < 0 ? 0 : mu.getUsed();
long nhc = mu.getCommitted() < 0 ? 0 : mu.getCommitted();
List<MemoryPoolMXBean> mpmbs = ManagementFactory.getMemoryPoolMXBeans();
List<MemoryPool> pools = new ArrayList<>();
for (MemoryPoolMXBean mpmb : mpmbs) {
    try {
        MemoryUsage u = mpmb.getUsage();
        MemoryUsage pu = mpmb.getPeakUsage();
        String n = GcNames.getByMemoryPoolName(mpmb.getName(), null);
        if (n == null) { // if we can't resolve it, its not interesting.... (Per Gen, Code Cache)
            continue;
        }
        pools.add(new MemoryPool(n,
                u.getUsed() < 0 ? 0 : u.getUsed(),
                u.getMax() < 0 ? 0 : u.getMax(),
                pu.getUsed() < 0 ? 0 : pu.getUsed(),
                pu.getMax() < 0 ? 0 : pu.getMax()
        ));
    } catch (Exception ex) {
        /* ignore some JVMs might barf here with:
         * java.lang.InternalError: Memory Pool not found
         * we just omit the pool in that case!*/
    }
}
