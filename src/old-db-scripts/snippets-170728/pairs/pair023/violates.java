// eclipse/che - JavaFormatter.java
private static final class VariableTracker {
    private static final String CATEGORY = "__template_variables"; //$NON-NLS-1$
    private              org.eclipse.jface.text.Document            fDocument;
    private final        org.eclipse.jface.text.templates.TemplateBuffer      fBuffer;
    private              List<org.eclipse.jface.text.TypedPosition> fPositions;
