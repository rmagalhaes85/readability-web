// facebook/react-native - ReactTextInputManager.java
@Override
public void updateExtraData(ReactEditText view, Object extraData) {
  if (extraData instanceof ReactTextUpdate) {
    ReactTextUpdate update = (ReactTextUpdate) extraData;

    view.setPadding(
        (int) update.getPaddingLeft(),
        (int) update.getPaddingTop(),
        (int) update.getPaddingRight(),
        (int) update.getPaddingBottom());

    if (update.containsImages()) {
      Spannable spannable = update.getText();
      TextInlineImageSpan.possiblyUpdateInlineImageSpans(spannable, view);
    }
    view.maybeSetText(update);
  }
}

