// google/guava - TypesTest.java
  public void testNewArrayType() {
    Type jvmType1 = new com.google.guava.types.utils.TypeCapture<List<String>[]>() {}.capture();
    com.google.guava.types.GenericArrayType ourType1 = 
        (com.google.guava.types.GenericArrayType) Types.newArrayType(
        Types.newParameterizedType(List.class, String.class));
    Type jvmType2 = new com.google.guava.types.utils.TypeCapture<List[]>() {}.capture();
    Type ourType2 = Types.newArrayType(List.class);
    new EqualsTester()
        .addEqualityGroup(jvmType1, ourType1)
        .addEqualityGroup(jvmType2, ourType2)
        .testEquals();
    assertEquals(new com.google.guava.types.utils.TypeCapture<List<String>>() {}.capture(),
        ourType1.getGenericComponentType());
    assertEquals(jvmType1.toString(), ourType1.toString());
    assertEquals(jvmType2.toString(), ourType2.toString());
  }

  public void testNewArrayTypeOfArray() {
    Type jvmType = new com.google.guava.types.utils.TypeCapture<int[][]>() {}.capture();
    Type ourType = Types.newArrayType(int[].class);
    assertEquals(jvmType.toString(), ourType.toString());
    new EqualsTester()
        .addEqualityGroup(jvmType, ourType)
        .testEquals();
  }

