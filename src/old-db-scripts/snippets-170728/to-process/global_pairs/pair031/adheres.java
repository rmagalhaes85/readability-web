// elastic/elasticsearch - JvmStats.java
MemoryUsage memUsage = memoryMXBean.getHeapMemoryUsage();
long heapUsed = memUsage.getUsed() < 0 ? 0 : memUsage.getUsed();
long heapCommitted = memUsage.getCommitted() < 0 ? 0 : memUsage.getCommitted();
long heapMax = memUsage.getMax() < 0 ? 0 : memUsage.getMax();
memUsage = memoryMXBean.getNonHeapMemoryUsage();
long nonHeapUsed = memUsage.getUsed() < 0 ? 0 : memUsage.getUsed();
long nonHeapCommitted = memUsage.getCommitted() < 0 ? 0 : memUsage.getCommitted();
List<MemoryPoolMXBean> memoryPoolMXBeans = ManagementFactory.getMemoryPoolMXBeans();
List<MemoryPool> pools = new ArrayList<>();
for (MemoryPoolMXBean memoryPoolMXBean : memoryPoolMXBeans) {
    try {
        MemoryUsage usage = memoryPoolMXBean.getUsage();
        MemoryUsage peakUsage = memoryPoolMXBean.getPeakUsage();
        String name = GcNames.getByMemoryPoolName(memoryPoolMXBean.getName(), null);
        if (name == null) { // if we can't resolve it, its not interesting.... (Per Gen, Code Cache)
            continue;
        }
        pools.add(new MemoryPool(name,
                usage.getUsed() < 0 ? 0 : usage.getUsed(),
                usage.getMax() < 0 ? 0 : usage.getMax(),
                peakUsage.getUsed() < 0 ? 0 : peakUsage.getUsed(),
                peakUsage.getMax() < 0 ? 0 : peakUsage.getMax()
        ));
    } catch (Exception ex) {
        /* ignore some JVMs might barf here with:
         * java.lang.InternalError: Memory Pool not found
         * we just omit the pool in that case!*/
    }
}
