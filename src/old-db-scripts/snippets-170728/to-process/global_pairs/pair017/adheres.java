// elastic/elasticsearch - SharedClusterSnapshotRestoreIT.java
public void testCloseOrDeleteIndexDuringSnapshot() throws Exception {
    // ...
    try {
        if (allowPartial && randomBoolean()) {
            logger.info("--> delete index while partial snapshot is running");
            client.admin().indices().prepareDelete("test-idx-1").get();
        } else if (allowPartial) {
            logger.info("--> close index while partial snapshot is running");
            client.admin().indices().prepareClose("test-idx-1").get();
        } else if (randomBoolean()) {
            // non-partial snapshots do not allow close / delete operations on indices where snapshot has not been completed
            try {
                logger.info("--> delete index while non-partial snapshot is running");
                client.admin().indices().prepareDelete("test-idx-1").get();
                fail("Expected deleting index to fail during snapshot");
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), containsString("Cannot delete indices that are being snapshotted: [[test-idx-1/"));
            }
        } else {
            try {
                logger.info("--> close index while non-partial snapshot is running");
                client.admin().indices().prepareClose("test-idx-1").get();
                fail("Expected closing index to fail during snapshot");
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), containsString("Cannot close indices that are being snapshotted: [[test-idx-1/"));
            }
        }
    } finally {
        if (initBlocking) {
            logger.info("--> unblock running master node");
            unblockNode("test-repo", internalCluster().getMasterName());
        } else {
            logger.info("--> unblock all data nodes");
            unblockAllDataNodes("test-repo");
        }
    }
    // ...
}
