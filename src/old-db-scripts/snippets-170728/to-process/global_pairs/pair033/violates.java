// libgdx/libgdx AndroidOnscreenKeyboard.java
h.post(new Runnable() {
    @Override
    public void run () {
        d = createDialog();
        d.show();

        h.post(new Runnable() {
            @Override
            public void run () {
                d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                InputMethodManager i = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (i != null) i.showSoftInput(tv, InputMethodManager.SHOW_FORCED);
            }
        });

        final View c = d.getWindow().findViewById(Window.ID_ANDROID_CONTENT);
        c.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            int[] sl = new int[2];
            private int kh;
            private boolean ks;

            @Override
            public boolean onPreDraw () {
                c.getLocationOnScreen(sl);
                kh = Math.abs(sl[1]);
                if (kh > 0) ks = true;
                if (kh == 0 && ks) {
                    d.dismiss();
                    d = null;
                }
                return true;
            }
        });
    }
});
