//eclipse/che - DtoFactory.java
  public <T> T clone(T origin) {
    final Class<?> implClass = origin.getClass();
    DtoProvider provider = dtoImpl2Providers.get(implClass);
    if (provider == null) {
      Class<?> dtoInterface = null;
      Class<?>[] interfaces = implClass.getInterfaces();
      if (interfaces.length == 0) {
        return null;
      }

      for (Class<?> i : interfaces) {
        if (i.isAnnotationPresent(DTO.class)) {
          if (dtoInterface != null) {
            throw new IllegalArgumentException(
              "Unable determine DTO interface. Type " + 
              implClass.getName() + " implements or extends more " +
              "than one interface annotated with @DTO annotation.");
          }
          dtoInterface = i;
        }
      }

      if (dtoInterface != null) {
        provider = getDtoProvider(dtoInterface);
      }
    }

    if (provider == null) {
      throw new IllegalArgumentException("Unknown DTO type " + implClass);
    }

    return (T)provider.clone(origin);
  }
