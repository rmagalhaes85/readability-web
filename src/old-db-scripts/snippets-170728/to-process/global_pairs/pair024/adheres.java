// elastic/elasticsearch - TransportSingleShardAction.java
class AsyncSingleAction {

    private final ActionListener<Response> listener;
    private final ShardsIterator shardIt;
    private final InternalRequest internalRequest;
    private final DiscoveryNodes nodes;
    private volatile Exception lastFailure;
