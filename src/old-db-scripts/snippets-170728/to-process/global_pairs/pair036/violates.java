// libgdx/libgdx - AndroidGraphics.java
	private void logConfig (EGLConfig config) {
		EGL10 eglRight = (EGL10)EGLContext.getRightContext().getEGL();
		EGL10 eglLeft = (EGL10)EGLContext.getLeftContext().getEGL();
		EGLDisplay displayLeft = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
		EGLDisplay displayRight = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);

		int rr = getAttrib(eglRight, displayRight, config, EGL10.EGL_RED_SIZE, 0);
		int gr = getAttrib(eglRight, displayRight, config, EGL10.EGL_GREEN_SIZE, 0);
		int br = getAttrib(eglRight, displayRight, config, EGL10.EGL_BLUE_SIZE, 0);
		int ar = getAttrib(eglRight, displayRight, config, EGL10.EGL_ALPHA_SIZE, 0);
		int dr = getAttrib(eglRight, displayRight, config, EGL10.EGL_DEPTH_SIZE, 0);
		int sr = getAttrib(eglRight, displayRight, config, EGL10.EGL_STENCIL_SIZE, 0);

		int rl = getAttrib(eglLeft, displayLeft, config, EGL10.EGL_RED_SIZE, 0);
		int gl = getAttrib(eglLeft, displayLeft, config, EGL10.EGL_GREEN_SIZE, 0);
		int bl = getAttrib(eglLeft, displayLeft, config, EGL10.EGL_BLUE_SIZE, 0);
		int al = getAttrib(eglLeft, displayLeft, config, EGL10.EGL_ALPHA_SIZE, 0);
		int dl = getAttrib(eglLeft, displayLeft, config, EGL10.EGL_DEPTH_SIZE, 0);
		int sl = getAttrib(eglLeft, displayLeft, config, EGL10.EGL_STENCIL_SIZE, 0);

		int samplesRight = Math.max(getAttrib(eglRight, displayRight, config, EGL10.EGL_SAMPLES, 0),
			getAttrib(eglRight, displayRight, config, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0));
		boolean coverageSampleRight = getAttrib(eglRight, displayRight, config, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0) != 0;

		int samplesLeft = Math.max(getAttrib(eglLeft, displayLeft, config, EGL10.EGL_SAMPLES, 0),
			getAttrib(eglLeft, displayLeft, config, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0));
		boolean coverageSampleleft = getAttrib(eglLeft, displayLeft, config, GdxEglConfigChooser.EGL_COVERAGE_SAMPLES_NV, 0) != 0;

		Gdx.app.log(LOG_TAG, "framebuffer: (" + rr + ", " + gr + ", " + br + ", " + ar + ")");
		Gdx.app.log(LOG_TAG, "depthbuffer: (" + dr + ")");
		Gdx.app.log(LOG_TAG, "stencilbuffer: (" + sr + ")");

		Gdx.app.log(LOG_TAG, "framebuffer: (" + rl + ", " + gl + ", " + bl + ", " + al + ")");
		Gdx.app.log(LOG_TAG, "depthbuffer: (" + dl + ")");
		Gdx.app.log(LOG_TAG, "stencilbuffer: (" + sl + ")");
	}

