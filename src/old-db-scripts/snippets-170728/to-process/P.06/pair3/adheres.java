// elastic/elasticsearch - QueryStringQueryBuilder.java
protected void buildFieldsAndWeights() {
    initializeTokenCompilation();
    while ((token = parser.nextToken()) != XContentParser.Token.END_OBJECT) {
        processFieldNameToken();
        processStartArrayToken();
    }
}

protected void initializeTokenCompilation() {
    currentFieldName = null;
}

protected void processFieldNameToken() {
    if (token == XContentParser.Token.FIELD_NAME) {
        currentFieldName = parser.currentName();
    }
}

protected void processInitToken() {
    if (token == XContentParser.Token.START_ARRAY) {
        if (FIELDS_FIELD.match(currentFieldName)) {
            processQueryTokens();
        } else {
            throw new ParsingException(parser.getTokenLocation(), "[" + QueryStringQueryBuilder.NAME +
                    "] query does not support [" + currentFieldName + "]");
        }
    }
}

protected void processQueryTokens() {
    while ((token = parser.nextToken()) != XContentParser.Token.END_ARRAY) {
        String fField = null;
        float fBoost = AbstractQueryBuilder.DEFAULT_BOOST;
        char[] text = parser.textCharacters();
        int end = parser.textOffset() + parser.textLength();
        for (int i = parser.textOffset(); i < end; i++) {
            if (text[i] == '^') {
                int relativeLocation = i - parser.textOffset();
                fField = new String(text, parser.textOffset(), relativeLocation);
                fBoost = Float.parseFloat(new String(text, i + 1, parser.textLength() - relativeLocation - 1));
                break;
            }
        }
        if (fField == null) {
            fField = parser.text();
        }
        fieldsAndWeights.put(fField, fBoost);
    }

}
