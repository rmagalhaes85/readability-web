create or replace function loop_test()
returns table(experiment_session_id int) as $$
begin
	for experiment_session_id in
		select e.experiment_session_id
		from experiment_sessions e
		where e.start_time > '20170801'
	loop
		return next;
	end loop;

end;
$$ language plpgsql;
