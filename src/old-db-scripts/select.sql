--select
--  s.snippet_pair_id, s.hash 
--from experiment_sessions e 
--  inner join working_sets_snippet_pairs wssp on (e.working_set_id = wssp.working_set_id)
--  inner join snippet_pairs s on (wssp.snippet_pair_id = s.snippet_pair_id)
--where 
--  e.experiment_session_id = 7 and
--  wssp.snippet_pair_id not in (
--    select v.snippet_pair_id from votes v where v.experiment_session_id = 7)
--order by wssp.presentation_order
--limit 1

--select
--  sp.hash
--from working_sets ws
--  inner join working_sets_snippet_pairs wssp on (ws.working_set_id = wssp.working_set_id)
--  inner join experiment_sessions e on (e.working_set_id = ws.working_set_id)
--  inner join snippet_pairs sp on (sp.snippet_pair_id = wssp.snippet_pair_id)
--  inner join votes v on (v.snippet_pair_id = sp.snippet_pair_id and v.snippet_order <> sp.most_readable)
--where e.experiment_session_id = 7
--order by wssp.presentation_order
--limit 1;

select  
--  sp.snippet_pair_id  
  sp.*, v.*
from working_sets ws  
  inner join working_sets_snippet_pairs wssp on (ws.working_set_id = wssp.working_set_id)  
  inner join experiment_sessions e on (e.working_set_id = ws.working_set_id)  
  inner join snippet_pairs sp on (sp.snippet_pair_id = wssp.snippet_pair_id)  
  inner join votes v on (v.snippet_pair_id = sp.snippet_pair_id 
	and v.experiment_session_id = e.experiment_session_id 
	and v.snippet_order <> sp.most_readable)  
where e.experiment_session_id = 9 
order by wssp.presentation_order  
--limit 1

