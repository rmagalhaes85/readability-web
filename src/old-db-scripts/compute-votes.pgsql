create or replace function compute_votes()
returns table(snippet_pair_id int, divergents bigint, total bigint) as $$
begin
	if exists(select relname from pg_class where relname = 'votes_by_snippet_pair') then
		drop table votes_by_snippet_pair;
	end if;

	create temp table votes_by_snippet_pair as
	select sp.snippet_pair_id, sp.most_readable, v.snippet_order, (sp.most_readable <> v.snippet_order) as diverges
	from snippet_pairs sp inner join votes v on sp.snippet_pair_id = v.snippet_pair_id
			      inner join experiment_sessions e on v.experiment_session_id = e.experiment_session_id
	where v.last_vote = true and e.end_time is not null
	order by sp.snippet_pair_id;

--	select snippet_pair_id, count(*)
--	from divergence_by_snippet_pair
--	where diverges = false
--	group by snippet_pair_id;

	return query
	select sp.snippet_pair_id,
		(select count(div.*) from votes_by_snippet_pair div where div.diverges = true and div.snippet_pair_id = sp.snippet_pair_id) as divergents,
		(select count(tot.*) from votes_by_snippet_pair tot where tot.snippet_pair_id = sp.snippet_pair_id) as total
	from snippet_pairs sp;
end;
$$ language plpgsql;
