create or replace function loop_test2(_snippet_pair_id int)
returns table(experiment_session_id integer, total bigint, divergents bigint) as $$
	select e.experiment_session_id,
		(select 
			count(tot.*) 
			from votes tot 
			where tot.experiment_session_id = e.experiment_session_id
				and tot.snippet_pair_id = $1
				and tot.last_vote=true) as total,
		(select
			count(div.*)
			from votes div
				inner join snippet_pairs sp 
				on div.snippet_pair_id = sp.snippet_pair_id
					and div.snippet_order <> sp.most_readable
			where div.experiment_session_id = e.experiment_session_id
				and div.snippet_pair_id = $1
				and div.last_vote=true) as divergents
	from experiment_sessions e
	where e.start_time > '20170801'
		and exists(
			select v.vote_id 
			from votes v 
			where v.experiment_session_id = e.experiment_session_id);
$$ language sql;
