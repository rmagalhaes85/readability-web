#!/bin/bash

init_file() {
	echo > insert-snippets.sql
}

emit_command() {
	echo -e $1 >> insert-snippets.sql
}

init_file
emit_command 'delete from snippets;'

for index in {1..64};
do
	file_content=$(cat ./template.html | sed -e "s/{{template_text}}/Template $index/g" -e "s/\r\n/aaaaaaaaaaaaaaaaa/g")
	insert_command="insert into snippets(content) values('$file_content');"
	#[ $index -eq 1 ] && echo "$file_content"
	[ $index -eq 1 ] && echo "$insert_command"
	emit_command "$insert_command"
done;

emit_command
emit_command "delete from snippet_pairs;"

for index in {1..32};
do
	pair_hash=`echo "$index" | md5sum | cut -c -32`
	emit_command "insert into snippet_pairs(hash, snippet_a, snippet_b, most_readable) values ('$pair_hash', $(( index * 2 - 1 )), $(( index * 2 )), 'A');"
done;

