drop function if exists sp_columns_list();
create function sp_columns_list()
returns table(snippet_pair_id int, name text) as $$
	select sp.snippet_pair_id, concat(lower(replace(sp.practice, '.', '')), '_',
		left(concat('0', sp.snippet_pair_id), 3)) as name
	from snippet_pairs sp
	where sp.active=true
	order by sp.practice, sp.snippet_pair_id;
$$ language sql;
