(function(angular) {
  var app = angular.module('readability');

  app.config(['$provide', '$httpProvider', function($provide, $httpProvider) {
    $provide.factory('sessionCheckInterceptor', 
    ['$log', '$q', '$window', function($log, $q, $window) {

      function responseError(error) {

        if (!angular.isDefined(error.status)) {
          return $q.reject(error);
        }

        var status = error.status;

        if (status === 403) {
          $log.warn('Server returned HTTP FORBIDDEN... Going back to the start page');
          $window.location.href = '/';
          return $q.reject(error);
        } else if (status === 404) {
          $log.warn('HTTP NOT FOUND (usual situation, proceeding to application code...');
          return $q.resolve(error);
        } else if (status >= 500 && status <= 599) {
          $log.error('Server error: ' + error + '. Passing the response down to the application code...');
          return $q.resolve(error);
        } else {
          return $q.reject(error);
        }
      }

      return { 'responseError': responseError };

    }]);

    $httpProvider.interceptors.push('sessionCheckInterceptor');

  }]);
})(angular);
