(function(angular) {
  var app = angular.module('readability');

  app.controller('VoteCommentsController', 
    ['$rootScope', '$scope', '$log', '$window', '$sce', '$timeout', 'VoteCommentsService', 'SessionEventService',
      function ($rootScope, $scope, $log, $window, $sce, $timeout, VoteCommentsService, SessionEventService) {

        initialize();
        loadData();

        function nextScreen() {
          SessionEventService.save('VoteCommentsController', 'nextScreen');
          $window.location.href = '/pages/spa#!/profile';
        }

        function initialize() {
          SessionEventService.save('VoteCommentsController', 'initialize');
          $scope.snippetPair = {};
          $scope.comments = '';
          $scope.selectedSnippet = '';
          $scope.trustedSnippet = function(content) {
            return $sce.trustAsHtml(content);
          };

          $scope.submitForm = function(isValid) {
            if (isValid) {
              var data = {};
              data.snippetPairHash = $scope.snippetPair.hash;
              data.comments = $scope.comments;

              VoteCommentsService.save(data).then(
                nextScreen,
                function(error) { 
                  $log.error('Error saving vote comments (going to the next screen anyway...):', error);
                  $timeout(undefined, 3000).then(nextScreen);
                }
              );
            }
          };

        }

        function loadData() {
          VoteCommentsService.get().then(
            function(data) { 
              $log.debug('loadData#resolve', JSON.stringify(data));
              $scope.snippetPair = data.snippetPair;
              $scope.selectedSnippet = data.vote.snippetOrder;
            },
            function(error) { $log.error('loadData#reject', JSON.stringify(error)); }
          );
        }

//        function setSnippetPairByHash(hash) {
//          if (typeof hash === 'undefined') {
//            throw new Error('Undefined hash');
//          }
//
//          $log.debug('Looking for the snippet pair with hash = \'' + hash + '\'...');
//
//          var snippetPairIndex = -1;
//          var selectedSnippet = '';
//          for (var i = 0; i < $rootScope.workingSet.snippetPairs.length; i++) {
//            var snippetPair = $rootScope.workingSet.snippetPairs[i];
//            if (snippetPair.hash === hash) {
//              $log.debug('Found');
//              snippetPairIndex = i;
//              selectedSnippet = $rootScope.votes[i].snippetOrder;
//              break;
//            }
//          }
//          if (snippetPairIndex === -1) {
//            throw new Error('There is no snippet pair associated to the hash ' + hash);
//          }
//
//          $scope.pairIndex = snippetPairIndex;
//          $scope.selectedSnippet = selectedSnippet;
//        }

//        VoteService.checkDivergentVote().then(
//          function(hashObj) {
//            if (angular.isDefined(hashObj) && angular.isDefined(hashObj.hash)) {
//              setSnippetPairByHash(hashObj.hash);
//            } else {
//              // the system shouldn't have directed the user here if there is no hash.
//              // This may have happened due direct access to this page's url.
//              $log.error('No hash');
//              $window.location.href = '/';
//            }
//          },
//          function(error) {
//            $log.error('VoteCommentsController: ' + error);
//          }
//        );
      }
    ]
  );
})(angular);

