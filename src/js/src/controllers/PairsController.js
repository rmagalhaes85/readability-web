(function(angular) {
  var app = angular.module('readability');
  app.controller('PairsController',
    ['$scope', '$rootScope', '$log', '$http', '$sce', '$window', '$q',
     '$routeParams', 'totalPairs', 'WorkingSetService', 'VoteService',
      'ModalService', 'SessionEventService', 'VoteCommentsService',
    function($scope, $rootScope, $log, $http, $sce, $window, $q,
        $routeParams, totalPairs, WorkingSetService, VoteService,
        ModalService, SessionEventService, VoteCommentsService) {
      
      VOTING_END_WANTS_RETURN_TO_NOT_VOTED = "VOTING_END_WANTS_RETURN_TO_NOT_VOTED";
      VOTING_END_WANTS_FINALIZE = "VOTING_END_WANTS_FINALIZE";
      VOTING_END_WANTS_STAY = "VOTING_END_WANTS_STAY";

      $scope.totalPairs = totalPairs;
      $scope.pairIndex = parseInt($routeParams.pairIndex);

      $scope.trustedSnippet = function(content) {
        return $sce.trustAsHtml(content);
      };

      $scope.nextScreen = function() {
        $log.debug('$scope.nextScreen called');
        inquireVoteComments().then(function() {
          $scope.postVoteCategoricalVariables(); 

          var next = $scope.pairIndex + 1;
          var passedLast = (next > $scope.totalPairs);
          if (!passedLast) {
            showSnippetPair(next);
          } else {
            inquireUserVotingEnd().then(
              function(processResult) {
                $log.debug('processResult = ' + processResult);
                if (processResult === VOTING_END_WANTS_RETURN_TO_NOT_VOTED) {
                  returnToFirstUnvoted();
                } else if (processResult === VOTING_END_WANTS_FINALIZE) {
                  finalizeVotation();
                } else if (processResult === VOTING_END_WANTS_STAY) {
                  // do nothing
                } else {
                  throw new Error('Unkown result: ' + processResult);
                }
              },
              function(error) {
                $log.error('Error while processing user action: ' + error);
              });
          }
        }, angular.noop);
      };

      $scope.previousScreen = function() {
        $log.debug('$scope.previousScreen called');
        $scope.postVoteCategoricalVariables();
        var prev = $scope.pairIndex - 1;
        var passedFirst = (prev < 1);
        if (passedFirst) {
          $window.location.href = '/pages/spa#!/instructions';
        } else {
          showSnippetPair(prev);
        }
      };

//      $scope.$watch('selectedSnippet', function() {
//        vote($scope.selectedSnippet);
//      });

      $scope.postVote = function(snippetOrder) {
        $log.info('Posting vote...');
        var activePairIndex = $scope.pairIndex - 1;
        var activePairHash = $scope.workingSet.snippetPairs[activePairIndex].hash;
        var comments = $scope.comments;
        var strength = $scope.strength;
        $scope.selectedSnippet = snippetOrder;
        SessionEventService.saveMetrics('PairsController', 'vote(' + snippetOrder + '); $scope.pairIndex = ' + $scope.pairIndex);
        VoteService.vote(activePairHash, snippetOrder, strength, comments).then(
          function() { 
            $rootScope.votes[activePairIndex] = snippetOrder;
            $rootScope.voteComments[activePairIndex] = comments;
            $rootScope.strengths[activePairIndex] = strength;
            $log.info('Vote posted');
          },
          function(error) { $log.error('An error has ocurred...', error); }
        ); 
      }; 

      $scope.postVoteCategoricalVariables = function() {
        var activePairIndex = $scope.pairIndex - 1;
        var activePairHash = $scope.workingSet.snippetPairs[activePairIndex].hash;
        $rootScope.voteComments[activePairIndex] = $scope.comments;
        $rootScope.strengths[activePairIndex] = $scope.strength;
        $log.debug('postVoteCategoricalVariables; activePairIndex=' + activePairIndex + 
          '; activePairHash=' + activePairHash + 
          '; $rootScope.voteComments=' + angular.toJson($rootScope.voteComments) +
          '; $rootScope.strengths=' + angular.toJson($rootScope.strengths));
        VoteCommentsService.save({
          'snippetPairHash': activePairHash,
          'comments': $scope.comments,
          'strength': $scope.strength
        }).then(angular.noop, angular.noop);
      };

      if (!angular.isDefined($rootScope.workingSet) &&
          $rootScope.workingSet !== 'loading') {
        $rootScope.workingSet = 'loading';
        WorkingSetService.get().then(
          function(workingSet) {
            if (angular.isDefined(workingSet)) {
              $rootScope.workingSet = workingSet;
            }
          });
      }

      if (!angular.isDefined($rootScope.votes)) {
        $rootScope.votes = new Array(totalPairs);
        $rootScope.votes.fill('');
        $rootScope.voteComments = new Array(totalPairs);
        $rootScope.voteComments.fill(''); 
        $rootScope.strengths = new Array(totalPairs);
        $rootScope.strengths.fill('1');
      }

      setSelectedSnippet();

      function inquireVoteComments(comments) {
        if (!!$scope.comments) {
          return $q.resolve();
        } 

        var deferred = $q.defer();
        ModalService.show('modal.vote.emptycomment.title', 'modal.vote.emptycomment.message').then(
          function() { deferred.reject(); },
          function() { deferred.reject(); }
        );
        return deferred.promise;
      }

      // used to remember the selected snippet in case the subject goes back
      // to previously voted snippet pairs. This will automatically highlight
      // the corresponding voting button
      function setSelectedSnippet() {
        var activePairIndex = $scope.pairIndex - 1;

        if (!angular.isDefined($rootScope.votes)) { 
          return;
        }

        if (angular.isDefined($rootScope.votes[activePairIndex])) {
          $scope.selectedSnippet = $rootScope.votes[activePairIndex];
        }

        if (angular.isDefined($rootScope.voteComments[activePairIndex])) {
          $scope.comments = $rootScope.voteComments[activePairIndex];
        }

        if (angular.isDefined($rootScope.strengths[activePairIndex])) {
          $scope.strength = $rootScope.strengths[activePairIndex];
        }
      }

      function showSnippetPair(index) {
        SessionEventService.save('PairsController', 'showSnippetPair', { 'index': index });
        setSelectedSnippet();
        $window.location.href = '/pages/spa#!/pair/' + index;
      }

      function returnToFirstUnvoted() {
        SessionEventService.save('PairsController', 'returnToFirstUnvoted');
        $log.debug('returnToFirstUnvoted()');
        var i = 0;
        for (; i < $rootScope.votes.length; i++) {
          if (typeof $rootScope.votes[i] === 'undefined') {
            $log.debug('returnToFirstUnvoted: vote [' + i + '] is undefined');
            break;
          }
        }
        showSnippetPair(i + 1);
      }

      function finalizeVotation() {
        SessionEventService.save('PairsController', 'finalizeVotation');
        VoteService.checkDivergentVote().then(
          function(data) { 
            if (angular.isDefined(data) && angular.isDefined(data.hash)) {
              $window.location.href = '/pages/spa#!/votecomments';
            } else {
              $window.location.href = '/pages/spa#!/profile';
            }
          },
          function(error) {
            $log.error('finalizeVotation(): ' + error + '. Redirecting to profile page...');
            $window.location.href = '/pages/spa#!/profile';
          }
        );
      }

      function isVoteComplete() {
        var i;
        for (i = 0; i < $rootScope.votes.length; i++) {
          $log.debug('Vote ' + i + ' = ' + $rootScope.votes[i]);
        }
        $log.debug('$scope.totalPairs = ' + $scope.totalPairs);
        var voteComplete = true;
        for (i = 0; i < $rootScope.votes.length && voteComplete; i++) {
          voteComplete &= (typeof $rootScope.votes[i] !== 'undefined');
        }
        $log.debug('voteComplete = ' + voteComplete);
        return voteComplete;
      }

      function inquireUserVotingEnd() {
        var deferred = $q.defer();
        var voteComplete = isVoteComplete();
        if (voteComplete) {
          ModalService.show('modal.votecomplete.end.title', 'modal.votecomplete.end.message',
            'modal.experiment.normalfinal.ok', 'modal.experiment.normalfinal.cancel').then(
            function() { return deferred.resolve(VOTING_END_WANTS_FINALIZE); },
            function() { return deferred.resolve(VOTING_END_WANTS_STAY); }
          );
        } else {
          ModalService.show('modal.voteincomplete.end.title', 'modal.voteincomplete.end.message',
            'modal.experiment.lackingfinal.ok', 'modal.experiment.lackingfinal.cancel').then(
            function() { return deferred.resolve(VOTING_END_WANTS_RETURN_TO_NOT_VOTED); },
            function() { return deferred.resolve(VOTING_END_WANTS_FINALIZE); }
          );
        }
        return deferred.promise;
      }
    }]);
})(angular);

