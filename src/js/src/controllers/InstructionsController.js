(function(angular) {
  var app = angular.module('readability');
  app.controller('InstructionsController',
    ['$scope', '$log', '$window', 'ModalService', 'SessionEventService',
    function($scope, $log, $window, ModalService, SessionEventService) {

      SessionEventService.saveMetrics('InstructionsController');
      SessionEventService.save('InstructionsController', 'init');

      function goNextStep() {
        SessionEventService.save('InstructionsController', 'goNextStep');
        $window.location.href = '/pages/spa#!/pair/1';
      }

      function stayInstructions() {
        SessionEventService.save('InstructionsController', 'stayInstructions');
      }

      $scope.startExperiment = function() {
        ModalService.show('modal.instructions.end.title', 'modal.instructions.end.message',
          'common.yes', 'common.no').then(goNextStep, stayInstructions);
      };
    }]);
})(angular);

