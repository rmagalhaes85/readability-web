(function(angular) {
  var app = angular.module('readability');
  app.controller('CommentsController',
    ['$scope', '$log', '$window', 'CommentsService', 'SessionEventService',
    function($scope, $log, $window, CommentsService, SessionEventService) {
      SessionEventService.save('CommentsController', 'initialize');

      function goNextStep() {
        SessionEventService.save('CommentsController', 'goNextStep');
        $window.location.href = '/pages/thanks';
      }

      $scope.comments = '';

      $scope.submitForm = function() {
        var comments = $scope.comments.trim();
        if (comments === '') {
          goNextStep();
        } else {
          CommentsService.save({'comments': comments})
          .then(goNextStep, goNextStep);
        }
      };
    }]);
})(angular);

