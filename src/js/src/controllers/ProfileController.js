(function(angular) {
  var app = angular.module('readability');
  app.controller('ProfileController',
    ['$scope', '$log', '$window', 'ProfileService', 'ModalService',
    function($scope, $log, $window, ProfileService, ModalService) {

      function clearServerAlerts() {
        $scope.serverAlerts = [];
      }

      function addServerAlert(msg) {
        $scope.serverAlerts.push({ 'type': 'danger', 'msg': msg });
      }

      function processErrorData(data) {
        clearServerAlerts();
        if (angular.isDefined(data.generalMessage)) {
          addServerAlert(data.generalMessage);
        }

        if (angular.isDefined(data.fieldErrors)) {
          angular.forEach(data.errors, function(error) {
            addServerAlert(error);
          });
        }
      }

      function goNextStep() {
        ModalService.show('modal.profile.end.title', 'modal.profile.end.message',
          'modal.profile.end.ok', 'modal.profile.end.cancel').then(
          function() {
            $log.log('Loading the experiment...');
            $window.location.href = '/pages/spa#!/comments';
          });
      }

      function initialize() {
        var profile = {};
        profile.name = '';
        profile.email = '';
        profile.programmingExperienceTime = null;
        profile.flossExperienceTime = null;
        profile.age = null;
        profile.yearsInSchool = null;
        profile.gender = null;
        profile.programmingLanguages = null;
        profile.naturalLanguages = null;
        profile.operatingSystems = null;
        profile.englishConfortLevel = null;
        profile.perceivedReadabilityLevel = null;
        profile.learningProfile = null;
        $scope.profile = profile;

        $scope.submitted = false;

        $scope.serverAlerts = [];

        $scope.submitForm = function(isValid) {
          $scope.submitted = true;

          $log.info('submitting profile...');
          if (isValid) {
            ProfileService.save($scope.profile)
            .then(
              goNextStep, 
              processErrorData);
          } else {
            $log.log('invalid');
          }
        };

      }

      initialize();
    }]);
})(angular);

