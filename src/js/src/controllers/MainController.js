(function(angular) {
  var app = angular.module('readability');

  app.controller('MainController',
    ['$window', function($window) {
      if (/lang/.test($window.location.search)) {
        $window.location.href = "/pages/spa";
      }
    }]);
})(angular);
