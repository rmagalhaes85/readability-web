(function(angular) {
  var app = angular.module('readability');
  app.factory('SessionEventService',
    ['$http', '$q', function($http, $q) {
      function save(sender, method, tag) {
        if (!angular.isDefined(sender)) {
          return;
        }

        var data = {};
        data.sender = sender;

        if (angular.isDefined(method)) {
          data.method = method;
        }

        if (angular.isDefined(tag)) {
          data.tag = tag;
        }

        $http({
          'method': 'POST',
          'url': '/rest/sessionevent/',
          'data': data,
          'headers': {'Content-type': 'application/x-www-form-urlencoded'},
          'transformRequest': function(obj) {
            return 'eventContent=' + JSON.stringify(obj);
          }
        }).then(angular.noop);
      }

      function saveMetrics(sender, method) {
        var tag = { 'width': $(window).width(), 'height': $(window).height() };
        save(sender, method, tag);
      }

      return { 'save': save ,
               'saveMetrics': saveMetrics };
    }]);
})(angular);

