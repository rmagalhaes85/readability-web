(function(angular) {
  var app = angular.module('readability');

  app.factory('VoteCommentsService', 
    ['$http', '$log', '$q', '$window',
      function ($http, $log, $q, $window) {
        var VOTECOMMENT_BASEURL = '/rest/votecomment/';
        function get() {
          var deferred = $q.defer();

          $http.get(VOTECOMMENT_BASEURL).then(
            function(response) {
              var data = response.data; 
              var dataOK = angular.isDefined(data);

              dataOK &= angular.isDefined(data.snippetPair) && angular.isDefined(data.vote);

              if (dataOK) {
                deferred.resolve(data);
              } else {
                // TODO improve error handling
                $log.error('Vote comment data is not ok');
                deferred.reject();
                //$window.location.href = '/';
              }
            },
            function(error) {
              if (angular.isDefined(error.data)) {
                deferred.reject(error.data);
              } else {
                deferred.reject(error);
              }
            }
          );
          return deferred.promise;
        }

        function save(data) {
          if (!angular.isDefined(data)) {
            return $q.reject('No data defined');
          }

          if (!angular.isDefined(data.snippetPairHash)) {
            return $q.reject('No data.snippetPairHash defined');
          }

          if (!angular.isDefined(data.comments)) {
            return $q.reject('No data.comments defined');
          }

          var deferred = $q.defer();

          $http.post(VOTECOMMENT_BASEURL, data).then(
            function(response) {
              deferred.resolve(response.data);
            },
            function(error) {
              deferred.reject(error);
            }
          );
          return deferred.promise;
        }

        return {
          'get': get,
          'save': save
        };
      }
    ]
  );
})(angular);

