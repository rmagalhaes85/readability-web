(function(angular) {
  var app = angular.module('readability');
  app.factory('ModalService',
    ['$uibModal', '$log', 'modalMessages', 
      function($uibModal, $log, modalMessages) {
        function show() {
          var title = '';
          var message = '';
          var okText = '';
          var cancelText = '';

          if (arguments.length < 2) {
            throw new Error('You must specify at least two arguments (modal\'s title and message codes)');
          }

          title = arguments[0];
          message = arguments[1];

          if (arguments.length >= 3) {
            okText = arguments[2];
          }

          if (arguments.length >= 4) {
            cancelText = arguments[3];
          }

          return $uibModal.open({
            'size': 'md',
            'templateUrl': '/static/html/modalMsg.html',
            'controller': ['$scope', function($scope) {
              var modal_title = !!modalMessages[title] ? modalMessages[title] : '';
              var modal_message = !!modalMessages[message] ? modalMessages[message] : '';
              var modal_ok_text = !!modalMessages[okText] ? modalMessages[okText] : 'Ok';
              var modal_cancel_text = !!modalMessages[cancelText] ? modalMessages[cancelText] : 'Cancel';
              $scope.modal_title = modal_title;
              $scope.modal_message = modal_message;
              $scope.modal_ok_text = modal_ok_text;
              $scope.modal_cancel_text = modal_cancel_text;
              $scope.modal_ok = function() { 
                $scope.$close();
              };
              $scope.modal_cancel = function() {
                $scope.$dismiss();
              };
            }]
          }).result;
        }

        return {
          'show': show
        };
      }]);
})(angular);
