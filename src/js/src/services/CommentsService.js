(function(angular) {
  var app = angular.module('readability');
  app.factory('CommentsService',
    ['$http', '$q', '$log', function($http, $q, $log) {
      function save(commentsData) {
        var deferred = $q.defer();
        if (!angular.isDefined(commentsData)) {
          return $q.reject();
        } else if (!angular.isDefined(commentsData.comments)) {
          return $q.reject();
        }

        $http({
          'method': 'POST',
          'url': '/rest/comments/', 
          'data': commentsData,
          'headers': {'Content-type': 'application/x-www-form-urlencoded'},
          'transformRequest': function(obj) {
            return "comments=" + obj.comments;
          }
        }).then(
          function(response) {
            deferred.resolve();
          }, 
          function(error) {
            deferred.reject(error.data);
          }
        );

        return deferred.promise;
      }

      return {
        'save': save
      };
    }]);
})(angular);

