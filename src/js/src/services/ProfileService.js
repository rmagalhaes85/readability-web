(function(angular) {
  var app = angular.module('readability');

  app.factory('ProfileService', 
    ['$http', '$q', '$log', function($http, $q, $log) {
      function save(profileData) {
        var deferred = $q.defer();

        $http.post('/rest/profile/', profileData)
        .then(function(response) {
          $log.log('ProfileService: save ok');
          deferred.resolve();
        }, function(error) {
          var data = error.data;
          deferred.reject(data);
        });
        
        return deferred.promise;
      }

      return {
        'save': save
      };
    }]);
})(angular);

