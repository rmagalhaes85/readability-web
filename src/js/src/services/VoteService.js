(function(angular) {
  var app = angular.module('readability');

  app.factory('VoteService', ['$http', '$log', '$q', function($http, $log, $q) {
    function vote(snippetPairHash, snippetOrder, strength, comments) {
      return $http.post('/rest/vote/', 
        { 'snippetPairHash': snippetPairHash, 
          'snippetOrder': snippetOrder,
          'strength': strength,
          'comments': comments });
    }

    function checkDivergentVote() {
      $log.debug('VoteService#checkDivergentVote()');
      var deferred = $q.defer();
      $http.get('/rest/vote/divergent', {'responseType': 'text'}).then(
        function(response) { 
          $log.debug('VoteService#get#/rest/vote/divergent#response: ' + response);
          $log.debug('response.data = ' + response.data);
          return deferred.resolve(response.data); 
        },
        function(error) {
          $log.debug('VoteService#get/rest/vote/divergent#error: ' + error);
          if (!angular.isDefined(error)) {
            $log.debug('error: undefined');
            return deferred.reject(undefined);
          }
          if (error.status === 404) {
            $log.debug('response.status = 404');
            return deferred.resolve(undefined);
          }
          $log.debug('response.status = ' + error.status);
          $log.error('checkDivergentVote()#$http#get: ' + error);
          return deferred.reject(error.data);
        }
      );
      return deferred.promise;
    }


    return {
      'vote': vote,
      'checkDivergentVote': checkDivergentVote
    };
  }]);
})(angular);

