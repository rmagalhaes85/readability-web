(function(angular) {
  var app = angular.module('readability');

  app.factory('WorkingSetService',
    ['$http', '$q', '$log', function($http, $q, $log) {
    function get() {
      var deferred = $q.defer();
      $http.get('/rest/experiment/workingset').then(
        function(response) {
          $log.info(response.data);
          return deferred.resolve(response.data);
        },
        function(error) {
          return deferred.reject(error.data);
        });
      return deferred.promise;
    }

    return {
      get: get
    };
  }]);
})(angular);

