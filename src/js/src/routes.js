(function(angular) {
  var app = angular.module('readability');
  app.config(
  ['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
      $routeProvider
      .when('/', { templateUrl: '/pages/instructions', controller: 'InstructionsController' })
      .when('/pair/:pairIndex', { templateUrl: '/pages/pairs', controller: 'PairsController' })
      .when('/votecomments', { templateUrl: '/pages/votecomments', controller: 'VoteCommentsController' })
      .when('/profile', { templateUrl: '/pages/profile', controller: 'ProfileController' })
      .when('/comments', { templateUrl: '/pages/comments', controller: 'CommentsController' })
      .otherwise({ redirectTo: '/' });
  }]);
})(angular);
