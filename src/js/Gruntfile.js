module.exports = function(grunt) {
  grunt.initConfig({
    'pkg': grunt.file.readJSON('package.json'),
    'concat': {
      'options': {
        'separator': ';'
      },
      'readability-app': {
        'src': [
          'src/app.js',
          'src/routes.js',
          'src/sessionCheckInterceptor.js',
          'src/controllers/MainController.js',
          'src/controllers/ProfileController.js',
          'src/controllers/PairsController.js',
          'src/controllers/CommentsController.js',
          'src/controllers/VoteCommentsController.js',
          'src/controllers/InstructionsController.js',
          'src/services/ProfileService.js',
          'src/services/WorkingSetService.js',
          'src/services/VoteService.js',
          'src/services/ModalService.js',
          'src/services/VoteCommentsService.js',
          'src/services/CommentsService.js',
          'src/services/SessionEventService.js'
        ],
        'dest': 'dist/readability-app.js',
        'nonull': true
      }
    },
    'jshint': {
      'options': {
        'curly': true,
        'eqeqeq': true,
        'eqnull': true,
        'browser': true
      },
      'files': {
        'src': 'src/**/*.js'
      }
    },
    'uglify': {
      'dist': {
        'files': {
          'dist/readability-app.min.js': 'dist/readability-app.js'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('lint', ['jshint']);
  grunt.registerTask('default', ['jshint', 'concat', 'uglify']);
};
