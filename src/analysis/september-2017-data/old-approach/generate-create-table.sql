drop function if exists generate_create_table();
create function generate_create_table()
returns table(command text) as $$
begin
	return query
	select concat(c.name,'_votes int not null default 0, ',c.name,'_divergents int not null default 0,') from sp_columns_list() c;
end;
$$ language plpgsql;
