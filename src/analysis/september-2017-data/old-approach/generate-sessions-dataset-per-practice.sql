create or replace function generate_sessions_dataset_per_practice()
returns table(experiment_session_id int, profile_id int, p01_071_votes int, p01_071_divergents int, p01_votes int, p01_divergents int) as $$
begin
	create table pg_temp.votes_per_session (
		experiment_session_id int not null,
		p01_071_votes int not null default 0,
		p01_071_divergents int not null default 0,
		p01_votes int not null default 0,
		p01_divergents int not null default 0
	);

	for esid in
		select experiment_session_id 
		from experiment_sessions
		where start_time > '20170801'
			and end_time is not null
	loop
		
	end loop;

	return query
	select 
		vps.experiment_session_id,
		p.profile_id,
		vps.p01_071_votes,
		vps.p01_071_divergents,
		vps.p01_votes,
		vps.p01_divergents
	from pg_temp.votes_per_session vps
		inner join experiment_sessions e on vps.experiment_session_id = e.experiment_session_id
		inner join profiles p on e.profile_id = p.profile_id;
end;
$$ language plpgsql;
