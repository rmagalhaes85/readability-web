drop function if exists generate_select_fields();
create function generate_select_fields()
returns table(command text) as $$
begin
	return query
	select concat('vps.',c.name,'_votes, ','vps.',c.name,'_divergents,') from sp_columns_list() c;
end;
$$ language plpgsql;
