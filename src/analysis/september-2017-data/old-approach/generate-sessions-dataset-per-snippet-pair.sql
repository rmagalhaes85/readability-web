drop function if exists generate_sessions_dataset_per_snippet_pair();

create function generate_sessions_dataset_per_snippet_pair()
returns table(experiment_session_id int, profile_id int, 
 p01_054_votes int, p01_054_divergents int,
 p01_060_votes int, p01_060_divergents int,
 p01_071_votes int, p01_071_divergents int,
 p02_062_votes int, p02_062_divergents int,
 p02_063_votes int, p02_063_divergents int,
 p02_064_votes int, p02_064_divergents int,
 p03_057_votes int, p03_057_divergents int,
 p03_066_votes int, p03_066_divergents int,
 p03_069_votes int, p03_069_divergents int,
 p04_052_votes int, p04_052_divergents int,
 p04_058_votes int, p04_058_divergents int,
 p04_078_votes int, p04_078_divergents int,
 p05_056_votes int, p05_056_divergents int,
 p05_059_votes int, p05_059_divergents int,
 p05_082_votes int, p05_082_divergents int,
 p06_037_votes int, p06_037_divergents int,
 p06_045_votes int, p06_045_divergents int,
 p06_080_votes int, p06_080_divergents int,
 p07_053_votes int, p07_053_divergents int,
 p07_065_votes int, p07_065_divergents int,
 p07_081_votes int, p07_081_divergents int,
 p08_044_votes int, p08_044_divergents int,
 p08_048_votes int, p08_048_divergents int,
 p08_070_votes int, p08_070_divergents int,
 p09_061_votes int, p09_061_divergents int,
 p09_075_votes int, p09_075_divergents int,
 p09_079_votes int, p09_079_divergents int,
 p10_068_votes int, p10_068_divergents int,
 p10_072_votes int, p10_072_divergents int,
 p10_076_votes int, p10_076_divergents int,
 p11_012_votes int, p11_012_divergents int,
 p11_041_votes int, p11_041_divergents int,
 p11_074_votes int, p11_074_divergents int,
 p12_051_votes int, p12_051_divergents int,
 p12_055_votes int, p12_055_divergents int,
 p12_067_votes int, p12_067_divergents int,
 p13_073_votes int, p13_073_divergents int,
 p13_077_votes int, p13_077_divergents int,
 p13_083_votes int, p13_083_divergents int
) as $$
begin
	create table pg_temp.votes_per_session (
		experiment_session_id int not null,
--		p01_071_votes int not null default 0,
--		p01_071_divergents int not null default 0,
 p01_054_votes int not null default 0, p01_054_divergents int not null default 0,
 p01_060_votes int not null default 0, p01_060_divergents int not null default 0,
 p01_071_votes int not null default 0, p01_071_divergents int not null default 0,
 p02_062_votes int not null default 0, p02_062_divergents int not null default 0,
 p02_063_votes int not null default 0, p02_063_divergents int not null default 0,
 p02_064_votes int not null default 0, p02_064_divergents int not null default 0,
 p03_057_votes int not null default 0, p03_057_divergents int not null default 0,
 p03_066_votes int not null default 0, p03_066_divergents int not null default 0,
 p03_069_votes int not null default 0, p03_069_divergents int not null default 0,
 p04_052_votes int not null default 0, p04_052_divergents int not null default 0,
 p04_058_votes int not null default 0, p04_058_divergents int not null default 0,
 p04_078_votes int not null default 0, p04_078_divergents int not null default 0,
 p05_056_votes int not null default 0, p05_056_divergents int not null default 0,
 p05_059_votes int not null default 0, p05_059_divergents int not null default 0,
 p05_082_votes int not null default 0, p05_082_divergents int not null default 0,
 p06_037_votes int not null default 0, p06_037_divergents int not null default 0,
 p06_045_votes int not null default 0, p06_045_divergents int not null default 0,
 p06_080_votes int not null default 0, p06_080_divergents int not null default 0,
 p07_053_votes int not null default 0, p07_053_divergents int not null default 0,
 p07_065_votes int not null default 0, p07_065_divergents int not null default 0,
 p07_081_votes int not null default 0, p07_081_divergents int not null default 0,
 p08_044_votes int not null default 0, p08_044_divergents int not null default 0,
 p08_048_votes int not null default 0, p08_048_divergents int not null default 0,
 p08_070_votes int not null default 0, p08_070_divergents int not null default 0,
 p09_061_votes int not null default 0, p09_061_divergents int not null default 0,
 p09_075_votes int not null default 0, p09_075_divergents int not null default 0,
 p09_079_votes int not null default 0, p09_079_divergents int not null default 0,
 p10_068_votes int not null default 0, p10_068_divergents int not null default 0,
 p10_072_votes int not null default 0, p10_072_divergents int not null default 0,
 p10_076_votes int not null default 0, p10_076_divergents int not null default 0,
 p11_012_votes int not null default 0, p11_012_divergents int not null default 0,
 p11_041_votes int not null default 0, p11_041_divergents int not null default 0,
 p11_074_votes int not null default 0, p11_074_divergents int not null default 0,
 p12_051_votes int not null default 0, p12_051_divergents int not null default 0,
 p12_055_votes int not null default 0, p12_055_divergents int not null default 0,
 p12_067_votes int not null default 0, p12_067_divergents int not null default 0,
 p13_073_votes int not null default 0, p13_073_divergents int not null default 0,
 p13_077_votes int not null default 0, p13_077_divergents int not null default 0,
 p13_083_votes int not null default 0, p13_083_divergents int not null default 0
	);

	insert into pg_temp.votes_per_session
	select e.experiment_session_id
	from experiment_sessions e
	where e.start_time > '20170801'
		and exists(
			select v.vote_id 
			from votes v 
			where v.experiment_session_id = e.experiment_session_id)
	order by e.experiment_session_id;

	 update pg_temp.votes_per_session set p01_054_votes = sub.total, p01_054_divergents = sub.divergents from (select * from loop_test2(54)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p01_060_votes = sub.total, p01_060_divergents = sub.divergents from (select * from loop_test2(60)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p01_071_votes = sub.total, p01_071_divergents = sub.divergents from (select * from loop_test2(71)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p02_062_votes = sub.total, p02_062_divergents = sub.divergents from (select * from loop_test2(62)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p02_063_votes = sub.total, p02_063_divergents = sub.divergents from (select * from loop_test2(63)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p02_064_votes = sub.total, p02_064_divergents = sub.divergents from (select * from loop_test2(64)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p03_057_votes = sub.total, p03_057_divergents = sub.divergents from (select * from loop_test2(57)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p03_066_votes = sub.total, p03_066_divergents = sub.divergents from (select * from loop_test2(66)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p03_069_votes = sub.total, p03_069_divergents = sub.divergents from (select * from loop_test2(69)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p04_052_votes = sub.total, p04_052_divergents = sub.divergents from (select * from loop_test2(52)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p04_058_votes = sub.total, p04_058_divergents = sub.divergents from (select * from loop_test2(58)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p04_078_votes = sub.total, p04_078_divergents = sub.divergents from (select * from loop_test2(78)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p05_056_votes = sub.total, p05_056_divergents = sub.divergents from (select * from loop_test2(56)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p05_059_votes = sub.total, p05_059_divergents = sub.divergents from (select * from loop_test2(59)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p05_082_votes = sub.total, p05_082_divergents = sub.divergents from (select * from loop_test2(82)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p06_037_votes = sub.total, p06_037_divergents = sub.divergents from (select * from loop_test2(37)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p06_045_votes = sub.total, p06_045_divergents = sub.divergents from (select * from loop_test2(45)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p06_080_votes = sub.total, p06_080_divergents = sub.divergents from (select * from loop_test2(80)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p07_053_votes = sub.total, p07_053_divergents = sub.divergents from (select * from loop_test2(53)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p07_065_votes = sub.total, p07_065_divergents = sub.divergents from (select * from loop_test2(65)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p07_081_votes = sub.total, p07_081_divergents = sub.divergents from (select * from loop_test2(81)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p08_044_votes = sub.total, p08_044_divergents = sub.divergents from (select * from loop_test2(44)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p08_048_votes = sub.total, p08_048_divergents = sub.divergents from (select * from loop_test2(48)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p08_070_votes = sub.total, p08_070_divergents = sub.divergents from (select * from loop_test2(70)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p09_061_votes = sub.total, p09_061_divergents = sub.divergents from (select * from loop_test2(61)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p09_075_votes = sub.total, p09_075_divergents = sub.divergents from (select * from loop_test2(75)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p09_079_votes = sub.total, p09_079_divergents = sub.divergents from (select * from loop_test2(79)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p10_068_votes = sub.total, p10_068_divergents = sub.divergents from (select * from loop_test2(68)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p10_072_votes = sub.total, p10_072_divergents = sub.divergents from (select * from loop_test2(72)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p10_076_votes = sub.total, p10_076_divergents = sub.divergents from (select * from loop_test2(76)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p11_012_votes = sub.total, p11_012_divergents = sub.divergents from (select * from loop_test2(12)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p11_041_votes = sub.total, p11_041_divergents = sub.divergents from (select * from loop_test2(41)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p11_074_votes = sub.total, p11_074_divergents = sub.divergents from (select * from loop_test2(74)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p12_051_votes = sub.total, p12_051_divergents = sub.divergents from (select * from loop_test2(51)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p12_055_votes = sub.total, p12_055_divergents = sub.divergents from (select * from loop_test2(55)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p12_067_votes = sub.total, p12_067_divergents = sub.divergents from (select * from loop_test2(67)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p13_073_votes = sub.total, p13_073_divergents = sub.divergents from (select * from loop_test2(73)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p13_077_votes = sub.total, p13_077_divergents = sub.divergents from (select * from loop_test2(77)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	 update pg_temp.votes_per_session set p13_083_votes = sub.total, p13_083_divergents = sub.divergents from (select * from loop_test2(83)) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;
	

	return query
	select 
		vps.experiment_session_id,
		p.profile_id,
--		vps.p01_071_votes,
--		vps.p01_071_divergents,
 vps.p01_054_votes, vps.p01_054_divergents,
 vps.p01_060_votes, vps.p01_060_divergents,
 vps.p01_071_votes, vps.p01_071_divergents,
 vps.p02_062_votes, vps.p02_062_divergents,
 vps.p02_063_votes, vps.p02_063_divergents,
 vps.p02_064_votes, vps.p02_064_divergents,
 vps.p03_057_votes, vps.p03_057_divergents,
 vps.p03_066_votes, vps.p03_066_divergents,
 vps.p03_069_votes, vps.p03_069_divergents,
 vps.p04_052_votes, vps.p04_052_divergents,
 vps.p04_058_votes, vps.p04_058_divergents,
 vps.p04_078_votes, vps.p04_078_divergents,
 vps.p05_056_votes, vps.p05_056_divergents,
 vps.p05_059_votes, vps.p05_059_divergents,
 vps.p05_082_votes, vps.p05_082_divergents,
 vps.p06_037_votes, vps.p06_037_divergents,
 vps.p06_045_votes, vps.p06_045_divergents,
 vps.p06_080_votes, vps.p06_080_divergents,
 vps.p07_053_votes, vps.p07_053_divergents,
 vps.p07_065_votes, vps.p07_065_divergents,
 vps.p07_081_votes, vps.p07_081_divergents,
 vps.p08_044_votes, vps.p08_044_divergents,
 vps.p08_048_votes, vps.p08_048_divergents,
 vps.p08_070_votes, vps.p08_070_divergents,
 vps.p09_061_votes, vps.p09_061_divergents,
 vps.p09_075_votes, vps.p09_075_divergents,
 vps.p09_079_votes, vps.p09_079_divergents,
 vps.p10_068_votes, vps.p10_068_divergents,
 vps.p10_072_votes, vps.p10_072_divergents,
 vps.p10_076_votes, vps.p10_076_divergents,
 vps.p11_012_votes, vps.p11_012_divergents,
 vps.p11_041_votes, vps.p11_041_divergents,
 vps.p11_074_votes, vps.p11_074_divergents,
 vps.p12_051_votes, vps.p12_051_divergents,
 vps.p12_055_votes, vps.p12_055_divergents,
 vps.p12_067_votes, vps.p12_067_divergents,
 vps.p13_073_votes, vps.p13_073_divergents,
 vps.p13_077_votes, vps.p13_077_divergents,
 vps.p13_083_votes, vps.p13_083_divergents
	from pg_temp.votes_per_session vps
		inner join experiment_sessions e on vps.experiment_session_id = e.experiment_session_id
		left join profiles p on e.profile_id = p.profile_id
	order by vps.experiment_session_id;
end;
$$ language plpgsql;
