drop function if exists generate_update_command_per_snippet_pair();
create function generate_update_command_per_snippet_pair()
returns table(command text) as $$
begin
	return query
	select concat('update pg_temp.votes_per_session set ',c.name,'_votes = sub.total, ',c.name,'_divergents = sub.divergents from (select * from loop_test2(',c.snippet_pair_id,')) sub where sub.experiment_session_id = pg_temp.votes_per_session.experiment_session_id;') from sp_columns_list() c;
end;
$$ language plpgsql;
