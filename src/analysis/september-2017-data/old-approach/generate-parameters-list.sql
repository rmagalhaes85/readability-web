drop function if exists generate_parameters_list();
create function generate_parameters_list()
returns table(command text) as $$
begin
	return query
	select concat(c.name,'_votes int, ',c.name,'_divergents int,') from sp_columns_list() c;
end;
$$ language plpgsql;
