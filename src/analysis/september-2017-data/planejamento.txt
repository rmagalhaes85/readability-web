Coisas que pretendemos conhecer com esta análise
================================================

1. Quais foram os impactos de cada prática e cada snippet pair; esta análise já
foi executada, procurar os scripts já rodados anteriormente; seria interessante
tentar otimizar o código, está com muitos elementos repetidos devido a, no
momento em que o script foi escrito, eu estar com pouca noção a respeito do R;

A análise já executada precisará ser rodada novamente, já que só agora dispomos
do conjunto de dados completo;

2. Correlações entre variáveis de perfil e votos divergentes (especificar quais
variáveis de perfil e quais processamentos os campos das variáveis de perfil
precisarão sofrer para poderem ser interpretados pela R); usar chisq.test

3. Correlações entre conteúdo dos comentários e votos divergentes (especificar
quais categorias serão utilizadas para enquadrar os comentários, e seus
respectivos valores. Por exemplo: uso de linguagem de baixo calão: alto, médio,
não usada); algumas categorias como, por exemplo, o comprimento do comentário,
poderão ser calculadas automaticamente, mas a maioria delas exigirá a leitura
do texto e posterior marcação das categorias em uma planilha.

4. Cálculo da frequência de comentários associados a votos divergentes,
separados por prática. Isso poderá ser feito apenas para as práticas que
apresentaram piora na legibilidade. A ideia aqui é encontrar features comuns
nos comentários na tentativa de encontrar explicações para as práticas que
pioraram a legibilidade.

5. Apresentar estatísticas descritivas (média e desvio-padrão talvez sejam
suficientes) com dados sobre os comentários, por prática/por categoria de voto.
Por exemplo, apresentar comprimento médio dos comentários da prática P.01 e
votos aderentes, P.01 e votos divergentes, P.01 e % de comentários inúteis etc.
Talvez seja possível aplicar testes de duas proporções aqui para a extração de
alguma conclusão interessante. Exemplo: "percentual de comentários úteis foi
maior no grupo de práticas que apresentou menor legibilidade".


Variáveis de perfil analisadas e suas transformações
====================================================

Necessidade de transformação de variáveis
-----------------------------------------

Nem todas as variáveis de perfil estão devidamente preparadas para
processamento em R. Por exemplo, na lista de linguagens conhecidas, há um texto
corrido que precisa ser dividido em categorias por linguagem; posso definir uma
lista de linguagens mais populares, como Python, C#, C++, Java, e usar a
cláusula ILIKE do postgres para ir preenchendo essas categorias com Y ou N.
Detalhando: cada categoria será representada por uma coluna no resultset
(exemplo: knows_csharp, knows_java) e possuirão valores 'Y' ou 'N';

Lista de transformações para cada coluna da tabela PROFILES
-----------------------------------------------------------

Coluna "programming_experience_time": nenhuma

Coluna "java_programming_experience_time": nenhuma

Coluna "floss_experience_time": nenhuma

Coluna "age":
Realizar transformações para seguir um modelo <ORDEM>_<FAIXA>, onde:
ORDEM: de 0 até onde for necessário
FAIXA: algo na linha de 0bt12 (between 0 and 12) ou gt51 (greater than 51)

Coluna "years_in_school": nenhuma (apesar do nome da coluna passar a ideia de tempo, aqui estamos falando sobre o nível de graduação do participante)

Coluna "gender": nenhuma

Coluna "programming_languages":
definir colunas para a lista de linguagens abaixo e usar a cláusula ILIKE para preenchê-las:
Python, C++, C#, Java, Javascript

Coluna "natural languages":
à semelhança do que foi feito com as linguagens de programação, fazer o mesmo
aqui. O ponto de atenção é que as pessoas usaram linguagem natural para
escolher seus idiomas, então é preciso considerar todos os idiomas possíveis no
script. Exemplo: coluna knows_english precisa considerar a presença de
"english", "inglês" (ACENTUADO OU NÃO), "angla" e "inglés" (ACENTUADO OU NÃO);
Idiomas a considerar: português, inglês, espanhol, alemão, francês

Coluna "operating_systems":
Para cada categoria, checar a presença das seguintes palavras:
Windows - window, vindozo
Linux - linux, ubuntu, fedora, kali, linukso, debian
Mac OS - mac

Coluna "english_confort_level": nenhuma

Coluna "perceived_readability_level": nenhuma

Coluna "learning_profile": nenhuma



Categorias de comentários
=========================


Processo de trabalho de categorização
-------------------------------------


Processo de upload das categorias para o banco de dados
-------------------------------------------------------

Motivação: permitir que o postgres já gere um csv com os dados de categorias disponíveis de forma conveniente

Caveats: é preciso considerar a possibilidade de, DEPOIS DE JÁ TER FEITO O UPLOAD PARA O BD, surgirem ideias de novas
categorizações; por isso, o processo deve suportar o upload de novas categorias posteriormente.

Processo:


Lista de fatores e seus respectivos levels
------------------------------------------

Comprimento: calculado dinamicamente pelo script sql. Vide levels já especificados no script

Afirmou não ter visto diferença: Y ou N

Afirmou ter preferência por mais espaço em branco: Y ou N

Mencionou nomenclatura de variáveis: Y ou N

Mencionou agradabilidade visual do código: Y ou N

Mencionou preferência por código mais compacto: Y ou N

Explicou seu processo decisório: Y ou N (aqui, verificamos se houve uma simples declaração de preferência OU se o participante
explicou essa preferência e/ou apresentou argumentos em defesa de seu voto; a ideia aqui é diferenciar comentários do tipo
"escolhi x porque tal separação de linhas é melhor" -- algo que o participante
já deixou claro por meio de seu voto -- de comentários do tipo "escolhi x
porque estou mais habituado a usar essa formatação quando estou programando em
Java". Outro exemplo de separação que essa categoria tenciona obter: "easier to read" vc "easier to read parameters", já que o segundo
comentário deu uma ideia mais precisa acerca de qual facilidade o participante está referindo)

Mencionou preferência por código aderente a convenções Java pré-estabelecidas: Y ou N

Criticou o survey, o exemplo de códigos fornecidos etc: Y ou N

Conteúdo emocional (xingou o exemplo, xingou a prática exercitada, citou deus ou alguma emoção etc): Y ou N


