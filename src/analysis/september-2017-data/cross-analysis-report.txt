
Prática P.01 vs Conhece Javascript
   A D divergence
N  7 6      46.15
Y 20 0       0.00
valor-p: 0.002999
v-cramer: 0.584705


Prática P.02 vs Tamanho da Tela
    A  D divergence
SM 10  1       9.09
XL 24 30      55.56
valor-p: 0.005497
v-cramer: 0.348815


Prática P.02 vs Orientação da Tela
   A  D divergence
N 10  1       9.09
Y 24 30      55.56
valor-p: 0.006997
v-cramer: 0.348815


Prática P.05 vs Orientação da Tela
   A  D divergence
N 13  1       7.14
Y 26 16      38.10
valor-p: 0.044978
v-cramer: 0.291492


Prática P.06 vs Grau de Escolaridade
     A D divergence
b   18 0       0.00
doo  0 2     100.00
hs   6 2      25.00
m    8 3      27.27
valor-p: 0.004998
v-cramer: 0.59914


Prática P.07 vs Conhece C#
   A D divergence
N 13 4      23.53
Y 21 0       0.00
valor-p: 0.031984
v-cramer: 0.38122


Prática P.10 vs Perfil de Aprendizagem
   A  D divergence
dk 5  0       0.00
t  4 10      71.43
v  4  8      66.67
valor-p: 0.024988
v-cramer: 0.517893


Relação geral com Conhece C#
    A  D divergence
N 148 82      35.65
Y 166 58      25.89
valor-p: 0.027986
v-cramer: 0.105652


Relação geral com Requer Esforço
    A   D divergence
Y 157 124      44.13
N 373 112      23.09
valor-p: 5e-04
v-cramer: 0.219573

