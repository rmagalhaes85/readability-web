# procura relações de dependência entre outras variáveis categóricas,
# sem considerar práticas de codificação e votos divergentes/aderentes

cd <- read.csv('categories-data.csv')
cols <- colnames(cd)
ignore_cols <- c('vote_id', 'snippet_pair_id', 'practice', 'alias', 'divergent', 'screen_height')
cols <- cols[!cols %in% ignore_cols]
alpha <- 0.1

for (icol in cols) {
	for (jcol in cols) {
		if (icol != jcol) {
			t = table(cd[[icol]], cd[[jcol]])
			chisq = chisq.test(t, simulate.p.value=TRUE)
			p.value = chisq$p.value
			if (!is.na(p.value) && p.value < alpha) {
				print(paste(icol, ' e ', jcol, ' ==> valor-p: ', p.value, collapse=''))
			}
		}
	}
}

