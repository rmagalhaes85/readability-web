#!/bin/bash

# Script para execução dos comandos SQL de obtenção dos dados do
# banco postgresql e posterior geração de arquivo .csv para processamento
# em R

psql -U postgres -q -w -d readability_dump_20170829_201500 -t -F',' -A < query-data.sql
cp /tmp/survey-data.csv .

# testing purporses only
head survey-data.csv
