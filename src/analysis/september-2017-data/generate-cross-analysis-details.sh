#!/bin/bash

# Gera no STDOUT as tabelas de contingência das variáveis que apresentaram
# relação estatisticamente significativa com as opiniões
# Essas tabelas serão observadas para a redação do texto na seção de
# apresentação dos resultados

R --quiet --vanilla --slave -f cross-analysis-details.R > cross-analysis-report.txt

