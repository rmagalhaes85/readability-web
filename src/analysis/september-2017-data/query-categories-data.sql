-- script para extração de dados do banco postgresql
-- contendo as respostas dos participantes até 8 de setembro.
-- O formato retornado por este script visa a realização de testes chi-square

-- tabela auxiliar para filtrar apenas os últimos comentários fornecidos para
-- cada voto:
create temporary table vote_comments_last_tag as 
select v.vote_id, vc.vote_comment_id, sp.practice, sp.alias, vc.comments 
from vote_comments vc inner join votes v on vc.vote_id = v.vote_id and v.last_vote = true
	inner join snippet_pairs sp on v.snippet_pair_id = sp.snippet_pair_id
where v.arrival_time between '20170801' and '20170909'
order by sp.practice, sp.alias, v.vote_id;
alter table vote_comments_last_tag add column last boolean null;

create index vclt_ordering on vote_comments_last_tag(practice, alias, vote_comment_id);

-- marcação dos últimos comentários de cada vote_id
update vote_comments_last_tag vlt 
set last = case when exists (
	select 1 
	from vote_comments_last_tag vlt1 
	where vlt1.vote_comment_id > vlt.vote_comment_id and vlt1.vote_id = vlt.vote_id) then false else true end;

delete from vote_comments_last_tag where last = FALSE;
alter table vote_comments_last_tag drop column last;

-- carga da tabela de categoriação de comentários
create temporary table vote_comments_categorizations (
	vote_id int not null,
	vote_comment_id int not null,
	practice varchar(10) not null,
	alias varchar(15) not null,
	comments varchar(5000) null,
	-- mencionou nomenclatura de variáveis
	mentions_no_difference char(1) null,
	mentions_likes_whitespace char(1) null,
	mentions_variable_naming char(1) null,
	mentions_visual_smoothness char(1) null,
	mentions_likes_more_compact_visualizations char(1) null,
	explained_decision_process char(1) null,
	mentions_adherence_to_known_java_conventions char(1) null,
	comment_contains_critics_to_survey char(1) null,
	emotional_content char(1) null
);

copy vote_comments_categorizations from '/tmp/vote-comments-categorizations.csv'
with (header TRUE, format 'csv');

-- levantamento de eventos. Esta tabela será usada para filtrar e manter apenas
-- as primeiras ocorrências de cada evento, eliminando casos em que o participante
-- vai para outro snippet e depois retoma o experimento do início:
create temporary table first_snippet_pair_appearances as
select e.experiment_session_id, se.session_event_id, se.arrival_time, se.content, 
	case
	when se.content = '{"sender":"InstructionsController","method":"goNextStep"}' then 0
	when se.content = '{"sender":"PairsController","method":"showSnippetPair","tag":{"index":2}}' then 1
	when se.content = '{"sender":"PairsController","method":"showSnippetPair","tag":{"index":3}}' then 2
	when se.content = '{"sender":"PairsController","method":"showSnippetPair","tag":{"index":4}}' then 3
	when se.content = '{"sender":"PairsController","method":"showSnippetPair","tag":{"index":5}}' then 4
	end as snippet_pair_order,
	false as first_occurrence
from experiment_sessions e inner join session_events se 
	on e.experiment_session_id = se.experiment_session_id 
where e.start_time between '20170801' and '20170909' and se.content in (
	'{"sender":"InstructionsController","method":"goNextStep"}',
	'{"sender":"PairsController","method":"showSnippetPair","tag":{"index":2}}',
	'{"sender":"PairsController","method":"showSnippetPair","tag":{"index":3}}',
	'{"sender":"PairsController","method":"showSnippetPair","tag":{"index":4}}',
	'{"sender":"PairsController","method":"showSnippetPair","tag":{"index":5}}')
order by e.experiment_session_id, se.session_event_id;

update first_snippet_pair_appearances fe
set first_occurrence = case when exists (
	select 1 from first_snippet_pair_appearances fe1 
	where fe1.session_event_id < fe.session_event_id 
		and fe1.experiment_session_id = fe.experiment_session_id
		and fe1.snippet_pair_order = fe.snippet_pair_order) then false else true end;

delete from first_snippet_pair_appearances where first_occurrence = false;

--------------------------------------------------------------------------

-- levantamento de eventos indicando os tamanhos de tela. Aqui deverá ser feito um tratamento para conseguir apenas
-- 1 ocorrência de cada evento, já que pode haver casos de sessões contendo mais de um evento, o que geraria
-- multiplicidade de dados em decorrência do inner join

create temporary table screen_sizes as
select se.experiment_session_id, se.session_event_id, se.content, 
	cast('0' as varchar) as width, cast('0' as varchar) as height, false as last_occurrence
from session_events se inner join experiment_sessions e on se.experiment_session_id = e.experiment_session_id
where e.start_time between '20170801' and '20170909' 
	and exists(select 1 from votes v where v.experiment_session_id = e.experiment_session_id)
	and se.content like '{"sender":"InstructionsController","tag":{"width":%'
order by se.experiment_session_id, se.session_event_id;

-- removendo instruções desnecessários do objeto json com dados do evento:
update screen_sizes
set content = replace(content, '{"sender":"InstructionsController","tag":{', '');

update screen_sizes
set content = replace(content, '}}', '');

-- carrega as medidas a partir do que sobrou dos dados do evento:
update screen_sizes
set width = regexp_replace(content, '"width":(\d+),"height":(\d+)', E'\\1'),
  height = regexp_replace(content, '"width":(\d+),"height":(\d+)', E'\\2');

-- mantendo apenas a última ocorrência do evento:
update screen_sizes ss
set last_occurrence = case when exists (
	select 1 from screen_sizes ss1
	where ss1.experiment_session_id = ss.experiment_session_id
		and ss1.session_event_id > ss.session_event_id) then false else true end;

delete from screen_sizes where last_occurrence = false;


-------------------------------------------------------------------------- 


create temporary table experiment_sessions_and_profiles as
select
	e.experiment_session_id,
	p.profile_id,
	case 
		when p.age in ('-12', '13-20') then 'A-0-20'
		when p.age in ('21-28', '29-36') then 'B-21-36'
		when p.age in ('37-44', '45-50', '51-') then 'C-37-I'
		else 'NA'
	end as profile_age,
	case when p.gender is null then 'NA' else p.gender end as profile_gender,
	--case when p.programming_experience_time = 0 then 'NA' else cast(p.programming_experience_time as varchar) end as profile_programming_experience_time,
	case 
		when p.programming_experience_time between 0 and 3 then 'A_0_3'
		when p.programming_experience_time between 4 and 7 then 'B_4_7'
		when p.programming_experience_time > 7 then 'C_8_I'
		else 'NA'
	end as profile_programming_experience_time,
--	case when p.java_programming_experience_time = 0 then 'NA' else cast(p.java_programming_experience_time as varchar) end as profile_java_programming_experience_time,
	case 
		when p.java_programming_experience_time between 0 and 3 then 'A_0_3'
		when p.java_programming_experience_time between 4 and 7 then 'B_4_7'
		when p.java_programming_experience_time > 7 then 'C_8_I'
		else 'NA'
	end as profile_java_programming_experience_time,
--	case when p.floss_experience_time = 0 then 'NA' else cast(p.floss_experience_time as varchar) end as profile_floss_experience_time,
	case 
		when p.floss_experience_time between 0 and 3 then 'A_0_3'
		when p.floss_experience_time between 4 and 7 then 'B_4_7'
		when p.floss_experience_time > 7 then 'C_8_I'
		else 'NA'
	end as profile_floss_experience_time,
	case when p.years_in_school is null then 'NA' else p.years_in_school end as profile_years_in_school,
	case when p.learning_profile is null then 'NA' else p.learning_profile end as profile_learning_profile,
	-- lista de linguagens de programação conhecidas
	case when p.programming_languages is null then 'NA' when p.programming_languages ~* '\mJAVA\M' then 'Y' else 'N' end as knows_java,
	case when p.programming_languages is null then 'NA' when p.programming_languages ~* '\mJAVASCRIPT\M' then 'Y' else 'N' end as knows_javascript,
	case when p.programming_languages is null then 'NA' when p.programming_languages ~* 'C\+\+' then 'Y' else 'N' end as knows_cpp,
	case when p.programming_languages is null then 'NA' when p.programming_languages ~* '\mC\M' then 'Y' else 'N' end as knows_c,
	case when p.programming_languages is null then 'NA' when p.programming_languages ~* '\mC#' then 'Y' else 'N' end as knows_csharp,
	case when ss.width is null or ss.width = '0' then 'NA' else ss.width end as screen_width,
	case when ss.height is null or ss.height = '0' then 'NA' else ss.height end as screen_height
from experiment_sessions e left join profiles p on e.profile_id = p.profile_id
	left join screen_sizes ss on e.experiment_session_id = ss.experiment_session_id
where 
	-- considerar sessões do período entre 2017-08-01 e 2017-09-08 e que possuam pelo menos 1 voto associado
	-- a ela (se não houver votos, a sessão nem sequer vai ser considerada no cômputo de votos)
	e.start_time between '20170801' and '20170909'
	and exists (
		select 1 from votes v 
		where v.experiment_session_id = e.experiment_session_id 
			and v.arrival_time between '20170801' and '20170909');

copy experiment_sessions_and_profiles to '/tmp/experiment-sessions-and-profiles.csv' csv header; 

create temporary table vsummary as 
select 
	v.vote_id, 
	sp.snippet_pair_id,
	sp.practice,
	sp.alias,
	case when p.profile_age is null then 'NA' else p.profile_age end as profile_age,
	case when p.profile_gender is null then 'NA' else p.profile_gender end as profile_gender,
	case when p.profile_programming_experience_time is null then 'NA' else p.profile_programming_experience_time end as profile_programming_experience_time,
	case when p.profile_java_programming_experience_time is null then 'NA' else p.profile_java_programming_experience_time end as profile_java_programming_experience_time,
	case when p.profile_floss_experience_time is null then 'NA' else p.profile_floss_experience_time end as profile_floss_experience_time,
	case when p.profile_years_in_school is null then 'NA' else p.profile_years_in_school end as profile_years_in_school,
	case when p.profile_learning_profile is null then 'NA' else p.profile_learning_profile end as profile_learning_profile,
	-- div: 'A' = voto aderente; 'D' = voto divergente
	case when v.snippet_order <> sp.most_readable then 'D' else 'A' end as divergent, 
	case when vc.comments is null then 'NA' else case when trim(vc.comments) = '.' then 'Y' else 'N' end end as commented_single_dot,
	case when p.knows_java is null then 'NA' else p.knows_java end as knows_java,
	case when p.knows_javascript is null then 'NA' else p.knows_javascript end as knows_javascript,
	case when p.knows_cpp is null then 'NA' else p.knows_cpp end as knows_cpp,
	case when p.knows_c is null then 'NA' else p.knows_c end as knows_c,
	case when p.knows_csharp is null then 'NA' else p.knows_csharp end as knows_csharp,
	case when vcc.mentions_no_difference is null then 'NA' else vcc.mentions_no_difference end as mentions_no_difference,
	case when vcc.explained_decision_process is null then 'NA' else vcc.explained_decision_process end as explained_decision_process,
	case 
		when fspa.arrival_time is not null and v.arrival_time > fspa.arrival_time 
		then date_part('second', v.arrival_time - fspa.arrival_time) * 1000 else null end as thinking_time_ms,
	case when ss.width is null or ss.width = '0' then 'NA' else ss.width end as screen_width,
	case when ss.height is null or ss.height = '0' then 'NA' else ss.height end as screen_height
from 
	votes v inner join snippet_pairs sp on v.snippet_pair_id = sp.snippet_pair_id and v.last_vote = true
		left join vote_comments_last_tag vc on v.vote_id = vc.vote_id
		left join vote_comments_categorizations vcc on vc.vote_comment_id = vcc.vote_comment_id
		inner join experiment_sessions e on v.experiment_session_id = e.experiment_session_id
		inner join working_sets_snippet_pairs wssp on wssp.working_set_id = e.working_set_id and wssp.snippet_pair_id = sp.snippet_pair_id 
		left join first_snippet_pair_appearances fspa on fspa.experiment_session_id = e.experiment_session_id and fspa.snippet_pair_order = wssp.presentation_order
		--left join profiles p on e.profile_id = p.profile_id
		left join experiment_sessions_and_profiles p on e.profile_id = p.profile_id
		left join screen_sizes ss on e.experiment_session_id = ss.experiment_session_id
where 
	v.arrival_time between '20170801' and '20170909'
order by 
	sp.practice, sp.alias, v.vote_id; 
--select cast(sum(div) as float) / sum(tot), lenclass from vcclasses group by lenclass;
copy vsummary to '/tmp/categories-data.csv' csv header;
