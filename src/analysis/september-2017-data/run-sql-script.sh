#!/bin/bash

run_sql_script() {
	if [ -z $1 ]; then
		echo "Please, inform a valid SQL script file name"
		return
	fi

	psql -U postgres -d readability_dump_20170909_122300 -w -f "$1"
}
