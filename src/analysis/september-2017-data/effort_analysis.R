run_test <- function() {
	e = data.frame(practice=d$practice, divergent=d$divergent)
	e$requires_effort = factor(rep('N', length(d$practice)), levels=c('Y', 'N'))
	print(paste('Practices that requires effort: ', paste(require_effort, sep=','), collapse=''))
	for (practice in require_effort) {
		e[which(e$practice == practice), 'requires_effort'] = 'Y'
	}
	t = table(e$divergent, e$requires_effort)
	print(t)
	chisq.test(t)
	cramersV(t)
}
