***** PRÁTICA P.01 *****
Votos totais: 61
Votos divergentes: 11
Proporção de divergentes: 0.1803279
Valor-p: 0.0000011

***** PRÁTICA P.02 *****
Votos totais: 78
Votos divergentes: 39
Proporção de divergentes: 0.5
Valor-p: 1

***** PRÁTICA P.03 *****
Votos totais: 71
Votos divergentes: 9
Proporção de divergentes: 0.1267606
Valor-p: 0

***** PRÁTICA P.04 *****
Votos totais: 57
Votos divergentes: 34
Proporção de divergentes: 0.5964912
Valor-p: 0.1853263

***** PRÁTICA P.05 *****
Votos totais: 62
Votos divergentes: 21
Proporção de divergentes: 0.3387097
Valor-p: 0.0158217

***** PRÁTICA P.06 *****
Votos totais: 59
Votos divergentes: 12
Proporção de divergentes: 0.2033898
Valor-p: 0.0000096

***** PRÁTICA P.07 *****
Votos totais: 72
Votos divergentes: 13
Proporção de divergentes: 0.1805556
Valor-p: 0.0000001

***** PRÁTICA P.08 *****
Votos totais: 82
Votos divergentes: 12
Proporção de divergentes: 0.1463415
Valor-p: 0

***** PRÁTICA P.09 *****
Votos totais: 58
Votos divergentes: 18
Proporção de divergentes: 0.3103448
Valor-p: 0.0058257

***** PRÁTICA P.10 *****
Votos totais: 60
Votos divergentes: 30
Proporção de divergentes: 0.5
Valor-p: 1

***** PRÁTICA P.11 *****
Votos totais: 69
Votos divergentes: 14
Proporção de divergentes: 0.2028986
Valor-p: 0.0000015

***** PRÁTICA P.12 *****
Votos totais: 54
Votos divergentes: 32
Proporção de divergentes: 0.5925926
Valor-p: 0.2206714

***** PRÁTICA P.13 *****
Votos totais: 76
Votos divergentes: 36
Proporção de divergentes: 0.4736842
Valor-p: 0.7307533

Melhora inconclusiva: P.13
Piora inconclusiva: P.02, P.10
Melhora conclusiva: P.01, P.03, P.05, P.06, P.07, P.08, P.09, P.11
Piora conclusiva: P.04, P.12
