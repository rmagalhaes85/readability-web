create temporary table votes_report as
select 
	sp.practice, sp.alias, 1 as unity,
	case when sp.most_readable <> v.snippet_order then 1 else 0 end as divergent
from
	snippet_pairs sp inner join votes v on sp.snippet_pair_id = v.snippet_pair_id
where
	v.arrival_time between '20170801' and '20170909' and
	v.last_vote = true

order by
	sp.alias;

create temporary table votes_summary as
select practice, alias, sum(unity) as total, sum(divergent) as divergent
from votes_report
group by practice, alias
order by alias;

copy votes_summary to '/tmp/votes-summary.csv' csv header;
