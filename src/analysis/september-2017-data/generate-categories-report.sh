#!/bin/bash

# executa os comandos de importação de dados de categorias de comentários para 
# a base postgresql, comandos de exportação de dados do banco e execução dos
# scripts R para geração do relatório

. ./run-sql-script.sh

# copiando a planilha vote-comments-categorizations.csv (preenchida manualmente
# com comentários classificados em categorias) para o diretório /tmp
# de modo que possa ser carregada pelo postgresql e utilizada para a composição
# do arquivo de dados que serão processados pelo script categories-analysis.R
cp ./vote-comments-categorizations.csv /tmp/vote-comments-categorizations.csv

run_sql_script query-categories-data.sql

if [ $? -ne 0 ]
then
	echo "SQL script failed" 2>&1
	exit 1
fi

cp /tmp/categories-data.csv .
cp /tmp/experiment-sessions-and-profiles.csv .

R --quiet --vanilla --slave -f categories-analysis.R

