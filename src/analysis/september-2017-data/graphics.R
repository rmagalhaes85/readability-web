library(plyr)

# Gera gráficos diversos em PDF

sessions <- read.csv('experiment-sessions-and-profiles.csv')
categories_data <- read.csv('categories-data.csv')
practices <- c('P.01', 'P.02', 'P.03', 'P.04', 'P.05', 'P.06', 'P.07', 'P.08', 'P.09', 'P.10', 'P.11', 'P.12', 'P.13')

# preparação de colunas especiais envolvendo tamanho da tela
# os modelos de tela aqui foram classificados de acordo com os breakpoints definidos pela biblioteca
# bootstrap (a biblioteca foi utilizada para o desenvolvimento da aplicação web e então seus breakpoints
# influenciam o formato assumido pela tela)
attach(sessions)
sessions <- cbind(sessions, screen_landscape=ifelse((screen_width / screen_height) > 1, 'Y', 'N'))
sessions <- cbind(sessions, 
		  screen_model=factor(ifelse(screen_width < 576, 'SM', ifelse(screen_width < 768, 'MD', ifelse(screen_width < 992, 'LG', 'XL'))), 
				      ordered=TRUE, 
				      levels=c('SM', 'MD', 'LG', 'XL')))
detach(sessions)

attach(categories_data)
categories_data <- cbind(categories_data, screen_landscape=ifelse((screen_width / screen_height) > 1, 'Y', 'N'))
categories_data <- cbind(categories_data, screen_model=factor(ifelse(screen_width < 576, 'SM', ifelse(screen_width < 768, 'MD', ifelse(screen_width < 992, 'LG', 'XL'))), 
							      ordered=TRUE, 
							      levels=c('SM', 'MD', 'LG', 'XL')))
detach(categories_data)

# Funções auxiliares
generate_barplot <- function(basename, table, labels, main, las=0, height=7,
			     mar=c(5, 4, 4, 2) + 0.1) {
	pdf(file=paste(c("./graph-output/", basename, ".pdf"), collapse=''), height=height)
	par(las=las)
	mar=mar-c(0, 0, 4, 0)
	par(mar=mar)
	ylim = c(0, max(table) * 1.2)
	bp = barplot(table, names.arg=labels, main="", ylim=ylim,
		     cex.main=1.8, cex.names=1.4, col=c('white'))
	graph_values = as.vector(table)
	frequencies = (graph_values / sum(graph_values)) * 100
	texts = paste(graph_values, '\n(', round(frequencies, 2), ' %)', sep='')
	text(x=bp, y=graph_values, labels=texts, pos=3, cex=1.4)
	box(which="outer", lty="solid")
	dev.off()
}

# frequência de gêneros (incluindo NAs)
gender_graphics <- function() {
	t = table(sessions$profile_gender, useNA="ifany")
	generate_barplot("graph-gender", t, c("Fem.", "Masc.", "N. Inf."),
			 "Gênero")
}

# Divisão por faixa etária (incluindo NAs)
age_graphics <- function() {
	t = table(sessions$profile_age, useNA="ifany")
	generate_barplot("graph-age", t, 
			 c("< 21", "21 a 36", "> 36", "N. Inf."), 
			 "Faixa Etária")
}

# Tempo de Experiência com programação
programming_exp_graphics <- function() {
	t = table(sessions$profile_programming_experience_time, useNA="ifany")
	generate_barplot("graph-prog-exp", t,
			 c("< 4 anos", "4 a 7 anos", "> 7 anos", "N. Inf."),
			 "Tempo de Experiência em Programação")
}

# Tempo de Experiencia com Java
java_exp_graphics <- function() {
	t = table(sessions$profile_java_programming_experience_time, useNA="ifany")
	generate_barplot("graph-java-exp", t,
			 c("< 4 anos", "4 a 7 anos", "> 7 anos", "N. Inf."),
			 "Tempo de Experiência com Java")
}

# Tempo de Experiência FLOSS
floss_exp_graphics <- function() {
	t = table(sessions$profile_floss_experience_time, useNA="ifany")
	generate_barplot("graph-floss-exp", t,
			 c("< 4 anos", "4 a 7 anos", "> 7 anos", "N. Inf."),
			 "Tempo de Experiência com Software Livre")
}

# Grau de instrução formal
school_graphics <- function() {
	school_fac = factor(sessions$profile_years_in_school)
	ordered = factor(school_fac, levels=c('hs', 'b', 'm', 'doo'), ordered=TRUE)
	t = table(ordered, useNA="ifany")
	generate_barplot("graph-school", t, las=2, mar=c(9, 4, 4, 2) + 0.1,
			 c("Ensino\nMédio", "Bacharelado", 
			   "Mestrado", "Doutorado\nou além", "N. Inf."),
			 "Grau de Instrução Formal")
}

# Modelos de Telas
screen_model_graphics <- function() {
	t = table(sessions$screen_model)
	generate_barplot("graph-screen-models", t, 
			 mar=c(8, 4, 4, 2) + 0.1, las=2,
			 c('< 576px', 'entre 576\ne 767px', 'entre 768\ne 991px', '> 992px'), "Modelos de Tela")
}

# Orientações de Tela
screen_orientation_graphics <- function() {
	t = table(sessions$screen_landscape)
	generate_barplot("graph-screen-orientations", t,
			 c("Retrato", "Paisagem"), "Orientações de Tela")
}


# Tempos de Pensamento por Prática de Codificação
thinking_time_per_prac_graphics <- function() {
	pdf(file="./graph-output/graph-thinking-per-practice.pdf", width=14)
	boxplot(thinking_time_ms / 1000 ~ practice, data=categories_data, las=2, notch=TRUE, at=seq(1, 39, by=3)
		#main="Tempos de Pensamento\npor Prática (em segundos)")
		)
	grid(NA, 5, col="gray")
	mean_line=mean(categories_data$thinking_time_ms, na.rm=TRUE) / 1000
	abline(h=mean_line, lty="dotdash", lwd=1)
	text(x=2.5, y=mean_line + 0.1, labels=c("Média"), pos=3)
	box(which="outer", lty="solid")
	dev.off()
}

# Tempos de Pensamento por Práticas agrupadas conforme suas efetividades
thinking_time_per_prac_group <- function() {
	pdf(file="./graph-output/graph-thinking-per-prac-group.pdf")
	cd = categories_data[, c('practice', 'thinking_time_ms')] 
	rows = length(categories_data$practice)
	cd = cbind(cd, conclusion=factor(character(rows), levels=c('CI', 'II', 'IW')))
	eff_data = read.csv('practice-effectivity-results.csv')
	for (p in practices) {
		conclusion = eff_data[which(eff_data$practice == p), 'conclusion']	
		cd[which(cd$practice == p), 'conclusion'] = conclusion
	}
	rm(eff_data)
	boxplot(thinking_time_ms / 1000 ~ conclusion, data=cd, notch=TRUE,
		#main="Tempos de Pensamento\npor Grupo de Efetividade (em segundos)",
		names=c("Melhora\nConclusiva", "Melhora\nInconclusiva", "Piora\nInconclusiva"),
		at=seq(1, 4, length.out=3))
	grid(NA, 5, col="gray")
	mean_line=mean(cd$thinking_time_ms, na.rm=TRUE) / 1000
	abline(h=mean_line, lty="dotdash", lwd=1)
	text(x=1.75, y=mean_line + 0.2, labels=c("Média"), pos=3)
	box(which="outer", lty="solid")
	dev.off()
}

thinking_time_per_divergence <- function() {
	pdf(file="./graph-output/graph-thinking-per-divergence.pdf")
	boxplot(thinking_time_ms / 1000 ~ divergent, data=categories_data, notch=TRUE,
		names=c("Aderente", "Violador"))
	grid(NA, 5, col="gray")
	mean_line=mean(categories_data$thinking_time_ms, na.rm=TRUE) / 1000
	abline(h=mean_line, lty="dotdash", lwd=1)
	text(x=2.5, y=mean_line + 0.1, labels=c("Média"), pos=3)
	box(which="outer", lty="solid")
	dev.off()
}


# CHAMADAS DAS FUNÇÕES GERADORAS DOS GRÁFICOS
gender_graphics()
age_graphics()
programming_exp_graphics()
java_exp_graphics()
floss_exp_graphics()
school_graphics()
screen_model_graphics()
screen_orientation_graphics()
thinking_time_per_prac_graphics()
thinking_time_per_prac_group()
thinking_time_per_divergence()

