***** PRÁTICA P.01 *****
Votos totais: 55
Votos divergentes: 8
Proporção de divergentes: 0.1454545
Valor-p: 0.0000003

***** PRÁTICA P.02 *****
Votos totais: 65
Votos divergentes: 31
Proporção de divergentes: 0.4769231
Valor-p: 0.8040807

***** PRÁTICA P.03 *****
Votos totais: 66
Votos divergentes: 9
Proporção de divergentes: 0.1363636
Valor-p: 0

***** PRÁTICA P.04 *****
Votos totais: 53
Votos divergentes: 32
Proporção de divergentes: 0.6037736
Valor-p: 0.1695641

***** PRÁTICA P.05 *****
Votos totais: 56
Votos divergentes: 17
Proporção de divergentes: 0.3035714
Valor-p: 0.0050123

***** PRÁTICA P.06 *****
Votos totais: 57
Votos divergentes: 11
Proporção de divergentes: 0.1929825
Valor-p: 0.0000067

***** PRÁTICA P.07 *****
Votos totais: 68
Votos divergentes: 11
Proporção de divergentes: 0.1617647
Valor-p: 0

***** PRÁTICA P.08 *****
Votos totais: 71
Votos divergentes: 8
Proporção de divergentes: 0.1126761
Valor-p: 0

***** PRÁTICA P.09 *****
Votos totais: 52
Votos divergentes: 15
Proporção de divergentes: 0.2884615
Valor-p: 0.0035892

***** PRÁTICA P.10 *****
Votos totais: 54
Votos divergentes: 26
Proporção de divergentes: 0.4814815
Valor-p: 0.8917559

***** PRÁTICA P.11 *****
Votos totais: 61
Votos divergentes: 11
Proporção de divergentes: 0.1803279
Valor-p: 0.0000011

***** PRÁTICA P.12 *****
Votos totais: 46
Votos divergentes: 28
Proporção de divergentes: 0.6086957
Valor-p: 0.1845161

***** PRÁTICA P.13 *****
Votos totais: 62
Votos divergentes: 29
Proporção de divergentes: 0.4677419
Valor-p: 0.703203

Melhora inconclusiva: P.02, P.10, P.13
Piora inconclusiva: P.04, P.12
Melhora conclusiva: P.01, P.03, P.05, P.06, P.07, P.08, P.09, P.11
Piora conclusiva: 
