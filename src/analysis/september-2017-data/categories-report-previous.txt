
=============[ Faixa etária ]=============
Níveis: A-0-20
Níveis: B-21-36
Níveis: C-37-I
Frequências: 15
Frequências: 330
Frequências: 134
Geral  Valor-p:0.1099; df:NA; x-squared:4.48505019015909;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.1214; df:NA; x-squared:6.33971774193548;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.0285; df:NA; x-squared:6.83916083916084;cramers-V:0.424238042636469; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.03  Valor-p:1; df:NA; x-squared:0.177994844661511;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.3708; df:NA; x-squared:1.07171428571429;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.3188; df:NA; x-squared:2.69387755102041;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.5227; df:NA; x-squared:1.36863354037267;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.2109; df:NA; x-squared:3.46929824561404;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.4178; df:NA; x-squared:1.58137278680572;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.4448; df:NA; x-squared:2.70340909090909;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.6772; df:NA; x-squared:1.42592592592593;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:1; df:NA; x-squared:0.331896551724138;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:1; df:NA; x-squared:0.61994301994302;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.5347; df:NA; x-squared:1.71066433566434;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.0345; df:NA; x-squared:6.80135508140623;cramers-V:0.149330254326006; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com melhora inconclusiva  Valor-p:0.2769; df:NA; x-squared:2.51587050936077;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.7336; df:NA; x-squared:1.07374608852353;cramers-V:0; conclusão: não houve efeito :

=============[ Gênero ]=============
Níveis: F
Níveis: M
Frequências: 15
Frequências: 464
Geral  Valor-p:0.7866; df:NA; x-squared:0.106274803381558;cramers-V:0; conclusão: não houve efeito :
Prática P.01  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.02  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.03  Valor-p:1; df:NA; x-squared:0.448227073227073;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:1; df:NA; x-squared:0.540706605222734;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.5222; df:NA; x-squared:1.2534435261708;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:1; df:NA; x-squared:0.383292383292383;cramers-V:0; conclusão: não houve efeito :
Prática P.07  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.08  Valor-p:1; df:NA; x-squared:0.0539811066126856;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:1; df:NA; x-squared:0.534565253258538;cramers-V:0; conclusão: não houve efeito :
Prática P.10  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.11  Valor-p:1; df:NA; x-squared:0.142241379310345;cramers-V:0; conclusão: não houve efeito :
Prática P.12  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.13  Valor-p:1; df:NA; x-squared:0.039328231292517;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:1; df:NA; x-squared:0.0000265695967652582;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:1; df:NA; x-squared:0;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:1; df:NA; x-squared:0.569879984514131;cramers-V:0; conclusão: não houve efeito :

=============[ Tempo de experiência com programação ]=============
Níveis: A_0_3
Níveis: B_4_7
Níveis: C_8_I
Frequências: 159
Frequências: 96
Frequências: 328
Geral  Valor-p:0.6192; df:NA; x-squared:1.03105372201077;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.1889; df:NA; x-squared:3.13793103448276;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.4113; df:NA; x-squared:1.95310124657951;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.4743; df:NA; x-squared:1.70162872841444;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.8861; df:NA; x-squared:0.456989769489769;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.7606; df:NA; x-squared:0.805529953917051;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.3973; df:NA; x-squared:2.32496505125815;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.1814; df:NA; x-squared:3.86060606060606;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.3503; df:NA; x-squared:1.9884904622157;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.6522; df:NA; x-squared:1.10171927257525;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.2434; df:NA; x-squared:2.92366600790514;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.4293; df:NA; x-squared:1.87262737262737;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:0.91; df:NA; x-squared:0.247361111111111;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:1; df:NA; x-squared:0.158730158730159;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.1609; df:NA; x-squared:3.93017879731311;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.2354; df:NA; x-squared:3.01412872841444;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.7546; df:NA; x-squared:0.672288359788359;cramers-V:0; conclusão: não houve efeito :

=============[ Tempo de experiência com programação Java ]=============
Níveis: A_0_3
Níveis: B_4_7
Níveis: C_8_I
Frequências: 385
Frequências: 103
Frequências: 90
Geral  Valor-p:0.6037; df:NA; x-squared:1.00918970263496;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.8186; df:NA; x-squared:0.972222222222222;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.4113; df:NA; x-squared:1.79684692777578;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.8491; df:NA; x-squared:0.47874876446305;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.0715; df:NA; x-squared:5.7890148046398;cramers-V:0.395550072158835; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.05  Valor-p:0.3243; df:NA; x-squared:2.65806451612903;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.4613; df:NA; x-squared:1.85273368606702;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.4703; df:NA; x-squared:2.15353535353535;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.1574; df:NA; x-squared:3.42976305609284;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:1; df:NA; x-squared:0.0509511993382961;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.08; df:NA; x-squared:5.34248500749625;cramers-V:0.344560428742873; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.11  Valor-p:0.4583; df:NA; x-squared:1.52700831024931;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:0.1209; df:NA; x-squared:4.50589285714286;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.4788; df:NA; x-squared:1.91358024691358;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.7336; df:NA; x-squared:0.707522878417673;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:1; df:NA; x-squared:0.0376271015805901;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.0455; df:NA; x-squared:6.14851050310234;cramers-V:0.284431809030194; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :

=============[ Tempo de experiência com software livre ]=============
Níveis: A_0_3
Níveis: B_4_7
Níveis: C_8_I
Frequências: 415
Frequências: 64
Frequências: 104
Geral  Valor-p:0.5702; df:NA; x-squared:1.13170883751285;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.0975; df:NA; x-squared:5.09876543209877;cramers-V:0.348423846707903; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.02  Valor-p:0.3458; df:NA; x-squared:2.05617412478826;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.8506; df:NA; x-squared:0.878021978021978;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:1; df:NA; x-squared:0.026986768573307;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.2779; df:NA; x-squared:2.77754043552905;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.8376; df:NA; x-squared:0.58392199017199;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.5872; df:NA; x-squared:1.3044922913344;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.4913; df:NA; x-squared:1.19893617021277;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.5422; df:NA; x-squared:1.82880108173077;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.2694; df:NA; x-squared:3.20638820638821;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.5912; df:NA; x-squared:1.67020114388535;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:0.7806; df:NA; x-squared:0.522321428571429;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.9105; df:NA; x-squared:0.301724137931035;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.3113; df:NA; x-squared:2.31915742669956;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.7761; df:NA; x-squared:0.489249271137026;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:1; df:NA; x-squared:0.189090450995213;cramers-V:0; conclusão: não houve efeito :

=============[ Escolaridade ]=============
Níveis: b
Níveis: doo
Níveis: hs
Níveis: m
Frequências: 276
Frequências: 25
Frequências: 60
Frequências: 118
Geral  Valor-p:0.4248; df:NA; x-squared:2.81655694962888;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.018; df:NA; x-squared:11.6269444444444;cramers-V:0.55314755917587; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.02  Valor-p:0.022; df:NA; x-squared:9.03349282296651;cramers-V:0.48756896470266; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.03  Valor-p:0.7221; df:NA; x-squared:1.42979365079365;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.6842; df:NA; x-squared:1.79313725490196;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.3898; df:NA; x-squared:2.12631578947368;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.004; df:NA; x-squared:13.9997970779221;cramers-V:0.599140347367923; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.07  Valor-p:0.3858; df:NA; x-squared:2.72064777327935;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:1; df:NA; x-squared:1.31756756756757;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.3173; df:NA; x-squared:3.8752052545156;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.05; df:NA; x-squared:7.17013888888889;cramers-V:0.452615853800009; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.11  Valor-p:0.8171; df:NA; x-squared:0.846336206896552;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:0.8301; df:NA; x-squared:1.15450980392157;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.4888; df:NA; x-squared:2.97440476190476;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.3548; df:NA; x-squared:3.18190717791008;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.076; df:NA; x-squared:7.12549788381262;cramers-V:0.253364706754794; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com piora inconclusiva  Valor-p:0.946; df:NA; x-squared:0.468370940393896;cramers-V:0; conclusão: não houve efeito :

=============[ Perfil de aprendizagem ]=============
Níveis: dk
Níveis: n
Níveis: t
Níveis: v
Frequências: 71
Frequências: 25
Frequências: 190
Frequências: 169
Geral  Valor-p:0.6192; df:NA; x-squared:1.87375180370192;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.8871; df:NA; x-squared:0.72;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.9285; df:NA; x-squared:0.695970695970696;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.8721; df:NA; x-squared:1.03236424394319;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.036; df:NA; x-squared:7.55582417582418;cramers-V:0.493696530002251; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.05  Valor-p:0.6332; df:NA; x-squared:2.11990030171848;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:1; df:NA; x-squared:0.744681201151789;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.2249; df:NA; x-squared:4.73400673400673;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.2364; df:NA; x-squared:4.05982905982906;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.3928; df:NA; x-squared:3.3645552013909;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.0195; df:NA; x-squared:8.31461131461132;cramers-V:0.517893105006495; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.11  Valor-p:1; df:NA; x-squared:0.424276377217554;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:0.0715; df:NA; x-squared:5.17602305102305;cramers-V:0.422473139363384; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.13  Valor-p:0.0955; df:NA; x-squared:6.21650326797386;cramers-V:0.42144320979815; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com melhora conclusiva  Valor-p:0.6077; df:NA; x-squared:1.95346558413863;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.9305; df:NA; x-squared:0.462838915470494;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.5462; df:NA; x-squared:2.40781440781441;cramers-V:0; conclusão: não houve efeito :

=============[ Comentou com um único ponto ]=============
Níveis: N
Níveis: Y
Frequências: 494
Frequências: 232
Geral  Valor-p:0.6027; df:NA; x-squared:0.327225279060875;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.4023; df:NA; x-squared:1.18604651162791;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:1; df:NA; x-squared:0.0422222222222221;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.7066; df:NA; x-squared:0.578947368421053;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.0725; df:NA; x-squared:3.80293680910741;cramers-V:0.2814744219103; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.05  Valor-p:0.1724; df:NA; x-squared:2.22022746728629;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:1; df:NA; x-squared:0.0613636363636363;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.4533; df:NA; x-squared:1.10413137587051;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.6867; df:NA; x-squared:0.288274966042638;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.7426; df:NA; x-squared:0.308467741935484;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.7546; df:NA; x-squared:0.339497041420119;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.0915; df:NA; x-squared:3.25820887346653;cramers-V:0.233031073516907; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.12  Valor-p:0.3208; df:NA; x-squared:1.53409090909091;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.2749; df:NA; x-squared:1.39423076923077;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.2734; df:NA; x-squared:1.55748915828151;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.3618; df:NA; x-squared:0.957131354308773;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.6312; df:NA; x-squared:0.344792032292032;cramers-V:0; conclusão: não houve efeito :

=============[ Conhecimento de Java ]=============
Níveis: N
Níveis: Y
Frequências: 145
Frequências: 309
Geral  Valor-p:0.2834; df:NA; x-squared:1.32768231642547;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.051; df:NA; x-squared:4.59130434782609;cramers-V:0.373001923296126; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.02  Valor-p:0.1234; df:NA; x-squared:3.63984674329502;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.6732; df:NA; x-squared:0.303354399611084;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:1; df:NA; x-squared:0.00442410373760489;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.7016; df:NA; x-squared:0.589285714285714;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:1; df:NA; x-squared:0.154131582703011;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.1004; df:NA; x-squared:3.3047963800905;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:1; df:NA; x-squared:0.461148648648649;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:1; df:NA; x-squared:0.027237175063262;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:1; df:NA; x-squared:0.0678733031674208;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.3018; df:NA; x-squared:1.97747601763028;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:1; df:NA; x-squared:0.0231481481481482;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.4033; df:NA; x-squared:1.17743360136452;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.2659; df:NA; x-squared:1.31781303392726;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.076; df:NA; x-squared:3.83275779947607;cramers-V:0.192902153361782; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com piora inconclusiva  Valor-p:1; df:NA; x-squared:0.0283837052129735;cramers-V:0; conclusão: não houve efeito :

=============[ Conhecimento de Javascript ]=============
Níveis: N
Níveis: Y
Frequências: 235
Frequências: 219
Geral  Valor-p:0.1399; df:NA; x-squared:2.34713680781845;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.001; df:NA; x-squared:11.2820512820513;cramers-V:0.584705346204686; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.02  Valor-p:0.3263; df:NA; x-squared:1.72727272727273;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:1; df:NA; x-squared:0.0422077922077922;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.2364; df:NA; x-squared:2.08442317916002;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.7131; df:NA; x-squared:0.29010989010989;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.6807; df:NA; x-squared:0.673597502242003;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:1; df:NA; x-squared:0.0124183006535948;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:1; df:NA; x-squared:0.263513513513514;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.7036; df:NA; x-squared:0.410309724929608;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.2729; df:NA; x-squared:1.83257918552036;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.6097; df:NA; x-squared:0.565757065076484;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:0.7156; df:NA; x-squared:0.555555555555556;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.5192; df:NA; x-squared:0.695680684554523;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.2059; df:NA; x-squared:1.85573382842479;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.0525; df:NA; x-squared:4.28632123170651;cramers-V:0.203996983408748; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com piora inconclusiva  Valor-p:0.7801; df:NA; x-squared:0.142006437460983;cramers-V:0; conclusão: não houve efeito :

=============[ Conhecimento de C++ ]=============
Níveis: N
Níveis: Y
Frequências: 178
Frequências: 276
Geral  Valor-p:0.4688; df:NA; x-squared:0.655617418182567;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.6422; df:NA; x-squared:0.645652173913043;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:1; df:NA; x-squared:0;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.3813; df:NA; x-squared:1.53573164803111;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:1; df:NA; x-squared:0.135761772853186;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.4738; df:NA; x-squared:1.11746031746032;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.2309; df:NA; x-squared:2.33766233766234;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:1; df:NA; x-squared:0.0500906244850882;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:1; df:NA; x-squared:0.263513513513514;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.7321; df:NA; x-squared:0.158558879392213;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.4458; df:NA; x-squared:0.889172746757886;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.1449; df:NA; x-squared:2.95862068965517;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:1; df:NA; x-squared:0.0226244343891403;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.7301; df:NA; x-squared:0.218144044321329;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.2624; df:NA; x-squared:1.52687325137606;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.8391; df:NA; x-squared:0.0792051009110295;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.7946; df:NA; x-squared:0.101194079454949;cramers-V:0; conclusão: não houve efeito :

=============[ Conhecimento de C ]=============
Níveis: N
Níveis: Y
Frequências: 65
Frequências: 389
Geral  Valor-p:0.1199; df:NA; x-squared:2.98640418609112;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:1; df:NA; x-squared:0.473118279569892;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.4088; df:NA; x-squared:1.57603686635945;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.3413; df:NA; x-squared:1.23106060606061;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.2914; df:NA; x-squared:1.76113360323887;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.6437; df:NA; x-squared:0.681887755102041;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:1; df:NA; x-squared:0.173160173160173;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.1654; df:NA; x-squared:2.96665763079425;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:0.098; df:NA; x-squared:8.72443389335281;cramers-V:0.472972972972973; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.09  Valor-p:1; df:NA; x-squared:0.565767195767196;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.1064; df:NA; x-squared:3.52941176470588;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.5742; df:NA; x-squared:1.01149425287356;cramers-V:0; conclusão: não houve efeito :
Prática P.12  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.13  Valor-p:0.1954; df:NA; x-squared:2.33141447368421;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.6752; df:NA; x-squared:0.321035991573836;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.018; df:NA; x-squared:6.49940062500616;cramers-V:0.251199078306047; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com piora inconclusiva  Valor-p:0.2849; df:NA; x-squared:1.87934362934363;cramers-V:0; conclusão: não houve efeito :

=============[ Conhecimento de C# ]=============
Níveis: N
Níveis: Y
Frequências: 230
Frequências: 224
Geral  Valor-p:0.0295; df:NA; x-squared:5.067722816903;cramers-V:0.10565218908881; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.01  Valor-p:0.3693; df:NA; x-squared:1.33086419753086;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.7421; df:NA; x-squared:0.422222222222222;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:1; df:NA; x-squared:0.0787878787878787;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:0.4358; df:NA; x-squared:0.814783281733746;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:1; df:NA; x-squared:0.109126984126984;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.4013; df:NA; x-squared:0.925270016179107;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:0.032; df:NA; x-squared:5.52249134948097;cramers-V:0.381220041082815; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.08  Valor-p:1; df:NA; x-squared:0.0125482625482625;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:0.4808; df:NA; x-squared:0.70682261208577;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:0.1354; df:NA; x-squared:3.39366515837104;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.1314; df:NA; x-squared:2.9362684729064;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:1; df:NA; x-squared:0;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.4723; df:NA; x-squared:0.940241228070176;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.1174; df:NA; x-squared:2.57015652126083;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.3323; df:NA; x-squared:1.16517312735241;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.7716; df:NA; x-squared:0.253769394394394;cramers-V:0; conclusão: não houve efeito :

=============[ Explicou seu processo decisório ]=============
Níveis: N
Níveis: Y
Frequências: 159
Frequências: 277
Geral  Valor-p:0.6662; df:NA; x-squared:0.274361655819082;cramers-V:0; conclusão: não houve efeito :
Prática P.01  Valor-p:0.3278; df:NA; x-squared:1.30243055555556;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:0.5202; df:NA; x-squared:0.465028830818305;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:0.7066; df:NA; x-squared:0.198979591836735;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:1; df:NA; x-squared:0.187962962962963;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:1; df:NA; x-squared:0.0826071169208423;cramers-V:0; conclusão: não houve efeito :
Prática P.06  Valor-p:0.2839; df:NA; x-squared:2.07357859531773;cramers-V:0; conclusão: não houve efeito :
Prática P.07  Valor-p:1; df:NA; x-squared:0.0503496503496503;cramers-V:0; conclusão: não houve efeito :
Prática P.08  Valor-p:1; df:NA; x-squared:0.114866908650938;cramers-V:0; conclusão: não houve efeito :
Prática P.09  Valor-p:1; df:NA; x-squared:0.0132285601221097;cramers-V:0; conclusão: não houve efeito :
Prática P.10  Valor-p:1; df:NA; x-squared:0.0611111111111112;cramers-V:0; conclusão: não houve efeito :
Prática P.11  Valor-p:0.076; df:NA; x-squared:5.71428571428571;cramers-V:0.377964473009227; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.12  Valor-p:0.4368; df:NA; x-squared:0.914439946018894;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:1; df:NA; x-squared:0.0222910216718266;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.7456; df:NA; x-squared:0.25480349867245;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora inconclusiva  Valor-p:0.8371; df:NA; x-squared:0.109143216015148;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.3843; df:NA; x-squared:0.862293735676089;cramers-V:0; conclusão: não houve efeito :

=============[ Afirma não ter visto diferença ]=============
Níveis: N
Níveis: Y
Frequências: 408
Frequências: 28
Geral  Valor-p:0.053; df:NA; x-squared:3.9458395122755;cramers-V:0.0951319642627287; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Prática P.01  Valor-p:0.2464; df:NA; x-squared:2.61797573435504;cramers-V:0; conclusão: não houve efeito :
Prática P.02  Valor-p:1; df:NA; x-squared:0.885227272727273;cramers-V:0; conclusão: não houve efeito :
Prática P.03  Valor-p:1; df:NA; x-squared:0.461148648648649;cramers-V:0; conclusão: não houve efeito :
Prática P.04  Valor-p:1; df:NA; x-squared:0.466071428571428;cramers-V:0; conclusão: não houve efeito :
Prática P.05  Valor-p:0.6847; df:NA; x-squared:0.55493620711012;cramers-V:0; conclusão: não houve efeito :
Prática P.06  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.07  Valor-p:1; df:NA; x-squared:0.0935064935064935;cramers-V:0; conclusão: não houve efeito :
Prática P.08  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.09  Valor-p:0.1634; df:NA; x-squared:2.72267625585703;cramers-V:0; conclusão: não houve efeito :
Prática P.10  A categoria avaliada não apresenta níveis suficientes para o teste; :
Prática P.11  Valor-p:0.1129; df:NA; x-squared:9.23076923076923;cramers-V:0; conclusão: não houve efeito :
Prática P.12  Valor-p:1; df:NA; x-squared:0.23165811965812;cramers-V:0; conclusão: não houve efeito :
Prática P.13  Valor-p:0.2074; df:NA; x-squared:2.36678200692042;cramers-V:0; conclusão: não houve efeito :
Práticas com melhora conclusiva  Valor-p:0.008; df:NA; x-squared:9.20905539238208;cramers-V:0.185025421667214; conclusão: EFEITO SIGNIFICATIVO, MAS FRACO :
Práticas com melhora inconclusiva  Valor-p:0.6097; df:NA; x-squared:0.381207865631542;cramers-V:0; conclusão: não houve efeito :
Práticas com piora inconclusiva  Valor-p:0.6452; df:NA; x-squared:0.335561136976231;cramers-V:0; conclusão: não houve efeito :
