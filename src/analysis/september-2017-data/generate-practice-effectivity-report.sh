#!/bin/bash

# Gera um relatório com os dados sobre a efetividade das práticas,
# de modo que possa ser legível por humanos

# Provavelmente farei o relatório conter também comandos latex
# para impressão de tabelas, tão logo eu saiba quais tabelas eu
# vou querer incluir no texto

. ./run-sql-script.sh

run_sql_script query-proportions-data.sql

if [ $? -ne 0 ]
then
	echo "SQL script failed" 2>&1
	exit 1
fi

cp /tmp/votes-summary.csv .

R --quiet --vanilla --slave -f practice-effectivity.R

