#!/bin/bash

# executa os comandos de importação de dados de categorias de comentários para 
# a base postgresql, comandos de exportação de dados do banco e execução dos
# scripts R para geração do relatório

. ./run-sql-script.sh

# copiando a planilha vote-comments-categorizations.csv (preenchida manualmente
# com comentários classificados em categorias) para o diretório /tmp
# de modo que possa ser carregada pelo postgresql e utilizada para a composição
# do arquivo de dados que serão processados pelo script categories-analysis.R

R --quiet --vanilla --slave -f post-tests.R

