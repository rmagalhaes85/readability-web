#!/bin/bash

JAVASCRIPT=$(wc -l ./js/dist/readability-app.js | tail -n1 | cut -d' ' -f1)
JAVA=$(find java -name '*.java' | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
CSS=$(find . -name '*.css' | grep -v node_modules | grep -v bootstrap | grep -v exploded_build | grep -v reports | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
PYTHON=$(find . -name '*.py' | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
SQL=$(find db -name '*.sql' | grep -v dump |  xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
JSP=$(find . -name '*.jsp' | grep -v exploded_build | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
SHELL=$(find . -name '*.sh' | grep -v node_modules | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
R=$(find . -name '*.R' | grep -v node_modules | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)
MESSAGES=$(wc -l ./java/readability-web/codegen/messages.json | sed 's/^\s*//' | cut -d' ' -f1)
PROCEEDINGS=$(find . -name 'procedimentos*.html' | xargs wc -l | tail -n1 | sed 's/^\s*//' | cut -d' ' -f1)

echo "JAVASCRIPT=$JAVASCRIPT"
echo "JAVA=$JAVA"
echo "CSS=$CSS"
echo "PYTHON=$PYTHON"
echo "SQL=$SQL"
echo "JSP=$JSP"
echo "SHELL=$SHELL"
echo "R=$R"
echo "MESSAGES=$MESSAGES"
echo "PROCEEDINGS=$PROCEEDINGS"
echo "-----"
TOTAL=$((JAVASCRIPT+JAVA+CSS+PYTHON+SQL+JSP+SHELL+R+MESSAGES+PROCEEDINGS))
echo "TOTAL=$TOTAL"
