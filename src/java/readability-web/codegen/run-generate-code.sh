#!/bin/bash

FIELDS_FILE="/tmp/fields"
DEFINITION_FILE="/tmp/def"
OUTPUT_FILE="/tmp/out"
ACCUMULATED_FILE="/tmp/acc"

echo '' > "$FIELDS_FILE"
echo '' > "$OUTPUT_FILE"
echo '' > "$ACCUMULATED_FILE"

echo 'flossExperienceTime' >> "$FIELDS_FILE"
echo 'age' >> "$FIELDS_FILE"
echo 'yearsInSchool' >> "$FIELDS_FILE"
echo 'gender' >> "$FIELDS_FILE"
echo 'programmingLanguages' >> "$FIELDS_FILE"
echo 'naturalLanguages' >> "$FIELDS_FILE"
echo 'operatingSystems' >> "$FIELDS_FILE"
echo 'englishConfortLevel' >> "$FIELDS_FILE"
echo 'perceivedReadabilityLevel' >> "$FIELDS_FILE"

for field in $(cat "$FIELDS_FILE");
do
	echo "{ \"fieldName\": \"$field\" }" > "$DEFINITION_FILE"
	./generate-code.py --definition "$DEFINITION_FILE" --schema ./fields/required-field-template-schema.json --template ./fields/required-field-template.xml > "$OUTPUT_FILE"
	cat "$OUTPUT_FILE" >> "$ACCUMULATED_FILE"
done

cat "$ACCUMULATED_FILE" | xclip

