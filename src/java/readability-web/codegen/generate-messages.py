#!/usr/bin/env python3
import os.path
import json

class MessagesProcessor:
    def __init__(self):
        self.BASE_PATH = "../src/main/resources/"
        self.languages = ['en', 'eo', 'pt', 'es']

    def __load(self):
        self.__check_messages_json_file()
        self.__init_write_handles()
        self.__init_messages_content()
        self.__open_write_handles()

    def __init_write_handles(self):
        self.write_handles = dict()
        for lang in self.languages:
            self.write_handles[lang] = None

    def __init_messages_content(self):
        self.messages_content = dict()
        for lang in self.languages:
            self.messages_content[lang] = self.__get_file_content(lang)

    def __check_messages_json_file(self):
        if not os.path.isfile("messages.json"):
            raise Exception("'messages.json': file not found")

    def __get_filename(self, lang):
        if lang == 'en': 
            filename = 'messages.properties'
        else: 
            filename = 'messages_' + lang + '.properties'
        return filename

    def __get_proceedings_filename(self, lang):
        return 'procedimentos_' + lang + '.html'
    
    def __get_proceedings_content(self, lang):
        contents = []
        with open(self.__get_proceedings_filename(lang), "r", encoding="utf8") as f:
            for line in f:
                contents.append(line.replace('\n', '') + ' \\')
            contents.append('')
        return '\n'.join(contents)

    def __get_file_content(self, lang):
        filename = self.__get_filename(lang)
        f = self.__get_read_handle(lang)
        content = f.read()
        f.close()
        return content

    def __get_file_handle(self, lang, mode):
        filename = self.__get_filename(lang)
        return open(self.BASE_PATH + filename, mode, encoding="utf8")

    def __get_write_handle(self, lang):
        return self.__get_file_handle(lang, "w")

    def __get_read_handle(self, lang):
        return self.__get_file_handle(lang, "r")

    def __open_write_handles(self):
        for lang in self.languages:
            self.write_handles[lang] = self.__get_write_handle(lang)

    def __close_write_handles(self):
        for lang in self.languages:
            f = self.write_handles[lang]
            if not f.closed:
                f.close()

    def __warn_language_lack(self, message_key):
        print('The message of key %s is not defined for all the supported languages' % message_key)
    
    def process(self):
        self.__load()

        message_properties_file = dict()
        for lang in self.languages:
            message_properties_file[lang] = []

        with open("messages.json", "r", encoding="utf8") as messages_file:
            messages = json.load(messages_file)
            for message_key in iter(messages):
                if not type(messages[message_key]) is dict:
                    raise Exception('message_key object type: expected \'dict\', got %s' % type(message_key))

                lang_ok = dict()
                for lang in self.languages:
                    lang_ok[lang] = False

                for language_key in iter(messages[message_key]):
                    content = messages[message_key][language_key]
                    message_properties_file[language_key].append('%s=%s' % (message_key, content))
                    lang_ok[language_key] = True

                all_lang_ok = True
                for lang in self.languages:
                    all_lang_ok &= lang_ok[lang]

                if not all_lang_ok:
                    self.__warn_language_lack(message_key)

        for lang in self.languages:
            proceedings_content = self.__get_proceedings_content(lang)
            message_properties_file[lang].append('instructions.text=%s\n' % proceedings_content)
            message_properties_file[lang].sort()
            f = self.write_handles[lang]
            #f.writelines(message_properties_file[lang])
            f.write('\n'.join(message_properties_file[lang]))

        self.__close_write_handles()

def main():
    processor = MessagesProcessor()
    processor.process()

main()
