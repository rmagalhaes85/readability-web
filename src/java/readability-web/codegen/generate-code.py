#!/usr/bin/env python3
import argparse
import json
import os.path

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--definition', 
        help='JSON file containing definitions to instantiate a template',
        required=True)
    parser.add_argument('--schema',
        help='Schema for definitions contraints and default values',
        required=True)
    parser.add_argument('--template',
        help='Template file',
        required=True)
    return parser.parse_args()

def extract_file_names(args):
    return args.definition, args.schema, args.template

def check_file(filename):
    if not os.path.isfile(filename):
        raise IOError('{f}: not found'.format(f=filename))

def create_augmented_definition(schema_obj, definition_obj):
    augmented_definition = dict()

    for key_name in iter(schema_obj):
        schema_key_def = schema_obj[key_name] 
        if 'required' in schema_key_def and schema_key_def['required']:
            if not key_name in definition_obj:
                raise Exception('The definition object doesn\'t contain the mandatory {s} property'.format(s=key_name))
            augmented_definition[key_name] = definition_obj[key_name]

        if 'default' in schema_key_def and not key_name in definition_obj:
            augmented_definition[key_name] = schema_key_def['default']
    
    for def_key in iter(definition_obj):
        if not def_key in augmented_definition:
            augmented_definition[def_key] = definition_obj[def_key]

    return augmented_definition

def instantiate_template(augmented_definition, template_str):
    template_instance_str = template_str
    for definition_key in iter(augmented_definition):
        template_instance_str = template_instance_str.replace(
            '<%' + definition_key + '%>', augmented_definition[definition_key])
    return template_instance_str

def main():
    try:
        args = parse_args()
        definition_path, schema_path, template_path = extract_file_names(args)
        check_file(definition_path)
        check_file(schema_path)
        check_file(template_path)

        definition_obj = json.load(open(definition_path, "r"))
        schema_obj = json.load(open(schema_path, "r"))

        augmented_definition = create_augmented_definition(schema_obj, definition_obj)

        template_str = open(template_path, "r").read()
        template_instance = instantiate_template(augmented_definition, template_str)
        print(template_instance)

    except Exception:
        print("Error")
        raise

main()
