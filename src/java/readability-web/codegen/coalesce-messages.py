#!/usr/bin/env python3

### Throwaway script for grouping multiple messages.properties files in
### a single json file (that json file is currently being used as the
### authoritative source for localized messages along the system

import json
import re
import os.path

LAST_BAR_PATTERN = "\s*\\\\\s*$"

def get_language(filepath):
    filename = os.path.basename(filepath)
    filepart = filename.split('.')[0]
    if filepart == 'messages':
        return 'en'
    return filepart.replace('messages_', '')

def ends_in_bar(text):
    return len(re.findall(LAST_BAR_PATTERN, text)) > 0

def save_message(msgdict, key, lang, value):
    if key != 'instructions.text':
        # this key will be ignored, their values are html files and we
        # are gonna treat them differently
        if not key in msgdict:
            msgdict[key] = dict()
        msgdict[key][lang] = value

def dump_json(obj):
    with open("messages.json", "w", encoding="utf8") as f:
        json.dump(obj, f, ensure_ascii=False, indent=4)
    return "File messages.json saved successfully"

def main():
    files = ['src/main/resources/messages.properties',
        'src/main/resources/messages_eo.properties',
        'src/main/resources/messages_pt.properties',
        'src/main/resources/messages_es.properties']

    messages = dict()

    for filepath in iter(files):
        current_lang = get_language(filepath)
        current_state = 'normal'
        current_key = ''
        current_value = ''
        with open(filepath, "r", encoding="utf-8") as f:
            for line in f:
                if current_state == 'normal':
                    try:
                        current_key, temp_value = line.split('=', 1)
                    except ValueError:
                        current_key = line
                        temp_value = ''

                    if ends_in_bar(temp_value):
                        current_state = 'continuation'
                        current_value += re.sub(LAST_BAR_PATTERN, '', temp_value)
                    else:
                        save_message(messages, current_key, current_lang, temp_value)
                elif current_state == 'continuation':
                    temp_value = line
                    current_value += re.sub(LAST_BAR_PATTERN, '', temp_value)
                    if not ends_in_bar(temp_value):
                        current_state = 'normal'
                        save_message(messages, current_key, current_lang, current_value)
                        current_key = ''
                        current_value = ''
                else:
                    raise Exception('Unkown state')

    print (dump_json(messages))


main()

