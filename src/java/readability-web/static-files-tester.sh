#!/bin/bash

RESULT_FILE="/tmp/ressss.txt"

FILES="img/thank-you-dog.png
html/modalMsg.html
css/common.css
css/ui-bootstrap-csp.css
css/spa.css
css/bootstrap-theme.min.css
css/pairs.css
css/languages.css
css/votecomments.css
css/bootstrap.min.css
snippets/snippet.html
snippets/included1.html
snippets/included3.html
snippets/included2.html
js/bootstrap.min.js
js/angular-resource.js
js/angular-resource.min.js
js/readability-app.min.js
js/readability-app.js
js/jquery.min.js
js/ui-bootstrap-tpls.js
js/angular.min.js
js/angular-route.min.js"

NOCACHERS="html/modalMsg.html
css/common.css
css/spa.css
css/pairs.css
css/languages.css
css/votecomments.css
snippets/snippet.html
snippets/included1.html
snippets/included3.html
snippets/included2.html
js/readability-app.min.js
js/readability-app.js"

GREEN='\033[0;32m'
RED='\033[0;31m'
NOCOLOR='\033[0m'

#while read file
#do
#	echo "Arquivo: '$file'"
#done < "$FILES"

function success() {
	echo -ne "$GREEN"
	echo "$1"
	echo -ne "$NOCOLOR"
}

function failure() {
	echo -ne "$RED"
	echo "$1"
	echo -ne "$NOCOLOR"
}

for file in $FILES; do
	echo '' > $RESULT_FILE
	echo -n "Testing $file..... "
	command="curl -v http://localhost/static/$file"
	# echo "$command"
	$command > /dev/null 2> $RESULT_FILE
	grep -P "HTTP/\d\.\d 404" $RESULT_FILE > /dev/null
	if [ $? -eq 0 ]; then
		echo 'Not found'
	else
		must_have_cache=0
		if [[ "$NOCACHERS" == *"$file"* ]]; then
			must_have_cache=1
		fi

		cache_control_present=0
		grep -i "Cache-control" $RESULT_FILE > /dev/null
		if [ $? -eq 0 ]; then
			cache_control_present=1
		fi

		if [ $must_have_cache -eq 0 ] && [ $cache_control_present -eq 1 ]; then
			failure "Has a CACHE-CONTROL header, but it shouldn't"
		elif [ $must_have_cache -eq 1 ] && [ $cache_control_present -eq 0 ]; then
			failure "Doesn't have a CACHE-CONTROL header, but it should"
		else
			success "OK"
		fi
	fi
done




