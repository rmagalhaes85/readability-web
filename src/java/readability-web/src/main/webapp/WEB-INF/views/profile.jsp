<%@ page session="false" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="readability" uri="ReadabilityLib"%>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="spa-title">
				<spring:message code="profile.title"></spring:message>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="fields-container">
				<div class="row">
					<div class="col-xs-12 text-center">
						<h4 class="optional-fields"><spring:message code="profile.alloptional"></spring:message></h4>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<form name="profileForm"
							ng-submit="submitForm(profileForm.$valid)" novalidate>
							<div uib-alert class="form-group"
								ng-repeat="alert in serverAlerts"
								ng-class="'alert-' + (alert.type || 'warning')">{{
								alert.msg }}</div>
							<readability:profileField fieldName="name" inputType="text" required="false" maxLength="100"></readability:profileField>
							<readability:profileField fieldName="email" inputType="email" required="false" validationErrors="email"></readability:profileField>
							<readability:profileField fieldName="programmingExperienceTime" inputType="number"></readability:profileField>
							<readability:profileField fieldName="javaProgrammingExperienceTime" inputType="number" ></readability:profileField>
							<readability:profileField fieldName="annotationsExperienceTime" inputType="number" ></readability:profileField>
<%-- 							<readability:profileField fieldName="flossExperienceTime" inputType="number"></readability:profileField> --%>
							<!--<readability:profileField fieldName="age" inputType="number" ></readability:profileField>-->
							<div class="form-group">
								<label for="age"><spring:message code="profileForm.labels.age"></spring:message></label>
								<select class="form-control" id="age" name="age" ng-model-options="{updateOn: 'blur'}" ng-model="profile.age">
									<option value="-12">&lt; 13</option>
									<option value="13-20">13-20</option>
									<option value="21-28">21-28</option>
									<option value="29-36">29-36</option>
									<option value="37-44">37-44</option>
									<option value="45-50">45-50</option>
									<option value="51-">&gt; 50</option>
								</select>
							</div>
							<div class="form-group">
								<label for="yearsInSchool"><spring:message code="profileForm.labels.yearsInSchool"></spring:message></label>
									<select class="form-control" id="yearsInSchool" name="yearsInSchool" ng-model-options="{updateOn: 'blur'}" ng-model="profile.yearsInSchool">
									<option value="lths"><spring:message code="profileForm.fields.yearsInSchool.options.lessThanHighSchool"></spring:message></option>
									<option value="hs"><spring:message code="profileForm.fields.yearsInSchool.options.highSchool"></spring:message></option>
									<option value="b"><spring:message code="profileForm.fields.yearsInSchool.options.bachelors"></spring:message></option>
									<option value="m"><spring:message code="profileForm.fields.yearsInSchool.options.masters"></spring:message></option>
									<option value="doo"><spring:message code="profileForm.fields.yearsInSchool.options.doctorateOrOther"></spring:message></option>
								</select>
							</div>
							<!-- <readability:profileField fieldName="gender" inputType="gender" ></readability:profileField> -->
							<readability:profileField fieldName="programmingLanguages" inputType="text"></readability:profileField>
							<readability:profileField fieldName="naturalLanguages" inputType="text"></readability:profileField>
							<readability:profileField fieldName="operatingSystems" inputType="text"></readability:profileField>
							<readability:profileField fieldName="englishConfortLevel" inputType="likert"></readability:profileField>
							<readability:profileField fieldName="perceivedReadabilityLevel" inputType="likert"></readability:profileField>
							<readability:profileField fieldName="professionalProfile" inputType="text"></readability:profileField>
							<div class="form-group">
								<label for="learningProfile"><spring:message code="profileForm.labels.learningProfile"></spring:message></label>
									<select class="form-control" id="learningProfile" name="learningProfile" ng-model-options="{updateOn: 'blur'}" ng-model="profile.learningProfile">
									<option value="v"><spring:message code="profileForm.fields.learningProfile.options.visual"></spring:message></option>
									<option value="t"><spring:message code="profileForm.fields.learningProfile.options.textual"></spring:message></option>
									<option value="n"><spring:message code="profileForm.fields.learningProfile.options.neither"></spring:message></option>
									<option value="dk"><spring:message code="profileForm.fields.learningProfile.options.dontKnow"></spring:message></option>
								</select>
							</div>
						</form>


					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<a href="/pages/spa#!/" class="link-button link-back"><spring:message
					code="common.back"></spring:message></a> 
			<a
				ng-click="submitForm(profileForm.$valid)"
				class="link-button link-next">
				<spring:message code="common.next"></spring:message>
			</a>
		</div>
	</div>

</div>
