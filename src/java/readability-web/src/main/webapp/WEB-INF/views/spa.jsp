<%@ page session="false" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="readability">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="/spa">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title><spring:message code="spa.title"></spring:message></title>
<link rel="stylesheet" href="/static/css/bootstrap.min.css"/>
<link rel="stylesheet" href="/static/css/ui-bootstrap-csp.css"/>
<link rel="stylesheet" href="/static/css/common.css"/>
<link rel="stylesheet" href="/static/css/spa.css" />
<link rel="stylesheet" href="/static/css/pairs.css" />
<link rel="stylesheet" href="/static/css/votecomments.css" />
<link rel="stylesheet" href="/static/css/code-style.css" />
</head>
<body ng-controller="MainController">

<ng-view autoscroll="true"></ng-view>

<script src="/static/js/jquery.min.js"></script>
<script src="/static/js/angular.min.js"></script>
<script src="/static/js/angular-route.min.js"></script>
<script src="/static/js/angular-resource.min.js"></script>
<script src="/static/js/ui-bootstrap-tpls.js"></script>
<script src="/static/js/readability-app.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93830438-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
(function(angular) {
	var app = angular.module('readability');	
	app.constant('totalPairs', <c:out value="${ workingSetSize }" />);
	app.constant('modalMessages', {
		'modal.profile.end.title': '<spring:message javaScriptEscape="true" code="modal.profile.end.title"></spring:message>',
		'modal.profile.end.message': '<spring:message javaScriptEscape="true" code="modal.profile.end.message"></spring:message>',
		'modal.profile.end.ok': '<spring:message javaScriptEscape="true" code="modal.profile.end.ok"></spring:message>',
		'modal.profile.end.cancel': '<spring:message javaScriptEscape="true" code="modal.profile.end.cancel"></spring:message>',
		'modal.votecomplete.end.title': '<spring:message javaScriptEscape="true" code="modal.votecomplete.end.title"></spring:message>',
		'modal.votecomplete.end.message': '<spring:message javaScriptEscape="true" code="modal.votecomplete.end.message"></spring:message>',
		'modal.voteincomplete.end.title': '<spring:message javaScriptEscape="true" code="modal.voteincomplete.end.title"></spring:message>',
		'modal.voteincomplete.end.message': '<spring:message javaScriptEscape="true" code="modal.voteincomplete.end.message"></spring:message>',
		'modal.vote.emptycomment.title': '<spring:message javaScriptEscape="true" code="modal.vote.emptycomment.title"></spring:message>',
		'modal.vote.emptycomment.message': '<spring:message javaScriptEscape="true" code="modal.vote.emptycomment.message"></spring:message>',
		'modal.experiment.normalfinal.ok': '<spring:message javaScriptEscape="true" code="modal.experiment.normalfinal.ok"></spring:message>',
		'modal.experiment.normalfinal.cancel': '<spring:message javaScriptEscape="true" code="modal.experiment.normalfinal.cancel"></spring:message>',
		'modal.experiment.lackingfinal.ok': '<spring:message javaScriptEscape="true" code="modal.experiment.lackingfinal.ok"></spring:message>',
		'modal.experiment.lackingfinal.cancel': '<spring:message javaScriptEscape="true" code="modal.experiment.lackingfinal.cancel"></spring:message>',
		'modal.instructions.end.title': '<spring:message javaScriptEscape="true" code="modal.instructions.end.title"></spring:message>',
		'modal.instructions.end.message': '<spring:message javaScriptEscape="true" code="modal.instructions.end.message"></spring:message>',
		'common.no': '<spring:message javaScriptEscape="true" code="common.no"></spring:message>',
		'common.yes': '<spring:message javaScriptEscape="true" code="common.yes"></spring:message>'
	});
	app.config(['$logProvider', function($logProvider) {
		$logProvider.debugEnabled(true);
	}])
})(angular);
</script>
</body>
</html>