<%@ page session="false" language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 text-center">
			<h1 class="spa-title">
				<spring:message code="pairs.title"></spring:message>
				<span class="ng-cloak">{{ pairIndex }}/{{ totalPairs }}</span>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-xs-12">
					<div ng-bind-html="trustedSnippet(workingSet.snippetPairs[pairIndex - 1].a.content)"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">
					<button ng-click="postVote('a')" class="btn-pairs-snippet btn btn-default" ng-class="{ 'btn-danger active': selectedSnippet == 'a'}">
						<spring:message code="pairs.this"></spring:message>
					</button>
					<div class="visible-xs">
						<p>&nbsp;</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-xs-12">
					<div ng-bind-html="trustedSnippet(workingSet.snippetPairs[pairIndex - 1].b.content)"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">					
					<button ng-click="postVote('b')" class="btn-pairs-snippet btn btn-default" ng-class="{ 'btn-danger active': selectedSnippet == 'b'}">
						<spring:message code="pairs.this"></spring:message>
					</button>
					<div class="visible-xs">
						<p>&nbsp;</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row strength-row">
		<div class="col-xs-12 text-center">
			<span><spring:message code="pairs.labels.howStronglyAgree"></spring:message></span>
		</div>
		<div class="col-xs-12 strength-container">
			<div class="option-container">
				<label class="radio-inline bold 18pt">
					<input type="radio" name="strength" ng-model="strength" value="1" checked="" /> - 1
				</label>
			</div>
			<div class="option-container">
				<label class="radio-inline bold 18pt">
					<input type="radio" name="strength" ng-model="strength" value="2" /> - 2
				</label>
			</div>
			<div class="option-container">
				<label class="radio-inline bold 18pt">
					<input type="radio" name="strength" ng-model="strength" value="3" /> - 3
				</label>
			</div>
			<div class="option-container">
				<label class="radio-inline bold 18pt">
					<input type="radio" name="strength" ng-model="strength" value="4" /> - 4
				</label>
			</div>
			<div class="option-container">
				<label class="radio-inline bold 18pt">
					<input type="radio" name="strength" ng-model="strength" value="5" /> - 5
				</label>
			</div>
		</div>
	</div>
	<div class="row comments-row">
		<div class="col-xs-12">
			<div class="form-group text-center">
				<label for="comments" class="pair-comments-label">
					<spring:message code="pairs.labels.comments"></spring:message>
				</label>
				<textarea id="comments" name="comments" ng-model="comments" maxlength="5000" class="form-control" rows="5"></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-lg-6 col-sm-offset-1 col-lg-offset-3">
			<a class="link-button link-back" ng-click="previousScreen()">
				<spring:message code="common.back"></spring:message>
			</a>
			<a class="link-button link-next" ng-click="nextScreen()">
				<spring:message code="common.next"></spring:message>
			</a>
		</div>
	</div>
</div>