<%@ page session="false" language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="spa-title">
				<spring:message code="instructions.title" text="Instructions" />
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="fields-container">
				<spring:message code="instructions.text"></spring:message>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<a ng-click="startExperiment()" 
				class="link-button link-next"><spring:message
				code="common.next"></spring:message></a>
		</div>
	</div>
</div>
