<%@ page session="false" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title><spring:message code="spa.title"></spring:message></title>
	<link rel="stylesheet" href="/static/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/static/css/common.css"/>
	<link rel="stylesheet" href="/static/css/spa.css"/>
</head>
<body>
	<div class="container text-center">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="spa-title"><spring:message code="thanks.title"></spring:message></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3">
				<img style="width: 100%" src="/static/img/thank-you-dog.png" />
			</div>
		</div>
	</div>
</body>
</html>