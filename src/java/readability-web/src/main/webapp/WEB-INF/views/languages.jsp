<%@ page session="false" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>A Survey on Code Readability</title>
<link rel="stylesheet" href="/static/css/bootstrap.min.css"/>
<link rel="stylesheet" href="/static/css/common.css"/>
<link rel="stylesheet" href="/static/css/languages.css"/>
</head>
<body>

<div class="container language-buttons-container">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="experiment-title">A Survey on Code Readability</h2>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<a class="language-sel" href="/pages/spa?lang=en">English</a>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<a class="language-sel" href="/pages/spa?lang=pt">Português</a>
		</div>
	</div>
	
</div>

<script src="/static/js/jquery.min.js"></script>
<script src="/static/js/bootstrap.min.js"></script>

</body>
</html>