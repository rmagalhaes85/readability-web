<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="readability" uri="ReadabilityLib" %>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 text-center">
			<h1 class="spa-title">
				<spring:message code="votecomment.title"></spring:message>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div ng-class="{ 'votecomment-unselected-snippet-container' : selectedSnippet == 'B', 'votecomment-selected-snippet-container' : selectedSnippet == 'A'}" 
				ng-bind-html="trustedSnippet(snippetPair.a.content)"></div>
		</div>
		<div class="col-sm-6">
			<div ng-class="{ 'votecomment-unselected-snippet-container' : selectedSnippet == 'A', 'votecomment-selected-snippet-container' : selectedSnippet == 'B'}"
				ng-bind-html="trustedSnippet(snippetPair.b.content)"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<form name="commentsForm" ng-submit="submitForm(commentsForm.$valid)" novalidate>
				<textarea id="comments" name="comments" maxlength="5000" ng-model="comments"
					class="form-control votecomment-textarea" rows="5"></textarea>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-lg-6 col-sm-offset-1 col-lg-offset-3">
<!-- 			<a class="link-button link-back" ng-click="previousScreen()"> -->
<%-- 				<spring:message code="common.back"></spring:message> --%>
<!-- 			</a> -->
			<a class="link-button link-next" ng-click="submitForm(commentsForm.$valid)">
				<spring:message code="common.next"></spring:message>
			</a>
		</div>
	</div>
</div>
