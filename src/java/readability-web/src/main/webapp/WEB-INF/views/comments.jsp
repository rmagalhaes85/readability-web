<%@ page session="false" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="container">
	<form name="commentsForm" ng-submit="submitForm(commentsForm.$valid)" novalidate>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="spa-title">
					<spring:message code="comments.title"></spring:message>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="fields-container">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="comments">
									<spring:message code="comments.form.labels.comments">
									</spring:message>
								</label>
								<textarea id="comments" name="comments" ng-model="comments" maxlength="5000" class="form-control" rows="5"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
<%-- 				<a href="/pages/spa#!/pair/1" class="link-button link-back"><spring:message --%>
<%-- 					code="common.back"></spring:message></a> --%>
				<a ng-click="submitForm(commentsForm.$valid)" 
					ng-disabled="commentsForm.$invalid" class="link-button link-next">
					<spring:message code="common.next"></spring:message>
				</a>
			</div>
		</div>
	</form>
</div>