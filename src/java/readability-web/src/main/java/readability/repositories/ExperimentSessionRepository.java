package readability.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import readability.model.ExperimentSession;

@Transactional(readOnly = true)
@Repository
@SuppressWarnings("unchecked")
public interface ExperimentSessionRepository extends CrudRepository<ExperimentSession, Integer>{
	@Modifying
	@Transactional
	public ExperimentSession save(ExperimentSession experimentSession);
	
	@Query("SELECT e FROM ExperimentSession e LEFT JOIN FETCH e.votes WHERE e.id = ?1")
	public ExperimentSession findOneWithVotes(Integer experimentSessionId);
	
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "UPDATE experiment_sessions SET comments = ?2 WHERE experiment_session_id = ?1")
	public void saveComment(Integer experimentSessionId, String comments);
}
