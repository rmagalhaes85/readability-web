package readability.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import readability.model.SessionEvent;

@Repository
@Transactional
public interface SessionEventRepository extends CrudRepository<SessionEvent, Integer> {
}
