package readability.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import readability.model.ExperimentSession;
import readability.model.SnippetPair;
import readability.model.Vote;

@Repository
@Transactional(readOnly = true)
public interface VoteRepository extends JpaRepository<Vote, Integer> {
	@Query(nativeQuery = true, value="select find_vote_by_experiment_session_and_snippet_pair(?1, ?2)")
	public Integer findIdByExperimentSessionAndSnippetPair(Integer experimentSessionId, SnippetPair snippetPair);
	
	@Modifying
	@Transactional
	//@Query(nativeQuery = true, value="insert into vote_comments(vote_id, comments) values (?1, ?2)")
	@Query(nativeQuery = true, value="update votes set comments = ?2 where vote_id = ?1")
	public void saveVoteComments(Integer voteId, String comments);
	
	@Modifying
	@Transactional
	@Query(nativeQuery = true, value="update votes set strength = ?2 where vote_id = ?1")
	public void saveVoteStrength(Integer voteId, String strength);
}
