package readability.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import readability.model.SnippetPair;

@Repository
@Transactional(readOnly=true)
public interface SnippetPairRepository extends CrudRepository<SnippetPair, Integer>,
		SnippetPairRepositoryCustom {
	@Cacheable(value = "snippetPairList")
	public List<SnippetPair> findAll();
	
	@Cacheable(value = "snippetPairListActive")
	public List<SnippetPair> findByActive(boolean active);
	
	@Cacheable(value = "snippetPairs", key = "#p0")
	public SnippetPair findByHash(String snippetPairHash);
}
