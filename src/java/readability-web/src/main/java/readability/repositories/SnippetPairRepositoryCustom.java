package readability.repositories;

import org.springframework.data.jpa.repository.Query;

public interface SnippetPairRepositoryCustom {
	@Query(nativeQuery=true, value=
		"select s.hash " +
		"from experiment_sessions e inner join working_sets_snippet_pairs wssp on (e.working_set_id = wssp.working_set_id) " +
		"                           inner join snippet_pairs s on (wssp.snippet_pair_id = s.snippet_pair_id) " +
		"where e.experiment_session_id = ?1 and " +
		"wssp.snippet_pair_id not in (select v.snippet_pair_id from votes v where v.experiment_session_id = ?1) " +
		"order by wssp.presentation_order " +
		"limit 1")
	public String getFirstNotVotedHash(Integer experimentSessionId);

	@Query(nativeQuery=true, value=
		"select " +
		"  sp.snippet_pair_id " +
		"from working_sets ws " +
		"  inner join working_sets_snippet_pairs wssp on (ws.working_set_id = wssp.working_set_id) " +
		"  inner join experiment_sessions e on (e.working_set_id = ws.working_set_id) " +
		"  inner join snippet_pairs sp on (sp.snippet_pair_id = wssp.snippet_pair_id) " +
		"  inner join (select v1.vote_id, v1.snippet_pair_id, v1.experiment_session_id, v1.snippet_order, v1.comments from votes v1 where v1.experiment_session_id = ?1 and v1.last_vote = true) v on (v.snippet_pair_id = sp.snippet_pair_id " +
		"    and v.experiment_session_id = e.experiment_session_id " +
		"    and v.snippet_order <> sp.most_readable " +
		"    and (v.comments is null or v.comments = '') ) " +
		"where e.experiment_session_id = ?1 " +
		"order by wssp.presentation_order " +
		"limit 1")
	public Integer getFirstDivergentSnippetPairId(Integer experimentSessionId);
}
