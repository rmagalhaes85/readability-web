package readability.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import readability.model.Profile;

@Repository
@Transactional(readOnly=true)
public interface ProfileRepository extends CrudRepository<Profile, Long> {
	@Modifying
	@Transactional
	public Profile save(Profile p);
}
