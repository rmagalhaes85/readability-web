package readability.configuration;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = { "readability.model" })
@EnableJpaRepositories(basePackages = { "readability.repositories" })
@EnableCaching
public class DataConfiguration {
	
	@Bean
	public CacheManager cacheManager() {
		SimpleCacheManager cacheManager = new SimpleCacheManager();
		cacheManager.setCaches(Arrays.asList(
				new ConcurrentMapCache("snippetPairList"),
				new ConcurrentMapCache("snippetPairs"),
				new ConcurrentMapCache("snippetPairListActive")));
		return cacheManager;
	}
}
