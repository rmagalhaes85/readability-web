package readability.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

public class CustomPostProcessor implements BeanPostProcessor {

	private static final Logger logger = LoggerFactory.getLogger(CustomPostProcessor.class);
	
	@Override
	public Object postProcessAfterInitialization(Object bean, String name)
			throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String name)
			throws BeansException {
		if (logger.isTraceEnabled()) {
			logger.trace("postProcessBeforeInitialization: init");
		}
		
		if (bean instanceof RequestMappingHandlerAdapter) {
			if (logger.isTraceEnabled()) {
				logger.trace("postProcessBeforeInitialization: configuring a RequestMappingHandlerAdapter bean");
			}
			
			// prevent concurrent access issues to session attributes
			RequestMappingHandlerAdapter handler = (RequestMappingHandlerAdapter) bean;
			handler.setSynchronizeOnSession(true);
			
			if (logger.isTraceEnabled()) {
				logger.trace("postProcessBeforeInitialization: RequestMappingHandlerAdapter bean configuration is OK");
			}
			
			return handler;
		}
		
		if (logger.isTraceEnabled()) {
			logger.trace("postProcessBeforeInitialization: end");
		}
		
		return bean;
	}

}
