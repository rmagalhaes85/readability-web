package readability.services;

import org.springframework.stereotype.Component;

import readability.model.ExperimentSession;
import readability.model.SessionEvent;
import readability.model.SnippetOrder;
import readability.model.SnippetPair;
import readability.model.Vote;

@Component
public interface ExperimentService {	
	public ExperimentSession newExperimentSession();
	public ExperimentSession vote(Integer experimentSessionId, String snippetPairHash, String selectedSnippet, String strength);
	public ExperimentSession vote(Integer experimentSessionId, String snippetPairHash, String selectedSnippet, String strength, String comments);
	public ExperimentSession saveSession(ExperimentSession session);
	public void saveComments(Integer experimentSessionId, String comments);
	public void saveVoteComments(Integer experimentSessionId, String snippetPairHash, String strength, String comment);
	public ExperimentSession findById(Integer experimentSessionId);
	public ExperimentSession findByIdWithVotes(Integer experimentSessionId);
	public boolean verifyVote(ExperimentSession experimentSession, String snippetPairHash, SnippetOrder selectedSnippet);
	public String getSnippetContent(String snippetPairHash, SnippetOrder snippetOrder);
	public String getFirstNotVotedHash(Integer experimentSessionId);
	public String getFirstDivergentVoteHash(Integer experimentSessionId);
	public SnippetPair getFirstDivergentSnippetPair(Integer experimentSessionId);
	public Vote getSnippetPairVote(Integer experimentSessionId, SnippetPair snippetPair);
	public SessionEvent saveSessionEvent(Integer experimentSessionId, String eventContent);
	public int getWorkingSetSize();
	//public ExperimentSession saveProfile(ExperimentSession experimentSession, Profile profile);
}
