package readability.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import readability.model.ExperimentSession;
import readability.model.SessionEvent;
import readability.model.SnippetOrder;
import readability.model.SnippetPair;
import readability.model.Vote;
import readability.model.WorkingSet;
import readability.model.WorkingSetSnippetPair;
import readability.model.WorkingSetSnippetPairId;
import readability.repositories.ExperimentSessionRepository;
import readability.repositories.ProfileRepository;
import readability.repositories.SessionEventRepository;
import readability.repositories.SnippetPairRepository;
import readability.repositories.VoteRepository;

@Service
public class ExperimentServiceImpl implements ExperimentService {
	
	private static final Logger logger = LoggerFactory.getLogger(ExperimentServiceImpl.class);

	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private SnippetPairRepository snippetPairRepository;
	
	@Autowired
	private ExperimentSessionRepository experimentSessionRepository;
	
	@Autowired
	private VoteRepository voteRepository;
	
	@Autowired
	private SessionEventRepository sessionEventRepository;
	
	@Autowired
	private Environment env;
	
	//private int workingSetSize = env.getProperty("experiment.workingSetSize", Integer.class, 10);
	private int workingSetSize = 5;
	
	@PostConstruct
	private void postConstruct() {
		int wssConfig = env.getProperty("experiment.workingSetSize", Integer.class, 5);
		logger.info("Working set size: " + wssConfig);
		workingSetSize = wssConfig;
	}
	
	@Override
	@Transactional(readOnly = false)
	public ExperimentSession newExperimentSession() {
		logger.trace("newExperimentSession: init");
		ExperimentSession experimentSession = new ExperimentSession();
		experimentSession.setWorkingSet(createRandomWorkingSet());
		
		experimentSessionRepository.save(experimentSession);
		
		if (logger.isDebugEnabled()) {
			logger.debug("Returning experimentSession: " + experimentSession.toString());
		}
		logger.trace("newExperimentSession: end");
		return experimentSession;
	}
	
	protected boolean checkSnippetPairs(List<SnippetPair> snippetPairs) {
		if (snippetPairs.size() < workingSetSize) {
			requestApplicationShutdown(String.format("There are no sufficient snippet pairs to run the experiment "
					+ "(Total snippet pairs: %s; Required working set size: %s",
					snippetPairs.size(), workingSetSize));
			
			return false;
		}
		return true;		
	}
	
	protected void logSnippetPairsIds(List<SnippetPair> snippetPairs) {
		List<String> ids = new ArrayList<String>();
		for (SnippetPair pair : snippetPairs) {
			ids.add(pair.getId().toString());
		}
		logger.debug("Snippet pairs Ids: " + String.join(", ", ids));
	}
	
	protected WorkingSet createRandomWorkingSet() {
		logger.trace("createRandomWorkingSet: init");

		WorkingSet workingSet = new WorkingSet();

		List<SnippetPair> snippetPairs = snippetPairRepository.findByActive(true);

		if (!checkSnippetPairs(snippetPairs)) {
			return null;
		}
		
		logger.debug("Shuffling the snippet pair list");
		Collections.shuffle(snippetPairs, new Random(System.nanoTime()));		

		for (int i = 0; i < workingSetSize; i++) {
			logger.debug(String.format("Processing snippet pair of Id '%d'", snippetPairs.get(i).getId()));
			WorkingSetSnippetPair association = new WorkingSetSnippetPair();
			WorkingSetSnippetPairId id = new WorkingSetSnippetPairId(workingSet, snippetPairs.get(i));
			association.setPk(id);
			association.setPresentationOrder(i);
			//workingSet.addSnippetPair(snippetPairs.get(i));
			workingSet.getWorkingSetSnippetPairs().add(association);
		}
		
		logger.trace("createRandomWorkingSet: end");
		return workingSet;
	}
	
	protected void requestApplicationShutdown(String message) {
		logger.info(message);
		eventPublisher.publishEvent(new PayloadApplicationEvent<String>(this, "shutdown"));
	}
	
	protected void assertWorkingSetContainsSnippetPairHash(WorkingSet workingSet, String snippetPairHash) {
		logger.trace("assertWorkingSetContainsSnippetPairHash: init");
		logger.debug(String.format("looking for snippetPairHash = '%s'", snippetPairHash));
		for (SnippetPair pair : workingSet.getSnippetPairs()) {
			if (pair.getHash().equals(snippetPairHash)) {
				logger.debug(String.format("snippetPairHash '%s' found", snippetPairHash));
				return;
			}
		}
		
		String message = String.format(
			"The current working set doesn't contain any pointer to the specified snippet pair hash ('%s')", 
			snippetPairHash);
		logger.error(message);
		throw new RuntimeException(message);
	}

	@Override
	@Transactional(readOnly = false)
	public ExperimentSession vote(Integer experimentSessionId, String snippetPairHash, String snippetOrder, String strength) {
		return vote(experimentSessionId, snippetPairHash, snippetOrder, strength, null);
	}
	
	@Override
	@Transactional(readOnly = false)
	public ExperimentSession vote(Integer experimentSessionId, String snippetPairHash, String snippetOrder, String strength, String comments) {
		logger.trace("vote: init");

		Assert.notNull(experimentSessionId);
		Optional<ExperimentSession> oexperimentSession = experimentSessionRepository.findById(experimentSessionId);
		Assert.isTrue(oexperimentSession.isPresent());
		
		ExperimentSession experimentSession = oexperimentSession.get();
		WorkingSet workingSet = experimentSession.getWorkingSet();
		Assert.notNull(workingSet);
		assertWorkingSetContainsSnippetPairHash(workingSet, snippetPairHash);
		
		SnippetPair snippetPair = snippetPairRepository.findByHash(snippetPairHash);
		Assert.notNull(snippetPair);
		
		Vote vote = new Vote();
		vote.setExperimentSession(experimentSession);
		vote.setSnippetPair(snippetPair);
		vote.setSnippetOrder(snippetOrder);
		vote.setArrivalTime(new Date());
		vote.setStrength(strength);
		vote.setComments(comments);
		
		logger.debug("Associating the vote to experiment session. Vote data: " + vote.toString());
		experimentSession.addVote(vote);
		
		logger.debug("Saving experiment session data: " + experimentSession.toString());
		ExperimentSession saved = experimentSessionRepository.save(experimentSession);
		logger.debug("Saved experiment session: " + saved.toString());
		
		logger.trace("vote: end");
		return saved;
	}
	
	protected String truncated(String text) {
		return text.length() > 13 ? text.substring(0, 10).concat("...") : text;
	}

	@Override
	public void saveComments(Integer experimentSessionId, String comments) {
		logger.trace("saveComments: init");
		Assert.notNull(experimentSessionId);
		if (comments == null) {
			logger.debug("Comments not informed");
			return;
		}
		logger.debug(String.format("Saving comments... experimentSessionId='%d', comments = '%s'",
				experimentSessionId, truncated(comments)));
		experimentSessionRepository.saveComment(experimentSessionId, comments);
		logger.trace("saveComments: end");
	}

	@Override
	@Transactional(readOnly = false)
	public void saveVoteComments(Integer experimentSessionId,
			String snippetPairHash, String strength, String comments) {
		logger.trace("saveVoteComments: init");
		logger.debug(String.format("experimentSessionId='%s', snippetPairHash='%s', comment='%s'",
				experimentSessionId, snippetPairHash, truncated(comments)));

		if (experimentSessionId == null || snippetPairHash == null || comments == null) {
			logger.debug("One of the parameters is null");
			return;
		}
		
		SnippetPair snippetPair = snippetPairRepository.findByHash(snippetPairHash);
		if (snippetPair == null) {
			logger.debug(String.format("Snippet pair not found according to the hash '%s'", snippetPairHash));
			return;
		}
		
		Integer voteId = voteRepository.findIdByExperimentSessionAndSnippetPair(experimentSessionId, snippetPair);
		if (voteId == null) {
			logger.debug(String.format("No voteId found, parameters: experimentSessionId='%s', snippetPair='%s'",
					experimentSessionId, snippetPair.toString()));
			return;
		}
		
		if (comments.length() > 5000) {
			logger.debug("Truncating comments...");
			comments = comments.substring(0, 5000);
		}
		
		voteRepository.saveVoteComments(voteId, comments);
		voteRepository.saveVoteStrength(voteId, strength);
		logger.trace("saveVoteComments: end");
	}

	@Override
	@Transactional(readOnly = true)
	public boolean verifyVote(ExperimentSession experimentSession, String snippetPairHash, SnippetOrder selectedSnippet) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public ExperimentSession saveSession(ExperimentSession session) {
		logger.trace("saveSession: init");
		Assert.notNull(session);
		ExperimentSession saved = experimentSessionRepository.save(session);
		logger.trace("saveSession: end");
		return saved;
	}

	@Override
	@Transactional(readOnly = true)
	public String getSnippetContent(String snippetPairHash,
			SnippetOrder snippetOrder) {
		// this feels like a waste of resources (we are getting the whole snippet pair to serve
		// a request which asked for the contents of a single snippet). But, the findByHash method
		// has its results cached, and when a request for a snippet arrives, it is hugely
		// likely that a request for the other snippet on the pair is coming right afterwards 
		SnippetPair snippetPair = snippetPairRepository.findByHash(snippetPairHash);
		
		if (snippetPair == null) {
			// strange situation, considering that the requests for snippet pairs are built by
			// the application itself..
			logger.warn(String.format("Received a request for an unexistent snippet pair (hash = %s)", snippetPairHash));
			return null;
		} else {
			String content = snippetPair.getSnippet(snippetOrder).getContent();
			return content;
		}
	}

	@Override
	@Transactional(readOnly = true)
	public String getFirstNotVotedHash(Integer experimentSessionId) {
		logger.trace("getFirstNotVotedHash: init");
		Assert.notNull(experimentSessionId);
		logger.debug("Looking for the first not voted hash of experiment session id = " + experimentSessionId);
		String hash = snippetPairRepository.getFirstNotVotedHash(experimentSessionId);
		if (hash != null) {
			logger.debug(String.format("Found hash '%s'", hash));
		} else {
			logger.debug("No hash found");
		}
		logger.trace("getFirstNotVotedHash: end");
		return hash;
	}
	
	@Override
	@Transactional(readOnly = true)
	public SnippetPair getFirstDivergentSnippetPair(Integer experimentSessionId) {
		logger.trace("getFirstDivergentSnippetPair: init");
		Assert.notNull(experimentSessionId);

		logger.debug("Looking for the first divergent snippet pair. Experiment Session Id = " + 
			experimentSessionId);

		Integer snippetPairId = snippetPairRepository.getFirstDivergentSnippetPairId(experimentSessionId);
		if (snippetPairId == null) {
			logger.debug("No snippet pair Id found");
			return null;
		} else {
			logger.debug("Snippet pair Id found: " + snippetPairId.toString());
		}
		
		Optional<SnippetPair> osp = snippetPairRepository.findById(snippetPairId);
		return osp.isPresent() ? osp.get() : null;
	}
	
	@Override
	@Transactional(readOnly = true)
	public String getFirstDivergentVoteHash(Integer experimentSessionId) {
		logger.trace("getFirstDivergentVoteHash: init");
		SnippetPair sp = getFirstDivergentSnippetPair(experimentSessionId);
		if (sp == null) {
			return null;
		}
		String hash = sp.getHash();
		if (hash != null) {
			logger.debug(String.format("Hash found: '%s'", hash));
		}
		logger.trace("getFirstDivergentVoteHash: end");
		return hash;
	}

	@Override
	@Transactional(readOnly=true)
	public Vote getSnippetPairVote(
			Integer experimentSessionId, SnippetPair snippetPair) {
		logger.trace("getSnippetPairVote: init");

		Assert.notNull(experimentSessionId);
		Assert.notNull(snippetPair);
		
		logger.debug("getSnippetPairVote with experimentSession id = " + 
			experimentSessionId + "; snippetPair id = " + snippetPair.getId());
		
		// TODO avoid this "two step" look up. Vote should be seek in a single step
		Integer voteId = voteRepository.findIdByExperimentSessionAndSnippetPair(experimentSessionId, snippetPair);
		if (voteId == null) {
			logger.debug("No voteId found");
			return null;
		}
		
		Optional<Vote> ov = voteRepository.findById(voteId);
		logger.debug(!ov.isPresent() ? "No vote found" : "Vote found: " + ov.get().toString());
		Vote vote = ov.get();
		
		logger.trace("getSnippetPairVote: end");
		return vote;
	}

	@Override
	@Transactional(readOnly=true)
	public ExperimentSession findById(Integer experimentSessionId) {
		Assert.notNull(experimentSessionId);
		logger.debug(String.format("Experiment session of ID '%s' requested", experimentSessionId));
		
		Optional<ExperimentSession> oes = experimentSessionRepository.findById(experimentSessionId);
		return oes.isPresent() ? oes.get() : null;
	}
	

	@Override
	@Transactional(readOnly=true)
	public ExperimentSession findByIdWithVotes(Integer experimentSessionId) {
		ExperimentSession experimentSession = experimentSessionRepository.findOneWithVotes(experimentSessionId);
		return experimentSession;
	}

	@Override
	public SessionEvent saveSessionEvent(Integer experimentSessionId,
			String eventContent) {
		logger.trace("saveSessionEvent: init");
		Assert.notNull(experimentSessionId);
		Assert.hasText(eventContent);
		
		SessionEvent sessionEvent = new SessionEvent();
		sessionEvent.setArrivalTime(new Date());
		sessionEvent.setExperimentSessionId(experimentSessionId);
		sessionEvent.setContent(eventContent);
		
		if (logger.isDebugEnabled()) {
			logger.debug("Saving session event with data: " + sessionEvent.toString());
		}
		
		SessionEvent result = sessionEventRepository.save(sessionEvent);
		Assert.isTrue(result.getId() != null && result.getId() != 0);
		logger.trace("saveSessionEvent: end");
		return result;
	}
	
	@Override
	public int getWorkingSetSize() {
		return workingSetSize;
	}
}
