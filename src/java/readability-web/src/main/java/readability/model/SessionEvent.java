package readability.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "session_events")
public class SessionEvent {
	
	@Id
	@Column(name = "session_event_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "experiment_session_id")
	private Integer experimentSessionId;
	
	@Column(name = "arrival_time")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date arrivalTime;
	
	@Column(name = "content")
	private String content;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getExperimentSessionId() {
		return experimentSessionId;
	}

	public void setExperimentSessionId(Integer experimentSessionId) {
		this.experimentSessionId = experimentSessionId;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + experimentSessionId;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionEvent other = (SessionEvent) obj;
		if (arrivalTime == null) {
			if (other.arrivalTime != null)
				return false;
		} else if (!arrivalTime.equals(other.arrivalTime))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (experimentSessionId != other.experimentSessionId)
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String subContent = content == null ? 
			null : content.length() > 20 ? 
				content.substring(0, 20).concat("...") : content;

		return "SessionEvent [id=" + id + ", experimentSessionId="
				+ experimentSessionId + ", arrivalTime=" + arrivalTime
				+ ", content=" + subContent + "]";
	}
}
