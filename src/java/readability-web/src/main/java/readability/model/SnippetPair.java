package readability.model;

import javax.persistence.*;

import org.springframework.util.Assert;

@Entity
@Table(name = "snippet_pairs")
public class SnippetPair {
	private static final String URL_PREFIX = "/snippetPair/";
	
	@Id
	@Column(name = "snippet_pair_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String hash;
	
	@OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "snippet_a", referencedColumnName = "snippet_id")
	private Snippet a;
	
	@OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "snippet_b", referencedColumnName = "snippet_id")
	private Snippet b;
	
	@Column(name = "most_readable")
	private String mostReadable;
	
	@Column(name = "active")
	private boolean active;
	
	public SnippetPair() {
		
	}
	
	public Integer getId() {
		return id;
	}
	
	public Snippet getA() {
		return getSnippet(SnippetOrder.A);
	}
	
	public String getPublicUrlA() {
		return getPublicUrl(SnippetOrder.A);
	}
	
	public Snippet getB() {
		return getSnippet(SnippetOrder.B);
	}
	
	public String getPublicUrlB() {
		return getPublicUrl(SnippetOrder.B);
	}
	
	protected String getPublicUrl(SnippetOrder snippetOrder) {
		return SnippetPair.URL_PREFIX.concat(hash).concat("/").concat(snippetOrder.getValue().toLowerCase());
	}
	
	public String getHash() {
		return hash;
	}
	
	public Snippet getSnippet(SnippetOrder order) {
		return (order == SnippetOrder.A) ? a : b; 
	}
	
	protected void throwInvalidMostReadableException() throws Exception {
		throw new Exception(String
			.format("Invalid most readable snippet (value stored internally contains %s)", mostReadable));
	}
	
	public Snippet getMostReadable() throws Exception {
		if ("A".equals(mostReadable.toUpperCase())) {
			return getA();
		} else if ("B".equals(mostReadable.toUpperCase())) {
			return getB();
		} else {
			// this should have been prevented by the check constraint on the database table
			throwInvalidMostReadableException();
			return null;
		}
	}
	
	public SnippetOrder getMostReadableSnippetOrder() throws Exception {
		Snippet mostReadable = getMostReadable();
		if (mostReadable.equals(getA())) {
			return SnippetOrder.A;
		}
		else if (mostReadable.equals(getB())) {
			return SnippetOrder.B;
		}
		else {
			// this should have been prevented by the check constraint on the database table
			throwInvalidMostReadableException();
			return null;
		}
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "SnippetPair [id=" + id + ", hash=" + hash + ", mostReadable=" + mostReadable + ", active=" + active + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result
				+ ((mostReadable == null) ? 0 : mostReadable.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SnippetPair other = (SnippetPair) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		if (active != other.active)
			return false;
		if (b == null) {
			if (other.b != null)
				return false;
		} else if (!b.equals(other.b))
			return false;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		if (mostReadable == null) {
			if (other.mostReadable != null)
				return false;
		} else if (!mostReadable.equals(other.mostReadable))
			return false;
		return true;
	}

}
