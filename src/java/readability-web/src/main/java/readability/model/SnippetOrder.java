package readability.model;

public enum SnippetOrder {
	A("A"), B("B");
	
	private final String value;
	
	SnippetOrder(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public static SnippetOrder parse(String value) {
		for (SnippetOrder snippetOrder : SnippetOrder.values()) {
			if (snippetOrder.getValue().equalsIgnoreCase(value)) {
				return snippetOrder;
			}
		}
		return null;
	}
}
