package readability.model;

public class VoteDTO {
	private String snippetPairHash;
	private String snippetOrder;
	private String comments;
	private String strength;

	public String getSnippetPairHash() {
		return snippetPairHash;
	}

	public void setSnippetPairHash(String snippetPairHash) {
		this.snippetPairHash = snippetPairHash;
	}

	public String getSnippetOrder() {
		return snippetOrder;
	}

	public void setSnippetOrder(String snippetPairOrder) {
		this.snippetOrder = snippetPairOrder;
	}
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	@Override
	public String toString() {
		return "VoteDTO [snippetPairHash=" + snippetPairHash + ", snippetOrder=" + snippetOrder + ", comments="
				+ comments + ", strength=" + strength + "]";
	}
}
