package readability.model;

public class VoteCommentDataDTO {
	private SnippetPair snippetPair;
	private String snippetPairHash;
	private Vote vote;
	private String strength;
	private String comments;
	
	public VoteCommentDataDTO() {
		
	}

	public VoteCommentDataDTO(SnippetPair snippetPair, String snippetPairHash, Vote vote, String strength, String comments) {
		this.snippetPair = snippetPair;
		this.snippetPairHash = snippetPairHash;
		this.vote = vote;
		this.strength = strength;
		this.comments = comments;
	}

	public SnippetPair getSnippetPair() {
		return snippetPair;
	}
	
	public void setSnippetPair(SnippetPair snippetPair) {
		this.snippetPair = snippetPair;
	}
	
	public String getSnippetPairHash() {
		return snippetPairHash;
	}
	
	public void setSnippetPairHash(String snippetPairHash) {
		this.snippetPairHash = snippetPairHash;
	}

	public Vote getVote() {
		return vote;
	}
	
	public void setVote(Vote vote) {
		this.vote = vote;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "VoteCommentDataDTO [snippetPair=" + (snippetPair == null ? null : snippetPair.getId())
			+ ", snippetPairHash=" + snippetPairHash + ", vote=" + vote
			+ ", strength=" + strength 
			+ ", comments = " + (comments == null ? null : comments.length() > 13 ? comments.substring(0, 10).concat("...") : comments) + "]";
	}

}
