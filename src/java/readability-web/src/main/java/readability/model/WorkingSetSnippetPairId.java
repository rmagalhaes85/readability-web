package readability.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class WorkingSetSnippetPairId implements Serializable {
	private WorkingSet workingSet;
	private SnippetPair snippetPair;
	
	@ManyToOne
	public WorkingSet getWorkingSet() {
		return workingSet;
	}
	
	public void setWorkingSet(WorkingSet workingSet) {
		this.workingSet = workingSet;
	}
	
	@ManyToOne
	public SnippetPair getSnippetPair() {
		return snippetPair;
	}
	
	public void setSnippetPair(SnippetPair snippetPair) {
		this.snippetPair = snippetPair;
	}
	
	public WorkingSetSnippetPairId() {
		
	}
	
	public WorkingSetSnippetPairId(WorkingSet workingSet, SnippetPair snippetPair) {
		this.workingSet = workingSet;
		this.snippetPair = snippetPair;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((snippetPair == null) ? 0 : snippetPair.hashCode());
		result = prime * result
				+ ((workingSet == null) ? 0 : workingSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkingSetSnippetPairId other = (WorkingSetSnippetPairId) obj;
		if (snippetPair == null) {
			if (other.snippetPair != null)
				return false;
		} else if (!snippetPair.equals(other.snippetPair))
			return false;
		if (workingSet == null) {
			if (other.workingSet != null)
				return false;
		} else if (!workingSet.equals(other.workingSet))
			return false;
		return true;
	}
	
}
