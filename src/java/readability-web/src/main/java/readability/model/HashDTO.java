package readability.model;

public class HashDTO {
	private String hash;

	public HashDTO() {
		
	}
	
	public HashDTO(String hash) {
		this.hash = hash;
	}
	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
}
