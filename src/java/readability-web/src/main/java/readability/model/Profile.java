package readability.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "profiles")
public class Profile {
	@Id
	@Column(name = "profile_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Length(max = 100, message="profile.form.fields.name.error.maxlength")
	private String name;
	
	@Email(message = "profile.form.fields.email.error.format")
	private String email;
	
	@Column(name = "programming_experience_time")
	private int programmingExperienceTime;
	
	@Column(name = "java_programming_experience_time")
	private int javaProgrammingExperienceTime;
	
	@Column(name = "floss_experience_time")
	private int flossExperienceTime;
	
	@Column(name = "annotations_experience_time")
	private int annotationsExperienceTime;	
	
	@Column(name = "age")
	private String age;

	@Column(name = "years_in_school")
	private String yearsInSchool;

	@Column(name = "gender")
	private String gender;

	@Column(name = "programming_languages")
	private String programmingLanguages;

	@Column(name = "natural_languages")
	private String naturalLanguages;

	@Column(name = "operating_systems")
	private String operatingSystems;

	@Column(name = "english_confort_level")
	private int englishConfortLevel;

	@Column(name = "perceived_readability_level")
	private int perceivedReadabilityLevel;
	
	@Column(name = "learning_profile")
	private String learningProfile;
	
	@Column(name = "professional_profile")
	private String professionalProfile;

	public int getProgrammingExperienceTime() {
		return programmingExperienceTime;
	}

	public void setProgrammingExperienceTime(int programmingExperienceTime) {
		this.programmingExperienceTime = programmingExperienceTime;
	}

	public int getJavaProgrammingExperienceTime() {
		return javaProgrammingExperienceTime;
	}

	public void setJavaProgrammingExperienceTime(int javaProgrammingExperienceTime) {
		this.javaProgrammingExperienceTime = javaProgrammingExperienceTime;
	}

	public int getFlossExperienceTime() {
		return flossExperienceTime;
	}

	public void setFlossExperienceTime(int flossExperienceTime) {
		this.flossExperienceTime = flossExperienceTime;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getYearsInSchool() {
		return yearsInSchool;
	}

	public void setYearsInSchool(String yearsInSchool) {
		this.yearsInSchool = yearsInSchool;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProgrammingLanguages() {
		return programmingLanguages;
	}

	public void setProgrammingLanguages(String programmingLanguages) {
		this.programmingLanguages = programmingLanguages;
	}

	public String getNaturalLanguages() {
		return naturalLanguages;
	}

	public void setNaturalLanguages(String naturalLanguages) {
		this.naturalLanguages = naturalLanguages;
	}

	public String getOperatingSystems() {
		return operatingSystems;
	}

	public void setOperatingSystems(String operatingSystems) {
		this.operatingSystems = operatingSystems;
	}

	public int getEnglishConfortLevel() {
		return englishConfortLevel;
	}

	public void setEnglishConfortLevel(int englishConfortLevel) {
		this.englishConfortLevel = englishConfortLevel;
	}

	public int getPerceivedReadabilityLevel() {
		return perceivedReadabilityLevel;
	}

	public void setPerceivedReadabilityLevel(int perceivedReadabilityLevel) {
		this.perceivedReadabilityLevel = perceivedReadabilityLevel;
	}

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLearningProfile() {
		return learningProfile;
	}

	public void setLearningProfile(String learningProfile) {
		this.learningProfile = learningProfile;
	}

	public int getAnnotationsExperienceTime() {
		return annotationsExperienceTime;
	}

	public void setAnnotationsExperienceTime(int annotationsExperienceTime) {
		this.annotationsExperienceTime = annotationsExperienceTime;
	}

	public String getProfessionalProfile() {
		return professionalProfile;
	}

	public void setProfessionalProfile(String professionalProfile) {
		this.professionalProfile = professionalProfile;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", name=" + name + ", email=" + email + ", programmingExperienceTime="
				+ programmingExperienceTime + ", javaProgrammingExperienceTime=" + javaProgrammingExperienceTime
				+ ", flossExperienceTime=" + flossExperienceTime + ", annotationsExperienceTime="
				+ annotationsExperienceTime + ", age=" + age + ", yearsInSchool=" + yearsInSchool + ", gender=" + gender
				+ ", programmingLanguages=" + programmingLanguages + ", naturalLanguages=" + naturalLanguages
				+ ", operatingSystems=" + operatingSystems + ", englishConfortLevel=" + englishConfortLevel
				+ ", perceivedReadabilityLevel=" + perceivedReadabilityLevel + ", learningProfile=" + learningProfile
				+ ", professionalProfile=" + professionalProfile + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + annotationsExperienceTime;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + englishConfortLevel;
		result = prime * result + flossExperienceTime;
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + javaProgrammingExperienceTime;
		result = prime * result + ((learningProfile == null) ? 0 : learningProfile.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((naturalLanguages == null) ? 0 : naturalLanguages.hashCode());
		result = prime * result + ((operatingSystems == null) ? 0 : operatingSystems.hashCode());
		result = prime * result + perceivedReadabilityLevel;
		result = prime * result + ((professionalProfile == null) ? 0 : professionalProfile.hashCode());
		result = prime * result + programmingExperienceTime;
		result = prime * result + ((programmingLanguages == null) ? 0 : programmingLanguages.hashCode());
		result = prime * result + ((yearsInSchool == null) ? 0 : yearsInSchool.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (annotationsExperienceTime != other.annotationsExperienceTime)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (englishConfortLevel != other.englishConfortLevel)
			return false;
		if (flossExperienceTime != other.flossExperienceTime)
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (javaProgrammingExperienceTime != other.javaProgrammingExperienceTime)
			return false;
		if (learningProfile == null) {
			if (other.learningProfile != null)
				return false;
		} else if (!learningProfile.equals(other.learningProfile))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (naturalLanguages == null) {
			if (other.naturalLanguages != null)
				return false;
		} else if (!naturalLanguages.equals(other.naturalLanguages))
			return false;
		if (operatingSystems == null) {
			if (other.operatingSystems != null)
				return false;
		} else if (!operatingSystems.equals(other.operatingSystems))
			return false;
		if (perceivedReadabilityLevel != other.perceivedReadabilityLevel)
			return false;
		if (professionalProfile == null) {
			if (other.professionalProfile != null)
				return false;
		} else if (!professionalProfile.equals(other.professionalProfile))
			return false;
		if (programmingExperienceTime != other.programmingExperienceTime)
			return false;
		if (programmingLanguages == null) {
			if (other.programmingLanguages != null)
				return false;
		} else if (!programmingLanguages.equals(other.programmingLanguages))
			return false;
		if (yearsInSchool == null) {
			if (other.yearsInSchool != null)
				return false;
		} else if (!yearsInSchool.equals(other.yearsInSchool))
			return false;
		return true;
	}
}
