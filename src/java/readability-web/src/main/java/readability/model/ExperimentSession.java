package readability.model;

import java.util.Collections;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.springframework.util.Assert;

@Entity()
@Table(name="experiment_sessions")
public class ExperimentSession {
	
	@Id
	@Column(name = "experiment_session_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
	private Profile profile;	
	
	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "working_set_id", referencedColumnName = "working_set_id")
	private WorkingSet workingSet;
	
	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "client_header_id", referencedColumnName = "client_header_id")
	private ClientHeader clientHeader;
	
	@OneToMany(mappedBy = "experimentSession", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Vote> votes;
	
	@Column(name = "ga_client_cookie")
	private String gaClientCookie;
	
	@Column(name = "locale")
	private String locale;
	
	@Column(name = "jsession_id")
	private String jSessionId;
	
	@Column(name = "start_time")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date startTime;
	
	@Column(name = "end_time")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date endTime;
	
	@Column(name = "origin")
	private String origin;
		
	public ExperimentSession() {
		votes = new ArrayList<Vote>();
	}
	
	public String getGaClientCookie() {
		return gaClientCookie;
	}

	public void setGaClientCookie(String gaClientCookie) {
		this.gaClientCookie = gaClientCookie;
	}
	
	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	public String getJSessionId() {
		return jSessionId;
	}
	
	public void setJSessionId(String jSessionId) {
		this.jSessionId = jSessionId;
	}
	
	public ClientHeader getClientHeader() {
		return clientHeader;
	}
	
	public String getClientHeaderData() {
		if (clientHeader == null) return null;
		return clientHeader.getJsonData();
	}
	
	public void setClientHeaderData(String clientHeaderData) {
		if(clientHeader == null) clientHeader = new ClientHeader();
		clientHeader.setJsonData(clientHeaderData);
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public void setWorkingSet(WorkingSet workingSet) {
		this.workingSet = workingSet; 
	}
	
	public Integer getId() {
		return id;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public WorkingSet getWorkingSet() {
		return workingSet;
	}
	
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public List<Vote> getVotes() {
		votes.sort((vote1, vote2) -> { return vote1.getArrivalTime().before(vote2.getArrivalTime()) ? -1 : 1; });
		return Collections.unmodifiableList(votes);
	}
	
	protected void resetLastFlagOnSameSnippetVote(SnippetPair snippetPair) {
		Assert.notNull(snippetPair);
		for (Vote vote : votes) {
			if (vote.getSnippetPair().equals(snippetPair)) {
				vote.setLast(false);
			}
		}
	}
	
	public void addVote(Vote vote) {
		SnippetPair voteSnippetPair = vote.getSnippetPair();
		resetLastFlagOnSameSnippetVote(voteSnippetPair);
		vote.setLast(true);
		votes.add(vote);
	}

	@Override
	public String toString() {
		return "ExperimentSession [id=" + id + ", clientHeader=" + clientHeader + ", gaClientCookie=" + gaClientCookie
				+ ", locale=" + locale + ", jSessionId=" + jSessionId + ", startTime=" + startTime + ", endTime="
				+ endTime + ", origin=" + origin + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientHeader == null) ? 0 : clientHeader.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((gaClientCookie == null) ? 0 : gaClientCookie.hashCode());
		result = prime * result + ((jSessionId == null) ? 0 : jSessionId.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + ((votes == null) ? 0 : votes.hashCode());
		result = prime * result + ((workingSet == null) ? 0 : workingSet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExperimentSession other = (ExperimentSession) obj;
		if (clientHeader == null) {
			if (other.clientHeader != null)
				return false;
		} else if (!clientHeader.equals(other.clientHeader))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (gaClientCookie == null) {
			if (other.gaClientCookie != null)
				return false;
		} else if (!gaClientCookie.equals(other.gaClientCookie))
			return false;
		if (jSessionId == null) {
			if (other.jSessionId != null)
				return false;
		} else if (!jSessionId.equals(other.jSessionId))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (votes == null) {
			if (other.votes != null)
				return false;
		} else if (!votes.equals(other.votes))
			return false;
		if (workingSet == null) {
			if (other.workingSet != null)
				return false;
		} else if (!workingSet.equals(other.workingSet))
			return false;
		return true;
	}
}
