package readability.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "working_sets_snippet_pairs")
@AssociationOverrides({
		@AssociationOverride(
				name = "pk.workingSet",
				joinColumns=@JoinColumn(name = "working_set_id")),
		@AssociationOverride(
				name = "pk.snippetPair",
				joinColumns=@JoinColumn(name = "snippet_pair_id"))
})
public class WorkingSetSnippetPair {
	private WorkingSetSnippetPairId pk = new WorkingSetSnippetPairId();
	
	@EmbeddedId
	@JsonIgnore
	public WorkingSetSnippetPairId getPk() {
		return pk;
	}
	
	public void setPk(WorkingSetSnippetPairId pk) {
		this.pk = pk;
	}
	
	@Column(name = "presentation_order")
	private int presentationOrder;

	public int getPresentationOrder() {
		return presentationOrder;
	}

	public void setPresentationOrder(int presentationOrder) {
		this.presentationOrder = presentationOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + presentationOrder;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkingSetSnippetPair other = (WorkingSetSnippetPair) obj;
		if (presentationOrder != other.presentationOrder)
			return false;
		return true;
	}
	
}
