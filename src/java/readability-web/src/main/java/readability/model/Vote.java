package readability.model;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "votes")
public class Vote {
	@Id
	@Column(name = "vote_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(targetEntity = ExperimentSession.class)
	@JoinColumn(name = "experiment_session_id")
	@JsonIgnore
	private ExperimentSession experimentSession;
	
	@ManyToOne(targetEntity = SnippetPair.class)
	@JoinColumn(name = "snippet_pair_id")
	private SnippetPair snippetPair;
	
	@Column(name = "snippet_order")
	private String snippetOrder;
	
	@Column(name = "arrival_time")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date arrivalTime;
	
	@Column(name = "comments")
	private String comments;
	
	@Column(name = "strength")
	private String strength;
	
	@Column(name = "other_categorical_variables")
	private String otherCategoricalVariables;
	
	@Column(name = "last_vote")
	private boolean last;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
		
	public SnippetPair getSnippetPair() {
		return snippetPair;
	}

	public void setSnippetPair(SnippetPair snippetPair) {
		this.snippetPair = snippetPair;
	}

	public String getSnippetOrder() {
		return snippetOrder;
	}
	
	public void setSnippetOrder(String snippetOrder) {
		this.snippetOrder = snippetOrder;
	}
	
	public ExperimentSession getExperimentSession() {
		return experimentSession;
	}
	
	public void setExperimentSession(ExperimentSession experimentSession) {
		this.experimentSession = experimentSession;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}
	
	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getOtherCategoricalVariables() {
		return otherCategoricalVariables;
	}

	public void setOtherCategoricalVariables(String otherCategoricalVariables) {
		this.otherCategoricalVariables = otherCategoricalVariables;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Vote() {
		this.last = false;
	}

	@Override
	public String toString() {
		return "Vote [id=" + id + ", experimentSession=" + experimentSession + ", snippetPair=" + snippetPair
				+ ", snippetOrder=" + snippetOrder + ", arrivalTime=" + arrivalTime + ", comments=" + comments
				+ ", strength=" + strength + ", otherCategoricalVariables=" + otherCategoricalVariables + ", last="
				+ last + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrivalTime == null) ? 0 : arrivalTime.hashCode());
		result = prime * result + ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((experimentSession == null) ? 0 : experimentSession.hashCode());
		result = prime * result + (last ? 1231 : 1237);
		result = prime * result + ((otherCategoricalVariables == null) ? 0 : otherCategoricalVariables.hashCode());
		result = prime * result + ((snippetOrder == null) ? 0 : snippetOrder.hashCode());
		result = prime * result + ((snippetPair == null) ? 0 : snippetPair.hashCode());
		result = prime * result + ((strength == null) ? 0 : strength.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vote other = (Vote) obj;
		if (arrivalTime == null) {
			if (other.arrivalTime != null)
				return false;
		} else if (!arrivalTime.equals(other.arrivalTime))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (experimentSession == null) {
			if (other.experimentSession != null)
				return false;
		} else if (!experimentSession.equals(other.experimentSession))
			return false;
		if (last != other.last)
			return false;
		if (otherCategoricalVariables == null) {
			if (other.otherCategoricalVariables != null)
				return false;
		} else if (!otherCategoricalVariables.equals(other.otherCategoricalVariables))
			return false;
		if (snippetOrder == null) {
			if (other.snippetOrder != null)
				return false;
		} else if (!snippetOrder.equals(other.snippetOrder))
			return false;
		if (snippetPair == null) {
			if (other.snippetPair != null)
				return false;
		} else if (!snippetPair.equals(other.snippetPair))
			return false;
		if (strength == null) {
			if (other.strength != null)
				return false;
		} else if (!strength.equals(other.strength))
			return false;
		return true;
	}
}
