package readability.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "working_sets")
public class WorkingSet {
	
	@Id
	@Column(name = "working_set_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.workingSet", cascade = CascadeType.ALL)
	private Set<WorkingSetSnippetPair> workingSetSnippetPairs = new HashSet<WorkingSetSnippetPair>(0);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Set<WorkingSetSnippetPair> getWorkingSetSnippetPairs() {
		return workingSetSnippetPairs;
	}
	
	public List<SnippetPair> getSnippetPairs() {
		List<WorkingSetSnippetPair> orderedWssp = new ArrayList<WorkingSetSnippetPair>();
		orderedWssp.addAll(workingSetSnippetPairs);
		orderedWssp.sort((w1, w2) -> w1.getPresentationOrder() - w2.getPresentationOrder());
		List<SnippetPair> snippetPairs = new ArrayList<SnippetPair>();		
		for (WorkingSetSnippetPair association : orderedWssp) {
			snippetPairs.add(association.getPk().getSnippetPair());
		}
		return snippetPairs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((workingSetSnippetPairs == null) ? 0
						: workingSetSnippetPairs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkingSet other = (WorkingSet) obj;
		if (workingSetSnippetPairs == null) {
			if (other.workingSetSnippetPairs != null)
				return false;
		} else if (!workingSetSnippetPairs.equals(other.workingSetSnippetPairs))
			return false;
		return true;
	}
	
}
