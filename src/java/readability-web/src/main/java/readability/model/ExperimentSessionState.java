package readability.model;

public enum ExperimentSessionState {
	SHOWING_INSTRUCTIONS("I"),
	SHOWING_SNIPPETS("S"),
	SHOWING_VOTE_COMMENTS("V"),
	SHOWING_PROFILE("P"),
	SHOWING_COMMENTS("C"),
	FINALIZED("F");
	
	public static final String SESSION_KEY = "ExperimentSessionState";
	
	private final String value;
	
	ExperimentSessionState(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
