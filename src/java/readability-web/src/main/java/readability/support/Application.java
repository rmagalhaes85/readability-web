package readability.support;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

@SpringBootApplication(scanBasePackages = {"readability.controllers", "readability.configuration"})
public class Application {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	private static ConfigurableApplicationContext context = null;
		
	public static void main(String[] args) {
		//System.out.println("main");
		
		if (logger.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Initializing application with the following parameters: ");
			for (String arg : args) {
				sb.append(arg + "; ");
			}
			logger.debug(sb.toString());
		}
		
		context = SpringApplication.run(Application.class, args);
	}
	
	@EventListener
	public static void handleApplicationShutdownRequest(PayloadApplicationEvent<String> event) {
		logger.info("An internal shutdown command has been issued");
		if (context == null) {
			logger.warn("Looks like the application is on an uninitialized state");
		} else {
			logger.info("Closing the application context");
			context.close();
		}
	}
	
//	@Bean
//	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//		return args -> {
//			System.out.println("Let's inspect the beans provided by Spring Boot;");
//			
//			String[] beanNames = ctx.getBeanDefinitionNames();
//			Arrays.sort(beanNames);
//			for (String beanName : beanNames) {
//				System.out.println(beanName);
//			}
//		};
//	}
}
