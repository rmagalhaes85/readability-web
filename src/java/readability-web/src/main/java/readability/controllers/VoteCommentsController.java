package readability.controllers;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import readability.model.SnippetPair;
import readability.model.Vote;
import readability.model.VoteCommentDataDTO;
import readability.services.ExperimentService;
import readability.util.SessionUtils;

@RestController
@RequestMapping("/rest/votecomment")
public class VoteCommentsController {

	private static final Logger logger = LoggerFactory.getLogger(VoteCommentsController.class);
	
	@Autowired
	private ExperimentService experimentService;
	
	@Autowired
	private SessionUtils sessionUtils;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
//		binder.addValidators(new VoteCommentsValidator());
		binder.setRequiredFields("snippetPairHash", "comments");
	}
	
	@GetMapping(value = "/")
	public ResponseEntity<VoteCommentDataDTO> getData(
			@SessionAttribute(name = "experimentSessionId", required=true) Integer experimentSessionId,
			HttpSession session,
			HttpServletResponse response) {
		logger.trace("getData: init");
		
		SnippetPair divergentSnippetPair = experimentService.getFirstDivergentSnippetPair(experimentSessionId);
		if (divergentSnippetPair == null) {
			return new ResponseEntity<VoteCommentDataDTO>(HttpStatus.NOT_FOUND);
		}
		
		Vote divergentVote = experimentService.getSnippetPairVote(experimentSessionId, divergentSnippetPair);
		if (divergentVote == null) {
			return new ResponseEntity<VoteCommentDataDTO>(HttpStatus.NOT_FOUND);
		}
		
		VoteCommentDataDTO dto = new VoteCommentDataDTO(divergentSnippetPair, 
				divergentSnippetPair.getHash(), divergentVote, divergentVote.getStrength(), null);
		
		logger.trace("getData: end");
		
		response.setContentType("application/json;charset=UTF-8");
		
		return new ResponseEntity<VoteCommentDataDTO>(dto, HttpStatus.OK);
	}
	
	@PostMapping(value = "/")
	public ResponseEntity<Void> saveData(
			@SessionAttribute(name = "experimentSessionId", required=true) Integer experimentSessionId,
			@RequestBody VoteCommentDataDTO voteCommentData,
			HttpServletResponse response) {
		logger.trace("saveData: init");
		logger.debug(String.format("experimentSessionId='%s', commentData='%s'",
				experimentSessionId, voteCommentData));
		
		String snippetPairHash = voteCommentData.getSnippetPairHash();
		String strength = voteCommentData.getStrength();
		String comments = voteCommentData.getComments();

		if (snippetPairHash == null || comments == null) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		}

		experimentService.saveVoteComments(experimentSessionId, snippetPairHash, strength, comments);
		
		logger.trace("saveData: end");
		
		response.setContentType("application/json;charset=UTF-8");
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
