package readability.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import readability.model.ExperimentSession;
import readability.model.HashDTO;
import readability.model.SnippetOrder;
import readability.model.VoteDTO;
import readability.services.ExperimentService;
import readability.validators.VoteValidator;

@RestController
@RequestMapping("/rest/vote")
public class VoteController {

	private static final Logger logger = LoggerFactory.getLogger(VoteController.class);

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(new VoteValidator());
	}
	
	@Autowired
	private ExperimentService experimentService;
	
	@PostMapping(value = "/")
	public ResponseEntity<Void> postVote(
			@RequestBody(required = true) VoteDTO voteDTO,
			@SessionAttribute(name="experimentSessionId", required=true) Integer experimentSessionId) {
		logger.debug(String.format("postVote requested with the following data: %s",
				voteDTO.toString()));
		
		String snippetPairHash = voteDTO.getSnippetPairHash();
		String snippetOrder = voteDTO.getSnippetOrder();
		String strength = voteDTO.getStrength();
		String comments = voteDTO.getComments();
		if (comments != null) {
			comments = comments.trim();
			comments = comments.isEmpty() ? null : comments;
		}
		experimentService.vote(experimentSessionId, snippetPairHash, snippetOrder, strength, comments);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	protected ResponseEntity<HashDTO> computeResponse(String value) {
		if (value == null) {
			return new ResponseEntity<HashDTO>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<HashDTO>(new HashDTO(value), HttpStatus.OK);
		}
	}
	
	@GetMapping(value = "/divergent")
	public ResponseEntity<HashDTO> getDivergent(
			@SessionAttribute(name="experimentSessionId", required=true)
			Integer experimentSessionId) {
		logger.trace("getDivergent: init");
		
		String hash = experimentService.getFirstDivergentVoteHash(experimentSessionId);
		
		if (logger.isDebugEnabled()) {
			if (hash != null) {
				logger.debug("hash = '" + hash + "'");
			} else {
				logger.debug("no hash returned");
			}
		}
		
		return computeResponse(hash);
	}
	
	@GetMapping(value = "/notvoted")
	public ResponseEntity<HashDTO> getFirstNotVoted(
			@SessionAttribute(name="experimentSessionId", required=true)
			Integer experimentSessionId) {
		if (logger.isTraceEnabled()) {
			logger.trace("getDivergent: init");
		}

		String hash = experimentService.getFirstNotVotedHash(experimentSessionId);
		
		if (logger.isDebugEnabled()) {
			if (hash != null) {
				logger.debug("hash = '" + hash + "'");
			} else {
				logger.debug("no hash returned");
			}
		}

		return computeResponse(hash);
	}
}
