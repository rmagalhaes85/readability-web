package readability.controllers;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import readability.util.ErrorMessageDTO;

@ControllerAdvice
public class ControllerValidationHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(ControllerValidationHandler.class);
	
	@Autowired
	private MessageSource messageSource;
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessageDTO processValidationError(MethodArgumentNotValidException ex) {
		return processErrors(ex.getBindingResult());
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorMessageDTO processUnhandledException(Exception ex) {
		logger.error("Unhandled exception");
		
		if (logger.isDebugEnabled()) {
			logger.debug("Exception details: ", ex);
		}
		
		ErrorMessageDTO result = new ErrorMessageDTO();
		Locale locale = LocaleContextHolder.getLocale();
		String msg = messageSource.getMessage("common.error.internal", null, locale);
		result.setGeneralMessage(msg);
		return result;
	}
	
	protected ErrorMessageDTO processErrors(BindingResult bindingResult) {
		Assert.notNull(bindingResult);
		ErrorMessageDTO result = new ErrorMessageDTO();
		Locale locale = LocaleContextHolder.getLocale();
		
		if (bindingResult.hasGlobalErrors()) {
			String msg = messageSource.getMessage(bindingResult.getGlobalError().getCode(),
					bindingResult.getGlobalError().getArguments(), locale);
			result.setGeneralMessage(msg);
		}

		List<FieldError> errors = bindingResult.getFieldErrors();
		
		for (FieldError fieldError : errors) {
			String msg = messageSource.getMessage(fieldError.getDefaultMessage(), fieldError.getArguments(), locale);
			result.addFieldError(fieldError.getField(), msg);
		}
		
		return result;
	}
	
}
