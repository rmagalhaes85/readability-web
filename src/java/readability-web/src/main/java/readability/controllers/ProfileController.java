package readability.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import readability.model.ExperimentSession;
import readability.model.Profile;
import readability.services.ExperimentService;
import readability.util.SessionUtils;
import readability.validators.ProfileValidator;

@Controller
@RequestMapping("/rest/profile")
public class ProfileController {
	private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
	
	@Autowired
	private ExperimentService experimentService;
	
	@Autowired
	private SessionUtils sessionUtils;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProfileValidator());
	}
	
	@PostMapping(value = "/")
	public ResponseEntity<Void> saveProfile(
			@Validated @RequestBody Profile profile, 
			@SessionAttribute(name="experimentSessionId", required=true) Integer experimentSessionId,
			HttpServletRequest request) throws Exception {

		logger.trace("saveProfile: init");
		logger.debug("Profile data about to be saved: " + profile.toString());
		
		HttpSession session = sessionUtils.assertSession(request);
		ExperimentSession experimentSession = sessionUtils.getExperimentSessionFromSession(session, true);
		experimentSession.setProfile(profile);
		experimentService.saveSession(experimentSession);
		
		logger.trace("saveProfile: end");
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
}
