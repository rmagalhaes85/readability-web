package readability.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import readability.model.ExperimentSession;
import readability.model.WorkingSet;
import readability.services.ExperimentService;
import readability.util.SessionUtils;

@RestController
@RequestMapping("/rest/experiment")
public class ExperimentController {
	private static final Logger logger = LoggerFactory.getLogger(ExperimentController.class);
	
	@Autowired
	private ExperimentService experimentService;
	
	@Autowired
	private SessionUtils sessionUtils;
	
	@GetMapping(value = "/workingset")
	public ResponseEntity<WorkingSet> getWorkingSet(
			@SessionAttribute(name = "experimentSessionId", required=true) Integer experimentSessionId,
			HttpServletRequest request) throws Exception {

		HttpSession session = sessionUtils.assertSession(request);
		ExperimentSession experimentSession = sessionUtils.getExperimentSessionFromSession(session, true);
		WorkingSet workingSet = experimentSession.getWorkingSet();
		if (workingSet == null) {
			logger.warn("getWorkingSet method: the workingSet is empty. "
					+ "It should've been set at the beginning of the experiment session");
			throw new NullPointerException("workingSet");
		}
		
		return new ResponseEntity<WorkingSet>(workingSet, HttpStatus.OK);
	}
}
