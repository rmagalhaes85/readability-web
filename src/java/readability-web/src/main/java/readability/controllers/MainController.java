package readability.controllers;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fasterxml.jackson.core.JsonProcessingException;

import readability.model.ExperimentSession;
import readability.model.ExperimentSessionState;
import readability.model.SnippetOrder;
import readability.services.ExperimentService;
import readability.util.RequestInfoExtractor;
import readability.util.SessionUtils;

@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	private static final String BASE_URL = "/";
	private static final String GA_COOKIE_NAME = "_ga";
	private static final String LOCALE_COOKIE_NAME = "localeCk";
	
	private static final String SPA_PAGE = "spa"; //SPA stands for Single Page Application
	private static final String VOTECOMMENTS_PAGE = "votecomments";
	private static final String THANKS_PAGE = "thanks";
	
	private ExperimentService experimentService;
	private RequestInfoExtractor requestInfoExtractor;
	private SessionUtils sessionUtils;
	
	@Autowired
	public void setExperimentService(ExperimentService experimentService) {
		this.experimentService = experimentService;
	}
	
	@Autowired
	public void setRequestInfoExtractor(RequestInfoExtractor requestInfoExtractor) {
		this.requestInfoExtractor = requestInfoExtractor;
	}
	
	@Autowired
	public void setSessionUtils(SessionUtils sessionUtils) {
		this.sessionUtils = sessionUtils;
	}
	
	protected void initializeExperimentSession(HttpServletRequest request) throws Exception {
		logger.trace("initializeExperimentSession: init");
		
		ExperimentSession experimentSession = experimentService.newExperimentSession();
		Assert.notNull(experimentSession);

		try {
			experimentSession.setClientHeaderData(requestInfoExtractor.extractFromRequestAsJsonString(request));
		} catch (JsonProcessingException e) {
			logger.error("Error processing json data regarding client headers", e);
			throw new Exception(e);
		} catch (Exception e) {
			logger.error("Error reading client headers", e);
			throw e;
		}
		
		String origin = request.getParameter("origin");
		if (StringUtils.hasText(origin)) {
			experimentSession.setOrigin(origin);
		}
		
		logger.trace("Getting or creating the web session");
		HttpSession session = request.getSession();
		
		experimentSession.setJSessionId(session.getId());
		logger.trace("Web session ID (" + session.getId() + ") associated to the experiment session");

		experimentSession.setStartTime(new Date());
		logger.trace("Experiment session started at " + experimentSession.getStartTime().toString());
		
		experimentService.saveSession(experimentSession);
		Assert.isTrue(experimentSession.getId() != 0);		
		
		logger.debug(String.format("ExperimentSession of Id '%s' created", experimentSession.getId()));
		sessionUtils.saveExperimentSessionIdInSession(session, experimentSession.getId());
		
		logger.trace("initializeExperimentSession: end");
	}
	
	@GetMapping(path = "/")
	public String index(HttpServletRequest request) throws Exception {
		initializeExperimentSession(request);
		return "languages";
	}
	
	protected ExperimentSessionState extractState(HttpSession session) {
		if (session == null) {
			return null;
		}
		return (ExperimentSessionState) session.getAttribute(ExperimentSessionState.SESSION_KEY);
	}
	
	protected ExperimentSessionState extractState(HttpServletRequest request) {
		Assert.notNull(request);
		return extractState(request.getSession(false));
	}
	
	protected String extractCookie(HttpServletRequest request, String name) {
		Assert.notNull(request);
		Assert.notNull(name);
		Cookie[] cookies = request.getCookies();
		if (cookies == null) {
			return null;
		} else {
			for (Cookie c : cookies) {
				if (name.equals(c.getName())) {
					return c.getValue();
				}
			}
			return null;
		}
	}
	
	protected void configureResponseHeaders(HttpServletResponse response) {
		response.setHeader("Expires", "0");		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
	}
	
	protected boolean checkValidState(HttpServletRequest request, ExperimentSessionState... states) {
		Assert.notNull(request);
		Assert.notNull(request.getSession(false));
		return checkValidState(request.getSession(false), states);
	}
	
	protected boolean checkValidState(HttpSession session, ExperimentSessionState... states) {
		Assert.notNull(session);
		Assert.notNull(states);

		ExperimentSessionState currentState = extractState(session);
		
		if (currentState == null) {
			return states.length == 0;
		} else {
			for (ExperimentSessionState state : states) {
				if (currentState.equals(state)) {
					return true;
				}
			}
			return false;
		}
	}
	
	protected boolean checkBasicData(HttpServletRequest request) {
//		String localeCookieVal = extractCookie(request, LOCALE_COOKIE_NAME);
//		logger.debug(String.format("localeCookieVal = '%s'", localeCookieVal));
//		return localeCookieVal != null;
		Locale locale = LocaleContextHolder.getLocale();
		return locale != null;
	}

	protected String resolveToLanguagesPage(HttpServletResponse response) throws IOException {
		response.sendRedirect(BASE_URL);
		return null;
	}
	
	protected String resolveToSpaPage(HttpServletRequest request, 
			HttpServletResponse response, ModelMap modelMap) throws IOException {
		ExperimentSession experimentSession = sessionUtils.getExperimentSessionFromSession(request, false);
		if (experimentSession == null) {
			response.sendRedirect("/");
			return null;
		} else {
			String gaCookieValue = extractCookie(request, GA_COOKIE_NAME);
			if (gaCookieValue != null) {
				experimentSession.setGaClientCookie(gaCookieValue);
			}
			String localeCookieValue = extractCookie(request, LOCALE_COOKIE_NAME);
			if (localeCookieValue != null) {
				localeCookieValue = localeCookieValue.length() > 10 ? localeCookieValue.substring(0, 10) : localeCookieValue;
				experimentSession.setLocale(localeCookieValue);
			}
			
			experimentService.saveSession(experimentSession);
			return SPA_PAGE;
		}
	}
	
	protected String resolveToVoteComments(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		boolean ok = false;

		HttpSession session = request.getSession(false);
		
		if (session != null) {
			Integer experimentSessionId = sessionUtils.getExperimentIdFromSession(session, false);
			if (experimentSessionId != null) {
				String hash = experimentService.getFirstDivergentVoteHash(experimentSessionId);
				ok = hash != null;
			}
		}
		
		if (!ok) {
			response.sendRedirect(BASE_URL);
			return null;
		} else {
			return VOTECOMMENTS_PAGE;
		}
	}
	
	protected String resolveToThanks(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			ExperimentSession experimentSession = sessionUtils.getExperimentSessionFromSession(session, false);
			if (experimentSession != null) {
				experimentSession.setEndTime(new Date());
				experimentService.saveSession(experimentSession);
			}
			session.invalidate();
		}
		return THANKS_PAGE;
	}
	
	protected String resolveGeneric(String pageName, HttpServletRequest request, HttpServletResponse response) 
			throws IOException {
		boolean sessionOk = false;
		HttpSession session = request.getSession(false);
		if (session != null) {
			// rigorously speaking, we should check also for the presence of a valid experiment session
			// using the ID stored on the session. But I'm assuming that the presence of an invalid ID
			// on the session is going to be a rare event and so, to be faster, I'm validating only the
			// presence of the ID
			sessionOk = sessionUtils.getExperimentIdFromSession(session, false) != null;
		}
		
		if (!sessionOk) {
			return resolveToLanguagesPage(response);
		} else {
			return pageName;
		}
	}
	
	protected String resolvePage(String pageName, HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap)
			throws IOException {
		if (!checkBasicData(request)) {
			return resolveToLanguagesPage(response);
		} else {
			configureResponseHeaders(response);
			switch(pageName) {
			case SPA_PAGE:
				return resolveToSpaPage(request, response, modelMap);
			case VOTECOMMENTS_PAGE:
				return resolveToVoteComments(request, response);
			case THANKS_PAGE:
				return resolveToThanks(request, response);
			default:
				return resolveGeneric(pageName, request, response);
			}
		}
	}
	
	@GetMapping(path = "/pages/{pageName:[a-z]+}")
	public String getPage(@PathVariable("pageName") String pageName,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) throws IOException {
		int workingSetSize = experimentService.getWorkingSetSize();
		modelMap.addAttribute("workingSetSize", workingSetSize);
		
		return resolvePage(pageName, request, response, modelMap);
	}
	
	@GetMapping(value = "/snippetPair/{snippetPairHash:[a-zA-Z0-9]{32}}/{snippetOrder:[a-bA-B]}")
	public void getSnippetContent(@PathVariable String snippetPairHash, @PathVariable String snippetOrder,
			HttpServletResponse response) throws IOException {
		logger.trace("getSnippetContent: init");
		logger.debug("Snippet request received. Snippet pair hash = '%s'; Snippet order = '%s'",
				snippetPairHash, snippetOrder);
		
		String content = experimentService.getSnippetContent(snippetPairHash, SnippetOrder.parse(snippetOrder));
		Assert.notNull(content);
		OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream());
		response.setStatus(200);
		response.setContentType("text/html;charset=utf-8");
		writer.write(content);
		writer.close();
		
		logger.trace("getSnippetContent: end");
	}
}
