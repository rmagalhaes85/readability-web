package readability.controllers;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import readability.services.ExperimentService;

@Controller
@RequestMapping("/rest/comments")
public class CommentsController {

	private static final Logger logger = LoggerFactory.getLogger(CommentsController.class);
	
	@Autowired
	private ExperimentService experimentService;
	
	@PostMapping(value = "/")
	public ResponseEntity<Void> save(
			@SessionAttribute(name = "experimentSessionId", required=true) Integer experimentSessionId,
			@RequestParam(name = "comments", required = false) String comments,
			HttpServletResponse response) {
		logger.trace("save: init");
		if (comments != null) {
			experimentService.saveComments(experimentSessionId, comments);
		}
		logger.trace("save: end");
		response.setContentType("application/json;charset=UTF-8");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
