package readability.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import readability.services.ExperimentService;

@Controller
@RequestMapping("/rest/sessionevent")
public class SessionEventsController {

	private static final Logger logger = LoggerFactory.getLogger(SessionEventsController.class);
	
	@Autowired
	private ExperimentService experimentService;
	
	@PostMapping(value = "/", consumes = "application/x-www-form-urlencoded")
	public ResponseEntity<Void> saveEvent(
			@SessionAttribute(name = "experimentSessionId", required = true) Integer experimentSessionId,
			@RequestParam(name="eventContent", required=true) String eventContent) {
		logger.trace("saveEvent: init");
		experimentService.saveSessionEvent(experimentSessionId, eventContent);
		logger.trace("saveEvent: end");
		return ResponseEntity.ok().header("Content-Type", "application/json;charset=UTF-8").build();
	}
}
