package readability.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.springframework.web.servlet.tags.RequestContextAwareTag;

@SuppressWarnings("serial")
public class ProfileFieldTag extends RequestContextAwareTag {
	private String fieldName;
	private String inputType;
	private Integer maxLength;
	private Boolean required;
	private String validationErrors;

	//@formatter:off
	private final String componentPrologue = 
"<div class=\"form-group\"" +
"    ng-class=\"{'has-error': profileForm.<%fieldName%>.$invalid && (profileForm.<%fieldName%>.$dirty || submitted)}\">" +
"    <div>" +
"        <label for=\"<%fieldName%>\"> <%fieldLabel%></label>";
	
	private final String inputTemplate =
"        <input type=\"<%inputType%>\" class=\"form-control\"" +
"            id=\"<%fieldName%>\"" +
"            name=\"<%fieldName%>\"" +
"            <%maxLengthText%> " +
"            ng-model-options=\"{updateOn: 'blur'}\" " +
"            ng-model=\"profile.<%fieldName%>\" <%required%> />" +
"    </div>    ";
	
	private final String likertTemplate =
"        <select class=\"form-control\""
+ "          id=\"<%fieldName%>\""
+ "          name=\"<%fieldName%>\""
+ "          ng-model-options=\"{updateOn: 'blur'}\" "
+ "          ng-model=\"profile.<%fieldName%>\">"
+ "          <option value=\"0\">0</option>"
+ "          <option value=\"1\">1</option>"
+ "          <option value=\"2\">2</option>"
+ "          <option value=\"3\">3</option>"
+ "          <option value=\"4\">4</option>"
+ "      </select>"
+ "  </div>";
	
	private final String genderTemplate =
  "      <div>"
+ "         <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" ng-model=\"profile.<%fieldName%>\" value=\"F\"/><%genderFemale%></label>"
+ "         <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" ng-model=\"profile.<%fieldName%>\" value=\"M\"/><%genderMale%></label>"
+ "      </div>"
+ "  </div>";
	
	private final String errorsPrologue = 
"    <div ng-show=\"profileForm.<%fieldName%>.$invalid && (profileForm.<%fieldName%>.$dirty || submitted)\">";
	
	private final String errorTemplate = 
"        <div>" +
"            <span><%errorMessage%></span>" +
"        </div>";
	
	private final String errorsEpilogue =
"    </div>";
	
	private final String componentEpilogue = 
"</div>";
	//@formatter:on

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public void setValidationErrors(String validationErrors) {
		this.validationErrors = validationErrors;
	}

	@Override
	protected int doStartTagInternal() throws Exception {
		final JspWriter writer = pageContext.getOut();

		if (fieldName == null) {
			throw new JspException("fieldName must be set");
		}

		if (inputType == null) {
			throw new JspException("inputType must be set");
		}
		
		String inputFieldTemplate = null;
		
		if ("likert".equals(inputType)) {
			inputFieldTemplate = likertTemplate;
		} else if ("gender".equals(inputType)) {
			inputFieldTemplate = genderTemplate;
		} else {
			inputFieldTemplate = inputTemplate;
		}

		String requiredText = (required != null && required) ? "required" : "";
		String fieldLabel = getRequestContext().getMessage(
				"profileForm.labels." + fieldName);
		String maxLengthText = (maxLength != null && maxLength > 0) ? "ng-maxlength=\""
				+ maxLength + "\""
				: "";
		String genderFemale = getRequestContext().getMessage("common.genderFemale");
		String genderMale = getRequestContext().getMessage("common.genderMale");

		String preludeText = componentPrologue + inputFieldTemplate;
		preludeText = preludeText.replace("<%fieldName%>", fieldName)
				.replace("<%inputType%>", inputType)
				.replace("<%fieldLabel%>", fieldLabel)
				.replace("<%maxLengthText%>", maxLengthText)
				.replace("<%required%>", requiredText)
				.replace("<%genderFemale%>", genderFemale)
				.replace("<%genderMale%>", genderMale);

		writer.println(preludeText);

		if (validationErrors != null) {
			String errorsPrologueText = errorsPrologue.replace("<%fieldName%>",
					fieldName);
			writer.println(errorsPrologueText);
			String[] validationErrorsArray = validationErrors.split(",");
			for (String errorName : validationErrorsArray) {
				String errorMessage = getRequestContext().getMessage(
						"profileForm.fields." + fieldName + ".error."
								+ errorName);
				String errorText = errorTemplate.replace("<%fieldName%>",
						fieldName).replace("<%errorMessage%>", errorMessage);
				writer.println(errorText);
			}
			writer.println(errorsEpilogue);
		}

		writer.println(componentEpilogue);
		return 0;
	}
}
