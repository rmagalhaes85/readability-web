package readability.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import readability.model.ExperimentSession;
import readability.services.ExperimentService;

@Component
public class SessionUtils {
	public static final String EXPERIMENT_SESSION_ID_SESSION_KEY = "experimentSessionId";
	
	@Autowired
	private ExperimentService experimentService;
	
	public Integer getExperimentIdFromSession(HttpSession session, boolean throwIfNotFound) {
		Assert.notNull(session);
		Integer experimentSessionId = (Integer) session.getAttribute(SessionUtils.EXPERIMENT_SESSION_ID_SESSION_KEY);
		if (experimentSessionId == null && throwIfNotFound) {
				throw new RuntimeException("Id not found on the session");
		}
		return experimentSessionId;
	}

	public ExperimentSession getExperimentSessionFromSession(HttpServletRequest request, boolean throwIfNotFound) {
		Assert.notNull(request);
		return getExperimentSessionFromSession(request.getSession(false), throwIfNotFound);
	}

	public ExperimentSession getExperimentSessionFromSession(HttpSession session, boolean throwIfNotFound) {
		Integer experimentSessionId = getExperimentIdFromSession(session, throwIfNotFound);

		if (experimentSessionId == null) {
			if (throwIfNotFound) {
				throw new RuntimeException("Id not found on the session");
			} else {
				return null;
			}
		}
		
		ExperimentSession experimentSession = experimentService.findById(experimentSessionId);
		if (experimentSession == null && throwIfNotFound) {
			throw new RuntimeException(String.format("Experiment Session of Id %s not found", experimentSessionId));
		}
		return experimentSession;
	}
	
	public void saveExperimentSessionIdInSession(HttpSession session, Integer experimentSessionId) {
		Assert.notNull(session);
		Assert.notNull(experimentSessionId);
		session.setAttribute(SessionUtils.EXPERIMENT_SESSION_ID_SESSION_KEY, experimentSessionId);
	}
	
	public HttpSession assertSession(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession(false);
		if (session == null) {
			throw new Exception("No Session found");
		}
		return session;
	}
}
