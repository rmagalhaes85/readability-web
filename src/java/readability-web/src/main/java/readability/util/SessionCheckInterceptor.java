package readability.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class SessionCheckInterceptor implements HandlerInterceptor {
	
	public enum Behavior {
		REDIRECT_HOME,
		FORBID
	}

	private static final Logger logger = LoggerFactory.getLogger(SessionCheckInterceptor.class);
	
	private Behavior behavior;
	
	public SessionCheckInterceptor(Behavior behavior) {
		this.behavior = behavior;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView) throws Exception {

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		logger.trace("preHandle: init");
		
		HttpSession session = request.getSession(false);
				
		boolean sessionOk = session != null && session.getAttribute(SessionUtils.EXPERIMENT_SESSION_ID_SESSION_KEY) != null;
		
		if (sessionOk) {
			logger.trace("preHandle: session " + session.getId() + " is present and "
					+ "associated to an experiment session");
			return true;
		} else if (session != null) {
			logger.warn("preHandle: session " + session.getId() + " is present, but no "
					+ "experiment session has been found.");
		} else {
			logger.warn("A sessionless request has been made to the REST API");	
		}
		
		if (behavior == Behavior.FORBID) {
			response.setStatus(403);			
		} else if (behavior == Behavior.REDIRECT_HOME) {
			response.sendRedirect("/");
		}		
		
		logger.trace("preHandle: end (no session found)");
		return false;
	}
}
