package readability.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ErrorMessageDTO {
	
	private String generalMessage;
	
	public String getGeneralMessage() {
		return generalMessage;
	}

	public void setGeneralMessage(String generalMessage) {
		this.generalMessage = generalMessage;
	}

	public class FieldError {
		private String field;
		private String message;
		
		public FieldError(String field, String message) {
			this.field = field;
			this.message = message;
		}
		
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}	
	}
	
	private List<ErrorMessageDTO.FieldError> fieldErrors;
	
	public ErrorMessageDTO() {
		fieldErrors = new ArrayList<ErrorMessageDTO.FieldError>();
	}
	
	public void addFieldError(ErrorMessageDTO.FieldError fieldError) {
		fieldErrors.add(fieldError);
	}
	
	public void addFieldError(String field, String message) {
		ErrorMessageDTO.FieldError fieldError = new ErrorMessageDTO.FieldError(field, message);
		addFieldError(fieldError);
	}
	
	public List<ErrorMessageDTO.FieldError> getErrors() {
		return Collections.unmodifiableList(fieldErrors);
	}
}
