package readability.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RequestInfoExtractor {
	private static final Logger logger = LoggerFactory.getLogger(RequestInfoExtractor.class);
	
	private static String[] headers = { "X-Forwarded-For", "User-Agent", "Accept-Language", "Forwarded" };
	
	public static final String REMOTE_ADDR_KEY = "XX-Remote-Addr";

	public Map<String, Object> extractFromRequest(HttpServletRequest request) {
		logger.trace("extractFromRequest: init");
		
		Assert.notNull(request);
		Map<String, Object> data = new HashMap<String, Object>();
		
		for (String header : headers) {
			logger.trace(String.format("Searching the header '%s'...", header));
			Enumeration<String> headerValues = request.getHeaders(header);
			if (headerValues == null) {
				logger.trace(String.format("No values for the header '%s'", header));
				continue;
			}
			List<String> values = new ArrayList<String>();
			for (String value : Collections.list(headerValues)) {
				if (value != null) {
					logger.trace(String.format("Got value '%s' from the header '%s'", value, header));
					values.add(value);
				}
			}
			if (!values.isEmpty()) {
				data.put(header, values);
			} else {
				logger.trace(String.format("No values found for the header '%s'", header));
			}
		}
		
		String remoteAddr = request.getRemoteAddr();
		if (remoteAddr != null) {
			logger.trace(String.format("Saving remote address '%s'", remoteAddr));
			data.put(REMOTE_ADDR_KEY, remoteAddr);
		} else {
			logger.warn(String.format("No remote addr found"));
		}
		
		logger.trace("extractFromRequest: end");
		return data;
	}
	
	public String toJsonString(Map<String, Object> data) throws JsonProcessingException {
		logger.trace("toJsonString: init");
		Assert.notNull(data);
		ObjectMapper mapper = new ObjectMapper();
		String valueAsString = mapper.writeValueAsString(data);
		logger.trace("toJsonString: end");
		return valueAsString;
	}
	
	public String extractFromRequestAsJsonString(HttpServletRequest request) throws JsonProcessingException {
		logger.trace("extractFromRequestAsJsonString: init");
		Assert.notNull(request);
		Map<String, Object> data = extractFromRequest(request);
		if (data == null) {
			logger.trace("No data available from the request");
			return null;
		}
		String jsonString = toJsonString(data);
		logger.trace("extractFromRequestAsJsonString: end");
		return jsonString;
	}
}
