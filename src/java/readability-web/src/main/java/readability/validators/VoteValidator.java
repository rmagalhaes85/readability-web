package readability.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import readability.model.VoteDTO;

public class VoteValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return VoteDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof VoteDTO) {
			VoteDTO vote = (VoteDTO) target;
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "snippetPairHash", "vote.hash.required");
			if (vote.getSnippetPairHash() == null || vote.getSnippetPairHash().length() != 32) {
				errors.reject("snippetPairHash", "vote.hash.size");
			}
		}
	}

}
