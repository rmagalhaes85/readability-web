package readability.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import readability.model.Profile;

public class ProfileValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ProfileValidator.class);
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Profile.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Profile profile = (Profile) target;
		
		if (profile != null) {
		
			if (logger.isDebugEnabled()) {
				logger.debug(profile.toString());
			} 

			if (profile.getName() != null && profile.getName().length() > 100) {
				errors.rejectValue("name", "profile.form.fields.name.error.maxlength");
			}
			
			if (profile.getProgrammingLanguages() != null && profile.getProgrammingLanguages().length() > 500) {
				errors.rejectValue("programmingLanguages", "profile.form.fields.programmingLanguages.error.maxlength");
			}
			
			if (profile.getNaturalLanguages() != null && profile.getNaturalLanguages().length() > 500) {
				errors.rejectValue("naturalLanguages", "profile.form.fields.naturalLanguages.error.maxlength");
			}
			
			if (profile.getOperatingSystems() != null && profile.getOperatingSystems().length() > 500) {
				errors.rejectValue("operatingSystems", "profile.form.fields.operatingSystems.error.maxlength");
			}
			
		}

		// NO VALIDATION
//		private int flossExperienceTime;

		// NO VALIDATION
//		private int age;
		
		// NO VALIDATION
//		private int yearsInSchool;
		
		// M OR F, IF ANY
//		private String gender;
		
		// MAX LENGTH 500
//		private String programmingLanguages;
		
		// MAX LENGTH 500
//		private String naturalLanguages;
		
		// MAX LENGTH 500
//		private String operatingSystems;
		
		// BETWEEN 0 AND 4
//		private int englishConfortLevel;
		
		// BETWEEN 0 AND 4
//		private int perceivedReadabilityLevel;
	}

}
