package readability.util.tests;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.Assert;

import readability.util.RequestInfoExtractor;

public class RequestInfoExtractorTests {
	private static final String DEFAULT_REMOTE_ADDR = "201.232.143.50";
	
	private static RequestInfoExtractor sut;
	
	@BeforeClass
	public static void initialize() {
		sut = new RequestInfoExtractor();
	}
	
	protected HttpServletRequest createRequest(Map<String, List<String>> headers, String remoteAddr) throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);
		
		for (String header : headers.keySet()) {
			Vector<String> vector = new Vector<String>();
			vector.addAll(headers.get(header));
			if (vector.isEmpty()) {
				throw new Exception("Empty value list (if you want no values for this header, the right semantics is not to create such header)");
			}
			when(request.getHeaders(header)).thenReturn(Collections.enumeration(vector));
		}
		
		when(request.getRemoteAddr()).thenReturn(remoteAddr);
		return request;
	}
	
	protected <T> List<T> makeList(@SuppressWarnings("unchecked") T... values) {
		Assert.notNull(values);
		List<T> list = new ArrayList<T>();
		for (T value : values) {
			list.add(value);
		}
		return list;
	}
	
	protected Map<String, List<String>> createEmptyDataSet() {
		return new HashMap<String, List<String>>();
	}
	
	protected Map<String, List<String>> createDataSet1() {
		Map<String, List<String>> data = new HashMap<String, List<String>>();
		data.put("X-Forwarded-For", makeList("192.168.0.100"));
		data.put("User-Agent", makeList("Chrome"));
		data.put("Accept-Language", makeList("en-US", "pt-BR"));
		data.put("Forwarded", makeList("201.232.143.18"));
		return data;
	}
	
	protected Map<String, List<String>> createDataSet2() {
		Map<String, List<String>> data = new HashMap<String, List<String>>();
		data.put("X-Forwarded-For", makeList("192.168.0.120"));
		data.put("User-Agent", makeList("Chrome"));
		data.put("Forwarded", makeList("201.232.143.18"));
		return data;
	}
	
	@SuppressWarnings("unchecked")
	protected Map<String, Object> deepCopy(Map<String, Object> source) {
		if (source == null) return null;
		Map<String, Object> result = new HashMap<String, Object>();
		for (String key : source.keySet()) {
			Object value = source.get(key);
			if (value instanceof String) {
				result.put(key, new String((String) value));
			} else if (value instanceof List<?>) {
				List<String> valueList = (List<String>) value;
				result.put(key, new ArrayList<String>(valueList));
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	protected boolean dataMatches(Map<String, Object> input, Map<String, List<String>> compare, String compareRemoteAddr)
			throws Exception {
		Map<String, Object> inputCopy = deepCopy(input);
		
		if (!inputCopy.containsKey(RequestInfoExtractor.REMOTE_ADDR_KEY) && compareRemoteAddr != null) {
			System.out.println("!inputCopy.containsKey(RequestInfoExtractor.REMOTE_ADDR_KEY) && compareRemoteAddr != null");
			return false;
		}
		
		String inputRemoteAddr = (String) inputCopy.get(RequestInfoExtractor.REMOTE_ADDR_KEY);
		if (inputRemoteAddr == null && compareRemoteAddr != null) {
			System.out.println("inputRemoteAddr == null && compareRemoteAddr != null");
			return false;
		} else if (!inputRemoteAddr.equals(compareRemoteAddr)) {
			System.out.println("!inputRemoteAddr.equals(compareRemoteAddr)");
			return false;
		}
		
		inputCopy.remove(RequestInfoExtractor.REMOTE_ADDR_KEY);
		Set<String> inputKeySet = inputCopy.keySet();
		Set<String> compareKeySet = compare.keySet();		
		
		if (inputKeySet.size() != compareKeySet.size()) {
			System.out.println("inputKeySet.size() = " + inputKeySet.size() + "; compareKeySet.size() = " + compareKeySet.size());
			return false;
		}
		
		if (!inputKeySet.containsAll(compareKeySet) || !compareKeySet.containsAll(inputKeySet)) {
			System.out.println("!inputKeySet.containsAll(compareKeySet) || !compareKeySet.containsAll(inputKeySet)");
			return false;
		}
		
		for (String inputKey : inputKeySet) {
			if (RequestInfoExtractor.REMOTE_ADDR_KEY.equals(inputKey)) {
				continue;
			}
			
			Object inputValues = inputCopy.get(inputKey);
			if (inputValues == null) {
				throw new Exception("There shouldn't be a empty result here (Do not specify the header in case you don't want a value for it)");
			}
			if (!(inputValues instanceof List<?>)) {
				throw new Exception("The value should be an instance of List<String>");
			}
			
			// assuming that there will be a List<String> (not the safest thing to assume)
			List<String> inputValuesList = (List<String>) inputValues;
			List<String> compareValuesList = (List<String>) compare.get(inputKey);
			
			if (inputValuesList.size() != compareValuesList.size()) {
				System.out.println("inputValuesList.size() != compareValuesList.size()");
				return false;
			}
			
			if (!inputValuesList.containsAll(compareValuesList) || !compareValuesList.containsAll(inputValuesList)) {
				System.out.println("!inputValuesList.containsAll(compareValuesList) || !compareValuesList.containsAll(inputValuesList)");
				return false;
			}
		}
		
		return true;
	}

	@Test
	public void whenThereAreNoHeadersThenOnlyRemoteAddrIsReturned() throws Exception {
		HttpServletRequest request = createRequest(createEmptyDataSet(), DEFAULT_REMOTE_ADDR);
		assertNotNull(request);
		Map<String, Object> data = sut.extractFromRequest(request);
		assertNotNull(data);
		assertEquals(1, data.size());
		assertTrue(data.containsKey(RequestInfoExtractor.REMOTE_ADDR_KEY));
		assertEquals(DEFAULT_REMOTE_ADDR, data.get(RequestInfoExtractor.REMOTE_ADDR_KEY));
	}
	
	@Test(expected = Exception.class)
	public void whenNullRequestIsPassedThenExceptionIsThrown() {
		sut.extractFromRequest(null);
	}
	
	protected void runBasicDataSetTest(Map<String, List<String>> dataSet) throws Exception {
		System.out.println("dataSet=" + dataSet.toString());
		HttpServletRequest request = createRequest(dataSet, DEFAULT_REMOTE_ADDR);
		Map<String, Object> result = sut.extractFromRequest(request);
		System.out.println("result=" + result.toString());
		System.out.println("jsonString=" + sut.toJsonString(result));
		assertTrue(dataMatches(result, dataSet, DEFAULT_REMOTE_ADDR));
	}
	
	@Test
	public void whenDataSet1IsSentThenResultMatches() throws Exception {
		runBasicDataSetTest(createDataSet1());
	}
	
	@Test
	public void whenDataSet2IsSentThenResultMatches() throws Exception {
		runBasicDataSetTest(createDataSet2());
	}
}
