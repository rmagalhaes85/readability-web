#!/bin/bash

# readability-app init.d script

### BEGIN INIT INFO
# Provides: readability-app
# Required-Start: $local_fs $network $named $syslog $time
# Required-Stop: $local_fs $network $named $syslog $time
# Default-Start: 3 4 5
# Default-Stop: 0 1 2 6
# Short-Description: Support application for the Readability Experiment
### END INIT INFO

if [ -f /etc/init.d/functions ]; then
	. /etc/init.d/functions
	LOG_SUCCESS_MSG=echo
	LOG_WARNING_MSG=echo
	LOG_FAILURE_MSG=echo
elif [ -f /lib/lsb/init-functions ]; then
	. /lib/lsb/init-functions
	LOG_SUCCESS_MSG=log_success_msg
	LOG_WARNING_MSG=log_warning_msg
	LOG_FAILURE_MSG=log_failure_msg
else
	echo "Initialization support functions file wasn't found" >&2
	exit 1
fi

PROGNAME="readability-app"
LOCKFILE="/var/lock/"$PROGNAME
PIDFILE="/var/run/$PROGNAME.pid"
pid=0
script_status=0

if [ -f "/etc/default/$PROGNAME" ]; then
	. "/etc/default/$PROGNAME"
fi

APPPATH="${READABILITY_APP_PATH:-/home/ec2-user/readability-app/current}"
LAUNCHER_CLASS="$APPPATH/org/springframework/boot/loader/WarLauncher.class"

start() {
	if [ -f "$PIDFILE" ]; then
		local stored_pid
		stored_pid=$(cat "$PIDFILE")
		ps --pid "$stored_pid" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			$LOG_WARNING_MSG "Process is already running (pid = $stored_pid)"
			exit 0
		fi
	fi

	if [ ! -d "$APPPATH" ] && [ ! -L "$APPPATH" ]; then
		$LOG_FAILURE_MSG "Application path not found: $APPPATH"
		exit 5
	fi

	if [ ! -f "$LAUNCHER_CLASS" ]; then
		$LOG_FAILURE_MSG "Application main class not found: $LAUNCHER_CLASS"
		exit 5
	fi

	which java > /dev/null 2>&1

	if [ $? -ne 0 ]; then
		$LOG_FAILURE_MSG "This service requires jre 1.8 to be installed"
		exit 6
	fi

	java -version 2>&1 | head -n1 | grep "1.8" > /dev/null 2>&1

	if [ $? -ne 0 ]; then
		$LOG_FAILURE_MSG "Java has been found, but this is not the 1.8 version"
		exit 6
	fi

	$LOG_SUCCESS_MSG "Initializing service..."

	(cd $APPPATH && 
	exec -c java -Dlogging.level.root=ERROR \
	     -Dlogging.level.org.springframework=ERROR \
	     -Dlogging.level.org.hibernate=ERROR \
	     -Dlogging.level.readability=TRACE \
	     -Dlogging.file=readability.log \
	     -Dspring.profiles.active=prod \
	     org.springframework.boot.loader.WarLauncher > /dev/null 2>&1 < /dev/null &) #

	sleep 2

	$LOG_SUCCESS_MSG "Reading the process id..."
	pid=$(ps afx | grep readability | grep java | grep -v grep | awk '{print $1}')
	
	if [ ${pid:-0} -eq 0 ]; then
		$LOG_FAILURE_MSG "Error getting the process PID"
		exit 1
	fi

	echo "$pid" > "$PIDFILE"
	touch $LOCKFILE

	$LOG_SUCCESS_MSG "Application running"
	exit 0
}

stop() {
	if [ ! -f "$PIDFILE" ]; then
		$LOG_WARNING_MSG "PID file not found (process may have already been stopped)"
		exit 0
	fi

	local stored_pid
	stored_pid=$(cat "$PIDFILE")
	ps --pid "$stored_pid" > /dev/null 2>&1

	if [ $? -ne 0 ]; then
		$LOG_WARNING_MSG "PID $stored_pid was stored, but the corresponding process hasn't been found"
		rm -f "$PIDFILE"
		rm -f "$LOCKFILE"
		exit 0
	fi

	#kill -9 "$stored_pid" > /dev/null 2>&1
	killproc -p "$PIDFILE"
	rm -f "$PIDFILE"
	rm -f "$LOCKFILE"
	exit 0
}

restart() {
	echo 
}

force_reload() {
	echo
}

print_status() {
	if [ ! -f "$PIDFILE" ]; then
		$LOG_SUCCESS_MSG "Process is not running"
		exit 3
	fi

	local stored_pid
	stored_pid=$(cat "$PIDFILE")

	ps --pid "$stored_pid" > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		$LOG_SUCCESS_MSG "Process is running (pid = $stored_pid)"
		exit 0
	fi

	if [ -f "$LOCKFILE" ]; then
		$LOG_WARNING_MSG "Process is dead, but the lock file $LOCKFILE exists"
		exit 2
	else
		$LOG_WARNING_MSG "Process is dead, but the pid file $PIDFILE exists"
		exit 1
	fi

	$LOG_FAILURE_MSG "Process status unknown"
	exit 4
}

case "$1" in
	start)
		$1
		;;
	stop)
		$1
		;;
	restart)
		$1
		;;
	force-reload)
		force_reload
		;;
	status)
		print_status
		;;
	*)
		echo "Usage: $0 {start|stop|restart|force-reload|status}" >&2
		exit 2
esac

exit 1
