package readability.services.integrationtests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import readability.model.ExperimentSession;
import readability.model.Snippet;
import readability.model.SnippetOrder;
import readability.model.SnippetPair;
import readability.model.Vote;
import readability.services.ExperimentService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = readability.support.Application.class)
public class ExperimentServiceTests {
	private static final Logger logger = LoggerFactory.getLogger(ExperimentServiceTests.class);
	private static final int WORKINGSET_SIZE = 10;

	@Autowired
	private ExperimentService experimentService;
	
	private JdbcTemplate jdbcTemplate;
	
	private class VoteTestResults {
		public Integer experimentSessionId;
		public String[] votedHashes;
	}
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	protected ExperimentSession createAndValidateNewExperimentSession() {
		ExperimentSession e = experimentService.newExperimentSession();
		assertNotNull(e);
		assertNotNull(e.getId());
		assertNotEquals((Integer) 0, e.getId());
		return e;
	}
	
	protected List<SnippetPair> getAndValidateSnippetPairs(ExperimentSession e) {
		List<SnippetPair> snippetPairs = e.getWorkingSet().getSnippetPairs();
		assertNotNull(snippetPairs);
		assertEquals(WORKINGSET_SIZE, snippetPairs.size());
		return snippetPairs;
	}
	
	@Test	
	public void whenCreateNewExperimentSessionThenIdIsNotZero() {
		logger.trace("whenCreateNewExperimentSessionThenIdIsNotZero: init");
		createAndValidateNewExperimentSession();
		logger.trace("whenCreateNewExperimentSessionThenIdIsNotZero: end");
	}
	
	@Test	
	public void whenCreateNewExperimentSessionThenWorkingSetIsDefined() {
		logger.trace("whenCreateNewExperimentSessionThenWorkingSetIsDefined: init");
		ExperimentSession e = createAndValidateNewExperimentSession();
		getAndValidateSnippetPairs(e);
		logger.trace("whenCreateNewExperimentSessionThenWorkingSetIsDefined: end");
	}
	
	@Test(expected = RuntimeException.class)
	
	public void whenVoteOnInexistentPairThenVoteIsRefused() {
		logger.trace("whenVoteOnInexistentPairThenVoteIsRefused: init");
		ExperimentSession e = createAndValidateNewExperimentSession();
		List<SnippetPair> snippetPairs = getAndValidateSnippetPairs(e);
		SnippetPair sp0 = snippetPairs.get(0);
		String hash = sp0.getHash();
		hash = "000".concat(hash.substring(3));
		experimentService.vote(e.getId(), hash, SnippetOrder.A);
		logger.trace("whenVoteOnInexistentPairThenVoteIsRefused: end");
	}
	
	@Test	
	public void whenVoteOnExistentPairThenVoteIsAccepted() {
		logger.trace("whenVoteOnExistentPairThenVoteIsAccepted: init");
		ExperimentSession e = createAndValidateNewExperimentSession();
		List<SnippetPair> snippetPairs = getAndValidateSnippetPairs(e);
		SnippetPair sp = snippetPairs.get(1);
		String hash = sp.getHash();
		experimentService.vote(e.getId(), hash, SnippetOrder.A);
		logger.trace("whenVoteOnExistentPairThenVoteIsAccepted: end");
	}
	
	@Test	
	public void whenVoteIncompleteThenPendingVoteIsSignaled() {
		logger.trace("whenVoteIncompleteThenPendingVoteIsSignaled: init");
		ExperimentSession e = createAndValidateNewExperimentSession();
		List<SnippetPair> snippetPairs = getAndValidateSnippetPairs(e);
		String hashFirst = snippetPairs.get(0).getHash();
		String hashSecond = snippetPairs.get(1).getHash();
		String hashThird = snippetPairs.get(2).getHash();
		String hashLast = snippetPairs.get(snippetPairs.size() - 1).getHash();
		String hashNotVoted = "";

		experimentService.vote(e.getId(), hashFirst, SnippetOrder.A);
		hashNotVoted = experimentService.getFirstNotVotedHash(e.getId());
		assertNotNull(hashNotVoted);
		assertEquals(hashSecond, hashNotVoted);
		
		experimentService.vote(e.getId(), hashSecond, SnippetOrder.B);
		hashNotVoted = experimentService.getFirstNotVotedHash(e.getId());
		assertNotNull(hashNotVoted);
		assertEquals(hashThird, hashNotVoted);
		
		for (int i = 2; i < snippetPairs.size() - 1; i++) {
			experimentService.vote(e.getId(), snippetPairs.get(i).getHash(), SnippetOrder.A);
		}
		hashNotVoted = experimentService.getFirstNotVotedHash(e.getId());
		assertNotNull(hashNotVoted);
		assertEquals(hashLast, hashNotVoted);
		logger.trace("whenVoteIncompleteThenPendingVoteIsSignaled: end");
	}
	
	protected VoteTestResults runVotingTest(Integer[] votes, Integer[] divergents) throws Exception {
		logger.trace("runVotingTest: init");
		Assert.notNull(votes);
		Assert.notEmpty(votes);
		Assert.notNull(divergents);

		List<Integer> divergentsList = new ArrayList<Integer>(Arrays.asList(divergents));
		
		for (Integer divergentVoteIndex : divergents) {
			if (divergentVoteIndex >= votes.length) {
				fail("The divergent votes list items must point to indexes on votes list");
			}
		}
		
		for (Integer voteIndex : votes) {
			if (voteIndex < 0 || voteIndex >= WORKINGSET_SIZE) {
				fail(String.format("One of the vote indexes (%d) is out of bounds (\"[0,%d)\")",
						voteIndex, WORKINGSET_SIZE));
			}
		}

		ExperimentSession e = createAndValidateNewExperimentSession();
		List<SnippetPair> snippetPairs = getAndValidateSnippetPairs(e);
		
		String[] votedHashes = new String[votes.length];
		SnippetOrder[] spVotes = new SnippetOrder[votes.length];

		for (int i = 0; i < votes.length; i++) {
			int voteIndex = votes[i];
			votedHashes[i] = snippetPairs.get(voteIndex).getHash();
		}
		
		for (int i = 0; i < votes.length; i++) {
			int voteIndex = votes[i];
			boolean diverge = divergentsList.contains(i);
			SnippetOrder voteOrder = snippetPairs.get(voteIndex).getMostReadableSnippetOrder();
			if (diverge) {
				voteOrder = voteOrder == SnippetOrder.A ? SnippetOrder.B : SnippetOrder.A;
			}
			spVotes[i] = voteOrder;
		}
		
		for (int i = 0; i < spVotes.length; i++) {
			experimentService.vote(e.getId(), votedHashes[i], spVotes[i]);
		}
		
		VoteTestResults products = new VoteTestResults();
		products.experimentSessionId = e.getId();
		products.votedHashes = votedHashes;

		logger.trace("runVotingTest: end");
		return products;
	}

	@Test	
	public void whenVoteCompleteThenNoPendingVoteIsSignaled() throws Exception {
		logger.trace("whenVoteCompleteThenNoPendingVoteIsSignaled: init");
		Integer[] votes =      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		Integer[] divergents = {};
		VoteTestResults products = runVotingTest(votes, divergents);
		String firstNotVoted = experimentService.getFirstNotVotedHash(products.experimentSessionId);
		assertNull(firstNotVoted);
		logger.trace("whenVoteCompleteThenNoPendingVoteIsSignaled: end");
	}

	@Test	
	public void whenThirdNotVotedThenThirdIsSignaled() throws Exception {
		logger.trace("whenVoteCompleteThenNoPendingVoteIsSignaled: init");
		Integer[] votes =      {0, 1, 3, 4, 5, 6, 7, 8, 9};
		Integer[] divergents = {};
		VoteTestResults products = runVotingTest(votes, divergents);
		String firstNotVoted = experimentService.getFirstNotVotedHash(products.experimentSessionId);
		ExperimentSession es = experimentService.findById(products.experimentSessionId);
		assertEquals(firstNotVoted, es.getWorkingSet().getSnippetPairs().get(2).getHash());
		logger.trace("whenVoteCompleteThenNoPendingVoteIsSignaled: end");
	}

	@Test	
	public void whenSecondVoteIsDivergentThenSecondVoteIsReturned() throws Exception {
		logger.trace("whenSecondVoteIsDivergentThenSecondVoteIsReturned: init");
		Integer[] votes =      {0, 1, 2};
		Integer[] divergents = {   1, 2};
		VoteTestResults products = runVotingTest(votes, divergents);
		
		String firstDivergentHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNotNull(firstDivergentHash);
		assertEquals(products.votedHashes[1], firstDivergentHash);
		logger.trace("whenSecondVoteIsDivergentThenSecondVoteIsReturned: end");
	}
	
	@Test	
	public void whenFirstVoteIsDivergentThenFirstVoteIsReturned() throws Exception {
		logger.trace("whenFirstVoteIsDivergentThenFirstVoteIsReturned: init");
		Integer[] votes =      {0, 1, 2};
		Integer[] divergents = {0};
		VoteTestResults products = runVotingTest(votes, divergents);
		
		String firstDivergentHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNotNull(firstDivergentHash);
		assertEquals(products.votedHashes[0], firstDivergentHash);
		logger.trace("whenFirstVoteIsDivergentThenFirstVoteIsReturned: end");
	}
	
	@Test	
	public void whenThereIsOnlyLastVoteAndDivergentThenLastVoteIsReturned() throws Exception {
		logger.trace("whenThereIsOnlyLastVoteAndDivergentThenLastVoteIsReturned: init");
		Integer[] votes =      {9};
		Integer[] divergents = {0};
		VoteTestResults products = runVotingTest(votes, divergents);
		
		String firstDivergentHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNotNull(firstDivergentHash);
		assertEquals(products.votedHashes[0], firstDivergentHash);
		logger.trace("whenThereIsOnlyLastVoteAndDivergentThenLastVoteIsReturned: end");
	}
	
	@Test	
	public void whenVoteCompleteButPenultimateIsDivergentThenPenultimateIsReturned() throws Exception {
		logger.trace("whenVoteCompleteButPenultimateIsDivergentThenPenultimateIsReturned: init");
		Integer[] votes =      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		Integer[] divergents = {                        8};
		VoteTestResults products = runVotingTest(votes, divergents);
		
		String firstDivergentHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNotNull(firstDivergentHash);
		assertEquals(products.votedHashes[8], firstDivergentHash);
		logger.trace("whenVoteCompleteButPenultimateIsDivergentThenPenultimateIsReturned: end");
	}
	
	@Test	
	public void whenThereIsNoDivergentThenNoDivergentVoteIsReturned() throws Exception {
		logger.trace("whenThereIsNoDivergentThenNoDivergentVoteIsReturned: init");
		Integer[] votes =      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		Integer[] divergents = {};
		VoteTestResults products = runVotingTest(votes, divergents);
		
		String firstDivergentHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNull(firstDivergentHash);
		logger.trace("whenThereIsNoDivergentThenNoDivergentVoteIsReturned: end");
	}
	
	@Test
	public void whenVoteIsRepeatedThenEverythingIsReturned() throws Exception {
		logger.trace("whenVoteIsRepeatedThenLastIsConsidered: init");
		Integer[] votes =      {0, 0};
		Integer[] divergents = {0};
		VoteTestResults products = runVotingTest(votes, divergents);
		ExperimentSession es = experimentService.findByIdWithVotes(products.experimentSessionId);
		List<Vote> resultingVotes = es.getVotes();
		for (Vote vote : resultingVotes) {
			logger.debug("Printing vote... " + vote.toString());
		}
		assertEquals(votes.length, resultingVotes.size());
		assertNotEquals(resultingVotes.get(0).getArrivalTime(), resultingVotes.get(1).getArrivalTime());
		logger.trace("whenVoteIsRepeatedThenLastIsConsidered: end");
	}
	
	@Test
	public void whenSessionHasNoVoteThenFindByVotesFindItEvenSo() throws Exception {
		logger.trace("whenSessionHasNoVoteThenFindByVotesFindItEvenSo: init");
		ExperimentSession es = createAndValidateNewExperimentSession();
		Integer experimentSessionId = es.getId();
		ExperimentSession queried = experimentService.findByIdWithVotes(experimentSessionId);
		assertNotNull(queried);
		logger.trace("whenSessionHasNoVoteThenFindByVotesFindItEvenSo: end");
	}
	
	@Test
	public void whenVoteIsRepeatedThenOnlyTheLastIsConsidered() throws Exception {
		logger.trace("whenVoteIsRepeatedThenOnlyTheLastIsConsidered: init");
		Integer[] votes =      {0, 0, 0, 1, 1, 4, 4, 4, 4, 4};
		Integer[] divergents = {0,    2,    4, 5, 6,    8   };
		VoteTestResults products = runVotingTest(votes, divergents);
		ExperimentSession es = experimentService.findByIdWithVotes(products.experimentSessionId);
		List<Vote> resultingVotes = es.getVotes();
		assertEquals(votes.length, resultingVotes.size());
		
		// Vote on snippet pair 0
		SnippetPair sp0 = es.getWorkingSet().getSnippetPairs().get(0);
		assertNotNull(sp0);
		assertNotNull(experimentService.getSnippetPairVote(products.experimentSessionId, sp0));
		Vote vote0 = experimentService.getSnippetPairVote(products.experimentSessionId, sp0);
		assertNotNull(vote0);
		// Last vote on snippet 0 was divergent
		assertNotEquals(sp0.getMostReadableSnippetOrder(), vote0.getSnippetOrder());
		
		// Vote on snippet pair 1
		SnippetPair sp1 = es.getWorkingSet().getSnippetPairs().get(1);
		assertNotNull(sp1);
		assertNotNull(experimentService.getSnippetPairVote(products.experimentSessionId, sp1));
		Vote vote1 = experimentService.getSnippetPairVote(products.experimentSessionId, sp1);
		assertNotNull(vote1);
		// Last vote on snippet 1 was divergent
		assertNotEquals(sp1.getMostReadableSnippetOrder(), vote1.getSnippetOrder());
		
		// Vote on snippet pair 4
		SnippetPair sp4 = es.getWorkingSet().getSnippetPairs().get(4);
		assertNotNull(sp4);
		Vote vote4 = experimentService.getSnippetPairVote(products.experimentSessionId, sp4);
		assertNotNull(vote4);
		// Last vote on snippet 4 was not divergent
		assertEquals(sp4.getMostReadableSnippetOrder(), vote4.getSnippetOrder());
		
		logger.trace("whenVoteIsRepeatedThenOnlyTheLastIsConsidered: end");
	}
	
	@Test
	public void whenVoteCommentIsSavedThenItIsStoredOnDatabase() throws Exception {
		logger.trace("whenVoteCommentIsSavedThenItIsStoredOnDatabase: init");

		final String COMMENT = "COMMENTS-COMENTS-COMENTS";
		final String SQL = "SELECT comments FROM vote_comments WHERE vote_id = ?";
		Integer[] votes      = {2, 4, 7};
		Integer[] divergents = {      2};

		VoteTestResults products = runVotingTest(votes, divergents);
		ExperimentSession es = experimentService.findByIdWithVotes(products.experimentSessionId);
		assertNotNull(es);

		SnippetPair sp = es.getWorkingSet().getSnippetPairs().get(7);
		assertNotNull(sp);
		
		Vote vote = experimentService.getSnippetPairVote(es.getId(), sp);
		assertNotNull(vote);
		
		String divergentVotePairHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNotNull(divergentVotePairHash);
		
		experimentService.saveVoteComments(products.experimentSessionId, divergentVotePairHash, COMMENT);
		
		String dbComment = (String) jdbcTemplate.queryForObject(SQL, new Object[] { vote.getId() }, String.class);
		assertEquals(COMMENT, dbComment);

		logger.trace("whenVoteCommentIsSavedThenItIsStoredOnDatabase: end");
	}
	
	@Test
	public void whenSessionCommentIsSavedThenItIsStoredOnDatabase() throws Exception {
		logger.trace("whenSessionCommentIsSavedThenItIsStoredOnDatabase: init");
		
		final String COMMENT = "SESSION-----COMMENT";
		final String SQL = "SELECT comments FROM experiment_sessions WHERE experiment_session_id = ?";
		ExperimentSession es = createAndValidateNewExperimentSession();
		experimentService.saveComments(es.getId(), COMMENT);
		
		String dbComment = (String) jdbcTemplate.queryForObject(SQL, new Object[] { es.getId() }, String.class);
		assertEquals(COMMENT, dbComment);
		
		logger.trace("whenSessionCommentIsSavedThenItIsStoredOnDatabase: end");
	}
	
	@Test
	public void whenAntepenultimateVoteIsDivergentThenItIsNotUsedToDecideDivergence() throws Exception {
		logger.trace("whenAntepenultimateVoteIsDivergentThenItIsNotUsedToDecideDivergence: init");
		
		Integer[] votes =      {2, 2, 2, 2, 2, 2, 2, 2, 2, 2,  2, 2};
		Integer[] divergents = {0,    2,       5,       8, 9, 10   };
		
		VoteTestResults products = runVotingTest(votes, divergents);
		String divergentVotePairHash = experimentService.getFirstDivergentVoteHash(products.experimentSessionId);
		assertNull(divergentVotePairHash);
		
		logger.trace("whenAntepenultimateVoteIsDivergentThenItIsNotUsedToDecideDivergence: end");
	}
}
