#!/bin/bash

DBNAME='readability_integration_tests'

psql -U postgres -w -f ../../db/db-down.sql -d $DBNAME 

#if [ "$1" = "reset" ] ; then
#  exit 0
#fi

psql -U postgres -w -f ../../db/db-up.sql -d $DBNAME
psql -U postgres -w -f ../../db/insert-snippets.sql -d $DBNAME

