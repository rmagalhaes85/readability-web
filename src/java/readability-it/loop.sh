#!/bin/bash

COMMAND="gradle test"
TIMES=50
counter=$TIMES
fails=0


while [ $counter -gt 0 ]
do
	echo -n "Running ($counter/$TIMES)... "
	$COMMAND > /dev/null 2>&1
	if [ $? -ne 0 ]; then
		echo "FAILED"
		fails=$(($fails+1))
	else
		echo "OK"
	fi
	counter=$(($counter-1))
done

echo "Fails: $fails of $TIMES"

