--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: find_vote_by_experiment_session_and_snippet_pair(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION find_vote_by_experiment_session_and_snippet_pair(e bigint, s bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
declare vote_id integer;
begin
	select v.vote_id into vote_id
	from votes v
	where v.experiment_session_id = $1
		and v.snippet_pair_id = $2
	order by v.arrival_time asc, v.vote_id desc
	limit 1;

	return vote_id;
end;
$_$;


ALTER FUNCTION public.find_vote_by_experiment_session_and_snippet_pair(e bigint, s bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client_headers; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE client_headers (
    client_header_id integer NOT NULL,
    json_data text
);


ALTER TABLE public.client_headers OWNER TO postgres;

--
-- Name: client_headers_client_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE client_headers_client_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_headers_client_header_id_seq OWNER TO postgres;

--
-- Name: client_headers_client_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE client_headers_client_header_id_seq OWNED BY client_headers.client_header_id;


--
-- Name: experiment_sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE experiment_sessions (
    experiment_session_id integer NOT NULL,
    profile_id integer,
    working_set_id integer,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    client_header_id integer,
    ga_client_cookie character varying(50)
);


ALTER TABLE public.experiment_sessions OWNER TO postgres;

--
-- Name: experiment_sessions_experiment_session_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE experiment_sessions_experiment_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.experiment_sessions_experiment_session_id_seq OWNER TO postgres;

--
-- Name: experiment_sessions_experiment_session_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE experiment_sessions_experiment_session_id_seq OWNED BY experiment_sessions.experiment_session_id;


--
-- Name: profiles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE profiles (
    profile_id integer NOT NULL,
    name character varying(100),
    email character varying(100),
    programming_experience_time real NOT NULL,
    java_programming_experience_time real NOT NULL,
    floss_experience_time real NOT NULL,
    age smallint NOT NULL,
    years_in_school smallint NOT NULL,
    gender character(1) NOT NULL,
    programming_languages character varying(500),
    natural_languages character varying(500),
    operating_systems character varying(500),
    english_confort_level smallint NOT NULL,
    perceived_readability_level smallint NOT NULL,
    CONSTRAINT profiles_english_confort_level_check CHECK (((english_confort_level >= 0) AND (english_confort_level <= 4))),
    CONSTRAINT profiles_gender_check CHECK ((gender = ANY (ARRAY['M'::bpchar, 'F'::bpchar]))),
    CONSTRAINT profiles_perceived_readability_level_check CHECK (((perceived_readability_level >= 0) AND (perceived_readability_level <= 4)))
);


ALTER TABLE public.profiles OWNER TO postgres;

--
-- Name: profiles_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE profiles_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_profile_id_seq OWNER TO postgres;

--
-- Name: profiles_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE profiles_profile_id_seq OWNED BY profiles.profile_id;


--
-- Name: snippet_pairs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE snippet_pairs (
    snippet_pair_id integer NOT NULL,
    hash character(32) NOT NULL,
    snippet_a integer NOT NULL,
    snippet_b integer NOT NULL,
    most_readable character(1) NOT NULL,
    CONSTRAINT snippet_pairs_most_readable_check CHECK ((most_readable = ANY (ARRAY['A'::bpchar, 'B'::bpchar])))
);


ALTER TABLE public.snippet_pairs OWNER TO postgres;

--
-- Name: snippet_pairs_snippet_pair_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE snippet_pairs_snippet_pair_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.snippet_pairs_snippet_pair_id_seq OWNER TO postgres;

--
-- Name: snippet_pairs_snippet_pair_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE snippet_pairs_snippet_pair_id_seq OWNED BY snippet_pairs.snippet_pair_id;


--
-- Name: snippets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE snippets (
    snippet_id integer NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.snippets OWNER TO postgres;

--
-- Name: snippets_snippet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE snippets_snippet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.snippets_snippet_id_seq OWNER TO postgres;

--
-- Name: snippets_snippet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE snippets_snippet_id_seq OWNED BY snippets.snippet_id;


--
-- Name: vote_comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vote_comments (
    vote_comment_id integer NOT NULL,
    vote_id integer NOT NULL,
    comments character varying(5000)
);


ALTER TABLE public.vote_comments OWNER TO postgres;

--
-- Name: vote_comments_vote_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vote_comments_vote_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vote_comments_vote_comment_id_seq OWNER TO postgres;

--
-- Name: vote_comments_vote_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE vote_comments_vote_comment_id_seq OWNED BY vote_comments.vote_comment_id;


--
-- Name: votes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE votes (
    vote_id integer NOT NULL,
    experiment_session_id integer NOT NULL,
    snippet_pair_id integer NOT NULL,
    snippet_order character(1) NOT NULL,
    arrival_time timestamp without time zone NOT NULL,
    CONSTRAINT votes_snippet_order_check CHECK ((snippet_order = ANY (ARRAY['A'::bpchar, 'B'::bpchar])))
);


ALTER TABLE public.votes OWNER TO postgres;

--
-- Name: votes_vote_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE votes_vote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.votes_vote_id_seq OWNER TO postgres;

--
-- Name: votes_vote_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE votes_vote_id_seq OWNED BY votes.vote_id;


--
-- Name: working_sets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE working_sets (
    working_set_id integer NOT NULL
);


ALTER TABLE public.working_sets OWNER TO postgres;

--
-- Name: working_sets_snippet_pairs; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE working_sets_snippet_pairs (
    working_set_id integer NOT NULL,
    snippet_pair_id integer NOT NULL,
    presentation_order smallint NOT NULL
);


ALTER TABLE public.working_sets_snippet_pairs OWNER TO postgres;

--
-- Name: working_sets_working_set_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE working_sets_working_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.working_sets_working_set_id_seq OWNER TO postgres;

--
-- Name: working_sets_working_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE working_sets_working_set_id_seq OWNED BY working_sets.working_set_id;


--
-- Name: client_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client_headers ALTER COLUMN client_header_id SET DEFAULT nextval('client_headers_client_header_id_seq'::regclass);


--
-- Name: experiment_session_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experiment_sessions ALTER COLUMN experiment_session_id SET DEFAULT nextval('experiment_sessions_experiment_session_id_seq'::regclass);


--
-- Name: profile_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY profiles ALTER COLUMN profile_id SET DEFAULT nextval('profiles_profile_id_seq'::regclass);


--
-- Name: snippet_pair_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY snippet_pairs ALTER COLUMN snippet_pair_id SET DEFAULT nextval('snippet_pairs_snippet_pair_id_seq'::regclass);


--
-- Name: snippet_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY snippets ALTER COLUMN snippet_id SET DEFAULT nextval('snippets_snippet_id_seq'::regclass);


--
-- Name: vote_comment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vote_comments ALTER COLUMN vote_comment_id SET DEFAULT nextval('vote_comments_vote_comment_id_seq'::regclass);


--
-- Name: vote_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes ALTER COLUMN vote_id SET DEFAULT nextval('votes_vote_id_seq'::regclass);


--
-- Name: working_set_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY working_sets ALTER COLUMN working_set_id SET DEFAULT nextval('working_sets_working_set_id_seq'::regclass);


--
-- Data for Name: client_headers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY client_headers (client_header_id, json_data) FROM stdin;
1	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
2	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
3	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
4	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
5	{"User-Agent":["Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
6	{"User-Agent":["Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
7	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
8	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
9	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
10	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
11	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
12	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
13	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
14	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
15	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
16	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
17	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
18	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
19	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
20	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
21	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
22	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
23	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
24	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
25	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
26	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
27	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
28	{"User-Agent":["Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"],"Accept-Language":["en-US,en;q=0.8,pt;q=0.6,eo;q=0.4,de;q=0.2,es;q=0.2"],"XX-Remote-Addr":"127.0.0.1"}
\.


--
-- Name: client_headers_client_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('client_headers_client_header_id_seq', 28, true);


--
-- Data for Name: experiment_sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY experiment_sessions (experiment_session_id, profile_id, working_set_id, start_time, end_time, client_header_id, ga_client_cookie) FROM stdin;
1	1	3	\N	\N	\N	\N
2	2	4	\N	\N	\N	\N
3	3	5	\N	\N	\N	\N
4	6	6	\N	\N	\N	\N
5	8	7	\N	\N	\N	\N
6	9	8	\N	\N	\N	\N
17	62	19	\N	\N	\N	\N
7	18	9	\N	\N	\N	\N
26	119	28	\N	\N	\N	\N
63	237	65	\N	\N	7	\N
40	177	42	\N	\N	\N	\N
41	180	43	\N	\N	\N	\N
18	73	20	\N	\N	\N	\N
8	28	10	\N	\N	\N	\N
19	74	21	\N	\N	\N	\N
20	\N	22	\N	\N	\N	\N
9	34	11	\N	\N	\N	\N
64	239	66	\N	\N	9	\N
10	36	12	\N	\N	\N	\N
11	\N	13	\N	\N	\N	\N
12	37	14	\N	\N	\N	\N
27	128	29	\N	\N	\N	\N
42	183	44	\N	\N	\N	\N
28	131	30	\N	\N	\N	\N
21	85	23	\N	\N	\N	\N
13	48	15	\N	\N	\N	\N
29	134	31	\N	\N	\N	\N
14	54	16	\N	\N	\N	\N
15	55	17	\N	\N	\N	\N
16	56	18	\N	\N	\N	\N
30	138	32	\N	\N	\N	\N
43	188	45	\N	\N	\N	\N
31	143	33	\N	\N	\N	\N
22	97	24	\N	\N	\N	\N
44	192	46	\N	\N	\N	\N
32	150	34	\N	\N	\N	\N
45	196	47	\N	\N	\N	\N
23	108	25	\N	\N	\N	\N
46	\N	48	\N	\N	\N	\N
33	155	35	\N	\N	\N	\N
24	113	26	\N	\N	\N	\N
25	116	27	\N	\N	\N	\N
65	252	67	\N	\N	22	\N
47	200	49	\N	\N	\N	\N
34	160	36	\N	\N	\N	\N
35	164	37	\N	\N	\N	\N
66	255	68	\N	\N	25	\N
67	\N	69	\N	\N	\N	\N
48	206	50	\N	\N	\N	\N
36	168	38	\N	\N	\N	\N
68	\N	70	\N	\N	\N	\N
37	171	39	\N	\N	\N	\N
38	\N	40	\N	\N	\N	\N
69	\N	71	\N	\N	\N	\N
70	\N	72	\N	\N	\N	\N
39	174	41	\N	\N	\N	\N
71	\N	73	\N	\N	\N	\N
72	\N	74	\N	\N	\N	\N
49	212	51	\N	\N	\N	\N
73	256	75	\N	\N	26	GA1.1.308755451.1489679278
50	218	52	\N	\N	\N	\N
74	\N	76	\N	\N	27	GA1.1.308755451.1489679278
75	257	77	\N	\N	28	GA1.1.308755451.1489679278
51	225	53	\N	\N	\N	\N
52	230	54	\N	\N	\N	\N
53	\N	55	\N	\N	\N	\N
54	\N	56	\N	\N	\N	\N
55	\N	57	\N	\N	\N	\N
56	\N	58	\N	\N	\N	\N
57	232	59	\N	\N	2	\N
58	234	60	\N	\N	4	\N
59	235	61	\N	\N	5	\N
60	\N	62	\N	\N	\N	\N
61	236	63	\N	\N	6	\N
62	\N	64	\N	\N	\N	\N
\.


--
-- Name: experiment_sessions_experiment_session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('experiment_sessions_experiment_session_id_seq', 75, true);


--
-- Data for Name: profiles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY profiles (profile_id, name, email, programming_experience_time, java_programming_experience_time, floss_experience_time, age, years_in_school, gender, programming_languages, natural_languages, operating_systems, english_confort_level, perceived_readability_level) FROM stdin;
1			1	1	1	1	1	F	1	1	1	0	1
2			1	1	1	1	1	M	1	1	1	0	2
3			1	1	1	1	1	M	1	1	1	0	1
6			1	1	1	1	1	M	1	1	1	0	1
8			1	1	1	1	1	M	1	1	1	0	1
9			1	1	1	1	1	F	1	1	1	0	1
10			1	1	1	1	1	F	1	1	1	0	1
14			1	1	1	1	1	F	1	1	1	0	1
15			1	1	1	1	1	F	1	1	1	0	1
16			1	1	1	1	1	F	1	1	1	0	1
17			1	1	1	1	1	F	1	1	1	0	1
18			1	1	1	1	1	F	1	1	1	0	1
19			1	1	1	1	1	F	1	1	1	0	1
20			1	1	1	1	1	F	1	1	1	0	1
21			1	1	1	1	1	F	1	1	1	0	1
22			1	1	1	1	1	F	1	1	1	0	1
23			1	1	1	1	1	F	1	1	1	0	1
24			1	1	1	1	1	F	1	1	1	0	1
25			1	1	1	1	1	F	1	1	1	0	1
26			1	1	1	1	1	F	1	1	1	0	1
27			1	1	1	1	1	F	1	1	1	0	1
28			1	1	1	1	1	F	1	1	1	0	1
29			1	1	1	1	1	F	1	1	1	0	1
30			1	1	1	1	1	F	1	1	1	0	1
31			1	1	1	1	1	F	1	1	1	0	1
32			1	1	1	1	1	F	1	1	1	0	1
33			1	1	1	1	1	F	1	1	1	0	1
34			1	1	1	1	1	F	1	1	1	0	1
35			2	2	2	22	2	F	1	1	1	0	1
36			11	1	1	1	1	M	1	1	1	0	1
37			1	1	1	1	1	F	1	1	1	0	1
38			1	1	1	1	1	M	1	1	1	0	1
39			1	1	1	1	1	M	1	1	1	0	1
40			1	1	1	1	1	F	1	1	1	0	1
41			1	1	1	1	1	F	1	1	1	0	1
42			1	1	1	1	1	M	1	1	1	0	1
43			1	1	1	1	1	M	1	1	1	0	1
44			1	1	1	1	1	M	1	1	1	0	1
45			1	1	1	1	1	M	1	1	1	0	1
46			1	1	1	1	1	M	1	1	1	0	1
47			1	1	1	1	1	M	1	1	1	0	1
48			1	1	1	1	1	M	1	1	1	0	1
49			1	1	1	1	1	M	1	1	1	0	1
50			1	1	1	1	1	M	1	1	1	0	1
51			1	1	1	1	1	M	1	1	1	0	1
52			1	1	1	1	1	M	1	1	1	0	1
53			1	1	1	1	1	M	1	1	1	0	1
54			1	1	1	1	1	M	1	1	1	0	1
55			2	1	1	1	1	M	2	2	2	0	2
56			1	1	1	1	1	M	1	1	1	0	1
57			2	2	2	2	2	M	2	22	2	0	2
58			2	2	2	2	2	M	2	22	2	0	2
59			2	2	2	2	2	M	2	22	2	0	2
60			2	2	2	2	2	M	2	22	2	0	2
61			2	2	2	2	2	M	2	22	2	0	2
62			2	2	2	2	2	M	2	22	2	0	2
63			1	1	1	1	1	M	1	1	1	0	1
64			1	1	1	1	1	M	1	1	1	0	1
65			1	1	1	1	1	M	1	1	1	0	1
66			1	1	1	1	1	M	1	1	1	0	1
67			1	1	1	1	1	M	1	1	1	0	1
68			1	1	1	1	1	M	1	1	1	0	1
69			1	1	1	1	1	M	1	1	1	0	1
70			1	1	1	1	1	M	1	1	1	0	1
71			1	1	1	1	1	M	1	1	1	0	1
72			1	1	1	1	1	M	1	1	1	0	1
73			1	1	1	1	1	M	1	1	1	0	1
74			1	1	1	1	1	M	1	1	1	0	1
75			1	1	1	1	1	M	1	1	1	0	1
76			1	1	1	1	1	M	1	1	1	0	1
77			1	1	1	1	1	M	1	1	1	0	1
78			1	1	1	1	1	M	1	1	1	0	1
79			1	1	1	1	1	M	1	1	1	0	1
80			1	1	1	1	1	M	1	1	1	0	1
81			1	1	1	1	1	M	1	1	1	0	1
82			1	1	1	1	1	M	1	1	1	0	1
83			1	1	1	1	1	M	1	1	1	0	1
84			1	1	1	1	1	M	1	1	1	0	1
85			1	1	1	1	1	M	1	1	1	0	1
86			1	1	1	1	1	M	1	1	1	0	1
87			1	1	1	1	1	M	1	1	1	0	1
88			1	1	1	1	1	M	1	1	1	0	1
89			1	1	1	1	1	M	1	1	1	0	1
90			1	1	1	1	1	M	1	1	1	0	1
91			1	1	1	1	1	M	1	1	1	0	1
92			1	1	1	1	1	M	1	1	1	0	1
93			1	1	1	1	1	M	1	1	1	0	1
94			1	1	1	1	1	M	1	1	1	0	1
95			1	1	1	1	1	M	1	1	1	0	1
96			1	1	1	1	1	M	1	1	1	0	1
97			1	1	1	1	1	M	1	1	1	0	1
98			1	1	1	1	1	M	11	1	1	0	1
99			1	1	1	1	1	M	11	1	1	0	1
100			1	1	1	1	1	M	11	1	1	0	1
101			1	1	1	1	1	M	11	1	1	0	1
102			1	1	1	1	1	M	11	1	1	0	1
103			1	1	1	1	1	M	11	1	1	0	1
104			1	1	1	1	1	M	11	1	1	0	1
105			1	1	1	1	1	M	11	1	1	0	1
106			1	1	1	1	1	M	11	1	1	0	1
107			1	1	1	1	1	M	11	1	1	0	1
108			1	1	1	1	1	M	11	1	1	0	1
109			1	1	1	1	1	F	1	1	1	0	1
110			1	1	1	1	1	F	1	1	1	0	1
111			1	1	1	1	1	F	1	1	1	0	1
112			1	1	1	1	1	F	1	1	1	0	1
113			1	1	1	1	1	F	1	1	1	0	1
114			1	1	1	1	1	M	11	1	1	0	1
115			1	1	1	1	1	M	11	1	1	0	1
116			1	1	1	1	1	M	11	1	1	0	1
117			1	1	1	1	1	F	1	1	1	0	2
118			1	1	1	1	1	F	1	1	1	0	2
119			1	1	1	1	1	F	1	1	1	0	2
120			1	1	1	1	1	F	1	1	1	0	1
121			1	1	1	1	1	F	1	1	1	0	1
122			1	1	1	1	1	F	1	1	1	0	1
123			1	1	1	1	1	F	1	1	1	0	1
124			1	1	1	1	1	F	1	1	1	0	1
125			1	1	1	1	1	F	1	1	1	0	1
126			1	1	1	1	1	F	1	1	1	0	1
127			1	1	1	1	1	F	1	1	1	0	1
128			1	1	1	1	1	F	1	1	1	0	1
129			1	1	1	1	1	M	1	1	1	0	1
130			1	1	1	1	1	M	1	1	1	0	1
131			1	1	1	1	1	M	1	1	1	0	1
132			2	2	2	2	2	F	1	1	1	0	1
133			2	2	2	2	2	F	1	1	1	0	1
134			2	2	2	2	2	F	1	1	1	0	1
135			1	1	1	1	1	M	1	1	1	0	1
136			1	1	1	1	1	M	1	1	1	0	1
137			1	1	1	1	1	M	1	1	1	0	1
138			1	1	1	1	1	M	1	1	1	0	1
139			1	1	1	1	1	M	1	1	1	0	1
140			1	1	1	1	1	M	1	1	1	0	1
141			1	1	1	1	1	M	1	1	1	0	1
142			1	1	1	1	1	M	1	1	1	0	1
143			1	1	1	1	1	M	1	1	1	0	1
144			1	1	1	1	1	M	1	1	1	0	1
145			1	1	1	1	1	M	1	1	1	0	1
146			1	1	1	1	1	M	1	1	1	0	1
147			1	1	1	1	1	M	1	1	1	0	1
148			1	1	1	1	1	M	1	1	1	0	1
149			1	1	1	1	1	M	1	1	1	0	1
150			1	1	1	1	1	M	1	1	1	0	1
151			22	2	1	1	1	M	1	1	1	0	1
152			22	2	1	1	1	M	1	1	1	0	1
153			22	2	1	1	1	M	1	1	1	0	1
154			22	2	1	1	1	M	1	1	1	0	1
155			22	2	1	1	1	M	1	1	1	0	1
156			1	1	1	1	1	M	1	1	1	0	1
157			1	1	1	1	1	M	1	1	1	0	1
158			1	1	1	1	1	M	1	1	1	0	1
159			1	1	1	1	1	M	1	1	1	0	1
160			1	1	1	1	1	M	1	1	1	0	1
161			1	1	1	1	1	M	1	1	1	0	1
162			1	1	1	1	1	M	1	1	1	0	1
163			1	1	1	1	1	M	1	1	1	0	1
164			1	1	1	1	1	M	1	1	1	0	1
165			1	1	1	1	1	M	1	1	1	0	1
166			1	1	1	1	1	M	1	1	1	0	1
167			1	1	1	1	1	M	1	1	1	0	1
168			1	1	1	1	1	M	1	1	1	0	1
169			1	1	1	1	1	M	1	1	1	0	1
170			1	1	1	1	1	M	1	1	1	0	1
171			1	1	1	1	1	M	1	1	1	0	1
172			1	1	1	1	1	F	1	1	1	0	1
173			1	1	1	1	1	F	1	1	1	0	1
174			1	1	1	1	1	F	1	1	1	0	1
175			1	1	1	1	1	M	1	1	1	0	1
176			1	1	1	1	1	M	1	1	1	0	1
177			1	1	1	1	1	M	1	1	1	0	1
178			1	1	1	1	1	M	1	1	1	0	1
179			1	1	1	1	1	M	1	1	1	0	1
180			1	1	1	1	1	M	1	1	1	0	1
181			1	1	1	1	1	M	1	1	1	0	1
182			1	1	1	1	1	M	1	1	1	0	1
183			1	1	1	1	1	M	1	1	1	0	1
184			1	1	1	1	1	F	1	1	1	0	1
185			1	1	1	1	1	F	1	1	1	0	1
186			1	1	1	1	1	F	1	1	1	0	1
187			1	1	1	1	1	F	1	1	1	0	1
188			1	1	1	1	1	F	1	1	1	0	1
189			1	1	1	1	1	M	1	1	1	0	1
190			1	1	1	1	1	M	1	1	1	0	1
191			1	1	1	1	1	M	1	1	1	0	1
192			1	1	1	1	1	M	1	1	1	0	1
193			1	1	1	1	1	M	1	1	1	0	1
194			1	1	1	1	1	M	1	1	1	0	1
195			1	1	1	1	1	M	1	1	1	0	1
196			1	1	1	1	1	M	1	1	1	0	1
197			1	1	1	1	1	M	1	1	1	0	1
198			1	1	1	1	1	M	1	1	1	0	1
199			1	1	1	1	1	M	1	1	1	0	1
200			1	1	1	1	1	M	1	1	1	0	1
201			1	1	1	1	1	M	1	1	1	0	1
202			1	1	1	1	1	M	1	1	1	0	1
203			1	1	1	1	1	M	1	1	1	0	1
204			1	1	1	1	1	M	1	1	1	0	1
205			1	1	1	1	1	M	1	1	1	0	1
206			1	1	1	1	1	M	1	1	1	0	1
207			1	1	1	1	1	M	1	1	1	0	1
208			1	1	1	1	1	M	1	1	1	0	1
209			1	1	1	1	1	M	1	1	1	0	1
210			1	1	1	1	1	M	1	1	1	0	1
211			1	1	1	1	1	M	1	1	1	0	1
212			1	1	1	1	1	M	1	1	1	0	1
213			1	1	1	1	1	M	1	1	1	0	1
214			1	1	1	1	1	M	1	1	1	0	1
215			1	1	1	1	1	M	1	1	1	0	1
216			1	1	1	1	1	M	1	1	1	0	1
217			1	1	1	1	1	M	1	1	1	0	1
218			1	1	1	1	1	M	1	1	1	0	1
219			1	1	1	1	1	M	11	11	1	0	1
220			1	1	1	1	1	M	11	11	1	0	1
221			1	1	1	1	1	M	11	11	1	0	1
222			1	1	1	1	1	M	11	11	1	0	1
223			1	1	1	1	1	M	11	11	1	0	1
224			1	1	1	1	1	M	11	11	1	0	1
225			1	1	1	1	1	M	11	11	1	0	1
226			2	2	2	2	2	F	2	2	2	0	3
227			2	2	2	2	2	F	2	2	2	0	3
228			2	2	2	2	2	F	2	2	2	0	3
229			2	2	2	2	2	F	2	2	2	0	3
230			2	2	2	2	2	F	2	2	2	0	3
231			1	1	1	1	1	F	1	1	1	0	1
232			1	1	1	1	1	F	1	1	1	0	1
233			1	1	1	1	1	F	1	1	1	0	1
234			1	1	1	1	1	F	1	1	1	0	1
235			1	1	1	1	1	M	1	1	1	0	1
236			1	1	1	1	1	F	1	1	1	0	1
237			1	1	1	1	1	M	1	1	1	0	2
238			1	1	1	1	1	M	1	1	1	0	1
239			1	1	1	1	1	M	1	1	1	0	1
240			1	1	1	1	1	F	1	1	1	0	1
241			1	1	1	1	1	M	1	1	1	0	4
242			1	1	1	1	1	M	1	1	1	0	4
243			1	1	1	1	1	M	1	1	1	0	4
244			1	1	1	1	1	M	1	1	1	0	4
245			1	1	1	1	1	M	1	1	1	0	4
246			1	1	1	1	1	M	1	1	1	0	4
247			1	1	1	1	1	M	1	1	1	0	4
248			1	1	1	1	1	M	1	1	1	0	4
249			1	1	1	1	1	M	1	1	1	0	4
250			1	1	1	1	1	M	1	1	1	0	4
251			1	1	1	1	1	M	1	1	1	0	4
252			1	1	1	1	1	M	1	1	1	0	4
253			1	1	1	1	1	F	1	1	1	0	1
254			1	1	1	1	1	F	1	1	1	0	1
255			1	1	1	1	1	F	1	1	1	0	1
256			1	1	1	1	1	F	1	1	1	0	1
257			2	2	2	2	2	F	2	2	2	0	2
\.


--
-- Name: profiles_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('profiles_profile_id_seq', 257, true);


--
-- Data for Name: snippet_pairs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY snippet_pairs (snippet_pair_id, hash, snippet_a, snippet_b, most_readable) FROM stdin;
1	b026324c6904b2a9cb4b88d6d61c81d1	1	2	A
2	26ab0db90d72e28ad0ba1e22ee510510	3	4	A
3	6d7fce9fee471194aa8b5b6e47267f03	5	6	A
4	48a24b70a0b376535542b996af517398	7	8	A
5	1dcca23355272056f04fe8bf20edfce0	9	10	A
6	9ae0ea9e3c9c6e1b9b6252c8395efdc1	11	12	A
7	84bc3da1b3e33a18e8d5e1bdd7a18d7a	13	14	A
8	c30f7472766d25af1dc80b3ffc9a58c7	15	16	A
9	7c5aba41f53293b712fd86d08ed5b36e	17	18	A
10	31d30eea8d0968d6458e0ad0027c9f80	19	20	A
11	166d77ac1b46a1ec38aa35ab7e628ab5	21	22	A
12	2737b49252e2a4c0fe4c342e92b13285	23	24	A
13	aa6ed9e0f26a6eba784aae8267df1951	25	26	A
14	367764329430db34be92fd14a7a770ee	27	28	A
15	8c9eb686bf3eb5bd83d9373eadf6504b	29	30	A
16	5b6b41ed9b343fed9cd05a66d36650f0	31	32	A
17	4d095eeac8ed659b1ce69dcef32ed0dc	33	34	A
18	cf4278314ef8e4b996e1b798d8eb92cf	35	36	A
19	3bb50ff8eeb7ad116724b56a820139fa	37	38	A
20	dbbf8220893d497d403bb9cdf49db7a4	39	40	A
21	fe9d26c3e620eeb69bd166c8be89fb8f	41	42	A
22	2fc57d6f63a9ee7e2f21a26fa522e3b6	43	44	A
23	2a53da1a6fbfc0bafdd96b0a2ea29515	45	46	A
24	7c67493bd72ceff21059c3d924d17518	47	48	A
25	2a52a5e65fc3c43f409550dfad1f904f	49	50	A
26	b0771132ab2531a40c9941375ed8e290	51	52	A
27	66a7c1d5cb75ef2542524d888fd32f4a	53	54	A
28	51a6d96331d5eaa300358c7a0faf168d	55	56	A
29	22cab69bca05d296a2d779a52cdee643	57	58	A
30	d5b4c7d9b06b60a7846c4529834c9812	59	60	A
31	4f89a0f6c113ae3ff279af1e6c6286bb	61	62	A
32	bb743fc2a7213949f25593c51cbee64f	63	64	A
\.


--
-- Name: snippet_pairs_snippet_pair_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('snippet_pairs_snippet_pair_id_seq', 32, true);


--
-- Data for Name: snippets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY snippets (snippet_id, content) FROM stdin;
1	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 1<span style="color: #333333">;</span> </pre></div>
2	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 2<span style="color: #333333">;</span> </pre></div>
3	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 3<span style="color: #333333">;</span> </pre></div>
4	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 4<span style="color: #333333">;</span> </pre></div>
5	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 5<span style="color: #333333">;</span> </pre></div>
6	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 6<span style="color: #333333">;</span> </pre></div>
7	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 7<span style="color: #333333">;</span> </pre></div>
8	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 8<span style="color: #333333">;</span> </pre></div>
9	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 9<span style="color: #333333">;</span> </pre></div>
10	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 10<span style="color: #333333">;</span> </pre></div>
11	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 11<span style="color: #333333">;</span> </pre></div>
12	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 12<span style="color: #333333">;</span> </pre></div>
13	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 13<span style="color: #333333">;</span> </pre></div>
14	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 14<span style="color: #333333">;</span> </pre></div>
15	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 15<span style="color: #333333">;</span> </pre></div>
16	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 16<span style="color: #333333">;</span> </pre></div>
17	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 17<span style="color: #333333">;</span> </pre></div>
18	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 18<span style="color: #333333">;</span> </pre></div>
19	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 19<span style="color: #333333">;</span> </pre></div>
20	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 20<span style="color: #333333">;</span> </pre></div>
21	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 21<span style="color: #333333">;</span> </pre></div>
22	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 22<span style="color: #333333">;</span> </pre></div>
23	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 23<span style="color: #333333">;</span> </pre></div>
24	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 24<span style="color: #333333">;</span> </pre></div>
25	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 25<span style="color: #333333">;</span> </pre></div>
26	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 26<span style="color: #333333">;</span> </pre></div>
27	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 27<span style="color: #333333">;</span> </pre></div>
28	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 28<span style="color: #333333">;</span> </pre></div>
29	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 29<span style="color: #333333">;</span> </pre></div>
30	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 30<span style="color: #333333">;</span> </pre></div>
31	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 31<span style="color: #333333">;</span> </pre></div>
32	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 32<span style="color: #333333">;</span> </pre></div>
33	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 33<span style="color: #333333">;</span> </pre></div>
34	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 34<span style="color: #333333">;</span> </pre></div>
35	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 35<span style="color: #333333">;</span> </pre></div>
36	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 36<span style="color: #333333">;</span> </pre></div>
37	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 37<span style="color: #333333">;</span> </pre></div>
38	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 38<span style="color: #333333">;</span> </pre></div>
39	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 39<span style="color: #333333">;</span> </pre></div>
40	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 40<span style="color: #333333">;</span> </pre></div>
41	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 41<span style="color: #333333">;</span> </pre></div>
42	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 42<span style="color: #333333">;</span> </pre></div>
43	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 43<span style="color: #333333">;</span> </pre></div>
44	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 44<span style="color: #333333">;</span> </pre></div>
45	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 45<span style="color: #333333">;</span> </pre></div>
46	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 46<span style="color: #333333">;</span> </pre></div>
47	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 47<span style="color: #333333">;</span> </pre></div>
48	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 48<span style="color: #333333">;</span> </pre></div>
49	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 49<span style="color: #333333">;</span> </pre></div>
50	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 50<span style="color: #333333">;</span> </pre></div>
51	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 51<span style="color: #333333">;</span> </pre></div>
52	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 52<span style="color: #333333">;</span> </pre></div>
53	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 53<span style="color: #333333">;</span> </pre></div>
54	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 54<span style="color: #333333">;</span> </pre></div>
55	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 55<span style="color: #333333">;</span> </pre></div>
56	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 56<span style="color: #333333">;</span> </pre></div>
57	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 57<span style="color: #333333">;</span> </pre></div>
58	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 58<span style="color: #333333">;</span> </pre></div>
59	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 59<span style="color: #333333">;</span> </pre></div>
60	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 60<span style="color: #333333">;</span> </pre></div>
61	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 61<span style="color: #333333">;</span> </pre></div>
62	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 62<span style="color: #333333">;</span> </pre></div>
63	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 63<span style="color: #333333">;</span> </pre></div>
64	<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Autowired</span> <span style="color: #008800; font-weight: bold">private</span> MessageSource Template 64<span style="color: #333333">;</span> </pre></div>
\.


--
-- Name: snippets_snippet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('snippets_snippet_id_seq', 64, true);


--
-- Data for Name: vote_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY vote_comments (vote_comment_id, vote_id, comments) FROM stdin;
\.


--
-- Name: vote_comments_vote_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vote_comments_vote_comment_id_seq', 1, false);


--
-- Data for Name: votes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY votes (vote_id, experiment_session_id, snippet_pair_id, snippet_order, arrival_time) FROM stdin;
7	7	14	B	2017-03-10 00:26:51.441
8	7	16	B	2017-03-10 00:39:54.644
9	7	14	B	2017-03-10 00:26:51.441
10	7	16	A	2017-03-10 00:39:54.644
11	7	14	B	2017-03-10 00:26:51.441
12	7	16	A	2017-03-10 00:39:54.644
13	7	22	A	2017-03-10 00:39:59.711
14	7	14	B	2017-03-10 00:26:51.441
15	7	16	A	2017-03-10 00:39:54.644
16	7	22	A	2017-03-10 00:39:59.711
17	7	10	B	2017-03-10 00:40:14.838
18	7	14	B	2017-03-10 00:26:51.441
19	7	16	A	2017-03-10 00:39:54.644
20	7	22	A	2017-03-10 00:39:59.711
21	7	10	A	2017-03-10 00:40:14.838
22	8	31	A	2017-03-10 01:53:23.944
23	8	31	A	2017-03-10 01:53:23.944
24	8	10	B	2017-03-10 01:53:25.114
25	8	31	A	2017-03-10 01:53:23.944
26	8	10	B	2017-03-10 01:53:25.114
27	8	3	A	2017-03-10 01:53:27.058
28	8	31	A	2017-03-10 01:53:23.944
29	8	10	B	2017-03-10 01:53:25.114
30	8	3	A	2017-03-10 01:53:27.058
31	8	14	B	2017-03-10 01:53:28.086
32	8	31	A	2017-03-10 01:53:23.944
33	8	10	B	2017-03-10 01:53:25.114
34	8	3	A	2017-03-10 01:53:27.058
35	8	14	B	2017-03-10 01:53:28.086
36	8	6	A	2017-03-10 01:53:29.22
37	8	31	A	2017-03-10 01:53:23.944
38	8	10	B	2017-03-10 01:53:25.114
39	8	3	A	2017-03-10 01:53:27.058
40	8	14	B	2017-03-10 01:53:28.086
41	8	6	A	2017-03-10 01:53:29.22
42	8	23	B	2017-03-10 01:53:30.528
43	8	31	A	2017-03-10 01:53:23.944
44	8	10	B	2017-03-10 01:53:25.114
45	8	3	A	2017-03-10 01:53:27.058
46	8	14	B	2017-03-10 01:53:28.086
47	8	6	A	2017-03-10 01:53:29.22
48	8	23	B	2017-03-10 01:53:30.528
49	8	20	A	2017-03-10 01:53:31.67
50	8	31	A	2017-03-10 01:53:23.944
51	8	10	B	2017-03-10 01:53:25.114
52	8	3	A	2017-03-10 01:53:27.058
53	8	14	B	2017-03-10 01:53:28.086
54	8	6	A	2017-03-10 01:53:29.22
55	8	23	B	2017-03-10 01:53:30.528
56	8	20	A	2017-03-10 01:53:31.67
57	8	29	B	2017-03-10 01:53:32.791
58	8	31	A	2017-03-10 01:53:23.944
59	8	10	B	2017-03-10 01:53:25.114
60	8	3	A	2017-03-10 01:53:27.058
61	8	14	B	2017-03-10 01:53:28.086
62	8	6	A	2017-03-10 01:53:29.22
63	8	23	B	2017-03-10 01:53:30.528
64	8	20	A	2017-03-10 01:53:31.67
65	8	29	B	2017-03-10 01:53:32.791
66	8	12	A	2017-03-10 01:53:34.321
67	9	24	B	2017-03-10 15:06:42.107
68	9	24	B	2017-03-10 15:06:42.107
69	9	18	B	2017-03-10 15:06:43.563
70	9	24	B	2017-03-10 15:06:42.107
71	9	18	B	2017-03-10 15:06:43.563
72	9	17	B	2017-03-10 15:06:46.21
73	9	24	B	2017-03-10 15:06:42.107
74	9	18	B	2017-03-10 15:06:43.563
75	9	17	B	2017-03-10 15:06:46.21
76	9	22	B	2017-03-10 15:06:46.985
77	9	24	B	2017-03-10 15:06:42.107
78	9	18	B	2017-03-10 15:06:43.563
79	9	17	B	2017-03-10 15:06:46.21
80	9	22	B	2017-03-10 15:06:46.985
81	9	30	B	2017-03-10 15:06:47.75
82	13	16	A	2017-03-10 15:45:13.677
83	13	16	A	2017-03-10 15:45:13.677
84	13	7	B	2017-03-10 15:45:14.797
85	13	16	A	2017-03-10 15:45:13.677
86	13	7	B	2017-03-10 15:45:14.797
87	13	27	A	2017-03-10 15:45:15.937
88	13	16	A	2017-03-10 15:45:13.677
89	13	7	B	2017-03-10 15:45:14.797
90	13	27	A	2017-03-10 15:45:15.937
91	13	26	B	2017-03-10 15:45:16.934
92	14	6	A	2017-03-10 15:59:47.508
93	14	6	A	2017-03-10 15:59:47.508
94	14	27	B	2017-03-10 15:59:49.761
95	14	6	A	2017-03-10 15:59:47.508
96	14	27	B	2017-03-10 15:59:49.761
97	14	11	A	2017-03-10 15:59:50.969
98	14	6	A	2017-03-10 15:59:47.508
99	14	27	B	2017-03-10 15:59:49.761
100	14	11	A	2017-03-10 15:59:50.969
101	17	23	A	2017-03-10 16:10:12.131
102	17	23	A	2017-03-10 16:10:12.131
103	17	27	B	2017-03-10 16:10:13.221
104	17	23	A	2017-03-10 16:10:12.131
105	17	27	B	2017-03-10 16:10:13.221
106	17	26	B	2017-03-10 16:10:15.111
107	17	23	B	2017-03-10 16:10:12.131
108	17	27	B	2017-03-10 16:10:13.221
109	17	26	B	2017-03-10 16:10:15.111
110	17	23	A	2017-03-10 16:10:12.131
111	17	27	B	2017-03-10 16:10:13.221
112	17	26	B	2017-03-10 16:10:15.111
113	18	7	A	2017-03-10 16:41:23.379
114	18	7	A	2017-03-10 16:41:23.379
115	18	8	B	2017-03-10 16:41:24.404
116	18	7	A	2017-03-10 16:41:23.379
117	18	8	B	2017-03-10 16:41:24.404
118	18	16	B	2017-03-10 16:41:26.554
119	18	7	A	2017-03-10 16:41:23.379
120	18	8	B	2017-03-10 16:41:24.404
121	18	16	B	2017-03-10 16:41:26.554
122	18	29	B	2017-03-10 16:41:27.396
123	18	7	A	2017-03-10 16:41:23.379
124	18	8	B	2017-03-10 16:41:24.404
125	18	16	B	2017-03-10 16:41:26.554
126	18	29	B	2017-03-10 16:41:27.396
127	18	13	A	2017-03-10 16:41:28.821
128	18	7	A	2017-03-10 16:41:23.379
129	18	8	B	2017-03-10 16:41:24.404
130	18	16	B	2017-03-10 16:41:26.554
131	18	29	B	2017-03-10 16:41:27.396
132	18	13	A	2017-03-10 16:41:28.821
133	18	27	A	2017-03-10 16:41:30.42
134	18	7	A	2017-03-10 16:41:23.379
135	18	8	B	2017-03-10 16:41:24.404
136	18	16	B	2017-03-10 16:41:26.554
137	18	29	B	2017-03-10 16:41:27.396
138	18	13	A	2017-03-10 16:41:28.821
139	18	27	A	2017-03-10 16:41:30.42
140	18	10	A	2017-03-10 16:41:40.811
141	18	7	A	2017-03-10 16:41:23.379
142	18	8	B	2017-03-10 16:41:24.404
143	18	16	B	2017-03-10 16:41:26.554
144	18	29	B	2017-03-10 16:41:27.396
145	18	13	A	2017-03-10 16:41:28.821
146	18	27	A	2017-03-10 16:41:30.42
147	18	10	A	2017-03-10 16:41:40.811
148	18	12	B	2017-03-10 16:41:43.301
149	18	7	A	2017-03-10 16:41:23.379
150	18	8	B	2017-03-10 16:41:24.404
151	18	16	B	2017-03-10 16:41:26.554
152	18	29	B	2017-03-10 16:41:27.396
153	18	13	A	2017-03-10 16:41:28.821
154	18	27	A	2017-03-10 16:41:30.42
155	18	10	A	2017-03-10 16:41:40.811
156	18	12	B	2017-03-10 16:41:43.301
157	18	9	A	2017-03-10 16:41:51.003
158	18	7	A	2017-03-10 16:41:23.379
159	18	8	B	2017-03-10 16:41:24.404
160	18	16	B	2017-03-10 16:41:26.554
161	18	29	B	2017-03-10 16:41:27.396
162	18	13	A	2017-03-10 16:41:28.821
163	18	27	A	2017-03-10 16:41:30.42
164	18	10	A	2017-03-10 16:41:40.811
165	18	12	B	2017-03-10 16:41:43.301
166	18	9	A	2017-03-10 16:41:51.003
167	18	30	B	2017-03-10 16:41:53.011
168	21	29	A	2017-03-10 18:38:39.667
169	21	29	A	2017-03-10 18:38:39.667
170	21	26	A	2017-03-10 18:38:41.055
171	21	29	A	2017-03-10 18:38:39.667
172	21	26	A	2017-03-10 18:38:41.055
173	21	23	B	2017-03-10 18:38:42.91
174	21	29	A	2017-03-10 18:38:39.667
175	21	26	A	2017-03-10 18:38:41.055
176	21	23	B	2017-03-10 18:38:42.91
177	21	11	A	2017-03-10 18:38:58.774
178	21	29	A	2017-03-10 18:38:39.667
179	21	26	A	2017-03-10 18:38:41.055
180	21	23	B	2017-03-10 18:38:42.91
181	21	11	A	2017-03-10 18:38:58.774
182	21	21	B	2017-03-10 18:39:00.273
183	21	29	A	2017-03-10 18:38:39.667
184	21	26	A	2017-03-10 18:38:41.055
185	21	23	B	2017-03-10 18:38:42.91
186	21	11	A	2017-03-10 18:38:58.774
187	21	21	B	2017-03-10 18:39:00.273
188	21	28	B	2017-03-10 18:39:00.956
189	21	29	A	2017-03-10 18:38:39.667
190	21	26	A	2017-03-10 18:38:41.055
191	21	23	B	2017-03-10 18:38:42.91
192	21	11	A	2017-03-10 18:38:58.774
193	21	21	B	2017-03-10 18:39:00.273
194	21	28	B	2017-03-10 18:39:00.956
195	21	20	B	2017-03-10 18:39:01.617
196	21	29	A	2017-03-10 18:38:39.667
197	21	26	A	2017-03-10 18:38:41.055
198	21	23	B	2017-03-10 18:38:42.91
199	21	11	A	2017-03-10 18:38:58.774
200	21	21	B	2017-03-10 18:39:00.273
201	21	28	B	2017-03-10 18:39:00.956
202	21	20	B	2017-03-10 18:39:01.617
203	21	7	B	2017-03-10 18:39:02.425
204	21	29	A	2017-03-10 18:38:39.667
205	21	26	A	2017-03-10 18:38:41.055
206	21	23	B	2017-03-10 18:38:42.91
207	21	11	A	2017-03-10 18:38:58.774
208	21	21	B	2017-03-10 18:39:00.273
209	21	28	B	2017-03-10 18:39:00.956
210	21	20	B	2017-03-10 18:39:01.617
211	21	7	B	2017-03-10 18:39:02.425
212	21	19	B	2017-03-10 18:39:03.11
213	21	29	A	2017-03-10 18:38:39.667
214	21	26	A	2017-03-10 18:38:41.055
215	21	23	B	2017-03-10 18:38:42.91
216	21	11	A	2017-03-10 18:38:58.774
217	21	21	B	2017-03-10 18:39:00.273
218	21	28	B	2017-03-10 18:39:00.956
219	21	20	B	2017-03-10 18:39:01.617
220	21	7	B	2017-03-10 18:39:02.425
221	21	19	B	2017-03-10 18:39:03.11
222	21	24	B	2017-03-10 18:39:08.283
223	22	18	B	2017-03-10 18:41:39.726
224	22	18	B	2017-03-10 18:41:39.726
225	22	8	B	2017-03-10 18:41:40.436
226	22	18	B	2017-03-10 18:41:39.726
227	22	8	B	2017-03-10 18:41:40.436
228	22	28	B	2017-03-10 18:41:41.114
229	22	18	B	2017-03-10 18:41:39.726
230	22	8	B	2017-03-10 18:41:40.436
231	22	28	B	2017-03-10 18:41:41.114
232	22	29	B	2017-03-10 18:41:41.762
233	22	18	B	2017-03-10 18:41:39.726
234	22	8	B	2017-03-10 18:41:40.436
235	22	28	B	2017-03-10 18:41:41.114
236	22	29	B	2017-03-10 18:41:41.762
237	22	5	B	2017-03-10 18:41:42.761
238	22	18	B	2017-03-10 18:41:39.726
239	22	8	B	2017-03-10 18:41:40.436
240	22	28	B	2017-03-10 18:41:41.114
241	22	29	B	2017-03-10 18:41:41.762
242	22	5	B	2017-03-10 18:41:42.761
243	22	25	B	2017-03-10 18:41:43.336
244	22	18	B	2017-03-10 18:41:39.726
245	22	8	B	2017-03-10 18:41:40.436
246	22	28	B	2017-03-10 18:41:41.114
247	22	29	B	2017-03-10 18:41:41.762
248	22	5	B	2017-03-10 18:41:42.761
249	22	25	B	2017-03-10 18:41:43.336
250	22	20	B	2017-03-10 18:41:44.026
251	22	18	B	2017-03-10 18:41:39.726
252	22	8	B	2017-03-10 18:41:40.436
253	22	28	B	2017-03-10 18:41:41.114
254	22	29	B	2017-03-10 18:41:41.762
255	22	5	B	2017-03-10 18:41:42.761
256	22	25	B	2017-03-10 18:41:43.336
257	22	20	B	2017-03-10 18:41:44.026
258	22	16	B	2017-03-10 18:41:45.683
259	22	18	B	2017-03-10 18:41:39.726
260	22	8	B	2017-03-10 18:41:40.436
261	22	28	B	2017-03-10 18:41:41.114
262	22	29	B	2017-03-10 18:41:41.762
263	22	5	B	2017-03-10 18:41:42.761
264	22	25	B	2017-03-10 18:41:43.336
265	22	20	B	2017-03-10 18:41:44.026
266	22	16	B	2017-03-10 18:41:45.683
267	22	7	B	2017-03-10 18:41:48.406
268	22	18	B	2017-03-10 18:41:39.726
269	22	8	B	2017-03-10 18:41:40.436
270	22	28	B	2017-03-10 18:41:41.114
271	22	29	B	2017-03-10 18:41:41.762
272	22	5	B	2017-03-10 18:41:42.761
273	22	25	B	2017-03-10 18:41:43.336
274	22	20	B	2017-03-10 18:41:44.026
275	22	16	B	2017-03-10 18:41:45.683
276	22	7	B	2017-03-10 18:41:48.406
277	22	23	A	2017-03-10 18:41:50.09
278	22	18	B	2017-03-10 18:41:39.726
279	22	8	B	2017-03-10 18:41:40.436
280	22	28	B	2017-03-10 18:41:41.114
281	22	29	B	2017-03-10 18:41:41.762
282	22	5	B	2017-03-10 18:41:42.761
283	22	25	B	2017-03-10 18:41:43.336
284	22	20	B	2017-03-10 18:41:44.026
285	22	16	B	2017-03-10 18:41:45.683
286	22	7	B	2017-03-10 18:41:48.406
287	22	23	A	2017-03-10 18:41:50.09
288	23	20	B	2017-03-10 22:00:16.15
289	23	20	B	2017-03-10 22:00:16.15
290	23	2	A	2017-03-10 22:00:17.629
291	23	20	B	2017-03-10 22:00:16.15
292	23	2	A	2017-03-10 22:00:17.629
293	23	18	B	2017-03-10 22:00:18.725
294	23	20	B	2017-03-10 22:00:16.15
295	23	2	A	2017-03-10 22:00:17.629
296	23	18	B	2017-03-10 22:00:18.725
297	23	10	B	2017-03-10 22:00:19.49
298	23	20	B	2017-03-10 22:00:16.15
299	23	2	A	2017-03-10 22:00:17.629
300	23	18	B	2017-03-10 22:00:18.725
301	23	10	B	2017-03-10 22:00:19.49
302	23	6	B	2017-03-10 22:00:20.293
303	23	20	B	2017-03-10 22:00:16.15
304	23	2	A	2017-03-10 22:00:17.629
305	23	18	B	2017-03-10 22:00:18.725
306	23	10	B	2017-03-10 22:00:19.49
307	23	6	B	2017-03-10 22:00:20.293
308	23	1	B	2017-03-10 22:00:21.065
309	23	20	B	2017-03-10 22:00:16.15
310	23	2	A	2017-03-10 22:00:17.629
311	23	18	B	2017-03-10 22:00:18.725
312	23	10	B	2017-03-10 22:00:19.49
313	23	6	B	2017-03-10 22:00:20.293
314	23	1	B	2017-03-10 22:00:21.065
315	23	28	B	2017-03-10 22:00:21.753
316	23	20	B	2017-03-10 22:00:16.15
317	23	2	A	2017-03-10 22:00:17.629
318	23	18	B	2017-03-10 22:00:18.725
319	23	10	B	2017-03-10 22:00:19.49
320	23	6	B	2017-03-10 22:00:20.293
321	23	1	B	2017-03-10 22:00:21.065
322	23	28	B	2017-03-10 22:00:21.753
323	23	12	B	2017-03-10 22:00:22.443
324	23	20	B	2017-03-10 22:00:16.15
325	23	2	A	2017-03-10 22:00:17.629
326	23	18	B	2017-03-10 22:00:18.725
327	23	10	B	2017-03-10 22:00:19.49
328	23	6	B	2017-03-10 22:00:20.293
329	23	1	B	2017-03-10 22:00:21.065
330	23	28	B	2017-03-10 22:00:21.753
331	23	12	B	2017-03-10 22:00:22.443
332	23	31	B	2017-03-10 22:00:23.19
333	23	20	B	2017-03-10 22:00:16.15
334	23	2	A	2017-03-10 22:00:17.629
335	23	18	B	2017-03-10 22:00:18.725
336	23	10	B	2017-03-10 22:00:19.49
337	23	6	B	2017-03-10 22:00:20.293
338	23	1	B	2017-03-10 22:00:21.065
339	23	28	B	2017-03-10 22:00:21.753
340	23	12	B	2017-03-10 22:00:22.443
341	23	31	B	2017-03-10 22:00:23.19
342	23	13	B	2017-03-10 22:00:23.977
343	24	11	A	2017-03-10 22:03:24.117
344	24	11	A	2017-03-10 22:03:24.117
345	24	14	B	2017-03-10 22:03:25.658
346	24	11	A	2017-03-10 22:03:24.117
347	24	14	B	2017-03-10 22:03:25.658
348	24	11	A	2017-03-10 22:03:24.117
349	24	14	B	2017-03-10 22:03:25.658
350	25	12	B	2017-03-10 22:16:36.923
351	25	12	B	2017-03-10 22:16:36.923
352	25	23	A	2017-03-10 22:16:38.091
353	26	17	B	2017-03-10 22:25:43.063
354	26	17	B	2017-03-10 22:25:43.063
355	26	8	A	2017-03-10 22:25:44.328
356	27	10	A	2017-03-10 22:35:40.211
357	27	10	A	2017-03-10 22:35:40.211
358	27	28	B	2017-03-10 22:35:41.425
359	27	10	A	2017-03-10 22:35:40.211
360	27	28	A	2017-03-10 22:35:41.425
361	27	10	A	2017-03-10 22:35:40.211
362	27	28	A	2017-03-10 22:35:41.425
363	27	26	B	2017-03-10 22:36:21.778
364	27	10	A	2017-03-10 22:35:40.211
365	27	28	A	2017-03-10 22:35:41.425
366	27	26	A	2017-03-10 22:36:21.778
367	27	10	A	2017-03-10 22:35:40.211
368	27	28	A	2017-03-10 22:35:41.425
369	27	26	A	2017-03-10 22:36:21.778
370	27	25	B	2017-03-10 22:36:38.725
371	27	10	A	2017-03-10 22:35:40.211
372	27	28	A	2017-03-10 22:35:41.425
373	27	26	A	2017-03-10 22:36:21.778
374	27	25	B	2017-03-10 22:36:38.725
375	27	17	A	2017-03-10 22:36:39.756
376	27	10	B	2017-03-10 22:35:40.211
377	27	28	A	2017-03-10 22:35:41.425
378	27	26	A	2017-03-10 22:36:21.778
379	27	25	B	2017-03-10 22:36:38.725
380	27	17	A	2017-03-10 22:36:39.756
381	28	14	A	2017-03-10 22:40:30.12
382	28	14	A	2017-03-10 22:40:30.12
383	28	15	B	2017-03-10 22:40:31.228
384	29	24	A	2017-03-10 22:49:19.511
385	29	24	A	2017-03-10 22:49:19.511
386	29	1	B	2017-03-10 22:49:20.557
387	30	15	B	2017-03-10 22:54:49.829
388	30	15	B	2017-03-10 22:54:49.829
389	30	17	B	2017-03-10 22:54:50.571
390	30	15	B	2017-03-10 22:54:49.829
391	30	17	B	2017-03-10 22:54:50.571
392	30	3	A	2017-03-10 22:54:51.727
393	31	1	A	2017-03-10 23:17:50.356
394	31	1	A	2017-03-10 23:17:50.356
395	31	32	B	2017-03-10 23:17:51.298
396	31	1	A	2017-03-10 23:17:50.356
397	31	32	B	2017-03-10 23:17:51.298
398	31	19	A	2017-03-10 23:17:52.548
399	31	1	A	2017-03-10 23:17:50.356
400	31	32	B	2017-03-10 23:17:51.298
401	31	19	A	2017-03-10 23:17:52.548
402	31	4	B	2017-03-10 23:17:53.621
403	32	21	A	2017-03-10 23:20:36.818
404	32	21	A	2017-03-10 23:20:36.818
405	32	26	B	2017-03-10 23:20:37.912
406	32	21	A	2017-03-10 23:20:36.818
407	32	26	B	2017-03-10 23:20:37.912
408	32	6	A	2017-03-10 23:20:39.07
409	32	21	A	2017-03-10 23:20:36.818
410	32	26	B	2017-03-10 23:20:37.912
411	32	6	B	2017-03-10 23:20:39.07
412	32	21	A	2017-03-10 23:20:36.818
413	32	26	B	2017-03-10 23:20:37.912
414	32	6	A	2017-03-10 23:20:39.07
415	32	21	A	2017-03-10 23:20:36.818
416	32	26	B	2017-03-10 23:20:37.912
417	32	6	A	2017-03-10 23:20:39.07
418	32	8	B	2017-03-10 23:20:41.638
419	33	31	A	2017-03-10 23:27:32.757
420	33	31	A	2017-03-10 23:27:32.757
421	33	28	B	2017-03-10 23:27:33.924
422	33	31	A	2017-03-10 23:27:32.757
423	33	28	B	2017-03-10 23:27:33.924
424	33	14	A	2017-03-10 23:27:35.067
425	33	31	A	2017-03-10 23:27:32.757
426	33	28	B	2017-03-10 23:27:33.924
427	33	14	A	2017-03-10 23:27:35.067
428	33	13	B	2017-03-10 23:27:36.221
429	34	3	A	2017-03-10 23:38:28.773
430	34	3	A	2017-03-10 23:38:28.773
431	34	7	B	2017-03-10 23:38:29.937
432	34	3	A	2017-03-10 23:38:28.773
433	34	7	B	2017-03-10 23:38:29.937
434	34	6	A	2017-03-10 23:38:31.113
435	34	3	A	2017-03-10 23:38:28.773
436	34	7	B	2017-03-10 23:38:29.937
437	34	6	A	2017-03-10 23:38:31.113
438	34	18	B	2017-03-10 23:38:32.226
439	35	2	B	2017-03-10 23:49:51.983
440	35	2	B	2017-03-10 23:49:51.983
441	35	27	A	2017-03-10 23:49:52.809
442	35	2	B	2017-03-10 23:49:51.983
443	35	27	A	2017-03-10 23:49:52.809
444	35	15	B	2017-03-10 23:49:54.199
445	36	17	A	2017-03-11 00:08:13.407
446	36	17	A	2017-03-11 00:08:13.407
447	36	5	B	2017-03-11 00:08:14.209
448	36	17	A	2017-03-11 00:08:13.407
449	36	5	B	2017-03-11 00:08:14.209
450	36	21	A	2017-03-11 00:08:15.527
451	37	17	A	2017-03-11 01:49:17.385
452	37	17	A	2017-03-11 01:49:17.385
453	37	10	B	2017-03-11 01:49:18.474
454	39	5	A	2017-03-11 01:52:37.079
455	39	5	A	2017-03-11 01:52:37.079
456	39	17	B	2017-03-11 01:52:38.027
457	40	32	A	2017-03-11 01:58:53.384
458	40	32	A	2017-03-11 01:58:53.384
459	40	22	B	2017-03-11 01:58:54.281
460	41	26	A	2017-03-11 02:12:51.438
461	41	26	A	2017-03-11 02:12:51.438
462	41	5	B	2017-03-11 02:12:52.482
463	42	12	A	2017-03-11 02:18:55.953
464	42	12	A	2017-03-11 02:18:55.953
465	42	8	B	2017-03-11 02:18:57.041
466	43	17	A	2017-03-11 02:29:21.635
467	43	17	A	2017-03-11 02:29:21.635
468	43	22	B	2017-03-11 02:29:23.121
469	43	17	B	2017-03-11 02:29:21.635
470	43	22	B	2017-03-11 02:29:23.121
471	43	17	A	2017-03-11 02:29:21.635
472	43	22	B	2017-03-11 02:29:23.121
473	44	27	A	2017-03-13 15:33:35.225
474	44	27	A	2017-03-13 15:33:35.225
475	44	8	B	2017-03-13 15:33:46.159
476	44	27	A	2017-03-13 15:33:35.225
477	44	8	B	2017-03-13 15:33:46.159
478	44	25	B	2017-03-13 15:33:58.247
479	45	16	A	2017-03-13 15:48:04.34
480	45	16	A	2017-03-13 15:48:04.34
481	45	32	B	2017-03-13 15:48:05.824
482	45	16	A	2017-03-13 15:48:04.34
483	45	32	B	2017-03-13 15:48:05.824
484	45	6	B	2017-03-13 15:48:22.151
485	47	15	B	2017-03-14 11:32:13.555
486	47	15	B	2017-03-14 11:32:13.555
487	47	10	A	2017-03-14 11:32:14.153
488	47	15	B	2017-03-14 11:32:13.555
489	47	10	A	2017-03-14 11:32:14.153
490	47	23	B	2017-03-14 11:32:16.204
491	48	17	A	2017-03-14 11:40:36.688
492	48	17	A	2017-03-14 11:40:36.688
493	48	31	B	2017-03-14 11:40:37.643
494	48	17	A	2017-03-14 11:40:36.688
495	48	31	B	2017-03-14 11:40:37.643
496	48	22	B	2017-03-14 11:40:38.86
497	48	17	A	2017-03-14 11:40:36.688
498	48	31	B	2017-03-14 11:40:37.643
499	48	22	B	2017-03-14 11:40:38.86
500	48	10	B	2017-03-14 11:40:39.76
501	48	17	A	2017-03-14 11:40:36.688
502	48	31	B	2017-03-14 11:40:37.643
503	48	22	B	2017-03-14 11:40:38.86
504	48	10	B	2017-03-14 11:40:39.76
505	48	23	B	2017-03-14 11:40:42.108
506	49	8	A	2017-03-14 11:44:36.973
507	49	8	A	2017-03-14 11:44:36.973
508	49	13	B	2017-03-14 11:44:37.925
509	49	8	A	2017-03-14 11:44:36.973
510	49	13	B	2017-03-14 11:44:37.925
511	49	3	B	2017-03-14 11:44:39.029
512	49	8	A	2017-03-14 11:44:36.973
513	49	13	B	2017-03-14 11:44:37.925
514	49	3	B	2017-03-14 11:44:39.029
515	49	18	B	2017-03-14 11:44:39.866
516	49	8	A	2017-03-14 11:44:36.973
517	49	13	B	2017-03-14 11:44:37.925
518	49	3	B	2017-03-14 11:44:39.029
519	49	18	B	2017-03-14 11:44:39.866
520	49	17	B	2017-03-14 11:44:41.593
521	50	28	B	2017-03-14 11:52:58.187
522	50	28	B	2017-03-14 11:52:58.187
523	50	13	A	2017-03-14 11:52:59.4
524	50	28	B	2017-03-14 11:52:58.187
525	50	13	A	2017-03-14 11:52:59.4
526	50	4	B	2017-03-14 11:53:00.372
527	50	28	B	2017-03-14 11:52:58.187
528	50	13	A	2017-03-14 11:52:59.4
529	50	4	B	2017-03-14 11:53:00.372
530	50	11	B	2017-03-14 11:53:02.351
531	50	28	B	2017-03-14 11:52:58.187
532	50	13	A	2017-03-14 11:52:59.4
533	50	4	B	2017-03-14 11:53:00.372
534	50	11	B	2017-03-14 11:53:02.351
535	50	32	A	2017-03-14 11:53:03.418
536	51	13	A	2017-03-14 11:58:18.28
537	51	13	A	2017-03-14 11:58:18.28
538	51	23	B	2017-03-14 11:58:19.122
539	51	13	A	2017-03-14 11:58:18.28
540	51	23	B	2017-03-14 11:58:19.122
541	51	5	A	2017-03-14 11:58:20.052
542	51	13	A	2017-03-14 11:58:18.28
543	51	23	B	2017-03-14 11:58:19.122
544	51	5	A	2017-03-14 11:58:20.052
545	51	17	B	2017-03-14 11:58:20.893
546	51	13	A	2017-03-14 11:58:18.28
547	51	23	B	2017-03-14 11:58:19.122
548	51	5	A	2017-03-14 11:58:20.052
549	51	17	B	2017-03-14 11:58:20.893
550	51	11	B	2017-03-14 11:58:21.589
551	51	13	A	2017-03-14 11:58:18.28
552	51	23	B	2017-03-14 11:58:19.122
553	51	5	A	2017-03-14 11:58:20.052
554	51	17	B	2017-03-14 11:58:20.893
555	51	11	B	2017-03-14 11:58:21.589
556	51	29	B	2017-03-14 11:58:23.385
557	52	8	B	2017-03-14 12:04:58.802
558	52	8	B	2017-03-14 12:04:58.802
559	52	16	B	2017-03-14 12:04:59.518
560	52	8	B	2017-03-14 12:04:58.802
561	52	16	B	2017-03-14 12:04:59.518
562	52	9	A	2017-03-14 12:05:00.442
563	52	8	B	2017-03-14 12:04:58.802
564	52	16	B	2017-03-14 12:04:59.518
565	52	9	A	2017-03-14 12:05:00.442
566	52	26	B	2017-03-14 12:05:01.879
567	57	20	B	2017-03-15 16:11:05.547
568	58	3	A	2017-03-15 22:01:07.799
569	65	21	A	2017-03-15 23:44:38.045
570	65	21	A	2017-03-15 23:44:38.045
571	65	17	B	2017-03-15 23:44:39.241
572	65	21	A	2017-03-15 23:44:38.045
573	65	17	B	2017-03-15 23:44:39.241
574	65	12	A	2017-03-15 23:44:40.382
575	65	21	A	2017-03-15 23:44:38.045
576	65	17	B	2017-03-15 23:44:39.241
577	65	12	A	2017-03-15 23:44:40.382
578	65	10	B	2017-03-15 23:44:41.645
579	65	21	A	2017-03-15 23:44:38.045
580	65	17	B	2017-03-15 23:44:39.241
581	65	12	A	2017-03-15 23:44:40.382
582	65	10	B	2017-03-15 23:44:41.645
583	65	6	A	2017-03-15 23:44:42.915
584	65	21	A	2017-03-15 23:44:38.045
585	65	17	B	2017-03-15 23:44:39.241
586	65	12	A	2017-03-15 23:44:40.382
587	65	10	B	2017-03-15 23:44:41.645
588	65	6	A	2017-03-15 23:44:42.915
589	65	16	A	2017-03-15 23:45:10.096
590	65	21	A	2017-03-15 23:44:38.045
591	65	17	B	2017-03-15 23:44:39.241
592	65	12	A	2017-03-15 23:44:40.382
593	65	10	B	2017-03-15 23:44:41.645
594	65	6	A	2017-03-15 23:44:42.915
595	65	16	A	2017-03-15 23:45:10.096
596	65	7	A	2017-03-15 23:45:13.717
597	65	21	A	2017-03-15 23:44:38.045
598	65	17	B	2017-03-15 23:44:39.241
599	65	12	A	2017-03-15 23:44:40.382
600	65	10	B	2017-03-15 23:44:41.645
601	65	6	A	2017-03-15 23:44:42.915
602	65	16	A	2017-03-15 23:45:10.096
603	65	7	A	2017-03-15 23:45:13.717
604	65	26	A	2017-03-15 23:46:10.549
605	65	21	A	2017-03-15 23:44:38.045
606	65	17	B	2017-03-15 23:44:39.241
607	65	12	A	2017-03-15 23:44:40.382
608	65	10	B	2017-03-15 23:44:41.645
609	65	6	A	2017-03-15 23:44:42.915
610	65	16	A	2017-03-15 23:45:10.096
611	65	7	A	2017-03-15 23:45:13.717
612	65	26	A	2017-03-15 23:46:10.549
613	65	20	B	2017-03-15 23:46:11.538
614	65	21	A	2017-03-15 23:44:38.045
615	65	17	B	2017-03-15 23:44:39.241
616	65	12	A	2017-03-15 23:44:40.382
617	65	10	B	2017-03-15 23:44:41.645
618	65	6	A	2017-03-15 23:44:42.915
619	65	16	A	2017-03-15 23:45:10.096
620	65	7	A	2017-03-15 23:45:13.717
621	65	26	A	2017-03-15 23:46:10.549
622	65	20	B	2017-03-15 23:46:11.538
623	65	9	B	2017-03-15 23:46:12.376
624	66	21	A	2017-03-15 23:49:54.115
625	66	21	A	2017-03-15 23:49:54.115
626	73	10	A	2017-03-16 15:29:11.752
627	73	11	B	2017-03-16 15:29:18.911
628	73	19	A	2017-03-16 15:29:20.342
629	73	24	B	2017-03-16 15:29:21.328
630	73	26	B	2017-03-16 15:29:21.96
631	75	24	B	2017-03-16 15:49:49.402
632	75	9	A	2017-03-16 15:49:50.132
633	75	16	B	2017-03-16 15:49:51.095
\.


--
-- Name: votes_vote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('votes_vote_id_seq', 633, true);


--
-- Data for Name: working_sets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY working_sets (working_set_id) FROM stdin;
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
\.


--
-- Data for Name: working_sets_snippet_pairs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY working_sets_snippet_pairs (working_set_id, snippet_pair_id, presentation_order) FROM stdin;
3	32	4
3	16	8
3	20	1
3	14	5
3	18	9
3	5	0
3	12	2
3	4	7
3	11	6
3	22	3
4	28	5
4	13	8
4	18	0
4	17	3
4	15	4
4	5	6
4	26	9
4	10	7
4	22	2
4	7	1
5	23	5
5	6	4
5	17	1
5	29	6
5	9	7
5	13	0
5	7	2
5	1	3
5	18	8
5	8	9
6	11	3
6	31	2
6	3	7
6	2	4
6	15	8
6	1	1
6	26	5
6	7	0
6	24	6
6	13	9
7	9	5
7	17	7
7	31	3
7	7	4
7	18	8
7	3	9
7	21	2
7	24	0
7	22	1
7	26	6
8	27	6
8	7	4
8	4	9
8	21	8
8	12	3
8	2	2
8	9	0
8	3	1
8	26	5
8	32	7
9	14	5
9	16	0
9	22	9
9	10	8
9	3	1
9	6	3
9	25	4
9	29	6
9	31	7
9	4	2
10	31	1
10	10	5
10	27	4
10	3	9
10	14	0
10	6	2
10	23	8
10	20	7
10	29	6
10	12	3
11	24	8
11	18	5
11	1	0
11	15	6
11	27	9
11	19	2
11	25	4
11	17	3
11	22	7
11	30	1
12	1	0
12	19	8
12	18	2
12	23	1
12	20	5
12	4	9
12	2	6
12	10	7
12	29	3
12	9	4
13	7	3
13	2	9
13	16	1
13	19	5
13	1	6
13	25	8
13	10	2
13	13	4
13	9	0
13	22	7
14	19	7
14	26	9
14	17	8
14	16	1
14	31	4
14	6	5
14	14	3
14	32	6
14	30	2
14	25	0
15	16	4
15	7	9
15	27	5
15	26	1
15	12	3
15	1	8
15	8	6
15	28	7
15	13	0
15	30	2
16	6	4
16	26	6
16	27	2
16	11	3
16	32	8
16	5	5
16	12	9
16	2	1
16	28	7
16	30	0
17	21	3
17	22	6
17	26	4
17	13	0
17	3	2
17	1	5
17	6	8
17	10	1
17	29	7
17	14	9
18	9	7
18	16	2
18	1	3
18	3	0
18	25	5
18	32	8
18	31	6
18	30	1
18	24	9
18	10	4
19	23	9
19	27	4
19	10	0
19	16	5
19	26	1
19	32	8
19	6	2
19	9	7
19	7	3
19	21	6
20	7	7
20	8	4
20	10	8
20	12	0
20	9	3
20	30	6
20	16	5
20	29	9
20	13	1
20	27	2
21	32	6
21	1	9
21	13	1
21	6	8
21	26	2
21	2	7
21	19	0
21	14	3
21	28	5
21	21	4
22	32	2
22	19	7
22	8	1
22	3	9
22	24	3
22	20	4
22	2	5
22	18	8
22	17	6
22	7	0
23	23	3
23	26	9
23	29	1
23	11	7
23	21	4
23	28	8
23	20	2
23	7	5
23	19	0
23	24	6
24	7	9
24	23	5
24	18	8
24	8	0
24	28	2
24	29	3
24	5	7
24	25	1
24	20	6
24	16	4
25	20	2
25	2	7
25	18	8
25	10	5
25	6	0
25	1	6
25	28	9
25	12	1
25	31	3
25	13	4
26	11	4
26	14	9
26	4	3
26	22	0
26	3	6
26	5	1
26	15	8
26	13	7
26	17	2
26	31	5
27	12	3
27	23	4
27	25	5
27	2	1
27	21	2
27	27	8
27	5	0
27	18	9
27	9	7
27	6	6
28	17	6
28	8	4
28	15	1
28	7	2
28	23	7
28	31	3
28	12	0
28	1	5
28	5	8
28	20	9
29	10	2
29	28	7
29	26	9
29	25	5
29	17	0
29	14	3
29	11	1
29	12	6
29	5	4
29	7	8
30	14	2
30	15	3
30	17	0
30	3	4
30	2	7
30	4	8
30	12	9
30	13	5
30	6	6
30	18	1
31	24	2
31	1	0
31	17	9
31	6	1
31	28	3
31	11	6
31	20	5
31	9	8
31	3	7
31	22	4
32	15	4
32	17	8
32	3	2
32	8	3
32	27	7
32	19	9
32	31	0
32	22	6
32	28	1
32	21	5
33	1	0
33	32	5
33	19	1
33	4	8
33	9	3
33	22	7
33	5	2
33	7	9
33	11	4
33	14	6
34	21	5
34	26	8
34	6	1
34	8	2
34	3	7
34	19	4
34	27	6
34	12	3
34	13	9
34	30	0
35	31	0
35	28	6
35	14	9
35	13	8
35	15	4
35	5	1
35	21	5
35	3	2
35	26	7
35	16	3
36	3	1
36	7	2
36	6	8
36	18	0
36	20	3
36	4	4
36	10	7
36	25	5
36	11	6
36	30	9
37	2	1
37	27	2
37	15	6
37	4	8
37	1	5
37	14	0
37	22	7
37	17	9
37	18	3
37	16	4
38	17	5
38	5	9
38	21	7
38	1	1
38	16	6
38	29	4
38	28	3
38	20	8
38	24	0
38	9	2
39	17	1
39	10	4
39	8	7
39	32	9
39	25	2
39	21	3
39	19	6
39	7	0
39	20	5
39	3	8
40	15	6
40	25	3
40	1	7
40	32	0
40	28	5
40	17	9
40	31	2
40	20	1
40	30	4
40	14	8
41	5	9
41	17	3
41	27	4
41	18	0
41	28	7
41	21	6
41	22	2
41	26	5
41	15	8
41	1	1
42	32	3
42	22	5
42	17	4
42	4	7
42	23	8
42	9	1
42	20	0
42	10	2
42	8	9
42	11	6
43	26	9
43	5	0
43	18	7
43	14	6
43	13	1
43	16	4
43	8	3
43	22	2
43	27	5
43	4	8
44	12	3
44	8	5
44	26	4
44	13	7
44	25	8
44	2	1
44	23	0
44	1	2
44	19	9
44	5	6
45	17	6
45	22	7
45	11	1
45	20	2
45	13	8
45	3	0
45	4	4
45	5	9
45	14	5
45	16	3
46	27	1
46	5	7
46	8	8
46	31	3
46	9	4
46	20	9
46	11	5
46	25	6
46	1	2
46	18	0
47	32	2
47	24	3
47	8	5
47	29	6
47	28	8
47	6	7
47	7	9
47	16	0
47	9	1
47	31	4
48	30	7
48	16	6
48	17	8
48	15	0
48	14	2
48	11	3
48	9	4
48	20	1
48	32	5
48	1	9
49	19	3
49	13	2
49	31	6
49	10	1
49	18	8
49	15	0
49	12	9
49	32	5
49	23	4
49	6	7
50	10	3
50	22	2
50	6	4
50	23	9
50	31	1
50	21	6
50	4	7
50	17	0
50	26	5
50	13	8
51	3	2
51	12	7
51	18	3
51	28	4
51	13	1
51	26	5
51	2	8
51	8	0
51	17	9
51	14	6
52	15	5
52	32	9
52	1	6
52	13	1
52	28	0
52	27	3
52	16	4
52	4	2
52	18	7
52	11	8
53	23	1
53	17	3
53	10	6
53	13	0
53	5	2
53	24	8
53	20	7
53	29	9
53	11	4
53	31	5
54	28	7
54	24	9
54	20	8
54	11	4
54	25	6
54	8	0
54	16	1
54	26	3
54	9	2
54	1	5
55	28	2
55	16	0
55	7	7
55	30	9
55	14	6
55	31	8
55	20	4
55	29	5
55	8	1
55	12	3
56	16	7
56	24	6
56	4	8
56	13	0
56	19	2
56	21	3
56	30	4
56	27	1
56	5	5
56	28	9
57	24	9
57	29	0
57	16	8
57	5	4
57	18	5
57	23	6
57	15	1
57	31	3
57	8	2
57	4	7
58	15	7
58	18	5
58	31	4
58	1	3
58	26	1
58	13	2
58	20	6
58	21	8
58	8	9
58	29	0
59	7	4
59	11	5
59	31	6
59	17	3
59	22	7
59	20	0
59	28	2
59	16	9
59	8	1
59	1	8
60	13	1
60	12	4
60	30	7
60	21	2
60	3	0
60	10	6
60	6	8
60	20	9
60	16	5
60	5	3
61	10	5
61	28	7
61	11	0
61	23	2
61	3	8
61	12	1
61	30	9
61	24	3
61	14	6
61	1	4
62	9	7
62	23	1
62	26	2
62	5	3
62	20	6
62	3	0
62	28	4
62	29	8
62	24	9
62	17	5
63	18	9
63	4	8
63	10	6
63	12	2
63	19	5
63	27	3
63	31	7
63	9	1
63	2	4
63	1	0
64	18	3
64	26	7
64	28	9
64	30	1
64	21	5
64	29	4
64	3	6
64	8	0
64	19	8
64	5	2
65	7	1
65	13	9
65	29	7
65	23	5
65	11	0
65	17	2
65	19	8
65	31	4
65	28	3
65	10	6
66	15	6
66	23	4
66	11	5
66	1	2
66	19	7
66	31	8
66	29	3
66	27	9
66	20	1
66	30	0
67	10	3
67	20	8
67	17	1
67	7	6
67	26	7
67	16	5
67	21	0
67	12	2
67	6	4
67	9	9
68	8	5
68	28	8
68	30	6
68	17	9
68	5	4
68	4	7
68	26	3
68	21	0
68	11	2
68	31	1
69	2	6
69	19	1
69	9	7
69	27	9
69	23	4
69	11	5
69	6	3
69	31	2
69	28	0
69	4	8
70	7	4
70	17	2
70	32	6
70	5	3
70	26	5
70	8	0
70	9	8
70	21	7
70	20	9
70	11	1
71	26	4
71	4	6
71	6	7
71	8	3
71	19	8
71	16	0
71	32	1
71	7	5
71	17	9
71	24	2
72	2	0
72	19	1
72	8	4
72	32	5
72	17	2
72	11	3
72	25	9
72	9	8
72	10	6
72	22	7
73	27	0
73	25	6
73	2	5
73	29	9
73	19	8
73	26	3
73	11	7
73	24	2
73	9	4
73	4	1
74	6	6
74	9	4
74	15	1
74	25	5
74	2	3
74	28	0
74	29	2
74	7	9
74	21	8
74	4	7
75	26	4
75	20	5
75	27	6
75	17	9
75	23	7
75	10	0
75	11	1
75	32	8
75	19	2
75	24	3
76	18	6
76	12	1
76	19	3
76	2	2
76	10	5
76	28	8
76	1	9
76	21	4
76	32	0
76	14	7
77	31	5
77	16	2
77	24	0
77	25	9
77	14	3
77	10	4
77	3	6
77	17	7
77	9	1
77	23	8
\.


--
-- Name: working_sets_working_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('working_sets_working_set_id_seq', 77, true);


--
-- Name: client_headers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY client_headers
    ADD CONSTRAINT client_headers_pkey PRIMARY KEY (client_header_id);


--
-- Name: experiment_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY experiment_sessions
    ADD CONSTRAINT experiment_sessions_pkey PRIMARY KEY (experiment_session_id);


--
-- Name: profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (profile_id);


--
-- Name: snippet_pairs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY snippet_pairs
    ADD CONSTRAINT snippet_pairs_pkey PRIMARY KEY (snippet_pair_id);


--
-- Name: snippets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY snippets
    ADD CONSTRAINT snippets_pkey PRIMARY KEY (snippet_id);


--
-- Name: vote_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vote_comments
    ADD CONSTRAINT vote_comments_pkey PRIMARY KEY (vote_comment_id);


--
-- Name: votes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_pkey PRIMARY KEY (vote_id);


--
-- Name: working_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY working_sets
    ADD CONSTRAINT working_sets_pkey PRIMARY KEY (working_set_id);


--
-- Name: experiment_sessions_client_header_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experiment_sessions
    ADD CONSTRAINT experiment_sessions_client_header_id_fkey FOREIGN KEY (client_header_id) REFERENCES client_headers(client_header_id);


--
-- Name: experiment_sessions_profile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experiment_sessions
    ADD CONSTRAINT experiment_sessions_profile_id_fkey FOREIGN KEY (profile_id) REFERENCES profiles(profile_id);


--
-- Name: experiment_sessions_working_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY experiment_sessions
    ADD CONSTRAINT experiment_sessions_working_set_id_fkey FOREIGN KEY (working_set_id) REFERENCES working_sets(working_set_id);


--
-- Name: snippet_pairs_snippet_a_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY snippet_pairs
    ADD CONSTRAINT snippet_pairs_snippet_a_fkey FOREIGN KEY (snippet_a) REFERENCES snippets(snippet_id);


--
-- Name: snippet_pairs_snippet_b_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY snippet_pairs
    ADD CONSTRAINT snippet_pairs_snippet_b_fkey FOREIGN KEY (snippet_b) REFERENCES snippets(snippet_id);


--
-- Name: vote_comments_vote_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vote_comments
    ADD CONSTRAINT vote_comments_vote_id_fkey FOREIGN KEY (vote_id) REFERENCES votes(vote_id);


--
-- Name: votes_experiment_session_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_experiment_session_id_fkey FOREIGN KEY (experiment_session_id) REFERENCES experiment_sessions(experiment_session_id);


--
-- Name: votes_snippet_pair_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_snippet_pair_id_fkey FOREIGN KEY (snippet_pair_id) REFERENCES snippet_pairs(snippet_pair_id);


--
-- Name: working_sets_snippet_pairs_snippet_pair_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY working_sets_snippet_pairs
    ADD CONSTRAINT working_sets_snippet_pairs_snippet_pair_id_fkey FOREIGN KEY (snippet_pair_id) REFERENCES snippet_pairs(snippet_pair_id);


--
-- Name: working_sets_snippet_pairs_working_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY working_sets_snippet_pairs
    ADD CONSTRAINT working_sets_snippet_pairs_working_set_id_fkey FOREIGN KEY (working_set_id) REFERENCES working_sets(working_set_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

